var path = require("path")
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')
var config = require('./webpack.base.config.js')

//var ip = '173.249.7.103'
var ip = '127.0.0.1'

config.devtool = "source-map"

config.plugins = config.plugins.concat([
  new BundleTracker({filename: 'handcam/webpack-stats-local.json'}),
])

config.entry = {
  App: [
    'webpack-dev-server/client?http://' + ip + ':3000',
    'webpack/hot/only-dev-server',
    './reactjs/App',
  ],
}

config.output.publicPath = 'http://' + ip + ':3000' + '/handcam/static/bundles/local/'
//config.output.path = '/Users/STUFF/2ndhandcam/static_cdn/bundles/local/'

config.plugins = config.plugins.concat([
  new webpack.HotModuleReplacementPlugin(),
  new webpack.optimize.ModuleConcatenationPlugin(),
  new webpack.optimize.CommonsChunkPlugin({
      name: 'vendors', 
      filename: 'vendors.bundle.js',
      minChunks: Infinity 
  }),
  
  new webpack.NoEmitOnErrorsPlugin(),
  new BundleTracker({filename: './webpack-stats-local.json'}),
  new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify('development'),
      'BASE_API_URL': JSON.stringify('http://' + ip + '/api/'),
  }}),
])

config.module.loaders.push(
  { test: /\.jsx?$/, exclude: /node_modules/, loaders: 'babel-loader' },
  { test: /\.css$/, loaders: 'style-loader!css-loader' },
  { test: /\.json$/, loader: 'json-loader' },
  { test: /\.scss$/, loader: 'style-loader!css-loader!sass-loader' },
  { test: /\.(png|jpg|woff|woff2|eot|ttf|svg|gif)$/, loader: 'url-loader?limit=300000&name=[name]-[hash].[ext]' }    
)

module.exports = config