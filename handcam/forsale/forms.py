from django import forms
from pagedown.widgets import PagedownWidget
from .models import SellCamera, SellLenses

class UploadCameraForm(forms.ModelForm):
    condition = forms.CharField(widget=PagedownWidget(show_preview=False))
    warranty = forms.CharField(widget=PagedownWidget(show_preview=False))

    class Meta:
        model = SellCamera
        fields = [
            "item",
            "price",
            "condition",
            "shutter_count",
            "warranty"
        ]

    