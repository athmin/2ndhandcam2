from .models import SellCamera
from django.db.models.signals import post_save
from django.dispatch import receiver

#@receiver(post_save, sender=SellCamera, dispatch_uid="update_record")
def index_post(sender, instance, **kwargs):
	print ('inside post save signal')
	print (instance)
	for photo in instance.actual_photos.all():
		print (photo.actual_photo.url)
	instance.indexing()