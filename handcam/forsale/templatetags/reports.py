# encoding=utf8 

from django import template
from django.apps import apps
from django.db.models import Sum, Count
from django.db.models.functions import Coalesce
from django.conf import settings
from forsale.models import SellCamera, SellVideoCamera, SellLenses, ForSaleActualPhoto, SellLighting, SellAccessories, SellDrone
import datetime
import sys  
from importlib import reload

# reload(sys)  
# sys.setdefaultencoding('utf8')
register = template.Library()


def get_forsale_cam():
	forsale_cam = SellCamera.objects.all()
	return forsale_cam

def get_expired_forsale_cam():
	expired_cam = SellCamera.objects.filter(expiry__lte=datetime.datetime.now())
	for item in expired_cam:
		item.status = "Expired"
		item.save()

		#alert = send_expired_notification(item.seller)
	return expired_cam

def get_active_forsale_cam():
	active_cam = SellCamera.objects.filter(status='Active')
	return active_cam

def get_hidden_forsale_cam():
	hidden_cam = SellCamera.objects.filter(status='Disabled')
	return hidden_cam

def get_sold_forsale_cam():
	sold_cam = SellCamera.objects.filter(status='Sold')
	return sold_cam



def get_forsale_videocam():
	forsale_videocam = SellVideoCamera.objects.all()
	return forsale_videocam

def get_expired_forsale_videocam():
	expired_videocam = SellVideoCamera.objects.filter(expiry__lte=datetime.datetime.now())
	for item in expired_videocam:
		item.status = "Expired"
		item.save()

		#alert = send_expired_notification(item.seller)
	return expired_videocam

def get_active_forsale_videocam():
	active = SellVideoCamera.objects.filter(status='Active')
	return active

def get_hidden_forsale_videocam():
	hidden_vid = SellVideoCamera.objects.filter(status='Disabled')
	return hidden_vid

def get_sold_forsale_videocam():
	sold_vid = SellVideoCamera.objects.filter(status='Sold')
	return sold_vid
	


def get_forsale_lenses():
	forsale_lenses = SellLenses.objects.all()
	return forsale_lenses

def get_expired_forsale_lenses():
	expired_lens = SellLenses.objects.filter(expiry__lte=datetime.datetime.now())
	for item in expired_lens:
		item.status = "Expired"
		item.save()

		#alert = send_expired_notification(item.seller)
	return expired_lens

def get_active_forsale_lenses():
	active = SellLenses.objects.filter(status='Active')
	return active

def get_hidden_forsale_lenses():
	hidden = SellLenses.objects.filter(status='Disabled')
	return hidden

def get_sold_forsale_lenses():
	sold = SellLenses.objects.filter(status='Sold')
	return sold




def get_forsale_lighting():
	forsale_lighting = SellLighting.objects.all()
	return forsale_lighting

def get_expired_forsale_lighting():
	expired_lighting = SellLighting.objects.filter(expiry__lte=datetime.datetime.now())
	for item in expired_lighting:
		item.status = "Expired"
		item.save()

		#alert = send_expired_notification(item.seller)
	return expired_lighting

def get_active_forsale_lighting():
	active = SellLighting.objects.filter(status='Active')
	return active

def get_hidden_forsale_lighting():
	hidden = SellLighting.objects.filter(status='Disabled')
	return hidden

def get_sold_forsale_lighting():
	sold = SellLighting.objects.filter(status='Sold')
	return sold



def get_forsale_accessories():	
	forsale_accessories = SellAccessories.objects.all()
	return forsale_accessories

def get_expired_forsale_accessories():
	expired_accessories = SellAccessories.objects.filter(expiry__lte=datetime.datetime.now())
	for item in expired_accessories:
		item.status = "Expired"
		item.save()

		#alert = send_expired_notification(item.seller)
	return expired_accessories

def get_active_forsale_accessories():
	active = SellAccessories.objects.filter(status='Active')
	return active

def get_hidden_forsale_accessories():
	hidden = SellAccessories.objects.filter(status='Disabled')
	return hidden

def get_sold_forsale_accessories():
	sold = SellAccessories.objects.filter(status='Sold')
	return sold




def get_forsale_drones():	
	forsale_drones = SellDrone.objects.all()
	return forsale_drones

def get_expired_forsale_drones():
	expired_drones = SellDrone.objects.filter(expiry__lte=datetime.datetime.now())
	for item in expired_drones:
		item.status = "Expired"
		item.save()

		#alert = send_expired_notification(item.seller)
	return expired_drones

def get_active_forsale_drones():
	active = SellDrone.objects.filter(status='Active')
	return active


def get_hidden_forsale_drones():
	hidden = SellDrone.objects.filter(status='Disabled')
	return hidden

def get_sold_forsale_drones():
	sold = SellDrone.objects.filter(status='Sold')
	return sold


@register.filter
def sort_apps(apps):
    count = len(apps)
    apps.sort(
        key = lambda x:
            settings.APP_ORDER.index(x['app_label'])
            if x['app_label'] in settings.APP_ORDER
            else count
    )
    return apps


@register.simple_tag()
def total_item_uploaded():
	still = get_forsale_cam()
	video = get_forsale_videocam()
	lens = get_forsale_lenses()
	acc = get_forsale_accessories()
	lighting = get_forsale_lighting()
	drone = get_forsale_drones()

	uploaded = len(still) + len(video) + len(lens) + len(acc) + len(lighting) + len(drone)
	return uploaded


@register.simple_tag()
def total_item_active():
	still = get_active_forsale_cam()
	video = get_active_forsale_videocam()
	lens = get_active_forsale_lenses()
	acc = get_active_forsale_accessories()
	lighting = get_active_forsale_lighting()
	drone = get_active_forsale_drones()

	active = len(still) + len(video) + len(lens) + len(acc) + len(lighting) + len(drone)
	return active


@register.simple_tag()
def total_item_sold():
	still = get_sold_forsale_cam()
	video = get_sold_forsale_videocam()
	lens = get_sold_forsale_lenses()
	acc = get_sold_forsale_accessories()
	lighting = get_sold_forsale_lighting()
	drone = get_sold_forsale_drones()

	sold = len(still) + len(video) + len(lens) + len(acc) + len(lighting) + len(drone)
	return sold


@register.simple_tag()
def total_item_expired():
	still = get_expired_forsale_cam()
	video = get_expired_forsale_videocam()
	lens = get_expired_forsale_lenses()
	acc = get_expired_forsale_accessories()
	lighting = get_expired_forsale_lighting()
	drone = get_expired_forsale_drones()

	expired = len(still) + len(video) + len(lens) + len(acc) + len(lighting) + len(drone)
	return expired


@register.simple_tag()
def total_item_hidden():
	still = get_hidden_forsale_cam()
	video = get_hidden_forsale_videocam()
	lens = get_hidden_forsale_lenses()
	acc = get_hidden_forsale_accessories()
	lighting = get_hidden_forsale_lighting()
	drone = get_hidden_forsale_drones()

	hidden = len(still) + len(video) + len(lens) + len(acc) + len(lighting) + len(drone)
	return hidden


@register.simple_tag()
def get_model_count(admin_url):
	
	admin_url_list = str(admin_url).split('/')
	app_label = admin_url_list[len(admin_url_list)-3]
	model_name = admin_url_list[len(admin_url_list)-2]

	result = apps.get_model(app_label,model_name).objects.count()
		
	return result


@register.simple_tag()
def get_model_sold_count(admin_url):
	
	admin_url_list = str(admin_url).split('/')
	app_label = admin_url_list[len(admin_url_list)-3]
	model_name = admin_url_list[len(admin_url_list)-2]

	result = apps.get_model(app_label,model_name).objects.filter(status='Sold').count()
		
	return result

@register.simple_tag()
def get_model_hidden_count(admin_url):
	
	admin_url_list = str(admin_url).split('/')
	app_label = admin_url_list[len(admin_url_list)-3]
	model_name = admin_url_list[len(admin_url_list)-2]

	result = apps.get_model(app_label,model_name).objects.filter(status='Disabled').count()
		
	return result

@register.simple_tag()
def get_model_expired_count(admin_url):
	
	admin_url_list = str(admin_url).split('/')
	app_label = admin_url_list[len(admin_url_list)-3]
	model_name = admin_url_list[len(admin_url_list)-2]

	result = apps.get_model(app_label,model_name).objects.filter(status='Expired').count()
		
	return result

@register.simple_tag()
def get_model_active_count(admin_url):
	
	admin_url_list = str(admin_url).split('/')
	app_label = admin_url_list[len(admin_url_list)-3]
	model_name = admin_url_list[len(admin_url_list)-2]

	result = apps.get_model(app_label,model_name).objects.filter(status='Active').count()
		
	return result


@register.simple_tag()
def get_user_count():
	
	app_label = 'auth'
	model_name = 'user'

	result = apps.get_model(app_label,model_name).objects.count()
		
	return result


@register.simple_tag()
def get_total_sales_still():

	app_label = 'forsale'
	model_name = 'sellcamera'

	total_sales = apps.get_model(app_label,model_name).objects.aggregate(Sum('price'))
	if total_sales['price__sum'] is None:
		return 0
	else:		
		return total_sales['price__sum']

@register.simple_tag()
def get_total_sales_video():

	app_label = 'forsale'
	model_name = 'sellvideocamera'

	total_sales = apps.get_model(app_label,model_name).objects.aggregate(Sum('price'))
	if total_sales['price__sum'] is None:
		return 0
	else:		
		return total_sales['price__sum']


@register.simple_tag()
def get_total_sales_lens():

	app_label = 'forsale'
	model_name = 'selllenses'

	total_sales = apps.get_model(app_label,model_name).objects.aggregate(Sum('price'))
	if total_sales['price__sum'] is None:
		return 0
	else:		
		return total_sales['price__sum']

@register.simple_tag()
def get_total_sales_accessories():

	app_label = 'forsale'
	model_name = 'selllighting'

	total_sales = apps.get_model(app_label,model_name).objects.aggregate(Sum('price'))
	if total_sales['price__sum'] is None:
		return 0
	else:		
		return total_sales['price__sum']

@register.simple_tag()
def get_total_sales_lighting():

	app_label = 'forsale'
	model_name = 'sellaccessories'

	total_sales = apps.get_model(app_label,model_name).objects.aggregate(Sum('price'))
	if total_sales['price__sum'] is None:
		return 0
	else:		
		return total_sales['price__sum']


@register.simple_tag()
def get_total_sales_drones():

	app_label = 'forsale'
	model_name = 'selldrone'

	total_sales = apps.get_model(app_label,model_name).objects.aggregate(Sum('price'))
	if total_sales['price__sum'] is None:
		return 0
	else:		
		return total_sales['price__sum']



@register.simple_tag()
def get_sales_still_brand():

	app_label = 'forsale'
	model_name = 'sellcamera'

	still_sales = apps.get_model(app_label,model_name).objects.values('item__cam_type__brand__company').order_by().annotate(Count('item__cam_type__brand__company'))

	return still_sales


@register.simple_tag()
def get_sales_video_brand():

	app_label = 'forsale'
	model_name = 'sellvideocamera'

	video_sales = apps.get_model(app_label,model_name).objects.values('item__cam_type__brand__company').order_by().annotate(Count('item__cam_type__brand__company'))

	return video_sales


@register.simple_tag()
def get_sales_lens_brand():

	app_label = 'forsale'
	model_name = 'selllenses'

	lens_sales = apps.get_model(app_label,model_name).objects.values('item__type_of_lens__brand__company').order_by().annotate(Count('item__type_of_lens__brand__company'))

	return lens_sales


@register.simple_tag()
def get_sales_acc_brand():

	app_label = 'forsale'
	model_name = 'sellaccessories'

	acc_sales = apps.get_model(app_label,model_name).objects.values('brand').order_by().annotate(Count('brand'))

	return acc_sales


@register.simple_tag()
def get_sales_lighting_brand():

	app_label = 'forsale'
	model_name = 'selllighting'

	light_sales = apps.get_model(app_label,model_name).objects.values('brand').order_by().annotate(Count('brand'))

	return light_sales


@register.simple_tag()
def get_sales_drone_brand():

	app_label = 'forsale'
	model_name = 'selldrone'

	dron_sales = apps.get_model(app_label,model_name).objects.values('brand').order_by().annotate(Count('brand'))

	return dron_sales


@register.simple_tag()
def get_hidden_still_brand():

	app_label = 'forsale'
	model_name = 'sellcamera'

	still_sales = apps.get_model(app_label,model_name).objects.filter(status="Hidden").count()

	return still_sales


@register.simple_tag()
def get_hidden_video_brand():

	app_label = 'forsale'
	model_name = 'sellvideocamera'

	video_sales = apps.get_model(app_label,model_name).objects.filter(status="Hidden").count()

	return video_sales


@register.simple_tag()
def get_hidden_lens_brand():

	app_label = 'forsale'
	model_name = 'selllenses'

	lens_sales = apps.get_model(app_label,model_name).objects.filter(status="Hidden").count()

	return lens_sales


@register.simple_tag()
def get_hidden_acc_brand():

	app_label = 'forsale'
	model_name = 'sellaccessories'

	acc_sales = apps.get_model(app_label,model_name).objects.filter(status="Hidden").count()

	return acc_sales


@register.simple_tag()
def get_hidden_lighting_brand():

	app_label = 'forsale'
	model_name = 'selllighting'

	light_sales = apps.get_model(app_label,model_name).objects.filter(status="Hidden").count()

	return light_sales


@register.simple_tag()
def get_hidden_drone_brand():

	app_label = 'forsale'
	model_name = 'selldrone'

	dron_sales = apps.get_model(app_label,model_name).objects.filter(status="Hidden").count()

	return dron_sales

