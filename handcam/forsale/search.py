from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Text, Date, Float, Boolean, Keyword, Integer, Long
from django.conf import settings

connections.create_connection(hosts=[settings.HTTP_SERVER_URL + '/es/'], timeout=20, port=443)
#connections.create_connection(hosts=['http://www.2ndhandcam.com/es/'], timeout=20)

class ForSaleIndex(DocType):
    description = Text(fields={'keyword': Keyword()})
    product_id = Text(fields={'keyword': Keyword()})
    seller_pk = Integer(fields={'keyword': Keyword()})
    seller = Text(fields={'keyword': Keyword()})
    seller_location = Text(fields={'keyword': Keyword()})
    seller_country = Text(fields={'keyword': Keyword()})
    seller_city = Text(fields={'keyword': Keyword()})
    seller_phonenumber = Text(fields={'keyword': Keyword()})
    label = Text(fields={'keyword': Keyword()})
    price = Float(fields={'keyword': Keyword()})
    neworused = Text(fields={'keyword': Keyword()}) 
    currency = Text(fields={'keyword': Keyword()})
    condition = Text(fields={'keyword': Keyword()})
    lens_description = Keyword(multi=True)
    shutter_count = Text(fields={'keyword': Keyword()})
    status = Text(fields={'keyword': Keyword()})
    date = Date()
    warranty = Text(fields={'keyword': Keyword()})
    package = Text(fields={'keyword': Keyword()})
    actual_photos = Keyword(multi=True)
    comments=Text(fields={'keyword': Keyword()})
    lens_photos = Keyword(multi=True)
    lens_package = Keyword(multi=True)
    brand = Text(fields={'keyword': Keyword()})
    category = Text(fields={'keyword': Keyword()})
    megapixels = Float(fields={'keyword': Keyword()})
    sensor_size = Text(fields={'keyword': Keyword()})
    sensor_type = Text(fields={'keyword': Keyword()})
    image_sensor_format = Text(fields={'keyword': Keyword()})
    lcd = Text(fields={'keyword': Keyword()})
    lcd_size = Text(fields={'keyword': Keyword()})
    lcd_resolution = Text(fields={'keyword': Keyword()})
    image = Text(fields={'keyword': Keyword()})
    weight = Text(fields={'keyword': Keyword()})
    iso = Text(fields={'keyword': Keyword()})
    iso_low = Text(fields={'keyword': Keyword()})
    iso_high = Text(fields={'keyword': Keyword()})
    video = Text(fields={'keyword': Keyword()})
    frames_per_sec = Float(fields={'keyword': Keyword()})
    af_points = Text(fields={'keyword': Keyword()})
    mirrorless = Text(fields={'keyword': Keyword()})
    optical_zoom = Text(fields={'keyword': Keyword()})
    digital_zoom = Text(fields={'keyword': Keyword()})
    aperture = Text(fields={'keyword': Keyword()})
    max_aperture = Text(fields={'keyword': Keyword()})
    wifi = Text(fields={'keyword': Keyword()})
    gps = Text(fields={'keyword': Keyword()})
    focal_length = Text(fields={'keyword': Keyword()})
    focal_length_from = Text(fields={'keyword': Keyword()})
    focal_length_to = Text(fields={'keyword': Keyword()})
    waterproof = Text(fields={'keyword': Keyword()})
    film_size = Text(fields={'keyword': Keyword()})
    features = Text(fields={'keyword': Keyword()})
    image_size = Text(fields={'keyword': Keyword()})
    kind_camera = Text(fields={'keyword': Keyword()})
    photo_capacity = Text(fields={'keyword': Keyword()})
    effective_pixels = Text(fields={'keyword': Keyword()})
    effective_resolution = Text(fields={'keyword': Keyword()})
    dynamic_range = Text(fields={'keyword': Keyword()})
    frame_rate = Text(fields={'keyword': Keyword()})
    view_finder = Text(fields={'keyword': Keyword()})
    lens_mount = Text(fields={'keyword': Keyword()})
    #cam_features = Text(analyzer='snowball')
    #cam_specifications = Text(analyzer='snowball')

    class Meta:
        index = 'forsale'


class ForSaleVideoIndex(DocType):
    description = Text(fields={'keyword': Keyword()})
    product_id = Text(fields={'keyword': Keyword()})
    seller_pk = Integer(fields={'keyword': Keyword()})
    seller = Text(fields={'keyword': Keyword()})
    seller_location = Text(fields={'keyword': Keyword()})
    seller_country = Text(fields={'keyword': Keyword()})
    seller_city = Text(fields={'keyword': Keyword()})
    seller_phonenumber = Text(fields={'keyword': Keyword()})
    label = Text(fields={'keyword': Keyword()})
    price = Float(fields={'keyword': Keyword()})
    neworused = Text(fields={'keyword': Keyword()})
    currency = Text(fields={'keyword': Keyword()})
    condition = Text(fields={'keyword': Keyword()})
    lens_description = Keyword(multi=True)
    status = Text(fields={'keyword': Keyword()})
    date = Date()
    warranty = Text(fields={'keyword': Keyword()})
    package = Text(fields={'keyword': Keyword()})
    actual_photos = Keyword(multi=True)
    comments=Text(fields={'keyword': Keyword()})
    lens_photos = Keyword(multi=True)
    lens_package = Keyword(multi=True)
    brand = Text(fields={'keyword': Keyword()})
    category = Text(fields={'keyword': Keyword()})
    megapixels = Float(fields={'keyword': Keyword()})
    sensor_size = Text(fields={'keyword': Keyword()})
    sensor_type = Text(fields={'keyword': Keyword()})
    image_sensor_format = Text(fields={'keyword': Keyword()})
    lcd = Text(fields={'keyword': Keyword()})
    lcd_size = Text(fields={'keyword': Keyword()})
    lcd_resolution = Text(fields={'keyword': Keyword()})
    image = Text(fields={'keyword': Keyword()})
    weight = Text(fields={'keyword': Keyword()})
    iso = Text(fields={'keyword': Keyword()})
    iso_low = Text(fields={'keyword': Keyword()})
    iso_high = Text(fields={'keyword': Keyword()})
    video = Text(fields={'keyword': Keyword()})
    frames_per_sec = Float(fields={'keyword': Keyword()})
    af_points = Text(fields={'keyword': Keyword()})
    mirrorless = Text(fields={'keyword': Keyword()})
    optical_zoom = Text(fields={'keyword': Keyword()})
    digital_zoom = Text(fields={'keyword': Keyword()})
    aperture = Text(fields={'keyword': Keyword()})
    max_aperture = Text(fields={'keyword': Keyword()})
    wifi = Text(fields={'keyword': Keyword()})
    gps = Text(fields={'keyword': Keyword()})
    focal_length = Text(fields={'keyword': Keyword()})
    focal_length_from = Text(fields={'keyword': Keyword()})
    focal_length_to = Text(fields={'keyword': Keyword()})
    waterproof = Text(fields={'keyword': Keyword()})
    film_size = Text(fields={'keyword': Keyword()})
    features = Text(fields={'keyword': Keyword()})
    image_size = Text(fields={'keyword': Keyword()})
    kind_camera = Text(fields={'keyword': Keyword()})
    photo_capacity = Text(fields={'keyword': Keyword()})
    effective_pixels = Text(fields={'keyword': Keyword()})
    effective_resolution = Text(fields={'keyword': Keyword()})
    dynamic_range = Text(fields={'keyword': Keyword()})
    frame_rate = Text(fields={'keyword': Keyword()})
    view_finder = Text(fields={'keyword': Keyword()})
    lens_mount = Text(fields={'keyword': Keyword()})
    cam_features = Text(fields={'keyword': Keyword()})
    cam_specifications = Text(fields={'keyword': Keyword()})

    class Meta:
        index = 'forsalevideo'


class ForSaleLensIndex(DocType):
    description=Text(fields={'keyword': Keyword()})
    product_id = Text(fields={'keyword': Keyword()})
    seller_pk = Integer(fields={'keyword': Keyword()})
    seller=Text(fields={'keyword': Keyword()})
    seller_location=Text(fields={'keyword': Keyword()})
    seller_country = Text(fields={'keyword': Keyword()})
    seller_city = Text(fields={'keyword': Keyword()})
    seller_phonenumber=Text(fields={'keyword': Keyword()})
    label=Text(fields={'keyword': Keyword()})
    price=Float(fields={'keyword': Keyword()})
    neworused = Text(fields={'keyword': Keyword()})
    currency = Text(fields={'keyword': Keyword()})
    condition=Text(fields={'keyword': Keyword()})
    status=Text(fields={'keyword': Keyword()})
    warranty=Text(fields={'keyword': Keyword()})
    with_camera=Text(fields={'keyword': Keyword()})
    actual_photos=Text(fields={'keyword': Keyword()})
    comments=Text(fields={'keyword': Keyword()})
    brand=Text(fields={'keyword': Keyword()})
    category=Text(fields={'keyword': Keyword()})
    model=Text(fields={'keyword': Keyword()})
    image=Text(fields={'keyword': Keyword()})
    focal_length_from=Float(fields={'keyword': Keyword()})
    focal_length_to=Float(fields={'keyword': Keyword()})
    aperture=Text(fields={'keyword': Keyword()})
    max_aperture=Text(fields={'keyword': Keyword()})
    weight=Text(fields={'keyword': Keyword()})
    format_range=Text(fields={'keyword': Keyword()})
    lens_type=Text(fields={'keyword': Keyword()})
    lens_type_and=Text(fields={'keyword': Keyword()})
    lens_format=Text(fields={'keyword': Keyword()})
    focus_type=Text(fields={'keyword': Keyword()})
    fixed_focal=Text(fields={'keyword': Keyword()})
    mounts=Text(fields={'keyword': Keyword()})
    magnification=Text(fields={'keyword': Keyword()})
    features=Text(fields={'keyword': Keyword()})
    specifications=Text(fields={'keyword': Keyword()})
    date = Date()

    class Meta:
        index = 'forsalelens'


class ForSaleLightingIndex(DocType):
    description=Text(fields={'keyword': Keyword()})
    product_id = Text(fields={'keyword': Keyword()})
    seller_pk = Integer(fields={'keyword': Keyword()})
    seller=Text(fields={'keyword': Keyword()})
    seller_location=Text(fields={'keyword': Keyword()})
    seller_country = Text(fields={'keyword': Keyword()})
    seller_city = Text(fields={'keyword': Keyword()})
    seller_phonenumber=Text(fields={'keyword': Keyword()})
    label=Text(fields={'keyword': Keyword()})
    price=Float(fields={'keyword': Keyword()})
    neworused = Text(fields={'keyword': Keyword()})
    currency = Text(fields={'keyword': Keyword()})
    condition=Text(fields={'keyword': Keyword()})
    status=Text(fields={'keyword': Keyword()})
    warranty=Text(fields={'keyword': Keyword()})
    comment=Text(fields={'keyword': Keyword()})
    actual_photos=Text(fields={'keyword': Keyword()})
    image=Text(fields={'keyword': Keyword()})
    brand=Text(fields={'keyword': Keyword()})
    category=Text(fields={'keyword': Keyword()})
    model=Text(fields={'keyword': Keyword()})
    date = Date()

    class Meta:
        index = 'forsalelighting'


class ForSaleAccessoriesIndex(DocType):
    description=Text(fields={'keyword': Keyword()})
    product_id = Text(fields={'keyword': Keyword()})
    seller_pk = Integer(fields={'keyword': Keyword()})
    seller=Text(fields={'keyword': Keyword()})
    seller_location=Text(fields={'keyword': Keyword()})
    seller_country = Text(fields={'keyword': Keyword()})
    seller_city = Text(fields={'keyword': Keyword()})
    seller_phonenumber=Text(fields={'keyword': Keyword()})
    label=Text(fields={'keyword': Keyword()})
    price=Float(fields={'keyword': Keyword()})
    neworused = Text(fields={'keyword': Keyword()})
    currency = Text(fields={'keyword': Keyword()})
    condition=Text(fields={'keyword': Keyword()})
    status=Text(fields={'keyword': Keyword()})
    warranty=Text(fields={'keyword': Keyword()})
    comment=Text(fields={'keyword': Keyword()})
    actual_photos=Text(fields={'keyword': Keyword()})
    image=Text(fields={'keyword': Keyword()})
    brand=Text(fields={'keyword': Keyword()})
    category=Text(fields={'keyword': Keyword()})
    model=Text(fields={'keyword': Keyword()})
    date = Date()

    class Meta:
        index = 'forsaleaccessories'


class ForSaleDronesIndex(DocType):
    description=Text(fields={'keyword': Keyword()})
    product_id = Text(fields={'keyword': Keyword()})
    seller_pk = Integer(fields={'keyword': Keyword()})
    seller=Text(fields={'keyword': Keyword()})
    seller_location=Text(fields={'keyword': Keyword()})
    seller_country = Text(fields={'keyword': Keyword()})
    seller_city = Text(fields={'keyword': Keyword()})
    seller_phonenumber=Text(fields={'keyword': Keyword()})
    label=Text(fields={'keyword': Keyword()})
    price=Float(fields={'keyword': Keyword()})
    neworused = Text(fields={'keyword': Keyword()})
    currency = Text(fields={'keyword': Keyword()})
    condition=Text(fields={'keyword': Keyword()})
    status=Text(fields={'keyword': Keyword()})
    warranty=Text(fields={'keyword': Keyword()})
    comment=Text(fields={'keyword': Keyword()})
    actual_photos=Text(fields={'keyword': Keyword()})
    image=Text(fields={'keyword': Keyword()})
    brand=Text(fields={'keyword': Keyword()})
    model=Text(fields={'keyword': Keyword()})
    drone_type=Text(fields={'keyword': Keyword()})
    camera=Text(fields={'keyword': Keyword()})
    video_resolution=Text(fields={'keyword': Keyword()})
    megapixels=Float(fields={'keyword': Keyword()})
    control_range=Text(fields={'keyword': Keyword()})
    operating_time=Text(fields={'keyword': Keyword()})
    date = Date()

    class Meta:
        index = 'forsaledrones'
