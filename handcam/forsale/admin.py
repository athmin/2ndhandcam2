from django.contrib import admin
from django.contrib.auth.models import User
from .models import SellCamera, SellVideoCamera, SellLenses, SellLighting, SellAccessories, SellDrone, ForSaleActualPhoto, CameraLensPackage


def user_country(obj):
		return obj.seller.country

def user_city(obj):
		return obj.seller.city

class SellCameraDetailAdmin(admin.ModelAdmin):
	def cam_image(self):
		if self.actual_photos:
			images = ''
			for photo in self.actual_photos.all():
				images += "<img src='/%s'  width='40' height='40' />" % photo.watermarked.url           
			return images
		else:
			return "No Image to Display"


	cam_image.allow_tags = True

	list_display = ('product_id', 'seller', user_country, user_city, 'label', 'price', 'status', 'condition', cam_image)
	list_filter = ('seller__user__username', 'item__cam_type__brand__company', 'price', 'seller__city', 'seller__country', 'status', 'condition')
	search_fields = ('product_id', 'seller__user__username', 'label', 'price', 'seller__city', 'seller__country')


class SellVideoDetailAdmin(admin.ModelAdmin):
	def cam_image(self):
		if self.actual_photos:
			images = ''
			for photo in self.actual_photos.all():
				images += "<img src='/%s'  width='40' height='40' />" % photo.watermarked.url  
			return images
		else:
			return "No Image to Display"


	cam_image.allow_tags = True

	list_display = ('product_id', 'seller', user_country, user_city, 'label', 'price', 'status', 'condition', cam_image)
	list_filter = ('seller__user__username', 'item__cam_type__brand__company', 'price', 'seller__city', 'seller__country', 'status', 'condition')
	search_fields = ('product_id', 'seller__user__username', 'label', 'price', 'seller__city', 'seller__country')


class SellLensesDetailAdmin(admin.ModelAdmin):
	# def lens_image(self):
	# 	if self.image:
	# 		return "<img src='{}'  width='40' height='40' />".format(self.image.url) 
	# 	else:
	# 		return "No Image to Display"

	#lens_image.allow_tags = True

	list_display = ('product_id', 'seller', user_country, user_city, 'label', 'price', 'status', 'condition')
	list_filter = ('seller__user__username', 'item__type_of_lens__brand__company', 'price', 'seller__city', 'seller__country', 'condition', 'status')
	search_fields = ('product_id', 'seller__user__username', 'label', 'price', 'seller__city', 'seller__country')


class SellLightingDetailAdmin(admin.ModelAdmin):

	list_display = ('product_id', 'category', 'seller', user_country, user_city, 'price', 'brand', 'model', 'status', 'condition')
	list_filter = ('seller__user__username', 'label', 'price', 'seller__city', 'seller__country', 'brand', 'status', 'category', 'condition')
	search_fields = ('product_id', 'seller__user__username', 'label', 'price', 'seller__city', 'seller__country', 'brand', 'model')


class SellAccessoriesDetailAdmin(admin.ModelAdmin):

	list_display = ('product_id', 'category', 'seller', user_country, user_city, 'price', 'brand', 'model', 'status', 'condition')
	list_filter = ('seller__user__username', 'label', 'price', 'seller__city', 'seller__country', 'brand', 'status', 'category', 'condition')
	search_fields = ('product_id', 'seller__user__username', 'label', 'price', 'seller__city', 'seller__country', 'brand', 'model')

class SellDroneDetailAdmin(admin.ModelAdmin):

	list_display = ('product_id', 'drone_type', 'seller', user_country, user_city, 'price', 'brand', 'model', 'status', 'condition')
	list_filter = ('seller__user__username', 'label', 'price', 'seller__city', 'seller__country', 'brand', 'status', 'drone_type', 'condition')
	search_fields = ('product_id', 'seller__user__username', 'label', 'price', 'seller__city', 'seller__country', 'brand', 'model')



class ActualPhotoDetailAdmin(admin.ModelAdmin):
	def cam_image(self):
		if self.actual_photo:
			return "<img src='{}'  width='40' height='40' />".format(self.actual_photo.url) 
		else:
			return "No Image to Display"

	cam_image.allow_tags = True

	list_display = ('user', 'cam_model', cam_image,)


class LensPackages(admin.ModelAdmin):
	list_display = ('lens_package', )
	


admin.site.register(SellCamera, SellCameraDetailAdmin)
admin.site.register(SellVideoCamera, SellVideoDetailAdmin)
admin.site.register(SellLenses, SellLensesDetailAdmin)
admin.site.register(SellLighting, SellLightingDetailAdmin)
admin.site.register(SellAccessories, SellAccessoriesDetailAdmin)
#admin.site.register(ForSaleActualPhoto, ActualPhotoDetailAdmin)
#admin.site.register(CameraLensPackage, LensPackages)
admin.site.register(SellDrone, SellDroneDetailAdmin)
