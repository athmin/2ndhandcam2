# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-01-09 04:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forsale', '0012_auto_20171224_0732'),
    ]

    operations = [
        migrations.AddField(
            model_name='sellcamera',
            name='description',
            field=models.TextField(default=1),
            preserve_default=False,
        ),
    ]
