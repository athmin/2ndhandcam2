# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-04-13 16:05
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forsale', '0005_auto_20180413_1603'),
    ]

    operations = [
        migrations.AddField(
            model_name='sellaccessories',
            name='sold_on',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sellcamera',
            name='sold_on',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='selllenses',
            name='sold_on',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='selllighting',
            name='sold_on',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sellvideocamera',
            name='sold_on',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='sellaccessories',
            name='expiry',
            field=models.DateTimeField(default=datetime.datetime(2018, 5, 28, 16, 5, 23, 334481)),
        ),
        migrations.AlterField(
            model_name='sellcamera',
            name='expiry',
            field=models.DateTimeField(default=datetime.datetime(2018, 5, 28, 16, 5, 23, 283666)),
        ),
        migrations.AlterField(
            model_name='selllenses',
            name='expiry',
            field=models.DateTimeField(default=datetime.datetime(2018, 5, 28, 16, 5, 23, 328269)),
        ),
        migrations.AlterField(
            model_name='selllighting',
            name='expiry',
            field=models.DateTimeField(default=datetime.datetime(2018, 5, 28, 16, 5, 23, 331117)),
        ),
        migrations.AlterField(
            model_name='sellvideocamera',
            name='expiry',
            field=models.DateTimeField(default=datetime.datetime(2018, 5, 28, 16, 5, 23, 321721)),
        ),
    ]
