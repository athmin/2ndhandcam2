# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-13 08:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forsale', '0005_auto_20171110_0259'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sellcamera',
            name='slug',
            field=models.SlugField(blank=True, max_length=255, unique=True),
        ),
    ]
