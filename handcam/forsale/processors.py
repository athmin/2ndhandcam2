from django.conf import settings
from imagekit import ImageSpec, register
from PIL import Image, ImageDraw, ImageFont, ImageMath

_default_font = ImageFont.truetype('/usr/share/fonts/dejavu/DejaVuSans-Bold.ttf', 40)

def add_text_overlay(image, text, font=_default_font):
	rgba_image = image.convert('RGBA')
	text_overlay = Image.new('RGBA', rgba_image.size, (255, 255, 255, 0))
	image_draw = ImageDraw.Draw(text_overlay)
	text_size_x, text_size_y = image_draw.textsize(text, font=font)
	text_xy = ((rgba_image.size[0] / 2) - (text_size_x / 2), (rgba_image.size[1] / 2) - (text_size_y / 2))
	image_draw.text(text_xy, text, font=font, fill=(255, 255, 255, 128))
	image_with_text_overlay = Image.alpha_composite(rgba_image, text_overlay)

	return image_with_text_overlay


def add_watermark(image):
	watermark = Image.open(settings.WATERMARK_IMAGE)
	image = resize_image(image)	
	rgba_image = image.convert('RGBA')
	rgba_watermark = watermark.convert('RGBA')

	image_x, image_y = rgba_image.size
	watermark_x, watermark_y = rgba_watermark.size

	watermark_scale = max(image_x / (5.0 * watermark_x), image_y / (5.0 * watermark_y))
	new_size = (int(watermark_x * watermark_scale), int(watermark_y * watermark_scale))
	rgba_watermark = rgba_watermark.resize(new_size, resample=Image.ANTIALIAS)

	#rgba_watermark_mask = rgba_watermark.convert("L").point(lambda x: min(x, 100))
	#rgba_watermark.putalpha(rgba_watermark_mask)

	watermark_x, watermark_y = rgba_watermark.size
	# print ("image_x : " + str (image_x))
	# print ("image_y : " + str (image_y))
	# print ("watermark_x : " + str (watermark_x))
	# print ("watermark_y : " + str (watermark_y))	
	# print ("x : " + str ((image_x - watermark_x) // 1))
	# print ("y : " + str ((image_y - watermark_y) // 1))
	x = ((image_x - watermark_x) // 1) - 40
	y = ((image_y - watermark_y) // 1) - 40
	rgba_image.paste(rgba_watermark, (x, y), rgba_watermark)

	return rgba_image


# image_x : 675
# image_y : 450
# watermark_x : 150
# watermark_y : 90
# x : 525
# y : 360

#37
#rgba_image.paste(rgba_watermark, (525-40, 360-40), rgba_watermark)

#38
#rgba_image.paste(rgba_watermark, (525-30, 360-30), rgba_watermark)


def resize_image(image):
	basewidth = 675
	new_image = image
	# (width, height) = new_image.size
	# size = (675, 'auto')
	wpercent = (basewidth/float(new_image.size[0]))
	hsize = int((float(new_image.size[1])*float(wpercent)))
	new_image = image.resize((basewidth,hsize), resample=Image.ANTIALIAS)

	# basewidth = 300
	# img = Image.open('somepic.jpg')
	# wpercent = (basewidth/float(img.size[0]))
	# hsize = int((float(img.size[1])*float(wpercent)))
	# img = img.resize((basewidth,hsize), Image.ANTIALIAS)
	
	return new_image


def image_size_checker(images):
	
	for image in images.getlist('actual_photos'):
		image = Image.open(image)
		rgba_image = image.convert('RGBA')
		image_x, image_y = rgba_image.size

		if (image_x < 400) and (image_y < 300):
			return "Invalid"
	
	return "Valid" 