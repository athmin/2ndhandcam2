from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.core.cache import cache
from django.shortcuts import render
from django.http import Http404
from django.template.loader import render_to_string
from django.utils.crypto import get_random_string
from django.utils.translation import ugettext_lazy as _

from rest_framework import generics
from rest_framework import status
from rest_framework_jwt.utils import jwt_encode_handler
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from .serializers import LensForSaleSerializer, GetActualPhotosSerializer, \
	GetCamForSaleSerializer, SellerDetailSerializer, CamForSaleSerializer, \
	GetVideoCamForSaleSerializer, VideoCamForSaleSerializer, GetLensForSaleSerializer, \
	LightingForSaleSerializer, GetLightingForSaleSerializer, AccessoriesForSaleSerializer, \
	GetAccessoriesForSaleSerializer, GetCameraLensPackageSerializer, CreateCameraLensPackageSerializer, \
	GetLensPackageSerializer, CreateAccessoriesDescription, GetAccessoriesDescription, \
	GetStillAccessoriesDescription, GetVideoAccessoriesDescription, GetDroneForSaleSerializer, DroneForSaleSerializer
from profiles.serializers import ProfileDetailSerializer
from forsale.models import SellCamera, SellVideoCamera, SellLenses, ForSaleActualPhoto, SellLighting, SellAccessories, SellDrone, CameraLensPackage, \
	CameraAccessoriesDescription
from camera.models import CameraDetails
from lens.models import LensDetails
from profiles.models import Profile, Review
from .search import ForSaleIndex, ForSaleVideoIndex, ForSaleLensIndex, ForSaleLightingIndex, ForSaleAccessoriesIndex, ForSaleDronesIndex

from imagekit import ImageSpec, register
from PIL import Image, ImageFont
from .processors import add_text_overlay, add_watermark, image_size_checker
from django.core.files.images import ImageFile
from io import BytesIO
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import InMemoryUploadedFile
from functools import lru_cache
import uuid
import datetime
import json

from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from django.conf import settings

client = Elasticsearch([{'host': settings.ESHOST, 'port': settings.ESPORT}])


def upload_camera_for_sale(request):
		
	form = UploadCameraForm
	return render(request, 'sellcamera.html', context)
 

# SELL ENDPOINT 

#
# HELPER FUNCTIONS
#
def get_profile(user):
	profile = ''
	try:
		profile = Profile.objects.get(user=user)
	except Profile.DoesNotExist:
		status = {"status_code": 404, "detail": _("User Profile does not exist!")}
		return Response(status)

	return profile	


def get_forsale_cam(profile):
		
	forsale_cam = SellCamera.objects.filter(seller=profile)
	return forsale_cam

def get_expired_forsale_cam(profile):
		
	expired_cam = SellCamera.objects.filter(seller=profile, expiry__lte=datetime.datetime.now())
	return expired_cam


def get_forsale_videocam(profile):
		
	forsale_cam = SellVideoCamera.objects.filter(seller=profile)
	return forsale_cam

def get_expired_forsale_videocam(profile):
		
	expired_videocam = SellVideoCamera.objects.filter(seller=profile, expiry__lte=datetime.datetime.now())
	return expired_videocam


def get_forsale_lens(profile):
		
	forsale_lens = SellLenses.objects.filter(seller=profile)
	return forsale_lens

def get_expired_forsale_lens(profile):
		
	expired_lens = SellLenses.objects.filter(seller=profile, expiry__lte=datetime.datetime.now())
	return expired_lens

def get_forsale_lighting(profile):
		
	forsale_lighting = SellLighting.objects.filter(seller=profile)
	return forsale_lighting

def get_expired_forsale_lighting(profile):
		
	expired_lighting = SellLighting.objects.filter(seller=profile, expiry__lte=datetime.datetime.now())
	return expired_lighting

def get_forsale_accessories(profile):
		
	forsale_accessories = SellAccessories.objects.filter(seller=profile)
	return forsale_accessories

def get_forsale_drones(profile):
		
	forsale_drones = SellDrone.objects.filter(seller=profile)
	return forsale_drones

def get_expired_forsale_accessories(profile):
		
	expired_accessories = SellAccessories.objects.filter(seller=profile, expiry__lte=datetime.datetime.now())
	return expired_accessories

def get_expired_forsale_drones(profile):
		
	expired_drones = SellDrone.objects.filter(seller=profile, expiry__lte=datetime.datetime.now())
	return expired_drones

def get_cam_model(pk):
	cam_model = ''
	try:
		cam_model = CameraDetails.objects.get(pk=pk)
	except CameraDetails.DoesNotExist:
		status = {"status_code": 404, "detail": _("Camera detail does not exist!")}
		return Response(status)

	return str(cam_model.cam_type.brand.company) +' '+ str(cam_model.model)


def get_lens_model(pk):
	lens_model = ''
	try:
		lens_model = LensDetails.objects.get(pk=pk)
	except LensDetails.DoesNotExist:
		status = {"status_code": 404, "detail": _("Lens detail does not exist!")}
		return Response(status)

	return str(lens_model.type_of_lens.brand.company) +' '+ str(lens_model.model)


def get_item_pk(model, pk):
	if (model=="Camera"):
		try:
			item = SellCamera.objects.get(pk=pk)
			return item
		except SellCamera.DoesNotExist:
			status = {'error' : 'You are trying to retrieve an item that is not in the database.'}
			return Response(status)
	elif (model=="Video"):
		try:
			item = SellVideoCamera.objects.get(pk=pk)
			return item
		except SellVideoCamera.DoesNotExist:
			status = {'error' : 'You are trying to retrieve an item that is not in the database.'}
			return Response(status)
	elif (model=="Lens"):
		try:
			item = SellLenses.objects.get(pk=pk)
			return item
		except SellLenses.DoesNotExist:
			status = {'error' : 'You are trying to retrieve an item that is not in the database.'}
			return Response(status)
	elif (model=="Lighting"):
		try:
			item = SellLighting.objects.get(pk=pk)
			return item
		except SellLighting.DoesNotExist:
			status = {'error' : 'You are trying to retrieve an item that is not in the database.'}
			return Response(status)
	elif (model=="Accessories"):
		try:
			item = SellAccessories.objects.get(pk=pk)
			return item
		except SellAccessories.DoesNotExist:
			status = {'error' : 'You are trying to retrieve an item that is not in the database.'}
			return Response(status)
	else:
		try:
			item = SellDrone.objects.get(pk=pk)
			return item
		except SellDrone.DoesNotExist:
			status = {'error' : 'You are trying to retrieve an item that is not in the database.'}
			return Response(status)


def get_actual_photo(photopk):
	try:
		photo = ForSaleActualPhoto.objects.get(pk=photopk)
		return photo
	except ForSaleActualPhoto.DoesNotExist:
		print ("photo not found in db")


def _create_result_id():
	return uuid.uuid4().hex


#
# Check for expiration of each for sale items
# /api/sell/expiration-check
#
@permission_classes((IsAuthenticated, ))
class CheckForSaleExpirationCount(APIView):

	def get(self, request, format=None):
		profile = get_profile(request.user)

		camera = get_expired_forsale_cam(profile)
		video = get_expired_forsale_videocam(profile)
		lens = get_expired_forsale_lens(profile)
		lighting = get_expired_forsale_lighting(profile)
		accessories = get_expired_forsale_accessories(profile)
		drones = get_expired_forsale_drones(profile)

		expired_items = len(camera) + len(video) + len(lens) + len(lighting) + len(accessories) + len(drones)

		return Response(expired_items)




#
# Endpoint for reactivating expired for sale items
# /api/sell/reactivate-forsale
#
@permission_classes((IsAuthenticated, ))
class ActivateExpiredItem(APIView):


	def post(self, request):
		data = self.request.data 

		self.model = data['model']
		self.pk = data['pk']

		forsale_item = get_item_pk(self.model, self.pk)
		forsale_item.status = "Active"
		forsale_item.expiry = datetime.datetime.now() + datetime.timedelta(days=45)
		forsale_item.save()

		profile = get_profile(request.user)

		cameras = get_forsale_cam(profile)
		video_cameras = get_forsale_videocam(profile)
		lenses = get_forsale_lens(profile)
		lightings = get_forsale_lighting(profile)
		accessories = get_forsale_accessories(profile)
		drones = get_forsale_drones(profile)

		xcamera = get_expired_forsale_cam(profile)
		xvideo = get_expired_forsale_videocam(profile)
		xlens = get_expired_forsale_lens(profile)
		xlighting = get_expired_forsale_lighting(profile)
		xaccessories = get_expired_forsale_accessories(profile)
		xdrones = get_expired_forsale_drones(profile)

		expired_items = len(xcamera) + len(xvideo) + len(xlens) + len(xlighting) + len(xaccessories) +len(xdrones)

		result = {
			'cameras' : GetCamForSaleSerializer(cameras, many=True).data,
			'video_cameras' : GetVideoCamForSaleSerializer(video_cameras, many=True).data,
			'lenses' : GetLensForSaleSerializer(lenses, many=True).data,
			'lightings' : GetLightingForSaleSerializer(lightings, many=True).data,
			'accessories' : GetAccessoriesForSaleSerializer(accessories, many=True).data,
			'drones' : GetDroneForSaleSerializer(drones, many=True).data,
			'user_profile' : ProfileDetailSerializer(profile).data,
			'expired_items' : expired_items,
			'error' : None,
			'status' : {'status': 'Item successfully reactivated.'}
		}

		return Response(result)


#
# Endpoint for changing status of item from Active to Sold
# /api/sell/sold-forsale
#
@permission_classes((IsAuthenticated, ))
class ChangeForsaleToSold(APIView):


	def post(self, request):

		data = self.request.data 

		self.model = data['model']
		self.pk = data['pk']


		forsale_item = get_item_pk(self.model, self.pk)
		forsale_item.status = "Sold"
		forsale_item.sold_on = datetime.datetime.now()
		forsale_item.save()

		profile = get_profile(request.user)

		cameras = get_forsale_cam(profile)
		video_cameras = get_forsale_videocam(profile)
		lenses = get_forsale_lens(profile)
		lightings = get_forsale_lighting(profile)
		accessories = get_forsale_accessories(profile)
		drones = get_forsale_drones(profile)

		xcamera = get_expired_forsale_cam(profile)
		xvideo = get_expired_forsale_videocam(profile)
		xlens = get_expired_forsale_lens(profile)
		xlighting = get_expired_forsale_lighting(profile)
		xaccessories = get_expired_forsale_accessories(profile)
		xdrones = get_expired_forsale_drones(profile)

		expired_items = len(xcamera) + len(xvideo) + len(xlens) + len(xlighting) + len(xaccessories) + len(xdrones)

		result = {
			'cameras' : GetCamForSaleSerializer(cameras, many=True).data,
			'video_cameras' : GetVideoCamForSaleSerializer(video_cameras, many=True).data,
			'lenses' : GetLensForSaleSerializer(lenses, many=True).data,
			'lightings' : GetLightingForSaleSerializer(lightings, many=True).data,
			'accessories' : GetAccessoriesForSaleSerializer(accessories, many=True).data,
			'drones' : GetDroneForSaleSerializer(drones, many=True).data,
			'user_profile' : ProfileDetailSerializer(profile).data,
			'expired_items' : expired_items,
			'error' : None,
			'status' : {'status': 'Item successfully changed status to sold.'}
		}

		return Response(result)


#
# Endpoint for changing status of item from Active to Disable
# /api/sell/disable-forsale
#
@permission_classes((IsAuthenticated, ))
class DisableForSaleItem(APIView):


	def post(self, request):
		data = self.request.data 

		self.model = data['model']
		self.pk = data['pk']

		forsale_item = get_item_pk(self.model, self.pk)
		forsale_item.status = "Disabled"
		forsale_item.save()

		profile = get_profile(request.user)

		cameras = get_forsale_cam(profile)
		video_cameras = get_forsale_videocam(profile)
		lenses = get_forsale_lens(profile)
		lightings = get_forsale_lighting(profile)
		accessories = get_forsale_accessories(profile)
		drones = get_forsale_drones(profile)

		xcamera = get_expired_forsale_cam(profile)
		xvideo = get_expired_forsale_videocam(profile)
		xlens = get_expired_forsale_lens(profile)
		xlighting = get_expired_forsale_lighting(profile)
		xaccessories = get_expired_forsale_accessories(profile)
		xdrones = get_expired_forsale_drones(profile)

		expired_items = len(xcamera) + len(xvideo) + len(xlens) + len(xlighting) + len(xaccessories) + len(xdrones)

		result = {
			'cameras' : GetCamForSaleSerializer(cameras, many=True).data,
			'video_cameras' : GetVideoCamForSaleSerializer(video_cameras, many=True).data,
			'lenses' : GetLensForSaleSerializer(lenses, many=True).data,
			'lightings' : GetLightingForSaleSerializer(lightings, many=True).data,
			'accessories' : GetAccessoriesForSaleSerializer(accessories, many=True).data,
			'drones' : GetDroneForSaleSerializer(drones, many=True).data,
			'user_profile' : ProfileDetailSerializer(profile).data,
			'expired_items' : expired_items,
			'error' : None,
			'status' : {'status': 'Item successfully disabled.'}
		}

		return Response(result)


#
# Endpoint for changing status of item from Disable to Active
# /api/sell/enable-forsale
#
@permission_classes((IsAuthenticated, ))
class EnableForSaleItem(APIView):


	def post(self, request):
		data = self.request.data 

		self.model = data['model']
		self.pk = data['pk']

		forsale_item = get_item_pk(self.model, self.pk)
		forsale_item.status = "Active"
		forsale_item.expiry = datetime.datetime.now() + datetime.timedelta(days=45)
		forsale_item.save()

		profile = get_profile(request.user)

		cameras = get_forsale_cam(profile)
		video_cameras = get_forsale_videocam(profile)
		lenses = get_forsale_lens(profile)
		lightings = get_forsale_lighting(profile)
		accessories = get_forsale_accessories(profile)
		drones = get_forsale_drones(profile)

		xcamera = get_expired_forsale_cam(profile)
		xvideo = get_expired_forsale_videocam(profile)
		xlens = get_expired_forsale_lens(profile)
		xlighting = get_expired_forsale_lighting(profile)
		xaccessories = get_expired_forsale_accessories(profile)
		xdrones = get_expired_forsale_drones(profile)

		expired_items = len(xcamera) + len(xvideo) + len(xlens) + len(xlighting) + len(xaccessories) + len(xdrones)

		result = {
			'cameras' : GetCamForSaleSerializer(cameras, many=True).data,
			'video_cameras' : GetVideoCamForSaleSerializer(video_cameras, many=True).data,
			'lenses' : GetLensForSaleSerializer(lenses, many=True).data,
			'lightings' : GetLightingForSaleSerializer(lightings, many=True).data,
			'drones' : GetDroneForSaleSerializer(drones, many=True).data,
			'accessories' : GetAccessoriesForSaleSerializer(accessories, many=True).data,
			'user_profile' : ProfileDetailSerializer(profile).data,
			'expired_items' : expired_items,
			'error' : None,
			'status' : {'status': 'Item successfully enabled.'}
		}

		return Response(result)




# sell camera 
# /api/sell/camera/upload
@permission_classes((IsAuthenticated, ))
class UploadCameraView(generics.CreateAPIView):
	
	def post(self, request, *args, **kwargs):
		profile = get_profile(request.user)
		cam_model = get_cam_model(request.data.get('item'))
		actual_photos = request.FILES
		photo_pks = [] # photos pk list to be passed to actual_photos

		#	(disabled by request 12-29-2018)
		#	Check uploaded images size
		#
		# size_check = image_size_checker(actual_photos)
		# if size_check == "Invalid":
			
		# 	status = {
		# 		#'error' : "Please upload an image with a decent size",
		# 		'status' : {'status': 'Please upload an image with a decent size.'}
		# 	}
		# 	return Response(status)

		for ap in actual_photos.getlist('actual_photos'):
			img_io = BytesIO()
			result_id = _create_result_id()
			photo = Image.open(ap)
			text = "www.2ndhandcam.com"
			pic = ''

			result_image = add_watermark(photo)
			result_image.save(img_io, format='PNG', quality=100)
			img_io.seek(0)
			img_content = ContentFile(img_io.read(), str(result_id)+'.png')
			
			photo_data = {
				'watermarked':img_content,
				'cam_model' : cam_model,
				'user' : profile.pk
			}
			
			images_serializer = GetActualPhotosSerializer(data=photo_data)
			if images_serializer.is_valid():
				pic = images_serializer.save()
				photo_pks.append(pic.pk)
				
			else:
				print (images_serializer.errors)


		sell_data = {
			'seller': profile.pk,
			'item': request.data.get('item', None),
			'price': request.data.get('price', None),
			'condition': request.data.get('condition', None),
			#'description': request.data.get('description', None),
			'comments': request.data.get('comments', None),
			'shutter_count': request.data.get('shutter_count', None),
			'status': request.data.get('status', None),
			'warranty': request.data.get('warranty', None),
			'actual_photos': photo_pks,
			'package' : request.data.get('camera_package', None)
		}
		sell_serializer = CamForSaleSerializer(data=sell_data)
		if sell_serializer.is_valid():
			sell_serializer.save()
			
			result = {
				'data': sell_serializer.data,
				'error' : None,
				'status' : {'status': 'Item successfully uploaded.'}
			}
			return Response(result)
		#print (sell_serializer.errors)
		status={'error': sell_serializer.errors}
		return Response(status)

# sell camera 
# /api/sell/camera/lenspackage
@permission_classes((IsAuthenticated, ))
class UploadLensPackageView(generics.CreateAPIView):
	
	def post(self, request, *args, **kwargs):
		print ('uploading lens packages')
		cam_pk = request.data.get('cam_id')
		
		camera = SellCamera.objects.get(pk=cam_pk)
		lens_package = json.loads(request.data.get('lensPackage'))
		
		
		profile = get_profile(request.user)
		lens_photos = request.FILES
		lens_package_pks = []
		
		for p in lens_package:
			
			try:
				lens = LensDetails.objects.get(pk=p['id'])
			except LensDetails.DoesNotExist:
				print ('lens id not found')
			data = {
				'lens_package': p['id']
			}

			package_serializer = CreateCameraLensPackageSerializer(data=data)
			if package_serializer.is_valid():
				pack = package_serializer.save()
				print ("saving lens package")
				lens_package_pks.append(pack.pk)
				
			else:
				print (package_serializer.errors)


		for package in lens_package_pks:
			print ("adding package to camera")
			camera.lens_package.add(package)
			camera.save()
	
		photo_pks = [] # photos pk list to be passed to actual_photos

		#
		#	Check uploaded images size
		#
		# size_check = image_size_checker(lens_photos)
		# if size_check == "Invalid":
			
		# 	status = {
		# 		'error' : "Please upload an image with a decent size",
		# 		'status' : {'status': 'Please upload an image with a decent size.'}
		# 	}
			
		# 	return Response(status)

		for ap in lens_photos.getlist('lens_photos'):
			img_io = BytesIO()
			result_id = _create_result_id()
			photo = Image.open(ap)
			text = "www.2ndhandcam.com"
			pic = ''

			result_image = add_watermark(photo)
			result_image.save(img_io, format='PNG', quality=100)
			img_io.seek(0)
			img_content = ContentFile(img_io.read(), str(result_id)+'.png')
			
			photo_data = {
				'watermarked':img_content,
				'cam_model' : "Lens Package" ,
				'user' : profile.pk
			}
			
			images_serializer = GetActualPhotosSerializer(data=photo_data)
			if images_serializer.is_valid():
				pic = images_serializer.save()
				photo_pks.append(pic.pk)
				print("saving to actual photo db")
				
			else:
				print (images_serializer.errors)

		for pk in photo_pks:
			camera.lens_photos.add(pk)
			camera.save()

		result = {
				
				'error' : None,
				'status' : {'status': 'Item successfully uploaded.'}
			}
		return Response(result)


# sell camera 
# /api/sell/camera/edit/lenspackage
@permission_classes((IsAuthenticated, ))
class EditLensPackageView(generics.CreateAPIView):
	
	def post(self, request, *args, **kwargs):
		print ('uploading lens packages')
		cam_pk = request.data.get('cam_id')
		#print (cam_pk)
		camera = SellCamera.objects.get(pk=cam_pk)
		lens_package = json.loads(request.data.get('lensPackage'))
		edit_lens = request.data.get('edit')
		removed_lens = json.loads(request.data.get('removed_lens'))
		add_lens = json.loads(request.data.get('add_lens'))
		
		profile = get_profile(request.user)
		lens_photos = request.FILES
		lens_package_pks = []
		
		if edit_lens:
			if len(removed_lens) > 0:
				for x in removed_lens:
					print (x)
					try:
						lens = CameraLensPackage.objects.get(pk=x['id'])
						camera.lens_package.remove(lens)
						camera.save()
						lens.delete()
					except CameraLensPackage.DoesNotExist:
						print ('lens id not found')

			if len(add_lens) > 0:
				for p in add_lens:
					try:
						lens = LensDetails.objects.get(pk=p['id'])
					except LensDetails.DoesNotExist:
						print ('lens id not found')
					data = {
						'lens_package': p['id']
					}

					package_serializer = CreateCameraLensPackageSerializer(data=data)
					if package_serializer.is_valid():
						pack = package_serializer.save()
						lens_package_pks.append(pack.pk)
					else:
						print (package_serializer.errors)
				
				for package in lens_package_pks:
					camera.lens_package.add(package)
					camera.save()
				

		else:
			print ("edit lens else")
	
		photo_pks = [] # photos pk list to be passed to actual_photos

		#
		#	Check uploaded images size
		#
		# size_check = image_size_checker(lens_photos)
		# if size_check == "Invalid":
			
		# 	status = {
		# 		'error' : "Please upload an image with a decent size",
		# 		'status' : {'status': 'Please upload an image with a decent size.'}
		# 	}
			
		# 	return Response(status)

		for ap in lens_photos.getlist('lens_photos'):
			img_io = BytesIO()
			result_id = _create_result_id()
			photo = Image.open(ap)
			text = "www.2ndhandcam.com"
			pic = ''

			result_image = add_watermark(photo)
			result_image.save(img_io, format='PNG', quality=100)
			img_io.seek(0)
			img_content = ContentFile(img_io.read(), str(result_id)+'.png')
			
			photo_data = {
				'watermarked':img_content,
				'cam_model' : "Lens Package" ,
				'user' : profile.pk
			}
			
			images_serializer = GetActualPhotosSerializer(data=photo_data)
			if images_serializer.is_valid():
				pic = images_serializer.save()
				photo_pks.append(pic.pk)
				print("saving to actual photo db")
				
			else:
				print (images_serializer.errors)

		for pk in photo_pks:
			camera.lens_photos.add(pk)
			camera.save()

		result = {
				
				'error' : None,
				'status' : {'status': 'Item successfully uploaded.'}
			}
		return Response(result)


# upload accessories description
# /api/sell/camera/accessories
#
@permission_classes((AllowAny,))
class UploadStillAccessories(APIView):

	def post(self, request, *args, **kwargs):
		print ("saving still accessories")
		cam_pk = request.data.get('cam_id')
		camera = SellCamera.objects.get(pk=cam_pk)
		accessories = json.loads(request.data.get('accessories_package'))

		acc_pk = []
		profile = get_profile(request.user)
		acc_photos = request.FILES
		acc_package_pks = []

		for acc in accessories:

			data = {
				'description' : acc
			}

			accessories_serializer = CreateAccessoriesDescription(data=data)
			if accessories_serializer.is_valid():
				pack = accessories_serializer.save()
				print ("saving lens package")
				acc_pk.append(pack.pk)
			else:
				print (accessories_serializer.errors)

		for pk in acc_pk:
			camera.description.add(pk)
			camera.save()

		photo_pks = [] # photos pk list to be passed to actual_photos

		#
		#	Check uploaded images size
		#
		# size_check = image_size_checker(acc_photos)
		# if size_check == "Invalid":
			
		# 	status = {
		# 		'error' : "Please upload an image with a decent size",
		# 		'status' : {'status': 'Please upload an image with a decent size.'}
		# 	}
			
		# 	return Response(status)

		for ap in acc_photos.getlist('acc_photos'):
			img_io = BytesIO()
			result_id = _create_result_id()
			photo = Image.open(ap)
			text = "www.2ndhandcam.com"
			pic = ''

			result_image = add_watermark(photo)
			result_image.save(img_io, format='PNG', quality=100)
			img_io.seek(0)
			img_content = ContentFile(img_io.read(), str(result_id)+'.png')
			
			photo_data = {
				'watermarked':img_content,
				'cam_model' : "Accessories Package" ,
				'user' : profile.pk
			}
			
			images_serializer = GetActualPhotosSerializer(data=photo_data)
			if images_serializer.is_valid():
				pic = images_serializer.save()
				photo_pks.append(pic.pk)
				print("saving to actual photo db")
				
			else:
				print (images_serializer.errors)

		for pk in photo_pks:
			camera.lens_photos.add(pk)
			camera.save()

		result = {
				
				'error' : None,
				'status' : {'status': 'Item successfully uploaded.'}
			}
		return Response(result)


# edit accessories description
# /api/sell/camera/accessories/edit
#
@permission_classes((AllowAny,))
class EditStillAccessories(APIView):

	def post(self, request, *args, **kwargs):
		print ("editing still accessories")
		cam_pk = request.data.get('cam_id')
		camera = SellCamera.objects.get(pk=cam_pk)
		accessories = json.loads(request.data.get('accessories_package'))
		add_acc = json.loads(request.data.get('add_acc'))
		edit_acc = request.data.get('edit_accessories')
		remove_acc = json.loads(request.data.get('removed_acc'))

		acc_pk = []
		profile = get_profile(request.user)
		acc_photos = request.FILES
		acc_package_pks = []

		if edit_acc:
			print ("inside edit acc")
			if len(remove_acc) > 0:
				for x in remove_acc:
					print (x)
					try:
						acc = CameraAccessoriesDescription.objects.get(pk=x['id'])
						camera.description.remove(acc)
						camera.save()
						acc.delete()
					except CameraAccessoriesDescription.DoesNotExist:
						print ('acc id not found')

			if len(add_acc) > 0:
				for acc in add_acc:
					data = {
						'description' : acc
					}

					accessories_serializer = CreateAccessoriesDescription(data=data)
					if accessories_serializer.is_valid():
						pack = accessories_serializer.save()
						print ("saving acc package")
						acc_pk.append(pack.pk)
					else:
						print (accessories_serializer.errors)

				for pk in acc_pk:
					camera.description.add(pk)
					camera.save()
				

		else:
			print ("edit lens else")

		photo_pks = [] # photos pk list to be passed to actual_photos

		#
		#	Check uploaded images size
		#
		# size_check = image_size_checker(acc_photos)
		# if size_check == "Invalid":
			
		# 	status = {
		# 		'error' : "Please upload an image with a decent size",
		# 		'status' : {'status': 'Please upload an image with a decent size.'}
		# 	}
			
		# 	return Response(status)

		for ap in acc_photos.getlist('acc_photos'):
			img_io = BytesIO()
			result_id = _create_result_id()
			photo = Image.open(ap)
			text = "www.2ndhandcam.com"
			pic = ''

			result_image = add_watermark(photo)
			result_image.save(img_io, format='PNG', quality=100)
			img_io.seek(0)
			img_content = ContentFile(img_io.read(), str(result_id)+'.png')
			
			photo_data = {
				'watermarked':img_content,
				'cam_model' : "Accessories Package" ,
				'user' : profile.pk
			}
			
			images_serializer = GetActualPhotosSerializer(data=photo_data)
			if images_serializer.is_valid():
				pic = images_serializer.save()
				photo_pks.append(pic.pk)
				print("saving to actual photo db")
				
			else:
				print (images_serializer.errors)

		for pk in photo_pks:
			camera.lens_photos.add(pk)
			camera.save()

		result = {
				
				'error' : None,
				'status' : {'status': 'Item successfully uploaded.'}
			}
		return Response(result)


# get camera for sale lens package
# /api/sell/camera/accessories/pk
#
@permission_classes((AllowAny,))
class GetAccessoriesView(APIView):

	def get_accessories(self, pk):
		try:
			camera = SellCamera.objects.get(pk=pk)
			return camera
		except SellCamera.DoesNotExist:
			print ("not found")
			status = {'error' : 'You are trying to get an item that is not in the database.'}
			return Response(status)	
	
	def get(self, request, pk, format=None): 
		print ("requesting accessories")

		camera = self.get_accessories(pk)
		
		serializer = GetAccessoriesDescription(camera.description.all(), many=True)
		#print(serializer.data)
		return Response(serializer.data)


# get camera for sale lens package
# /api/sell/camera/lens_package/pk
#
@permission_classes((AllowAny,))
class GetLensPackageView(APIView):

	def get_lens_package(self, pk):
		try:
			camera = SellCamera.objects.get(pk=pk)
			return camera
		except SellCamera.DoesNotExist:
			status = {'error' : 'You are trying to get an item that is not in the database.'}
			return Response(status)	
	
	def get(self, request, pk, format=None): 
		
		camera = self.get_lens_package(pk)
		serializer = GetCameraLensPackageSerializer(camera.lens_package.all(), many=True)
		
		return Response(serializer.data)
		



# edit camera 
# /api/sell/camera/edit
@permission_classes((IsAuthenticated, ))
class EditSaleCameraView(APIView):


	def post(self, request, *args, **kwargs):
		description = "Body Only"

		pk = request.data.get('pk')
		price = request.data.get('price') 
		condition = request.data.get('condition')
		description = request.data.get('description')
		shutter_count = request.data.get('shutter_count')
		status = request.data.get('status')
		warranty = request.data.get('warranty')
		package = request.data.get('camera_package')
		comments = request.data.get('comments')

		profile = get_profile(request.user)
		cam_item = request.data.get('item')
		cam_model = get_cam_model(cam_item)
		actual_photos = request.FILES
		photo_pks = [] # photos pk list to be passed to actual_photos
		if actual_photos:
			#
			#	Check uploaded images size
			#
			# size_check = image_size_checker(actual_photos)
			# if size_check == "Invalid":
				
			# 	status = {
			# 		'error' : "Please upload an image with a decent size"
			# 	}
			# 	return Response(status)

			for ap in actual_photos.getlist('actual_photos'):
				img_io = BytesIO()
				result_id = _create_result_id()
				photo = Image.open(ap)
				text = "www.2ndhandcam.com"
				pic = ''

				result_image = add_watermark(photo)
				result_image.save(img_io, format='PNG', quality=100)
				img_io.seek(0)
				img_content = ContentFile(img_io.read(), str(result_id)+'.png')
				
				photo_data = {
					'watermarked':img_content,
					'cam_model' : cam_model,
					'user' : profile.pk
				}
				
				images_serializer = GetActualPhotosSerializer(data=photo_data)
				if images_serializer.is_valid():
					pic = images_serializer.save()
					photo_pks.append(pic.pk)
					
				else:
					print (images_serializer.errors)

		try:
			camera = SellCamera.objects.get(pk=pk)
			acc_pks = []
			if description:
				for acc in description:
					acc_data = {
						'description' : description
					}
					acc_serializer = CreateAccessoriesDescription(data=acc_data)
					if CreateAccessoriesDescription.is_valid():
						acc = CreateAccessoriesDescription.save()
						acc_pks.append(acc.pk)

			if (camera.seller == profile):
				camera.price = price
				camera.condition = condition
				camera.shutter_count = shutter_count
				camera.status = status
				camera.warranty = warranty
				camera.package = package
				camera.comments = comments
				if len(acc_pks) > 0:
					for pk in acc_pks:
						camera.description.add(pk)

				if len(photo_pks) > 0:
					for pk in photo_pks:
						camera.actual_photos.add(pk)

				camera.save()
				camera.indexing()

				cameras = get_forsale_cam(profile)
				video_cameras = get_forsale_videocam(profile)
				lenses = get_forsale_lens(profile)
				lightings = get_forsale_lighting(profile)
				accessories = get_forsale_accessories(profile)
				drones = get_forsale_drones(profile)
				result = {
					'cameras' : GetCamForSaleSerializer(cameras, many=True).data,
					'video_cameras' : GetVideoCamForSaleSerializer(video_cameras, many=True).data,
					'lenses' : GetLensForSaleSerializer(lenses, many=True).data,
					'lightings' : GetLightingForSaleSerializer(lightings, many=True).data,
					'accessories' : GetAccessoriesForSaleSerializer(accessories, many=True).data,
					'drones' : GetDroneForSaleSerializer(drones, many=True).data,
					'user_profile' : ProfileDetailSerializer(profile).data,
					'error' : None,
					'status' : {'status': 'Item successfully saved.'}
				}

				return Response(result)
			else:
				status = {'error' : 'You are not allowed to edit this item.'}
				return Response(status)

		except SellCamera.DoesNotExist:
			status = {'error' : 'You are trying to edit a for sale item that doesnt exist.'}
			return Response(status)


#	REMOVE UPLOADED PHOTOS FROM EDIT PAGE
#	/api/sell/camera/remove_image/acc/photopk
#
@permission_classes((IsAuthenticated, ))
class RemoveCameraPhoto(APIView):

	def get_camera_sale(self, pk):
		try:
			cam = SellCamera.objects.get(pk=pk)
			return cam
		except SellCamera.DoesNotExist:
			print ("camera not found in db")

	def post(self, request, campk, photopk, format=None):
		print ("requesting server to remove photo from lens for sale")
		cam = self.get_camera_sale(campk)
		photo = get_actual_photo(photopk)

		cam.actual_photos.remove(photo)
		photo.delete()
		cam.indexing()

		status = {'status' : 'Successfully remove photo from camera for sale.'}
		return Response(status)


#	REMOVE FOR SALE ITEM
#	/api/sell/camera/remove/pk
#
@permission_classes((IsAuthenticated, ))
class RemoveSaleCameraView(APIView):

	def get_forsale_cam_pk(self, pk):
		try:
			camera = SellCamera.objects.filter(pk=pk)
			return camera
		except SellCamera.DoesNotExist:
			status = {'error' : 'You are trying to remove an item that is not in the database.'}
			return Response(status)

	def post(self, request, pk, format=None):
		print ('requesting server to remove an item with id ' + pk)
		profile = get_profile(request.user)
		camera = self.get_forsale_cam_pk(pk)
		for cam in camera:
			cam.status = "Deleted"
			cam.save() # -> updates the index

			cam.delete() # delete item on the database

		#s = Search(using=client, index='forsale').query('match', id=pk)
		#s.delete()
		print ('item successfully removed')
		cameras = get_forsale_cam(profile)
		video_cameras = get_forsale_videocam(profile)
		lenses = get_forsale_lens(profile)
		lightings = get_forsale_lighting(profile)
		accessories = get_forsale_accessories(profile)
		drones = get_forsale_drones(profile)
		result = {
			'cameras' : GetCamForSaleSerializer(cameras, many=True).data,
			'video_cameras' : GetVideoCamForSaleSerializer(video_cameras, many=True).data,
			'lenses' : GetLensForSaleSerializer(lenses, many=True).data,
			'lightings' : GetLightingForSaleSerializer(lightings, many=True).data,
			'drones' : GetDroneForSaleSerializer(drones, many=True).data,
			'accessories' : GetAccessoriesForSaleSerializer(accessories, many=True).data,
			'user_profile' : ProfileDetailSerializer(profile).data,
			'error' : None,
			'status' : {'status': 'Item successfully removed.'}
		}

		return Response(result)


# sell video camera 
# /api/sell/video-camera/upload
@permission_classes((AllowAny, ))
class UploadVideoCameraView(generics.CreateAPIView):
	
	def post(self, request, *args, **kwargs):
		
		profile = get_profile(request.user)
		cam_model = get_cam_model(request.data.get('item'))
		actual_photos = request.FILES
	
		photo_pks = [] # photos pk list to be passed to actual_photos

		#
		#	Check uploaded images size
		#
		# size_check = image_size_checker(actual_photos)
		# if size_check == "Invalid":
			
		# 	status = {
		# 		'error' : "Please upload an image with a decent size",
		# 		'status' : {'status': 'Please upload an image with a decent size.'}
		# 	}
		# 	return Response(status)

		for ap in actual_photos.getlist('actual_photos'):
			img_io = BytesIO()
			result_id = _create_result_id()
			photo = Image.open(ap)
			text = "www.2ndhandcam.com"
			pic = ''

			result_image = add_watermark(photo)
			result_image.save(img_io, format='PNG', quality=100)
			img_io.seek(0)
			img_content = ContentFile(img_io.read(), str(result_id)+'.png')
			
			photo_data = {
				'watermarked':img_content,
				'cam_model' : cam_model,
				'user' : profile.pk
			}
			
			images_serializer = GetActualPhotosSerializer(data=photo_data)
			if images_serializer.is_valid():
				pic = images_serializer.save()
				photo_pks.append(pic.pk)
				
			else:
				print (images_serializer.errors)
		sell_data = {
			'seller': profile.pk,
			'item': request.data.get('item', None),
			'price': request.data.get('price', None),
			'condition': request.data.get('condition', None),
			#'description': request.data.get('description', None),
			'comments': request.data.get('comments', None),
			'status': request.data.get('status', None),
			'warranty': request.data.get('warranty', None),
			'actual_photos': photo_pks,
			'package' : request.data.get('camera_package', None)
		}
		sell_serializer = VideoCamForSaleSerializer(data=sell_data)
		if sell_serializer.is_valid():
			sell_serializer.save()
			result = {
				'data': sell_serializer.data,
				'error' : None,
				'status' : {'status': 'Item successfully uploaded.'}
			}
			return Response(result)
		
		status={'error': sell_serializer.errors}
		return Response(status)


# sell camera 
# /api/sell/camera/lenspackage
@permission_classes((IsAuthenticated, ))
class UploadVideoLensPackageView(generics.CreateAPIView):
	
	def post(self, request, *args, **kwargs):
		print ('uploading lens packages')
		cam_pk = request.data.get('cam_id')
		#print (cam_pk)
		
		camera = SellVideoCamera.objects.get(pk=cam_pk)
		
		lens_package = json.loads(request.data.get('lensPackage'))
		
		profile = get_profile(request.user)
		lens_photos = request.FILES
		lens_package_pks = []
		
		#
		#	check if lens_package is not
		#
		if lens_package == "":
			for p in lens_package:
				
				try:
					lens = LensDetails.objects.get(pk=p['id'])
				except LensDetails.DoesNotExist:
					print ('id not found')
				data = {
					'lens_package': p['id']
				}

				package_serializer = CreateCameraLensPackageSerializer(data=data)
				if package_serializer.is_valid():
					pack = package_serializer.save()
					print ("saving lens package")
					lens_package_pks.append(pack.pk)
					
				else:
					print (package_serializer.errors)


			for package in lens_package_pks:
				print ("adding package to camera")
				camera.lens_package.add(package)
				camera.save()
	
		photo_pks = [] # photos pk list to be passed to actual_photos

		#
		#	Check uploaded images size
		#
		# size_check = image_size_checker(lens_photos)
		# if size_check == "Invalid":
			
		# 	status = {
		# 		'error' : "Please upload an image with a decent size",
		# 		'status' : {'status': 'Please upload an image with a decent size.'}
		# 	}
		# 	#print (status)
		# 	return Response(status)

		for ap in lens_photos.getlist('lens_photos'):
			img_io = BytesIO()
			result_id = _create_result_id()
			photo = Image.open(ap)
			text = "www.2ndhandcam.com"
			pic = ''

			result_image = add_watermark(photo)
			result_image.save(img_io, format='PNG', quality=100)
			img_io.seek(0)
			img_content = ContentFile(img_io.read(), str(result_id)+'.png')
			
			photo_data = {
				'watermarked':img_content,
				'cam_model' : "Lens Package" ,
				'user' : profile.pk
			}
			
			images_serializer = GetActualPhotosSerializer(data=photo_data)
			if images_serializer.is_valid():
				pic = images_serializer.save()
				photo_pks.append(pic.pk)
				print("saving to actual photo db")
				
			else:
				print (images_serializer.errors)

		for pk in photo_pks:
			camera.lens_photos.add(pk)
			camera.save()

		result = {
			'error' : None,
			'status' : {'status': 'Item successfully uploaded.'}
		}
		return Response(result)


# sell camera 
# /api/sell/camera/edit/lenspackage
@permission_classes((IsAuthenticated, ))
class EditVideoLensPackageView(generics.CreateAPIView):
	
	def post(self, request, *args, **kwargs):
		print ('uploading lens packages')
		cam_pk = request.data.get('cam_id')
		
		camera = SellVideoCamera.objects.get(pk=cam_pk)
		lens_package = json.loads(request.data.get('lensPackage'))
		edit_lens = request.data.get('edit')
		removed_lens = json.loads(request.data.get('removed_lens'))
		add_lens = json.loads(request.data.get('add_lens'))
		
		profile = get_profile(request.user)
		lens_photos = request.FILES
		lens_package_pks = []
		
		if edit_lens:
			if len(removed_lens) > 0:
				for x in removed_lens:
					print (x)
					try:
						lens = CameraLensPackage.objects.get(pk=x['id'])
						camera.lens_package.remove(lens)
						camera.save()
						lens.delete()
					except CameraLensPackage.DoesNotExist:
						print ('lens id not found')

			if len(add_lens) > 0:
				for p in add_lens:
					try:
						lens = LensDetails.objects.get(pk=p['id'])
					except LensDetails.DoesNotExist:
						print ('lens id not found')
					data = {
						'lens_package': p['id']
					}

					package_serializer = CreateCameraLensPackageSerializer(data=data)
					if package_serializer.is_valid():
						pack = package_serializer.save()
						lens_package_pks.append(pack.pk)
					else:
						print (package_serializer.errors)
				
				for package in lens_package_pks:
					camera.lens_package.add(package)
					camera.save()
				

		else:
			print ("edit lens else")
	
		photo_pks = [] # photos pk list to be passed to actual_photos

		#
		#	Check uploaded images size
		#
		# size_check = image_size_checker(lens_photos)
		# if size_check == "Invalid":
			
		# 	status = {
		# 		'error' : "Please upload an image with a decent size",
		# 		'status' : {'status': 'Please upload an image with a decent size.'}
		# 	}
			
		# 	return Response(status)

		for ap in lens_photos.getlist('lens_photos'):
			img_io = BytesIO()
			result_id = _create_result_id()
			photo = Image.open(ap)
			text = "www.2ndhandcam.com"
			pic = ''

			result_image = add_watermark(photo)
			result_image.save(img_io, format='PNG', quality=100)
			img_io.seek(0)
			img_content = ContentFile(img_io.read(), str(result_id)+'.png')
			
			photo_data = {
				'watermarked':img_content,
				'cam_model' : "Lens Package" ,
				'user' : profile.pk
			}
			
			images_serializer = GetActualPhotosSerializer(data=photo_data)
			if images_serializer.is_valid():
				pic = images_serializer.save()
				photo_pks.append(pic.pk)
				print("saving to actual photo db")
				
			else:
				print (images_serializer.errors)

		for pk in photo_pks:
			camera.lens_photos.add(pk)
			camera.save()

		result = {
				
				'error' : None,
				'status' : {'status': 'Item successfully uploaded.'}
			}
		return Response(result)


# upload accessories description
# /api/sell/video-camera/accessories
#
@permission_classes((AllowAny,))
class UploadVideoAccessories(APIView):

	def post(self, request, *args, **kwargs):
		print ("uploading video accessories")
		cam_pk = request.data.get('cam_id')
		camera = SellVideoCamera.objects.get(pk=cam_pk)
		accessories = json.loads(request.data.get('accessories_package'))

		acc_pk = []

		for acc in accessories:

			data = {
				'description' : acc
			}

			accessories_serializer = CreateAccessoriesDescription(data=data)
			if accessories_serializer.is_valid():
				pack = accessories_serializer.save()
				print ("saving accessories package")
				acc_pk.append(pack.pk)
			else:
				print (accessories_serializer.errors)

		for pk in acc_pk:
			camera.description.add(pk)
			camera.save()

		result = {
				
				'error' : None,
				'status' : {'status': 'Accessories successfully uploaded.'}
			}
		return Response(result)


# edit accessories description
# /api/sell/video-camera/accessories/edit
#
@permission_classes((AllowAny,))
class EditVideoAccessories(APIView):

	def post(self, request, *args, **kwargs):
		print ("editing still accessories")
		cam_pk = request.data.get('cam_id')
		camera = SellVideoCamera.objects.get(pk=cam_pk)
		accessories = json.loads(request.data.get('accessories_package'))
		add_acc = json.loads(request.data.get('add_acc'))
		edit_acc = request.data.get('edit_accessories')
		remove_acc = json.loads(request.data.get('removed_acc'))

		acc_pk = []
		profile = get_profile(request.user)
		acc_photos = request.FILES
		acc_package_pks = []

		if edit_acc:
			print ("inside edit acc")
			if len(remove_acc) > 0:
				for x in remove_acc:
					print (x)
					try:
						acc = CameraAccessoriesDescription.objects.get(pk=x['id'])
						camera.description.remove(acc)
						camera.save()
						acc.delete()
					except CameraAccessoriesDescription.DoesNotExist:
						print ('acc id not found')

			if len(add_acc) > 0:
				for acc in add_acc:
					data = {
						'description' : acc
					}

					accessories_serializer = CreateAccessoriesDescription(data=data)
					if accessories_serializer.is_valid():
						pack = accessories_serializer.save()
						print ("saving acc package")
						acc_pk.append(pack.pk)
					else:
						print (accessories_serializer.errors)

				for pk in acc_pk:
					camera.description.add(pk)
					camera.save()
				

		else:
			print ("edit lens else")

		photo_pks = [] # photos pk list to be passed to actual_photos

		#
		#	Check uploaded images size
		#
		# size_check = image_size_checker(acc_photos)
		# if size_check == "Invalid":
			
		# 	status = {
		# 		'error' : "Please upload an image with a decent size",
		# 		'status' : {'status': 'Please upload an image with a decent size.'}
		# 	}
			
		# 	return Response(status)

		for ap in acc_photos.getlist('acc_photos'):
			img_io = BytesIO()
			result_id = _create_result_id()
			photo = Image.open(ap)
			text = "www.2ndhandcam.com"
			pic = ''

			result_image = add_watermark(photo)
			result_image.save(img_io, format='PNG', quality=100)
			img_io.seek(0)
			img_content = ContentFile(img_io.read(), str(result_id)+'.png')
			
			photo_data = {
				'watermarked':img_content,
				'cam_model' : "Accessories Package" ,
				'user' : profile.pk
			}
			
			images_serializer = GetActualPhotosSerializer(data=photo_data)
			if images_serializer.is_valid():
				pic = images_serializer.save()
				photo_pks.append(pic.pk)
				print("saving to actual photo db")
				
			else:
				print (images_serializer.errors)

		for pk in photo_pks:
			camera.lens_photos.add(pk)
			camera.save()

		result = {
				
				'error' : None,
				'status' : {'status': 'Item successfully uploaded.'}
			}
		return Response(result)



# get camera for sale lens package
# /api/sell/video-camera/accessories/pk
#
@permission_classes((AllowAny,))
class GetVideoAccessoriesView(APIView):

	def get_accessories(self, pk):
		try:
			camera = SellVideoCamera.objects.get(pk=pk)
			return camera
		except SellVideoCamera.DoesNotExist:
			print ("not found")
			status = {'error' : 'You are trying to get an item that is not in the database.'}
			return Response(status)	
	
	def get(self, request, pk, format=None): 
		print ("requesting accessories")
		camera = self.get_accessories(pk)
		serializer = GetAccessoriesDescription(camera.description.all(), many=True)
		
		return Response(serializer.data)


# get camera for sale lens package
# /api/sell/camera/lens_package/pk
#
@permission_classes((AllowAny,))
class GetVideoLensPackageView(APIView):

	def get_lens_package(self, pk):
		try:
			camera = SellVideoCamera.objects.get(pk=pk)
			return camera
		except SellVideoCamera.DoesNotExist:
			print ("not found")
			status = {'error' : 'You are trying to get an item that is not in the database.'}
			return Response(status)	
	
	def get(self, request, pk, format=None): 
		print ("requesting lens package")
		camera = self.get_lens_package(pk)
		serializer = GetCameraLensPackageSerializer(camera.lens_package.all(), many=True)
		return Response(serializer.data)


# edit camera 
# /api/sell/video-camera/edit
@permission_classes((IsAuthenticated, ))
class EditSaleVideoCameraView(APIView):


	def post(self, request, *args, **kwargs):
		pk = request.data.get('pk')
		price = request.data.get('price') 
		condition = request.data.get('condition')
		status = request.data.get('status')
		warranty = request.data.get('warranty')
		package = request.data.get('camera_package')

		profile = get_profile(request.user)
		cam_item = request.data.get('item')
		cam_model = get_cam_model(cam_item)
		actual_photos = request.FILES
	
		photo_pks = [] # photos pk list to be passed to actual_photos

		#
		#	Check uploaded images size
		#
		# size_check = image_size_checker(actual_photos)
		# if size_check == "Invalid":
			
		# 	status = {
		# 		'error' : "Please upload an image with a decent size",
		# 		'status' : {'status': 'Please upload an image with a decent size.'}
		# 	}
		# 	return Response(status)

		for ap in actual_photos.getlist('actual_photos'):
			img_io = BytesIO()
			result_id = _create_result_id()
			photo = Image.open(ap)
			text = "www.2ndhandcam.com"
			pic = ''

			result_image = add_watermark(photo)
			result_image.save(img_io, format='PNG', quality=100)
			img_io.seek(0)
			img_content = ContentFile(img_io.read(), str(result_id)+'.png')
			
			photo_data = {
				'watermarked':img_content,
				'cam_model' : cam_model,
				'user' : profile.pk
			}
			
			images_serializer = GetActualPhotosSerializer(data=photo_data)
			if images_serializer.is_valid():
				pic = images_serializer.save()
				photo_pks.append(pic.pk)
				
			else:
				print (images_serializer.errors)

		try:
			camera = SellVideoCamera.objects.get(pk=pk)
			if (camera.seller == profile):
				camera.price = price
				camera.condition = condition
				camera.status = status
				camera.warranty = warranty
				camera.package = package

				if len(photo_pks) > 0:
					for pk in photo_pks:
						camera.actual_photos.add(pk)

				camera.save()
				camera.indexing()

				cameras = get_forsale_cam(profile)
				video_cameras = get_forsale_videocam(profile)
				lenses = get_forsale_lens(profile)
				lightings = get_forsale_lighting(profile)
				accessories = get_forsale_accessories(profile)
				drones = get_forsale_drones(profile)
				result = {
					'cameras' : GetCamForSaleSerializer(cameras, many=True).data,
					'video_cameras' : GetVideoCamForSaleSerializer(video_cameras, many=True).data,
					'lenses' : GetLensForSaleSerializer(lenses, many=True).data,
					'lightings' : GetLightingForSaleSerializer(lightings, many=True).data,
					'accessories' : GetAccessoriesForSaleSerializer(accessories, many=True).data,
					'drones' : GetDroneForSaleSerializer(drones, many=True).data,
					'user_profile' : ProfileDetailSerializer(profile).data,
					'error' : None,
					'status' : {'status': 'Item successfully saved.'}
				}

				return Response(result)
			else:
				status = {'error' : 'You are not allowed to edit this item.'}
				return Response(status)

		except SellVideoCamera.DoesNotExist:
			status = {'error' : 'You are trying to edit a for sale item that doesnt exist.'}
			return Response(status)


#	REMOVE UPLOADED PHOTOS FROM EDIT PAGE
#	/api/sell/video-camera/remove_image/acc/photopk
#
@permission_classes((IsAuthenticated, ))
class RemoveVideoCameraPhoto(APIView):

	def get_video_camera_sale(self, pk):
		try:
			vid = SellVideoCamera.objects.get(pk=pk)
			return vid
		except SellVideoCamera.DoesNotExist:
			print ("video camera not found in db")

	def post(self, request, vidpk, photopk, format=None):
		print ("requesting server to remove photo from lens for sale")
		video = self.get_video_camera_sale(vidpk)
		photo = get_actual_photo(photopk)

		video.actual_photos.remove(photo)
		photo.delete()
		video.indexing()

		status = {'status' : 'Successfully remove photo from video camera for sale.'}
		return Response(status)


#	REMOVE FOR SALE ITEM
#	/api/sell/video-camera/remove/pk
#
@permission_classes((IsAuthenticated, ))
class RemoveSaleVideoCameraView(APIView):

	def get_forsale_videocam_pk(self, pk):
		try:
			camera = SellVideoCamera.objects.filter(pk=pk)
			return camera
		except SellVideoCamera.DoesNotExist:
			status = {'error' : 'You are trying to remove an item that is not in the database.'}
			return Response(status)

	def post(self, request, pk, format=None):
		print ('requesting server to remove an item with id ' + pk)
		profile = get_profile(request.user)
		camera = self.get_forsale_videocam_pk(pk)
		for cam in camera:
			cam.status = "Deleted"
			cam.save() # -> updates the index

			cam.delete() # delete item on the database
		print ('item successfully removed')
		cameras = get_forsale_cam(profile)
		video_cameras = get_forsale_videocam(profile)
		lenses = get_forsale_lens(profile)
		lightings = get_forsale_lighting(profile)
		accessories = get_forsale_accessories(profile)
		drones = get_forsale_drones(profile)
		result = {
			'cameras' : GetCamForSaleSerializer(cameras, many=True).data,
			'video_cameras' : GetVideoCamForSaleSerializer(video_cameras, many=True).data,
			'lenses' : GetLensForSaleSerializer(lenses, many=True).data,
			'lightings' : GetLightingForSaleSerializer(lightings, many=True).data,
			'accessories' : GetAccessoriesForSaleSerializer(accessories, many=True).data,
			'drones' : GetDroneForSaleSerializer(drones, many=True).data,
			'user_profile' : ProfileDetailSerializer(profile).data,
			'error' : None,
			'status' : {'status': 'Item successfully removed.'}
		}

		return Response(result)


# sell lens 
# /api/sell/lens/upload
@permission_classes((AllowAny, ))
class UploadLensView(generics.CreateAPIView):
	
	def post(self, request, *args, **kwargs):
		print ("uploading lens")
		profile = get_profile(request.user)
		lens_model = get_lens_model(request.data.get('item'))
		actual_photos = request.FILES
	
		photo_pks = [] # photos pk list to be passed to actual_photos

		#
		#	Check uploaded images size
		#
		# size_check = image_size_checker(actual_photos)
		# if size_check == "Invalid":
			
		# 	status = {
		# 		'error' : "Please upload an image with a decent size",
		# 		'status' : {'status': 'Please upload an image with a decent size.'}
		# 	}
		# 	return Response(status)

		for ap in actual_photos.getlist('actual_photos'):
			img_io = BytesIO()
			result_id = _create_result_id()
			photo = Image.open(ap)
			text = "www.2ndhandcam.com"
			pic = ''

			result_image = add_watermark(photo)
			result_image.save(img_io, format='PNG', quality=100)
			img_io.seek(0)
			img_content = ContentFile(img_io.read(), str(result_id)+'.png')
			
			photo_data = {
				'watermarked':img_content,
				'cam_model' : lens_model,
				'user' : profile.pk
			}
			
			images_serializer = GetActualPhotosSerializer(data=photo_data)
			if images_serializer.is_valid():
				pic = images_serializer.save()
				photo_pks.append(pic.pk)
				
			else:
				print (images_serializer.errors)

		print (photo_pks)


		sell_data = {
			'seller': profile.pk,
			'item': request.data.get('item', None),
			'price': request.data.get('price', None),
			'condition': request.data.get('condition', None),
			'comments': request.data.get('comments', None),
			'status': request.data.get('status', None),
			'warranty': request.data.get('warranty', None),
			'actual_photos': photo_pks,
			'with_camera' : request.data.get('with_camera', None)
		}
		sell_serializer = LensForSaleSerializer(data=sell_data)
		if sell_serializer.is_valid():
			sell_serializer.save()
			print ('for sale lens saved')
			result = {
				'data': sell_serializer.data,
				'error' : None,
				'status' : {'status': 'Item successfully uploaded.'}
			}
			return Response(result)
		print (sell_serializer.errors)
		status={'error': sell_serializer.errors}
		return Response(status)


# edit camera 
# /api/sell/video-camera/edit
@permission_classes((IsAuthenticated, ))
class EditSaleLensView(APIView):


	def post(self, request, *args, **kwargs):
		pk = request.data.get('pk')
		price = request.data.get('price') 
		condition = request.data.get('condition')
		comments = request.data.get('comments')
		status = request.data.get('status')
		warranty = request.data.get('warranty')
		with_camera = request.data.get('with_camera')
		
		profile = get_profile(request.user)
		lens_model = get_lens_model(request.data.get('item'))
		actual_photos = request.FILES
	
		photo_pks = [] # photos pk list to be passed to actual_photos

		#
		#	Check uploaded images size
		#
		# size_check = image_size_checker(actual_photos)
		# if size_check == "Invalid":
			
		# 	status = {
		# 		'error' : "Please upload an image with a decent size",
		# 		'status' : {'status': 'Please upload an image with a decent size.'}
		# 	}
		# 	return Response(status)

		for ap in actual_photos.getlist('actual_photos'):
			img_io = BytesIO()
			result_id = _create_result_id()
			photo = Image.open(ap)
			text = "www.2ndhandcam.com"
			pic = ''

			result_image = add_watermark(photo)
			result_image.save(img_io, format='PNG', quality=100)
			img_io.seek(0)
			img_content = ContentFile(img_io.read(), str(result_id)+'.png')
			
			photo_data = {
				'watermarked':img_content,
				'cam_model' : lens_model,
				'user' : profile.pk
			}
			
			images_serializer = GetActualPhotosSerializer(data=photo_data)
			if images_serializer.is_valid():
				pic = images_serializer.save()
				photo_pks.append(pic.pk)
				
			else:
				print (images_serializer.errors)

		try:
			lens = SellLenses.objects.get(pk=pk)
			if (lens.seller == profile):
				lens.price = price
				lens.condition = condition
				lens.comments = comments
				lens.status = status
				lens.warranty = warranty
				lens.with_camera = with_camera

				if len(photo_pks) > 0:
					for pk in photo_pks:
						lens.actual_photos.add(pk)

				lens.save()
				lens.indexing()

				cameras = get_forsale_cam(profile)
				video_cameras = get_forsale_videocam(profile)
				lenses = get_forsale_lens(profile)
				lightings = get_forsale_lighting(profile)
				accessories = get_forsale_accessories(profile)
				drones = get_forsale_drones(profile)
				result = {
					'cameras' : GetCamForSaleSerializer(cameras, many=True).data,
					'video_cameras' : GetVideoCamForSaleSerializer(video_cameras, many=True).data,
					'lenses' : GetLensForSaleSerializer(lenses, many=True).data,
					'lightings' : GetLightingForSaleSerializer(lightings, many=True).data,
					'accessories' : GetAccessoriesForSaleSerializer(accessories, many=True).data,
					'drones' : GetDroneForSaleSerializer(drones, many=True).data,
					'user_profile' : ProfileDetailSerializer(profile).data,
					'error' : None,
					'status' : {'status': 'Item successfully saved.'}
				}

				return Response(result)
			else:
				status = {'error' : 'You are not allowed to edit this item.'}
				return Response(status)

		except SellLenses.DoesNotExist:
			status = {'error' : 'You are trying to edit a for sale item that doesnt exist.'}
			return Response(status)


#	REMOVE UPLOADED PHOTOS FROM EDIT PAGE
#	/api/sell/lens/remove_image/acc/photopk
#
@permission_classes((IsAuthenticated, ))
class RemoveLensPhoto(APIView):

	def get_lens_sale(self, pk):
		try:
			lens = SellLenses.objects.get(pk=pk)
			return lens
		except SellLenses.DoesNotExist:
			print ("lens not found in db")

	def post(self, request, lenspk, photopk, format=None):
		print ("requesting server to remove photo from lens for sale")
		lens = self.get_lens_sale(lenspk)
		photo = get_actual_photo(photopk)

		lens.actual_photos.remove(photo)
		photo.delete()
		lens.indexing()

		status = {'status' : 'Successfully remove photo from lens for sale.'}
		return Response(status)


#	REMOVE FOR SALE ITEM
#	/api/sell/video-camera/remove/pk
#
@permission_classes((IsAuthenticated, ))
class RemoveSaleLensView(APIView):

	def get_forsale_lens_pk(self, pk):
		try:
			lens = SellLenses.objects.filter(pk=pk)
			return lens
		except SellLenses.DoesNotExist:
			status = {'error' : 'You are trying to remove an item that is not in the database.'}
			return Response(status)

	def post(self, request, pk, format=None):
		print ('requesting server to remove an item with id ' + pk)
		profile = get_profile(request.user)
		lens = self.get_forsale_lens_pk(pk)
		for l in lens:
			l.status = "Deleted"
			l.save() # -> updates the index

			l.delete() # delete item on the database
		print ('item successfully removed')
		cameras = get_forsale_cam(profile)
		video_cameras = get_forsale_videocam(profile)
		lenses = get_forsale_lens(profile)
		lightings = get_forsale_lighting(profile)
		accessories = get_forsale_accessories(profile)
		drones = get_forsale_drones(profile)
		result = {
			'cameras' : GetCamForSaleSerializer(cameras, many=True).data,
			'video_cameras' : GetVideoCamForSaleSerializer(video_cameras, many=True).data,
			'lenses' : GetLensForSaleSerializer(lenses, many=True).data,
			'lightings' : GetLightingForSaleSerializer(lightings, many=True).data,
			'accessories' : GetAccessoriesForSaleSerializer(accessories, many=True).data,
			'drones' : GetDroneForSaleSerializer(drones, many=True).data,
			'user_profile' : ProfileDetailSerializer(profile).data,
			'error' : None,
			'status' : {'status': 'Item successfully removed.'}
		}

		return Response(result)


# sell lighting 
# /api/sell/lighting/upload
@permission_classes((AllowAny, ))
class UploadLightingView(generics.CreateAPIView):
	
	def post(self, request, *args, **kwargs):
		print("uploading lighting for sell")
		profile = get_profile(request.user)
		actual_photos = request.FILES
		photo_pks = [] # photos pk list to be passed to actual_photos

		#
		#	Check uploaded images size
		#
		# size_check = image_size_checker(actual_photos)
		# if size_check == "Invalid":
		# 	print ("invalid image size")
		# 	status = {
		# 		'error' : "Please upload an image with a decent size",
		# 		'status' : {'status': 'Please upload an image with a decent size.'}
		# 	}
		# 	return Response(status)

		for ap in actual_photos.getlist('actual_photos'):
			img_io = BytesIO()
			result_id = _create_result_id()
			photo = Image.open(ap)
			text = "www.2ndhandcam.com"
			pic = ''

			result_image = add_watermark(photo)
			result_image.save(img_io, format='PNG', quality=100)
			img_io.seek(0)
			img_content = ContentFile(img_io.read(), str(result_id)+'.png')
			
			photo_data = {
				'watermarked':img_content,
				'cam_model' : request.data.get('model', None),
				'user' : profile.pk
			}
			
			images_serializer = GetActualPhotosSerializer(data=photo_data)
			if images_serializer.is_valid():
				print("saving actual photo")
				pic = images_serializer.save()
				photo_pks.append(pic.pk)
				
			else:
				print (images_serializer.errors)


		sell_data = {
			'seller': profile.pk,
			'price': request.data.get('price', None),
			'condition': request.data.get('condition', None),
			'status': request.data.get('status', None),
			'warranty': request.data.get('warranty', None),
			'actual_photos': photo_pks,
			'brand' : request.data.get('brand', None),
			'model' : request.data.get('model', None),
			'category' : request.data.get('category', None),
			'comment' : request.data.get('comment', None),
		}
		sell_serializer = LightingForSaleSerializer(data=sell_data)
		if sell_serializer.is_valid():
			sell_serializer.save()
			print ('for sale lighting saved')
			result = {
				'data': sell_serializer.data,
				'error' : None,
				'status' : {'status': 'Item successfully uploaded.'}
			}
			return Response(result)
		print (sell_serializer.errors)
		status={'error': sell_serializer.errors}
		return Response(status)


# edit lighting 
# /api/sell/lighting/edit
@permission_classes((IsAuthenticated, ))
class EditSaleLightingView(APIView):


	def post(self, request, *args, **kwargs):
		pk = request.data.get('pk')
		price = request.data.get('price') 
		condition = request.data.get('condition')
		status = request.data.get('status')
		warranty = request.data.get('warranty')
		comment = request.data.get('comment')
		brand = request.data.get('brand')
		model = request.data.get('model')
		category = request.data.get('category')

		profile = get_profile(request.user)
		actual_photos = request.FILES
		photo_pks = [] # photos pk list to be passed to actual_photos

		#
		#	Check uploaded images size
		#
		# size_check = image_size_checker(actual_photos)
		# if size_check == "Invalid":
		# 	print ("invalid image size")
		# 	status = {
		# 		'error' : "Please upload an image with a decent size",
		# 		'status' : {'status': 'Please upload an image with a decent size.'}
		# 	}
		# 	return Response(status)

		for ap in actual_photos.getlist('actual_photos'):
			img_io = BytesIO()
			result_id = _create_result_id()
			photo = Image.open(ap)
			text = "www.2ndhandcam.com"
			pic = ''

			result_image = add_watermark(photo)
			result_image.save(img_io, format='PNG', quality=100)
			img_io.seek(0)
			img_content = ContentFile(img_io.read(), str(result_id)+'.png')
			
			photo_data = {
				'watermarked':img_content,
				'cam_model' : request.data.get('model', None),
				'user' : profile.pk
			}
			
			images_serializer = GetActualPhotosSerializer(data=photo_data)
			if images_serializer.is_valid():
				print("saving actual photo")
				pic = images_serializer.save()
				photo_pks.append(pic.pk)
				
			else:
				print (images_serializer.errors)

		try:
			lighting = SellLighting.objects.get(pk=pk)
			if (lighting.seller == profile):
				lighting.price = price
				lighting.condition = condition
				lighting.status = status
				lighting.warranty = warranty
				lighting.brand = brand
				lighting.model = model
				lighting.category = category
				lighting.comment = comment

				if len(photo_pks) > 0:
					for pk in photo_pks:
						lighting.actual_photos.add(pk)

				lighting.save()
				lighting.indexing()

				cameras = get_forsale_cam(profile)
				video_cameras = get_forsale_videocam(profile)
				lenses = get_forsale_lens(profile)
				lightings = get_forsale_lighting(profile)
				accessories = get_forsale_accessories(profile)
				drones = get_forsale_drones(profile)
				result = {
					'cameras' : GetCamForSaleSerializer(cameras, many=True).data,
					'video_cameras' : GetVideoCamForSaleSerializer(video_cameras, many=True).data,
					'lenses' : GetLensForSaleSerializer(lenses, many=True).data,
					'lightings' : GetLightingForSaleSerializer(lightings, many=True).data,
					'accessories' : GetAccessoriesForSaleSerializer(accessories, many=True).data,
					'drones' : GetDroneForSaleSerializer(drones, many=True).data,
					'user_profile' : ProfileDetailSerializer(profile).data,
					'error' : None,
					'status' : {'status': 'Item successfully saved.'}
				}

				return Response(result)
			else:
				status = {'error' : 'You are not allowed to edit this item.'}
				return Response(status)

		except SellLighting.DoesNotExist:
			status = {'error' : 'You are trying to edit a for sale item that doesnt exist.'}
			return Response(status)


#	REMOVE UPLOADED PHOTOS FROM EDIT PAGE
#	/api/sell/lighting/remove_image/acc/photopk
#
@permission_classes((IsAuthenticated, ))
class RemoveLightingPhoto(APIView):

	def get_lighting_sale(self, pk):
		try:
			drone = SellLighting.objects.get(pk=pk)
			return drone
		except SellLighting.DoesNotExist:
			print ("lighting not found in db")

	def post(self, request, lightpk, photopk, format=None):
		print ("requesting server to remove photo from lighting for sale")
		light = self.get_lighting_sale(lightpk)
		photo = get_actual_photo(photopk)

		light.actual_photos.remove(photo)
		photo.delete()
		light.indexing()

		status = {'status' : 'Successfully remove photo from lighting for sale.'}
		return Response(status)


#	REMOVE FOR SALE ITEM
#	/api/sell/video-camera/remove/pk
#
@permission_classes((IsAuthenticated, ))
class RemoveSaleLightingView(APIView):

	def get_forsale_lighting_pk(self, pk):
		try:
			light = SellLighting.objects.filter(pk=pk)
			return light
		except SellLighting.DoesNotExist:
			status = {'error' : 'You are trying to remove an item that is not in the database.'}
			return Response(status)

	def post(self, request, pk, format=None):
		print ('requesting server to remove an item with id ' + pk)
		profile = get_profile(request.user)
		lighting = self.get_forsale_lighting_pk(pk)
		for light in lighting:
			light.status = "Deleted"
			light.save()
			light.delete()
		print ('item successfully removed')
		cameras = get_forsale_cam(profile)
		video_cameras = get_forsale_videocam(profile)
		lenses = get_forsale_lens(profile)
		lightings = get_forsale_lighting(profile)
		accessories = get_forsale_accessories(profile)
		drones = get_forsale_drones(profile)
		result = {
			'cameras' : GetCamForSaleSerializer(cameras, many=True).data,
			'video_cameras' : GetVideoCamForSaleSerializer(video_cameras, many=True).data,
			'lenses' : GetLensForSaleSerializer(lenses, many=True).data,
			'lightings' : GetLightingForSaleSerializer(lightings, many=True).data,
			'accessories' : GetAccessoriesForSaleSerializer(accessories, many=True).data,
			'drones' : GetDroneForSaleSerializer(drones, many=True).data,
			'user_profile' : ProfileDetailSerializer(profile).data,
			'error' : None,
			'status' : {'status': 'Item successfully removed.'}
		}

		return Response(result)


# sell accessories 
# /api/sell/accessories/upload
@permission_classes((AllowAny, ))
class UploadAccessoriesView(generics.CreateAPIView):
	
	def post(self, request, *args, **kwargs):
		print("uploading accessories")
		#print (request.data.get('warranty'))
		profile = get_profile(request.user)
		actual_photos = request.FILES
	
		photo_pks = [] # photos pk list to be passed to actual_photos

		#
		#	Check uploaded images size
		#
		# size_check = image_size_checker(actual_photos)
		# if size_check == "Invalid":
		# 	print ("Please upload an image with a decent size")
		# 	status = {
		# 		'error' : "Please upload an image with a decent size",
		# 		'status' : {'status': 'Please upload an image with a decent size.'}
		# 	}
		# 	return Response(status)

		for ap in actual_photos.getlist('actual_photos'):
			img_io = BytesIO()
			result_id = _create_result_id()
			photo = Image.open(ap)
			text = "www.2ndhandcam.com"
			pic = ''

			result_image = add_watermark(photo)
			result_image.save(img_io, format='PNG', quality=100)
			img_io.seek(0)
			img_content = ContentFile(img_io.read(), str(result_id)+'.png')
			
			photo_data = {
				'watermarked':img_content,
				'cam_model' : request.data.get('model', None),
				'user' : profile.pk
			}
			
			images_serializer = GetActualPhotosSerializer(data=photo_data)
			if images_serializer.is_valid():
				pic = images_serializer.save()
				photo_pks.append(pic.pk)
				
			else:
				print (images_serializer.errors)


		sell_data = {
			'seller': profile.pk,
			'price': request.data.get('price', None),
			'condition': request.data.get('condition', None),
			'status': request.data.get('status', None),
			'warranty': request.data.get('warranty', None),
			'actual_photos': photo_pks,
			'brand' : request.data.get('brand', None),
			'model' : request.data.get('model', None),
			'category' : request.data.get('category', None),
			'comment' : request.data.get('comment', None),
		}
		sell_serializer = AccessoriesForSaleSerializer(data=sell_data)
		if sell_serializer.is_valid():
			sell_serializer.save()
			print ('for sale accessories saved')
			result = {
				'data': sell_serializer.data,
				'error' : None,
				'status' : {'status': 'Item successfully uploaded.'}
			}
			return Response(result)
		print (sell_serializer.errors)
		status={'error': sell_serializer.errors}
		return Response(status)


# edit accessories 
# /api/sell/accessories/edit
@permission_classes((IsAuthenticated, ))
class EditSaleAccessoriesView(APIView):


	def post(self, request, *args, **kwargs):
		pk = request.data.get('pk')
		price = request.data.get('price') 
		condition = request.data.get('condition')
		status = request.data.get('status')
		warranty = request.data.get('warranty')
		comment = request.data.get('comment')
		brand = request.data.get('brand')
		model = request.data.get('model')
		category = request.data.get('category')

		profile = get_profile(request.user)
		actual_photos = request.FILES
	
		photo_pks = [] # photos pk list to be passed to actual_photos

		#
		#	Check uploaded images size
		#
		# size_check = image_size_checker(actual_photos)
		# if size_check == "Invalid":
			
		# 	status = {
		# 		'error' : "Please upload an image with a decent size",
		# 		'status' : {'status': 'Please upload an image with a decent size.'}
		# 	}
		# 	return Response(status)

		for ap in actual_photos.getlist('actual_photos'):
			img_io = BytesIO()
			result_id = _create_result_id()
			photo = Image.open(ap)
			text = "www.2ndhandcam.com"
			pic = ''

			result_image = add_watermark(photo)
			result_image.save(img_io, format='PNG', quality=100)
			img_io.seek(0)
			img_content = ContentFile(img_io.read(), str(result_id)+'.png')
			
			photo_data = {
				'watermarked':img_content,
				'cam_model' : request.data.get('model', None),
				'user' : profile.pk
			}
			
			images_serializer = GetActualPhotosSerializer(data=photo_data)
			if images_serializer.is_valid():
				pic = images_serializer.save()
				photo_pks.append(pic.pk)
				
			else:
				print (images_serializer.errors)

		try:
			accessories = SellAccessories.objects.get(pk=pk)
			if (accessories.seller == profile):
				accessories.price = price
				accessories.condition = condition
				accessories.status = status
				accessories.warranty = warranty
				accessories.brand = brand
				accessories.model = model
				accessories.category = category
				accessories.comment = comment

				if len(photo_pks) > 0:
					for pk in photo_pks:
						accessories.actual_photos.add(pk)

				accessories.save()
				accessories.indexing()

				cameras = get_forsale_cam(profile)
				video_cameras = get_forsale_videocam(profile)
				lenses = get_forsale_lens(profile)
				lightings = get_forsale_lighting(profile)
				accessories = get_forsale_accessories(profile)
				drones = get_forsale_drones(profile)
				result = {
					'cameras' : GetCamForSaleSerializer(cameras, many=True).data,
					'video_cameras' : GetVideoCamForSaleSerializer(video_cameras, many=True).data,
					'lenses' : GetLensForSaleSerializer(lenses, many=True).data,
					'lightings' : GetLightingForSaleSerializer(lightings, many=True).data,
					'accessories' : GetAccessoriesForSaleSerializer(accessories, many=True).data,
					'drones' : GetDroneForSaleSerializer(drones, many=True).data,
					'user_profile' : ProfileDetailSerializer(profile).data,
					'error' : None,
					'status' : {'status': 'Item successfully saved.'}
				}

				return Response(result)
			else:
				status = {'error' : 'You are not allowed to edit this item.'}
				return Response(status)

		except SellAccessories.DoesNotExist:
			status = {'error' : 'You are trying to edit a for sale item that doesnt exist.'}
			return Response(status)


#	REMOVE UPLOADED PHOTOS FROM EDIT PAGE
#	/api/sell/accessories/remove_image/acc/photopk
#
@permission_classes((IsAuthenticated, ))
class RemoveAccessoriesPhoto(APIView):

	def get_acc_sale(self, accpk):
		try:
			drone = SellAccessories.objects.get(pk=accpk)
			return drone
		except SellAccessories.DoesNotExist:
			print ("accessories not found in db")

	def post(self, request, accpk, photopk, format=None):
		print ("requesting server to remove photo from accessories for sale")
		acc = self.get_acc_sale(accpk)
		photo = get_actual_photo(photopk)

		acc.actual_photos.remove(photo)
		photo.delete()
		acc.indexing()

		status = {'status' : 'Successfully remove photo from accessories for sale.'}
		return Response(status)


#	REMOVE FOR SALE ITEM
#	/api/sell/accessories/remove/pk
#
@permission_classes((IsAuthenticated, ))
class RemoveSaleAccessoriesView(APIView):

	def get_forsale_accessories_pk(self, pk):
		try:
			accessories = SellAccessories.objects.filter(pk=pk)
			return accessories
		except SellAccessories.DoesNotExist:
			status = {'error' : 'You are trying to remove an item that is not in the database.'}
			return Response(status)

	def post(self, request, pk, format=None):
		print ('requesting server to remove an item with id ' + pk)
		profile = get_profile(request.user)
		accessories = self.get_forsale_accessories_pk(pk)
		for acc in accessories:
			acc.status = "Deleted"
			acc.save()
			acc.delete()
		print ('item successfully removed')
		cameras = get_forsale_cam(profile)
		video_cameras = get_forsale_videocam(profile)
		lenses = get_forsale_lens(profile)
		lightings = get_forsale_lighting(profile)
		accessories = get_forsale_accessories(profile)
		drones = get_forsale_drones(profile)
		result = {
			'cameras' : GetCamForSaleSerializer(cameras, many=True).data,
			'video_cameras' : GetVideoCamForSaleSerializer(video_cameras, many=True).data,
			'lenses' : GetLensForSaleSerializer(lenses, many=True).data,
			'lightings' : GetLightingForSaleSerializer(lightings, many=True).data,
			'accessories' : GetAccessoriesForSaleSerializer(accessories, many=True).data,
			'drones' : GetDroneForSaleSerializer(drones, many=True).data,
			'user_profile' : ProfileDetailSerializer(profile).data,
			'error' : None,
			'status' : {'status': 'Item successfully removed.'}
		}

		return Response(result)



# sell drone 
# /api/sell/drone/upload
@permission_classes((AllowAny, ))
class UploadDroneView(generics.CreateAPIView):
	
	def post(self, request, *args, **kwargs):
		print("uploading drone")
		print(request.data.get('control_range', None))
		
		profile = get_profile(request.user)
		actual_photos = request.FILES
	
		photo_pks = [] # photos pk list to be passed to actual_photos

		#
		#	Check uploaded images size
		#
		# size_check = image_size_checker(actual_photos)
		# if size_check == "Invalid":
			
		# 	status = {
		# 		'error' : "Please upload an image with a decent size",
		# 		'status' : {'status': 'Please upload an image with a decent size.'}
		# 	}
		# 	return Response(status)

		for ap in actual_photos.getlist('actual_photos'):
			img_io = BytesIO()
			result_id = _create_result_id()
			photo = Image.open(ap)
			text = "www.2ndhandcam.com"
			pic = ''

			result_image = add_watermark(photo)
			result_image.save(img_io, format='PNG', quality=100)
			img_io.seek(0)
			img_content = ContentFile(img_io.read(), str(result_id)+'.png')
			
			photo_data = {
				'watermarked':img_content,
				'cam_model' : request.data.get('model', None),
				'user' : profile.pk
			}
			
			images_serializer = GetActualPhotosSerializer(data=photo_data)
			if images_serializer.is_valid():
				pic = images_serializer.save()
				photo_pks.append(pic.pk)
				
			else:
				print (images_serializer.errors)


		sell_data = {
			'seller': profile.pk,
			'price': request.data.get('price', None),
			'condition': request.data.get('condition', None),
			'status': request.data.get('status', None),
			'warranty': request.data.get('warranty', None),
			'actual_photos': photo_pks,
			'brand' : request.data.get('brand', None),
			'model' : request.data.get('model', None),
			'drone_type' : request.data.get('drone_type', None),
			'camera' : request.data.get('camera', None),
			'video_resolution' : request.data.get('video_resolution', None),
			'megapixels' : request.data.get('megapixels', None),
			'operating_time' : request.data.get('operating_time', None),
			'control_range' : request.data.get('control_range', None),
			'comment' : request.data.get('comment', None),
		}
		sell_serializer = DroneForSaleSerializer(data=sell_data)
		if sell_serializer.is_valid():
			sell_serializer.save()
			print ('for sale drone saved')
			result = {
				'data': sell_serializer.data,
				'error' : None,
				'status' : {'status': 'Item successfully uploaded.'}
			}
			return Response(result)
		print (sell_serializer.errors)
		status={'error': sell_serializer.errors}
		return Response(status)


# edit drone 
# /api/sell/drone/edit
@permission_classes((IsAuthenticated, ))
class EditSaleDroneView(APIView):


	def post(self, request, *args, **kwargs):

		print ("updating drone")
		print (request.data.get('pk'))
		pk = request.data.get('pk')
		price = request.data.get('price', None) 
		condition = request.data.get('condition', None)
		status = request.data.get('status', None)
		warranty = request.data.get('warranty', None)
		comment = request.data.get('comment', None)
		brand = request.data.get('brand', None)
		model = request.data.get('model', None)
		drone_type = request.data.get('drone_type', None)
		camera = request.data.get('camera', None)
		video_resolution = request.data.get('video_resolution', None)
		megapixels = request.data.get('megapixels', None)
		operating_time = request.data.get('operating_time', None)
		control_range = request.data.get('control_range', None)

		profile = get_profile(request.user)

		actual_photos = request.FILES
		photo_pks = [] # photos pk list to be passed to actual_photos

		if actual_photos:

			#
			#	Check uploaded images size
			#
			# size_check = image_size_checker(actual_photos)
			# if size_check == "Invalid":
				
			# 	status = {
			# 		'error' : "Please upload an image with a decent size",
			# 		'status' : {'status': 'Please upload an image with a decent size.'}
			# 	}
			# 	return Response(status)

			for ap in actual_photos.getlist('actual_photos'):
				img_io = BytesIO()
				result_id = _create_result_id()
				photo = Image.open(ap)
				text = "www.2ndhandcam.com"
				pic = ''

				result_image = add_watermark(photo)
				result_image.save(img_io, format='PNG', quality=100)
				img_io.seek(0)
				img_content = ContentFile(img_io.read(), str(result_id)+'.png')
				
				photo_data = {
					'watermarked':img_content,
					'cam_model' : request.data.get('model', None),
					'user' : profile.pk
				}
				
				images_serializer = GetActualPhotosSerializer(data=photo_data)
				if images_serializer.is_valid():
					pic = images_serializer.save()
					photo_pks.append(pic.pk)
					
				else:
					print (images_serializer.errors)

		try:
			drone = SellDrone.objects.get(pk=pk)
			
			print ("drone found, ready to update")
			if (drone.seller == profile):
				drone.price = price
				drone.condition = condition
				drone.status = status
				drone.warranty = warranty
				drone.brand = brand
				drone.model = model
				drone.comment = comment
				drone.drone_type = drone_type
				drone.camera = camera
				drone.video_resolution = video_resolution

				
				try:
					int(megapixels)
					drone.megapixels = megapixels
				except:
					drone.megapixels = None
				
				
				drone.operating_time = operating_time
				drone.control_range = control_range

				if len(photo_pks) > 0:
					for pk in photo_pks:
						drone.actual_photos.add(pk)

				drone.save()
				drone.indexing()

				cameras = get_forsale_cam(profile)
				video_cameras = get_forsale_videocam(profile)
				lenses = get_forsale_lens(profile)
				lightings = get_forsale_lighting(profile)
				accessories = get_forsale_accessories(profile)
				drones = get_forsale_drones(profile)
				result = {
					'cameras' : GetCamForSaleSerializer(cameras, many=True).data,
					'video_cameras' : GetVideoCamForSaleSerializer(video_cameras, many=True).data,
					'lenses' : GetLensForSaleSerializer(lenses, many=True).data,
					'lightings' : GetLightingForSaleSerializer(lightings, many=True).data,
					'accessories' : GetAccessoriesForSaleSerializer(accessories, many=True).data,
					'drones' : GetDroneForSaleSerializer(drones, many=True).data,
					'user_profile' : ProfileDetailSerializer(profile).data,
					'error' : None,
					'status' : {'status': 'Item successfully saved.'}
				}

				return Response(result)
			else:
				status = {'error' : 'You are not allowed to edit this item.'}
				return Response(status)

		except SellDrone.DoesNotExist:
			print ("drone not found")
			status = {'error' : 'You are trying to edit a for sale item that doesnt exist.'}
			return Response(status)


#	REMOVE UPLOADED PHOTOS FROM EDIT PAGE
#	/api/sell/drone/remove_image/pk
#
@permission_classes((IsAuthenticated, ))
class RemoveDronePhoto(APIView):

	def get_drone_sale(self, dronepk):
		try:
			drone = SellDrone.objects.get(pk=dronepk)
			return drone
		except SellDrone.DoesNotExist:
			print ("drone not found in db")

	def post(self, request, dronepk, photopk, format=None):
		print ("requesting server to remove photo from drone for sale")
		drone = self.get_drone_sale(dronepk)
		photo = get_actual_photo(photopk)

		drone.actual_photos.remove(photo)
		photo.delete()
		drone.indexing()

		status = {'status' : 'Successfully remove photo from drone for sale.'}
		return Response(status)




#	REMOVE FOR SALE ITEM
#	/api/sell/drone/remove/pk
#
@permission_classes((IsAuthenticated, ))
class RemoveSaleDroneView(APIView):

	def get_forsale_drone_pk(self, pk):
		try:
			drone = SellDrone.objects.filter(pk=pk)
			return drone
		except SellDrone.DoesNotExist:
			status = {'error' : 'You are trying to remove an item that is not in the database.'}
			return Response(status)

	def post(self, request, pk, format=None):
		print ('requesting server to remove an item with id ' + pk)
		profile = get_profile(request.user)
		drones = self.get_forsale_drone_pk(pk)
		for drone in drones:
			drone.status = "Deleted"
			drone.save()
			drone.delete()
		print ('item successfully removed')
		cameras = get_forsale_cam(profile)
		video_cameras = get_forsale_videocam(profile)
		lenses = get_forsale_lens(profile)
		lightings = get_forsale_lighting(profile)
		accessories = get_forsale_accessories(profile)
		idrones = get_forsale_drones(profile)
		result = {
			'cameras' : GetCamForSaleSerializer(cameras, many=True).data,
			'video_cameras' : GetVideoCamForSaleSerializer(video_cameras, many=True).data,
			'lenses' : GetLensForSaleSerializer(lenses, many=True).data,
			'lightings' : GetLightingForSaleSerializer(lightings, many=True).data,
			'accessories' : GetAccessoriesForSaleSerializer(accessories, many=True).data,
			'drones': GetDroneForSaleSerializer(idrones, many=True).data,
			'user_profile' : ProfileDetailSerializer(profile).data,
			'error' : None,
			'status' : {'status': 'Item successfully removed.'}
		}

		return Response(result)



#
# INDEX LOADER
#

def get_focal_length(self, low=None, high=None):
	if ((low) and (high)):
		return low + ' - ' + high
	elif ((low) and (high==None)):
		return low
	elif ((low==None) and (high)):
		return high
	else:
		return None

def get_iso(self, low=None, high=None):
	if ((low) and (high)):
		return low + ' - ' + high
	elif ((low) and (high==None)):
		return low
	elif ((low==None) and (high)):
		return high
	else:
		return None		

def get_lcd_monitor(self, size=None, resolution=None):
	if ((size) and (resolution)):
		
		return size + '" - ' + resolution

	elif ((size) and (resolution==None)):
		return size + '"'
	elif ((size==None) and (resolution)):
		return resolution
	else:
		return None

def load_forsaleindex(request):

	#seller_profile = get_profile(request.user)
	indexid = 10000000

	cameras = CameraDetails.objects.filter(cam_type__kind__kind_of_camera="Still Camera")

	for camera in cameras:

		label = str(camera.cam_type.brand.company) + " " + str(camera.cam_type.camera_type) + " " + str(camera.model) + " "+ str(camera.cam_type.kind.kind_of_camera)
		obj = ForSaleIndex(
			  meta={'id': indexid},
			  description="Index",
			  seller_pk=1,
			  seller="jglydes",
			  seller_location="Cavite City, Philippines",
			  seller_phonenumber="",
			  label=label,
			  price=0,
			  condition="",
			  lens_description="",
			  shutter_count=0,
			  status="",
			  package="",
			  date=datetime.datetime.now(),
			  warranty="",
			  camera_package="",
			  actual_photos="",
			  brand=camera.cam_type.brand.company,
			  kind=camera.cam_type.kind.kind_of_camera,
			  category=camera.cam_type.camera_type,
			  model=camera.model,
			  image="",
			  megapixels=camera.megapixels,
			  sensor_size=camera.sensor_size,
			  sensor_type=camera.sensor_type,
			  image_sensor_format=camera.image_sensor_format,
			  lcd=get_lcd_monitor(camera.lcd_size, camera.lcd_resolution),
			  lcd_size=camera.lcd_size,
			  lcd_resolution=camera.lcd_resolution,
			  weight=camera.weight,
			  iso=get_iso(camera.iso_low, camera.iso_high),
			  iso_low=camera.iso_low,
			  iso_high=camera.iso_high,
			  video=camera.video,
			  frames_per_sec=camera.frames_per_sec,
			  af_points=camera.af_points,
			  mirrorless=camera.mirrorless,
			  optical_zoom=camera.optical_zoom,
			  kind_camera=camera.cam_type.kind.kind_of_camera,
			  digital_zoom=camera.digital_zoom,
			  aperture=camera.aperture,
			  max_aperture=camera.max_aperture,
			  wifi=camera.wifi,
			  gps=camera.gps,
			  focal_length=get_focal_length(camera.focal_length_from, camera.focal_length_to),
			  focal_length_from=camera.focal_length_from,
			  focal_length_to=camera.focal_length_to,
			  waterproof=camera.waterproof,
			  film_size=camera.film_size,
			  image_size=camera.image_size,
			  photo_capacity=camera.photo_capacity,
			  effective_pixels=camera.effective_pixels,
			  effective_resolution=camera.effective_resolution,
			  dynamic_range=camera.dynamic_range,
			  frame_rate=camera.frame_rate,
			  view_finder=camera.view_finder,
			  lens_mount=camera.lens_mount,
			  cam_features=camera.cam_features,
			  cam_specifications=camera.cam_specifications
					  
			)
			
		obj.save()

		indexid += 1
		print ("finished indexing camera model: " + camera.model)

	return Response("Indexing finished.")


def load_forsalevideoindex(request):

	seller_profile = get_profile(request.user)
	indexid = 20000000

	cameras = CameraDetails.objects.filter(cam_type__kind__kind_of_camera="Video Camera")

	for camera in cameras:

		label = str(camera.cam_type.brand.company) + " " + str(camera.cam_type.camera_type) + " " + str(camera.model) + " "+ str(camera.cam_type.kind.kind_of_camera)
		obj = ForSaleVideoIndex(
			  meta={'id': indexid},
			  description="Index",
			  seller_pk=1,
			  seller="jglydes",
			  seller_location="Cavite City, Philippines",
			  seller_phonenumber="",
			  label=label,
			  price=0,
			  condition="",
			  status="",
			  package="",
			  date=datetime.datetime.now(),
			  warranty="",
			  camera_package="",
			  actual_photos="",
			  brand=camera.cam_type.brand.company,
			  kind=camera.cam_type.kind.kind_of_camera,
			  category=camera.cam_type.camera_type,
			  model=camera.model,
			  image="",
			  megapixels=camera.megapixels,
			  sensor_size=camera.sensor_size,
			  sensor_type=camera.sensor_type,
			  image_sensor_format=camera.image_sensor_format,
			  lcd=get_lcd_monitor(camera.lcd_size, camera.lcd_resolution),
			  lcd_size=camera.lcd_size,
			  lcd_resolution=camera.lcd_resolution,
			  weight=camera.weight,
			  iso=get_iso(camera.iso_low, camera.iso_high),
			  iso_low=camera.iso_low,
			  iso_high=camera.iso_high,
			  video=camera.video,
			  frames_per_sec=camera.frames_per_sec,
			  af_points=camera.af_points,
			  mirrorless=camera.mirrorless,
			  optical_zoom=camera.optical_zoom,
			  kind_camera=camera.cam_type.kind.kind_of_camera,
			  digital_zoom=camera.digital_zoom,
			  aperture=camera.aperture,
			  max_aperture=camera.max_aperture,
			  wifi=camera.wifi,
			  gps=camera.gps,
			  focal_length=get_focal_length(camera.focal_length_from, camera.focal_length_to),
			  focal_length_from=camera.focal_length_from,
			  focal_length_to=camera.focal_length_to,
			  waterproof=camera.waterproof,
			  film_size=camera.film_size,
			  image_size=camera.image_size,
			  photo_capacity=camera.photo_capacity,
			  effective_pixels=camera.effective_pixels,
			  effective_resolution=camera.effective_resolution,
			  dynamic_range=camera.dynamic_range,
			  frame_rate=camera.frame_rate,
			  view_finder=camera.view_finder,
			  lens_mount=camera.lens_mount,
			  cam_features=camera.cam_features,
			  cam_specifications=camera.cam_specifications
					  
			)
			
		obj.save()

		indexid += 1
		print ("finished indexing camera model: " + camera.model)

	return Response("Indexing finished.")


def load_forsalelensindex(request):

	seller_profile = get_profile(request.user)
	indexid = 30000000

	lenses = LensDetails.objects.all()

	for lens in lenses:
		image = ""
		if (lens.image):
			image = lens.image.url


		label = str(lens.type_of_lens.brand.company) + " " + str(lens.type_of_lens.lens_type) + " " + str(lens.model)
		obj = ForSaleLensIndex(
			  meta={'id': indexid},
			  description="Index",
			  seller_pk=1,
			  seller="jglydes",
			  seller_location="Cavite City, Philippines",
			  seller_phonenumber="",
			  label=label,
			  price=0,
			  condition="",
			  status="",
			  with_camera="",
			  date=datetime.datetime.now(),
			  warranty="",
			  actual_photos="",
			  brand=lens.type_of_lens.brand.company,
			  category=lens.type_of_lens.lens_type,
			  model=lens.model,
			  image=image,
			  focal_length_from=lens.focal_length_from,
			  focal_length_to=lens.focal_length_to,
			  aperture=lens.aperture,
			  max_aperture=lens.max_aperture,
			  weight=lens.weight,
			  format_range=lens.format_range,
			  lens_type=lens.lens_type,
			  lens_type_and=lens.lens_type_and,
			  lens_format=lens.lens_format,
			  focus_type=lens.focus_type,
			  fixed_focal=lens.fixed_focal,
			  mounts=lens.mounts,
			  magnification=lens.magnification,
			  features=lens.features,
			  specifications=lens.specifications
					  
			)
			
		obj.save()

		indexid += 1
		print ("finished indexing lens model: " + lens.model)

	return Response("Indexing finished.")