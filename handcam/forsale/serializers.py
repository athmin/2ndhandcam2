from __future__ import (absolute_import, division, print_function, unicode_literals)
from rest_framework_elasticsearch.es_serializer import ElasticModelSerializer
from rest_framework import serializers

from .search import ForSaleIndex
from camera.models import Feature, Specification, Kind, Brand, Type, CameraDetails
from forsale.models import ForSaleActualPhoto, CameraLensPackage, CameraAccessoriesDescription, SellCamera, SellVideoCamera, SellLenses, SellLighting, SellAccessories, SellDrone
from lens.models import LensFeature, LensSpecification, LensType, LensDetails
from profiles.models import Profile, Review

from .processors import add_text_overlay
from django.core.files.images import ImageFile

from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth.hashers import make_password
from django.template.defaultfilters import slugify

from camera.serializers import CameraDetailSerializer
from lens.serializers import LensDetailSerializer


def create_productID(pid):
	if pid < 10:
		return "000000"+str(pid)
	if pid < 100:
		return "00000"+str(pid)
	if pid < 1000:
		return "0000"+str(pid)
	if pid < 10000:
		return "000"+str(pid)
	if pid < 100000:
		return "00"+str(pid)
	if pid < 1000000:
		return "0"+str(pid)

class ElasticForsaleSerializer(ElasticModelSerializer):

	brand = serializers.SerializerMethodField()
	megapixels = serializers.SerializerMethodField()
	sensor_size = serializers.SerializerMethodField()
	sensor_type = serializers.SerializerMethodField()
	image_sensor_format = serializers.SerializerMethodField()
	lcd_size = serializers.SerializerMethodField()

	class Meta:
		
		es_model = ForSaleIndex
		fields = ('id', 
				'seller', 
				'label', 
				'price', 
				'condition', 
				'shutter_count',
				'status',
				'date',
				'warranty',
				'package',
				'actual_photos',
				'brand',
				'megapixels',
				'sensor_size',
				'sensor_type',
				'image_sensor_format',
				'lcd_size',
				'image',
			)

class SellerDetailSerializer(serializers.ModelSerializer):
	user = serializers.SerializerMethodField()

	class Meta:
		model = Profile
		fields = [
			'user',
			'city',
			'country',
			'phone_number',
			'image',
		]

	def get_user(self, obj):
		
		return obj.user.get_full_name()


class GetActualPhotosSerializer(serializers.ModelSerializer):
	id = serializers.IntegerField(required=False)
	class Meta:
		model = ForSaleActualPhoto
		fields = [
			'id',
			'user',
			'actual_photo',
			'watermarked',
			'cam_model'
		]


class CreateActualPhotosSerializer(serializers.ModelSerializer):

	class Meta:
		model = ForSaleActualPhoto
		fields = [
			'user',
			'actual_photo',
			'watermarked',
			'cam_model'
		]

	# def create(self, validated_data):
	# 	image = validated_data['actual_photo']
	# 	text = "2ndhandcam.com"
	# 	result_image = add_text_overlay(image, text)
	# 	image_file = ImageFile(result_image)
	# 	actual_image = ForSaleActualPhoto.objects.create(
	# 		actual_photo = image_file
	# 		)

	# 	return actual_image


class GetCameraLensPackageSerializer(serializers.ModelSerializer):
	lens_package = LensDetailSerializer()

	class Meta:
		model = CameraLensPackage
		fields = [
			'id',
			'lens_package',
		]


class CreateCameraLensPackageSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = CameraLensPackage
		fields = [
			'lens_package',
		]


class GetLensPackageSerializer(serializers.ModelSerializer):
	lens_package = LensDetailSerializer(many=True)

	class Meta:
		model = SellCamera
		fields = [
			'lens_package',
		]

class CreateAccessoriesDescription(serializers.ModelSerializer):

	class Meta:
		model = CameraAccessoriesDescription
		fields = [
			'description',
		]


class GetAccessoriesDescription(serializers.ModelSerializer):

	class Meta:
		model = CameraAccessoriesDescription
		fields = [
			'id',
			'description'
		]


class GetStillAccessoriesDescription(serializers.ModelSerializer):
	description = GetAccessoriesDescription(many=True)

	class Meta:
		model = SellCamera
		fields = [
			'description'
		]

class GetVideoAccessoriesDescription(serializers.ModelSerializer):
	description = GetAccessoriesDescription(many=True)

	class Meta:
		model = SellVideoCamera
		fields = [
			'description'
		]
		
		
class GetCamForSaleSerializer(serializers.ModelSerializer):
	actual_photos = GetActualPhotosSerializer(many=True)
	lens_photos = GetActualPhotosSerializer(many=True)
	seller = SellerDetailSerializer()
	item = CameraDetailSerializer()
	lens_package = GetCameraLensPackageSerializer(many=True)
	description = GetAccessoriesDescription(many=True)
	class Meta:
		model = SellCamera
		fields = [
			'id',
			'seller',
			'item',
			'price',
			'condition',
			'description',
			'shutter_count',
			'status',
			'warranty',
			'actual_photos',
			'package',
			'expiry',
			'comments',
			'lens_package',
			'lens_photos',
			'sold_on',
		]


class CamForSaleSerializer(serializers.ModelSerializer):
	#description = serializers.CharField(required=False, allow_null=True, allow_blank=True)
	shutter_count = serializers.IntegerField(required=False)
	package = serializers.CharField(required=False, allow_null=True, allow_blank=True)
	comments = serializers.CharField(required=False, allow_null=True, allow_blank=True)
	
	class Meta:
		model = SellCamera
		fields = [
			'id',
			'seller',
			'item',
			'price',
			'condition',
			#'description',
			'shutter_count',
			'status',
			'warranty',
			'actual_photos',
			'package',
			'comments',
			'lens_package',
			'lens_photos',
		]

	def create(self, validated_data):
	
		camforsale = SellCamera.objects.create(
			seller = validated_data['seller'],
			item = validated_data['item'],
			price = validated_data['price'],
			condition = validated_data['condition'],
			comments = validated_data['comments'],
			#description = validated_data['description'],
			shutter_count = validated_data['shutter_count'],
			status = validated_data['status'],
			warranty = validated_data['warranty'],
			package = validated_data['package']
		)
		print ("record saved")
		actual_photos = validated_data['actual_photos']
		for pk in actual_photos:
			camforsale.actual_photos.add(pk)
			camforsale.save()
		print ("actual photos added")

		camforsale.product_id = "ST-" + create_productID(camforsale.id)
		camforsale.save()
		camforsale.indexing()

		return camforsale


class GetVideoCamForSaleSerializer(serializers.ModelSerializer):
	actual_photos = GetActualPhotosSerializer(many=True)
	seller = SellerDetailSerializer()
	item = CameraDetailSerializer()
	lens_photos = GetActualPhotosSerializer(many=True)
	lens_package = GetCameraLensPackageSerializer(many=True)
	description = GetAccessoriesDescription(many=True)
	class Meta:
		model = SellVideoCamera
		fields = [
			'id',
			'seller',
			'item',
			'price',
			'condition',
			'description',
			'comments',
			'status',
			'warranty',
			'actual_photos',
			'package',
			'expiry',
			'lens_package',
			'lens_photos',
			'sold_on'
		]


class VideoCamForSaleSerializer(serializers.ModelSerializer):
	package = serializers.CharField(required=False, allow_null=True, allow_blank=True)
	comments = serializers.CharField(required=False, allow_null=True, allow_blank=True)
	#description = serializers.CharField(required=False, allow_null=True, allow_blank=True)
	class Meta:
		model = SellVideoCamera
		fields = [
			'id',
			'seller',
			'item',
			'price',
			'condition',
			#'description',
			'comments',
			'status',
			'warranty',
			'actual_photos',
			'package',
			'lens_package',
			'lens_photos'
		]

	def create(self, validated_data):
	
		camforsale = SellVideoCamera.objects.create(
			seller = validated_data['seller'],
			item = validated_data['item'],
			price = validated_data['price'],
			condition = validated_data['condition'],
			#description = validated_data['description'],
			comments = validated_data['comments'],
			status = validated_data['status'],
			warranty = validated_data['warranty'],
			package = validated_data['package']
		)
		print ("record saved")
		actual_photos = validated_data['actual_photos']
		for pk in actual_photos:
			camforsale.actual_photos.add(pk)
			camforsale.save()
		print ("actual photos added")

		camforsale.product_id = "VC-" + create_productID(camforsale.id)
		camforsale.save()

		camforsale.indexing()

		return camforsale



class LensForSaleSerializer(serializers.ModelSerializer):
	comments = serializers.CharField(required=False, allow_null=True, allow_blank=True)

	class Meta:
		model = SellLenses
		fields = [
			'seller',
			'item',
			'price',
			'condition',
			'comments',
			'actual_photos',
			'status',
			'warranty',
			'with_camera'
		]

	def create(self, validated_data):
	
		lensforsale = SellLenses.objects.create(
			seller = validated_data['seller'],
			item = validated_data['item'],
			price = validated_data['price'],
			condition = validated_data['condition'],
			comments = validated_data['comments'],
			status = validated_data['status'],
			warranty = validated_data['warranty'],
			with_camera = validated_data['with_camera']
		)
		print ("record saved")
		actual_photos = validated_data['actual_photos']
		for pk in actual_photos:
			lensforsale.actual_photos.add(pk)
			lensforsale.save()
		print ("actual photos added")

		lensforsale.product_id = "LN-" + create_productID(lensforsale.id)
		lensforsale.save()

		lensforsale.indexing()

		return lensforsale


class GetLensForSaleSerializer(serializers.ModelSerializer):
	item = LensDetailSerializer(read_only=True)
	seller = SellerDetailSerializer(read_only=True)
	actual_photos = GetActualPhotosSerializer(many=True)

	class Meta:
		model = SellLenses
		fields = [
			'id',
			'seller',
			'item',
			'price',
			'condition',
			'comments',
			'actual_photos',
			'status',
			'warranty',
			'with_camera',
			'expiry',
			'sold_on'
		]


class LightingForSaleSerializer(serializers.ModelSerializer):
	comment = serializers.CharField(required=False, allow_null=True, allow_blank=True)
	warranty = serializers.CharField(required=False, allow_null=True, allow_blank=True)
	model = serializers.CharField(required=False, allow_null=True, allow_blank=True)
	class Meta:
		model = SellLighting
		fields = [
			'seller',
			'price',
			'condition',
			'actual_photos',
			'status',
			'warranty',
			'brand',
			'model',
			'category',
			'comment'
		]

	def create_slug_lighting(self, label, new_slug=None):
		slug = slugify(label)
		if new_slug is not None:
			slug = new_slug
		qs = SellLighting.objects.filter(slug=slug).order_by("-id")
		exists = qs.exists()
		if exists:
			new_slug = "%s-%s" %(slug, qs.first().id)
			return self.create_slug_lighting(label, new_slug=new_slug)
		return slug


	def create(self, validated_data):

		label = str(validated_data['brand']) + " " + str(validated_data['model'])
		slug = self.create_slug_lighting(label)
		lightingforsale = SellLighting.objects.create(
			seller = validated_data['seller'],
			price = validated_data['price'],
			condition = validated_data['condition'],
			status = validated_data['status'],
			warranty = validated_data['warranty'],
			brand = validated_data['brand'],
			model = validated_data['model'],
			category = validated_data['category'],
			comment = validated_data['comment'],
			label = label,
			slug = slug
		)
		print ("record saved")
		actual_photos = validated_data['actual_photos']
		for pk in actual_photos:
			lightingforsale.actual_photos.add(pk)
			lightingforsale.save()
		print ("actual photos added")

		lightingforsale.product_id = "LT-" + create_productID(lightingforsale.id)
		lightingforsale.save()

		lightingforsale.indexing()

		return lightingforsale


class GetLightingForSaleSerializer(serializers.ModelSerializer):
	seller = SellerDetailSerializer(read_only=True)
	actual_photos = GetActualPhotosSerializer(many=True)

	class Meta:
		model = SellLighting
		fields = [
			'id',
			'seller',
			'price',
			'condition',
			'actual_photos',
			'status',
			'warranty',
			'brand',
			'model',
			'category',
			'comment',
			'expiry',
			'sold_on'
		]


class AccessoriesForSaleSerializer(serializers.ModelSerializer):
	comment = serializers.CharField(required=False, allow_null=True, allow_blank=True)
	warranty = serializers.CharField(required=False, allow_null=True, allow_blank=True)
	model = serializers.CharField(required=False, allow_null=True, allow_blank=True)
	
	class Meta:
		model = SellAccessories
		fields = [
			'seller',
			'price',
			'condition',
			'actual_photos',
			'status',
			'warranty',
			'brand',
			'model',
			'category',
			'comment'
		]

	def create_slug_accessories(self, label, new_slug=None):
		slug = slugify(label)
		if new_slug is not None:
			slug = new_slug
		qs = SellAccessories.objects.filter(slug=slug).order_by("-id")
		exists = qs.exists()
		if exists:
			new_slug = "%s-%s" %(slug, qs.first().id)
			return self.create_slug_accessories(label, new_slug=new_slug)
		return slug

	def create(self, validated_data):
		label = str(validated_data['brand']) + " " + str(validated_data['model'])
		slug = self.create_slug_accessories(label)
		accessoriesforsale = SellAccessories.objects.create(
			seller = validated_data['seller'],
			price = validated_data['price'],
			condition = validated_data['condition'],
			status = validated_data['status'],
			warranty = validated_data['warranty'],
			brand = validated_data['brand'],
			model = validated_data['model'],
			category = validated_data['category'],
			comment = validated_data['comment'],
			label = label,
			slug = slug
		)
		#accessoriesforsale.save()
		print ("record saved")
		actual_photos = validated_data['actual_photos']
		for pk in actual_photos:
			accessoriesforsale.actual_photos.add(pk)
			accessoriesforsale.save()
		print ("actual photos added")

		accessoriesforsale.product_id = "AC-" + create_productID(accessoriesforsale.id)
		accessoriesforsale.save()

		accessoriesforsale.indexing()

		return accessoriesforsale


class GetAccessoriesForSaleSerializer(serializers.ModelSerializer):
	seller = SellerDetailSerializer(read_only=True)
	actual_photos = GetActualPhotosSerializer(many=True)

	class Meta:
		model = SellAccessories
		fields = [
			'id',
			'seller',
			'price',
			'condition',
			'actual_photos',
			'status',
			'warranty',
			'brand',
			'model',
			'category',
			'comment',
			'expiry',
			'sold_on'
		]


class BlankableFloatField(serializers.FloatField):
    """
    We wanted to be able to receive an empty string ('') for a float field
    and in that case turn it into a None number
    """
    def to_internal_value(self, data):
        if data == '':
            return None

        return super(BlankableFloatField, self).to_internal_value(data)


class DroneForSaleSerializer(serializers.ModelSerializer):
	comment = serializers.CharField(required=False, allow_null=True, allow_blank=True)
	warranty = serializers.CharField(required=False, allow_null=True, allow_blank=True)
	model = serializers.CharField(required=False, allow_null=True, allow_blank=True)
	megapixels = BlankableFloatField(max_value=None, min_value=None, required=False)
	class Meta:
		model = SellDrone
		fields = [
			'seller',
			'price',
			'condition',
			'actual_photos',
			'status',
			'warranty',
			'brand',
			'model',
			'comment',
			'drone_type',
			'camera',
			'video_resolution',
			'megapixels',
			'operating_time',
			'control_range',
		]

	def create_slug_drone(self, label, new_slug=None):
		slug = slugify(label)
		if new_slug is not None:
			slug = new_slug
		qs = SellDrone.objects.filter(slug=slug).order_by("-id")
		exists = qs.exists()
		if exists:
			new_slug = "%s-%s" %(slug, qs.first().id)
			return self.create_slug_drone(label, new_slug=new_slug)
		return slug


	def create(self, validated_data):

		label = str(validated_data['brand']) + " " + str(validated_data['model'])
		slug = self.create_slug_drone(label)
		droneforsale = SellDrone.objects.create(
			seller = validated_data['seller'],
			price = validated_data['price'],
			condition = validated_data['condition'],
			status = validated_data['status'],
			warranty = validated_data['warranty'],
			brand = validated_data['brand'],
			model = validated_data['model'],
			comment = validated_data['comment'],
			drone_type = validated_data['drone_type'],
			camera = validated_data['camera'],
			video_resolution = validated_data['video_resolution'],
			megapixels = validated_data['megapixels'],
			operating_time = validated_data['operating_time'],
			control_range = validated_data['control_range'],
			label = label,
			slug = slug
		)
		print ("record saved")
		actual_photos = validated_data['actual_photos']
		for pk in actual_photos:
			droneforsale.actual_photos.add(pk)
			droneforsale.save()
		print ("actual photos added")

		droneforsale.product_id = "DN-" + create_productID(droneforsale.id)
		droneforsale.save()

		droneforsale.indexing()

		return droneforsale


class GetDroneForSaleSerializer(serializers.ModelSerializer):
	seller = SellerDetailSerializer(read_only=True)
	actual_photos = GetActualPhotosSerializer(many=True)

	class Meta:
		model = SellDrone
		fields = [
			'id',
			'seller',
			'price',
			'condition',
			'actual_photos',
			'status',
			'warranty',
			'brand',
			'model',
			'comment',
			'expiry',
			'sold_on',
			'label',
			'drone_type',
			'camera',
			'video_resolution',
			'megapixels',
			'operating_time',
			'control_range',
			'date',
		]

