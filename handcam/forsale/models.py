from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db.models.signals import post_delete, pre_save, post_save
from django.dispatch import receiver
from django.db import models
from django.template.defaultfilters import slugify
from django.utils.safestring import mark_safe
from markdown_deux import markdown

from star_ratings.models import Rating

from profiles.models import Profile
from camera.models import CameraDetails
from lens.models import LensDetails
from .search import ForSaleIndex, ForSaleVideoIndex, ForSaleLensIndex, ForSaleLightingIndex, ForSaleAccessoriesIndex, ForSaleDronesIndex

from datetime import datetime, timedelta

from messaging.utils import send_expired_notification
import os
from elasticsearch_dsl import Search

EXPIRATION = 45 # after 45 days upon uploading, change the item status to EXPIRED

SELL_STATUS = (
	('Disabled', 'Disabled'),
	('Active', 'Active'),
	('Sold', 'Sold'),
	('Expired', 'Expired'),
	)

CAMERA_PACKAGE = (
	('Body Only', 'Body Only'),
	('With Lens', 'With Lens'),
	('With Accessories', 'With Accessories'),
	('With Lens and Accessories', 'With Lens and Accessories'),
	)

def upload_location(instance, filename):
	
	return "user_{0}/sell/{1}/{2}".format(instance.user.user.username, instance.cam_model, filename)

def upload_location_watermark(instance, filename):
	
	return "user_{0}/sell/{1}/{2}".format(instance.user.user.username, instance.cam_model, "watermarked_"+ filename)


class ForSaleActualPhoto(models.Model):
	user 			= models.ForeignKey(Profile, related_name="pic_seller_profile", on_delete=models.CASCADE)
	cam_model 		= models.CharField(max_length=150, null=True, blank=True)
	actual_photo    = models.ImageField(upload_to=upload_location, 
						null=True, 
						blank=True,
						max_length=500)
	watermarked     = models.ImageField(upload_to=upload_location_watermark, 
						null=True, 
						blank=True,
						max_length=500)

	def __str__(self):
		return str(self.actual_photo)


class CameraLensPackage(models.Model):
	lens_package	= models.ForeignKey(LensDetails)

	def __str__(self):
		return str(self.lens_package.model)


class CameraAccessoriesDescription(models.Model):
	description     = models.TextField()

	def __str__(self):
		return str(self.description)

class SellCamera(models.Model):
	seller          = models.ForeignKey(Profile, related_name="cam_seller_profile", on_delete=models.CASCADE)
	label           = models.CharField(max_length=150, null=True, blank=True)
	slug            = models.SlugField(max_length=255, unique=True, blank=True)
	item			= models.ForeignKey(CameraDetails)
	price           = models.FloatField(default=None, null=True, blank=True)
	condition       = models.TextField()
	description     = models.ManyToManyField(CameraAccessoriesDescription, related_name="still_camera_accessories", blank=True)
	comments		= models.TextField(null=True, blank=True)
	shutter_count	= models.IntegerField(default=None, null=True, blank=True)
	actual_photos   = models.ManyToManyField(ForSaleActualPhoto, related_name="sale_cam_actual_photo", blank=True)
	date  		    = models.DateTimeField(auto_now_add=True)
	expiry			= models.DateTimeField(default=datetime.now() + timedelta(days=EXPIRATION))
	sold_on			= models.DateTimeField(null=True, blank=True)
	status			= models.CharField(max_length=10, choices=SELL_STATUS, default="Active")
	warranty		= models.TextField()
	package			= models.CharField(max_length=50, choices=CAMERA_PACKAGE, default="Body Only")
	lens_package 	= models.ManyToManyField(CameraLensPackage, related_name="packaged_lenses", blank=True)
	lens_photos     = models.ManyToManyField(ForSaleActualPhoto, related_name="lens_package_actual_photo", blank=True)
	product_id		= models.CharField(max_length=15)


	def __str__(self):
		return str(self.label)

	class Meta:
		verbose_name = 'Still Cameras'
		verbose_name_plural = 'Still Cameras'

	@models.permalink
	def get_absolute_url(self):
		return ('view_camera_sell', (), {'slug' :self.slug,})

	def get_markdown_condition(self):
		content = self.condition
		markdown_text = markdown(content)
		return mark_safe(markdown_text)

	def get_markdown_warranty(self):
		content = self.warranty
		markdown_text = markdown(content)
		return mark_safe(markdown_text)

	def get_actual_photos(self):
		nest = []
		
		for n in self.actual_photos.all():
			nest.append(n.actual_photo)
		
		return "{0}".format([p.watermarked.url for p in self.actual_photos.all()])

	def get_lens_photos(self):
		nest = []
		
		for n in self.lens_photos.all():
			nest.append(n.actual_photo)
		
		return "{0}".format([p.watermarked.url for p in self.lens_photos.all()])

	def get_lens_packages(self):
		nest = []
		for n in self.lens_package.all():
			nest.append(n)

		return "{0}".format([p.lens_package.pk for p in self.lens_package.all()])

	def get_accessories_description(self):
		nest = []

		for n in self.description.all():
			nest.append(n)

		return "{0}".format([p for p in self.description.all()])

	def test(self):
		return 'this is a test'

	def get_focal_length(self, low, high):
		if ((low) and (high)):
			return low + ' - ' + high
		elif ((low) and (high==None)):
			return low
		elif ((low==None) and (high)):
			return high
		else:
			return None

	def get_iso(self, low, high):
		if ((low) and (high)):
			return low + ' - ' + high
		elif ((low) and (high==None)):
			return low
		elif ((low==None) and (high)):
			return high
		else:
			return None		

	def get_lcd_monitor(self, size, resolution):
		if ((size) and (resolution)):
			
			return size + '" - ' + resolution

		elif ((size) and (resolution==None)):
			return size + '"'
		elif ((size==None) and (resolution)):
			return resolution
		else:
			return None

	def get_currency(self, country):

		currency = ""
		if (country == "Saudi Arabia"):
			currency = "SAR"
		elif (country == "United Arab Emirates"):
			currency = "AED"
		elif (country == "Kuwait"):
			currency = "KWD"
		elif (country == "Bahrain"):
			currency = "BHD"
		elif (country == "Oman"):
			currency = "OMR"
		else:
			currency = "SAR"

		return currency

	def get_condition(self, condition):

		if (condition=="Brand new - not used"):
			return "Brand new - not used"
		else:
			return "Used"


	def indexing(self):
		print ('indexing models forsale')
		obj = ForSaleIndex(
		  meta={'id': self.id},
		  description="forsale",
		  product_id=self.product_id,
		  seller_pk=self.seller.pk,
		  #seller=self.seller.user.get_full_name(),
                  seller=self.seller.user.username,
		  seller_location=self.seller.city + ', ' + self.seller.country,
		  seller_country = self.seller.country,
		  seller_city = self.seller.city,
		  seller_phonenumber=self.seller.phone_number,
		  label=self.label,
		  price=self.price,
		  neworused=self.get_condition(self.condition),
		  currency=self.get_currency(self.seller.country),
		  condition=self.condition,
		  lens_description=self.get_accessories_description(),
		  shutter_count=self.shutter_count,
		  status=self.status,
		  package=self.package,
		  date=self.date,
		  warranty=self.warranty,
		  #camera_package=self.package,
		  actual_photos=self.get_actual_photos(),
		  comments=self.comments,
		  lens_photos=self.get_lens_photos(),
		  lens_package=self.get_lens_packages(),
		  brand=self.item.cam_type.brand.company,
		  kind=self.item.cam_type.kind.kind_of_camera,
		  category=self.item.cam_type.camera_type,
		  model=self.item.model,
		  image=self.item.image.url,
		  megapixels=self.item.megapixels,
		  sensor_size=self.item.sensor_size,
		  sensor_type=self.item.sensor_type,
		  image_sensor_format=self.item.image_sensor_format,
		  #lcd=self.item.lcd_size+ ' - ' + self.item.lcd_resolution,
		  lcd=self.get_lcd_monitor(self.item.lcd_size, self.item.lcd_resolution),
		  lcd_size=self.item.lcd_size,
		  lcd_resolution=self.item.lcd_resolution,
		  weight=self.item.weight,
		  #iso=self.item.iso_low + ' - ' + self.item.iso_high,
		  iso=self.get_iso(self.item.iso_low, self.item.iso_high),
		  iso_low=self.item.iso_low,
		  iso_high=self.item.iso_high,
		  video=self.item.video,
		  frames_per_sec=self.item.frames_per_sec,
		  af_points=self.item.af_points,
		  mirrorless=self.item.mirrorless,
		  optical_zoom=self.item.optical_zoom,
		  kind_camera=self.item.cam_type.kind.kind_of_camera,
		  digital_zoom=self.item.digital_zoom,
		  aperture=self.item.aperture,
		  max_aperture=self.item.max_aperture,
		  wifi=self.item.wifi,
		  gps=self.item.gps,
		  #focal_length=self.item.focal_length_from+ ' - ' +self.item.focal_length_to,
		  focal_length=self.get_focal_length(self.item.focal_length_from, self.item.focal_length_to),
		  focal_length_from=self.item.focal_length_from,
		  focal_length_to=self.item.focal_length_to,
		  waterproof=self.item.waterproof,
		  film_size=self.item.film_size,
		  image_size=self.item.image_size,
		  photo_capacity=self.item.photo_capacity,
		  effective_pixels=self.item.effective_pixels,
		  effective_resolution=self.item.effective_resolution,
		  dynamic_range=self.item.dynamic_range,
		  frame_rate=self.item.frame_rate,
		  view_finder=self.item.view_finder,
		  lens_mount=self.item.lens_mount,
		  #cam_features=self.item.cam_features,
		  #cam_specifications=self.item.cam_specifications
				  
		)
		#print (self.get_actual_phtotos())
		obj.save()
		return obj.to_dict(include_meta=True)


class SellVideoCamera(models.Model):
	seller          = models.ForeignKey(Profile, related_name="videocam_seller_profile", on_delete=models.CASCADE)
	label           = models.CharField(max_length=150, null=True, blank=True)
	slug            = models.SlugField(max_length=255, unique=True, blank=True)
	item			= models.ForeignKey(CameraDetails)
	price           = models.FloatField(default=None, null=True, blank=True)
	condition       = models.TextField()
	comments		= models.TextField(null=True, blank=True)
	description     = models.ManyToManyField(CameraAccessoriesDescription, related_name="video_camera_accessories", blank=True)
	actual_photos   = models.ManyToManyField(ForSaleActualPhoto, related_name="sale_videocam_actual_photo", blank=True)
	date  		    = models.DateTimeField(auto_now_add=True)
	expiry			= models.DateTimeField(default=datetime.now() + timedelta(days=EXPIRATION))
	sold_on			= models.DateTimeField(null=True, blank=True)
	status			= models.CharField(max_length=10, choices=SELL_STATUS, default="Active")
	warranty		= models.TextField()
	package			= models.CharField(max_length=50, choices=CAMERA_PACKAGE, default="Body Only")
	lens_package 	= models.ManyToManyField(CameraLensPackage, related_name="packaged_video_lenses", blank=True)
	lens_photos     = models.ManyToManyField(ForSaleActualPhoto, related_name="lens_video_package_actual_photo", blank=True)
	product_id		= models.CharField(max_length=15)

	def __str__(self):
		return str(self.label)

	class Meta:
		verbose_name = 'Video Cameras'
		verbose_name_plural = 'Video Cameras'

	@models.permalink
	def get_absolute_url(self):
		return ('view_camera_sell', (), {'slug' :self.slug,})

	def get_markdown_condition(self):
		content = self.condition
		markdown_text = markdown(content)
		return mark_safe(markdown_text)

	def get_markdown_warranty(self):
		content = self.warranty
		markdown_text = markdown(content)
		return mark_safe(markdown_text)

	def get_actual_photos(self):
		nest = []
		
		for n in self.actual_photos.all():
			nest.append(n.actual_photo)
		
		return "{0}".format([p.watermarked.url for p in self.actual_photos.all()])

	def get_lens_photos(self):
		nest = []
		
		for n in self.lens_photos.all():
			nest.append(n.actual_photo)
		
		return "{0}".format([p.watermarked.url for p in self.lens_photos.all()])

	def get_lens_packages(self):
		nest = []
		for n in self.lens_package.all():
			nest.append(n)

		return "{0}".format([p.lens_package.pk for p in self.lens_package.all()])

	def get_accessories_description(self):
		nest = []

		for n in self.description.all():
			nest.append(n)

		return "{0}".format([p for p in self.description.all()])

	def test(self):
		return 'this is a test'

	def get_focal_length(self, low, high):
		if ((low) and (high)):
			return low + ' - ' + high
		elif ((low) and (high==None)):
			return low
		elif ((low==None) and (high)):
			return high
		else:
			return None

	def get_iso(self, low, high):
		if ((low) and (high)):
			return low + ' - ' + high
		elif ((low) and (high==None)):
			return low
		elif ((low==None) and (high)):
			return high
		else:
			return None		

	def get_lcd_monitor(self, size, resolution):
		if ((size) and (resolution)):
			
			return size + '" - ' + resolution

		elif ((size) and (resolution==None)):
			return size + '"'
		elif ((size==None) and (resolution)):
			return resolution
		else:
			return None

	def get_currency(self, country):

		currency = ""
		if (country == "Saudi Arabia"):
			currency = "SAR"
		elif (country == "United Arab Emirates"):
			currency = "AED"
		elif (country == "Kuwait"):
			currency = "KWD"
		elif (country == "Bahrain"):
			currency = "BHD"
		elif (country == "Oman"):
			currency = "OMR"
		else:
			currency = "SAR"

		return currency

	def get_condition(self, condition):

		if (condition=="Brand new - not used"):
			return "Brand new - not used"
		else:
			return "Used"

	def indexing(self):
		print ('indexing models forsale')
		obj = ForSaleVideoIndex(
		  meta={'id': self.id},
		  description="forsale",
		  product_id=self.product_id,
		  seller_pk=self.seller.pk,
		  #seller=self.seller.user.get_full_name(),
                  seller=self.seller.user.username,
		  seller_location=self.seller.city + ', ' + self.seller.country,
		  seller_country = self.seller.country,
		  seller_city = self.seller.city,
		  seller_phonenumber=self.seller.phone_number,
		  label=self.label,
		  price=self.price,
		  neworused=self.get_condition(self.condition),
		  currency=self.get_currency(self.seller.country),
		  condition=self.condition,
		  lens_description=self.get_accessories_description(),
		  status=self.status,
		  package=self.package,
		  date=self.date,
		  warranty=self.warranty,
		  comments=self.comments,
		  lens_photos=self.get_lens_photos(),
		  lens_package=self.get_lens_packages(),
		  camera_package=self.package,
		  actual_photos=self.get_actual_photos(),
		  brand=self.item.cam_type.brand.company,
		  kind=self.item.cam_type.kind.kind_of_camera,
		  category=self.item.cam_type.camera_type,
		  model=self.item.model,
		  image=self.item.image.url,
		  megapixels=self.item.megapixels,
		  sensor_size=self.item.sensor_size,
		  sensor_type=self.item.sensor_type,
		  image_sensor_format=self.item.image_sensor_format,
		  #lcd=self.item.lcd_size+ ' - ' + self.item.lcd_resolution,
		  lcd=self.get_lcd_monitor(self.item.lcd_size, self.item.lcd_resolution),
		  lcd_size=self.item.lcd_size,
		  lcd_resolution=self.item.lcd_resolution,
		  weight=self.item.weight,
		  #iso=self.item.iso_low + ' - ' + self.item.iso_high,
		  iso=self.get_iso(self.item.iso_low, self.item.iso_high),
		  iso_low=self.item.iso_low,
		  iso_high=self.item.iso_high,
		  video=self.item.video,
		  frames_per_sec=self.item.frames_per_sec,
		  af_points=self.item.af_points,
		  mirrorless=self.item.mirrorless,
		  optical_zoom=self.item.optical_zoom,
		  kind_camera=self.item.cam_type.kind.kind_of_camera,
		  digital_zoom=self.item.digital_zoom,
		  aperture=self.item.aperture,
		  max_aperture=self.item.max_aperture,
		  wifi=self.item.wifi,
		  gps=self.item.gps,
		  #focal_length=self.item.focal_length_from+ ' - ' +self.item.focal_length_to,
		  focal_length=self.get_focal_length(self.item.focal_length_from, self.item.focal_length_to),
		  focal_length_from=self.item.focal_length_from,
		  focal_length_to=self.item.focal_length_to,
		  waterproof=self.item.waterproof,
		  film_size=self.item.film_size,
		  image_size=self.item.image_size,
		  photo_capacity=self.item.photo_capacity,
		  effective_pixels=self.item.effective_pixels,
		  effective_resolution=self.item.effective_resolution,
		  dynamic_range=self.item.dynamic_range,
		  frame_rate=self.item.frame_rate,
		  view_finder=self.item.view_finder,
		  lens_mount=self.item.lens_mount,
		  cam_features=self.item.cam_features,
		  cam_specifications=self.item.cam_specifications
				  
		)
		#print (self.get_actual_phtotos())
		obj.save()
		return obj.to_dict(include_meta=True)


class SellLenses(models.Model):
	seller          = models.ForeignKey(Profile, related_name="lens_seller_profile", on_delete=models.CASCADE)
	label           = models.CharField(max_length=150, null=True, blank=True)
	slug            = models.SlugField(max_length=255, unique=True, blank=True)
	item			= models.ForeignKey(LensDetails)
	price           = models.IntegerField(default=None, null=True, blank=True)
	condition       = models.TextField()
	comments		= models.TextField(null=True, blank=True)
	actual_photos   = models.ManyToManyField(ForSaleActualPhoto, related_name="sale_lens_actual_photo", blank=True)
	date  		    = models.DateTimeField(auto_now_add=True)
	expiry			= models.DateTimeField(default=datetime.now() + timedelta(days=EXPIRATION))
	sold_on			= models.DateTimeField(null=True, blank=True)
	status			= models.CharField(max_length=10, choices=SELL_STATUS, default="Active")
	warranty		= models.TextField()
	with_camera		= models.CharField(max_length=10, null=True, blank=True)
	product_id		= models.CharField(max_length=15)

	def __str__(self):
		return str(self.label)

	class Meta:
		verbose_name = 'Lenses'
		verbose_name_plural = 'Lenses'

	@models.permalink
	def get_absolute_url(self):
		return ('view_lens_sell', (), {'slug' :self.slug,})

	def get_markdown_condition(self):
		content = self.condition
		markdown_text = markdown(content)
		return mark_safe(markdown_text)

	def get_markdown_warranty(self):
		content = self.warranty
		markdown_text = markdown(content)
		return mark_safe(markdown_text)


	def get_focal_length(self, low, high):
		if ((low) and (high)):
			return low + 'mm - ' + high + "mm"
		elif ((low) and (high==None)):
			return low
		elif ((low==None) and (high)):
			return high
		else:
			return None

	def get_actual_photos(self):
		nest = []
		
		for n in self.actual_photos.all():
			nest.append(n.actual_photo)
		
		return "{0}".format([p.watermarked.url for p in self.actual_photos.all()])

	def get_currency(self, country):

		currency = ""
		if (country == "Saudi Arabia"):
			currency = "SAR"
		elif (country == "United Arab Emirates"):
			currency = "AED"
		elif (country == "Kuwait"):
			currency = "KWD"
		elif (country == "Bahrain"):
			currency = "BHD"
		elif (country == "Oman"):
			currency = "OMR"
		else:
			currency = "SAR"

		return currency

	def get_condition(self, condition):

		if (condition=="Brand new - not used"):
			return "Brand new - not used"
		else:
			return "Used"


	def indexing(self):
		print ('indexing models forsale')
		obj = ForSaleLensIndex(
		  meta={'id': self.id},
		  description="forsale",
		  product_id=self.product_id,
		  seller_pk=self.seller.pk,
		  #seller=self.seller.user.get_full_name(),
                  seller=self.seller.user.username,
		  seller_location=self.seller.city + ', ' + self.seller.country,
		  seller_country = self.seller.country,
		  seller_city = self.seller.city,
		  seller_phonenumber=self.seller.phone_number,
		  label=self.label,
		  price=self.price,
		  neworused=self.get_condition(self.condition),
		  currency=self.get_currency(self.seller.country),
		  condition=self.condition,
		  status=self.status,
		  date=self.date,
		  warranty=self.warranty,
		  comments=self.comments,
		  with_camera=self.with_camera,
		  actual_photos=self.get_actual_photos(),
		  brand=self.item.type_of_lens.brand.company,
		  category=self.item.type_of_lens.lens_type,
		  model=self.item.model,
		  image=self.item.image.url,
		  focal_length_from=self.item.focal_length_from,
		  focal_length_to=self.item.focal_length_to,
		  aperture=self.item.aperture,
		  max_aperture=self.item.max_aperture,
		  weight=self.item.weight,
		  format_range=self.item.format_range,
		  lens_type=self.item.lens_type,
		  lens_type_and=self.item.lens_type_and,
		  lens_format=self.item.lens_format,
		  focus_type=self.item.focus_type,
		  fixed_focal=self.item.fixed_focal,
		  mounts=self.item.mounts,
		  magnification=self.item.magnification,
		  features=self.item.features,
		  specifications=self.item.specifications
		)
		
		obj.save()
		return obj.to_dict(include_meta=True)


class SellLighting(models.Model):
	seller          = models.ForeignKey(Profile, related_name="lighting_seller_profile", on_delete=models.CASCADE)
	label           = models.CharField(max_length=150, null=True, blank=True)
	slug            = models.SlugField(max_length=255, unique=True, blank=True)
	price           = models.FloatField(default=None, null=True, blank=True)
	brand  			= models.CharField(max_length=100, null=True, blank=True)
	category  		= models.CharField(max_length=100, null=True, blank=True)
	model  			= models.CharField(max_length=150, null=True, blank=True)
	condition       = models.TextField()
	actual_photos   = models.ManyToManyField(ForSaleActualPhoto, related_name="sale_lighting_actual_photo", blank=True)
	date  		    = models.DateTimeField(auto_now_add=True)
	expiry			= models.DateTimeField(default=datetime.now() + timedelta(days=EXPIRATION))
	sold_on			= models.DateTimeField(null=True, blank=True)
	status			= models.CharField(max_length=10, choices=SELL_STATUS, default="Active")
	warranty		= models.TextField(default="No warranty")
	comment			= models.TextField(null=True, blank=True)
	product_id		= models.CharField(max_length=15)


	def __str__(self):
		return str(self.label)

	class Meta:
		verbose_name = 'Lightings'
		verbose_name_plural = 'Lightings'

	@models.permalink
	def get_absolute_url(self):
		return ('view_lighting_sell', (), {'slug' :self.slug,})

	def get_markdown_comment(self):
		content = self.comment
		markdown_text = markdown(content)
		return mark_safe(markdown_text)

	def get_markdown_condition(self):
		content = self.condition
		markdown_text = markdown(content)
		return mark_safe(markdown_text)

	def get_markdown_warranty(self):
		content = self.warranty
		markdown_text = markdown(content)
		return mark_safe(markdown_text)

	def get_actual_photos(self):
		nest = []
		
		for n in self.actual_photos.all():
			nest.append(n.actual_photo)
		
		return "{0}".format([p.watermarked.url for p in self.actual_photos.all()])

	def get_image(self):
		images = self.actual_photos.all()
		print("get image function")
		print(images)
		if len(images) > 0:
			image = images[0].watermarked.url
		
			return image
		return None

	def get_currency(self, country):

		currency = ""
		if (country == "Saudi Arabia"):
			currency = "SAR"
		elif (country == "United Arab Emirates"):
			currency = "AED"
		elif (country == "Kuwait"):
			currency = "KWD"
		elif (country == "Bahrain"):
			currency = "BHD"
		elif (country == "Oman"):
			currency = "OMR"
		else:
			currency = "SAR"

		return currency

	def get_condition(self, condition):

		if (condition=="Brand new - not used"):
			return "Brand new - not used"
		else:
			return "Used"


	def indexing(self):
		print ('indexing lighting models forsale')
		obj = ForSaleLightingIndex(
		  meta={'id': self.id},
		  description="forsale",
		  product_id=self.product_id,
		  seller_pk=self.seller.pk,
		  #seller=self.seller.user.get_full_name(),
                  seller=self.seller.user.username,
		  seller_location=self.seller.city + ', ' + self.seller.country,
		  seller_country = self.seller.country,
		  seller_city = self.seller.city,
		  seller_phonenumber=self.seller.phone_number,
		  label=self.label,
		  price=self.price,
		  neworused=self.get_condition(self.condition),
		  currency=self.get_currency(self.seller.country),
		  status=self.status,
		  date=self.date,
		  actual_photos=self.get_actual_photos(),
		  image=self.get_image(),
		  brand=self.brand,
		  model=self.model,
		  category=self.category,
		  condition=self.condition,
		  warranty=self.warranty,
		  comment=self.comment
		)
		
		obj.save()
		return obj.to_dict(include_meta=True)


class SellAccessories(models.Model):
	seller          = models.ForeignKey(Profile, related_name="accessories_seller_profile", on_delete=models.CASCADE)
	label           = models.CharField(max_length=150, null=True, blank=True)
	slug            = models.SlugField(max_length=255, unique=True, blank=True)
	price           = models.FloatField(default=None, null=True, blank=True)
	brand  			= models.CharField(max_length=100, null=True, blank=True)
	model  			= models.CharField(max_length=150, null=True, blank=True)
	category  		= models.CharField(max_length=100, null=True, blank=True)
	condition       = models.TextField()
	actual_photos   = models.ManyToManyField(ForSaleActualPhoto, related_name="sale_accessories_actual_photo", blank=True)
	date  		    = models.DateTimeField(auto_now_add=True)
	expiry			= models.DateTimeField(default=datetime.now() + timedelta(days=EXPIRATION))
	sold_on			= models.DateTimeField(null=True, blank=True)
	status			= models.CharField(max_length=10, choices=SELL_STATUS, default="Active")
	warranty		= models.TextField(default="No warranty")
	comment			= models.TextField(null=True, blank=True)
	product_id		= models.CharField(max_length=15)


	def __str__(self):
		return str(self.label)

	class Meta:
		verbose_name = 'Accessories'
		verbose_name_plural = 'Accessories'

	@models.permalink
	def get_absolute_url(self):
		return ('view_accessories_sell', (), {'slug' :self.slug,})

	def get_markdown_comment(self):
		content = self.comment
		markdown_text = markdown(content)
		return mark_safe(markdown_text)

	def get_markdown_condition(self):
		content = self.condition
		markdown_text = markdown(content)
		return mark_safe(markdown_text)

	def get_markdown_warranty(self):
		content = self.warranty
		markdown_text = markdown(content)
		return mark_safe(markdown_text)

	def get_actual_photos(self):
		nest = []
		
		for n in self.actual_photos.all():
			nest.append(n.actual_photo)
		
		return "{0}".format([p.watermarked.url for p in self.actual_photos.all()])

	def get_image(self):
		images = self.actual_photos.all()
		
		if len(images) > 0:
			image = images[0].watermarked.url
		
			return image
		return None

	def get_currency(self, country):

		currency = ""
		if (country == "Saudi Arabia"):
			currency = "SAR"
		elif (country == "United Arab Emirates"):
			currency = "AED"
		elif (country == "Kuwait"):
			currency = "KWD"
		elif (country == "Bahrain"):
			currency = "BHD"
		elif (country == "Oman"):
			currency = "OMR"
		else:
			currency = "SAR"

		return currency

	def get_condition(self, condition):

		if (condition=="Brand new - not used"):
			return "Brand new - not used"
		else:
			return "Used"


	def indexing(self):
		print ('indexing accessories models forsale')
		obj = ForSaleAccessoriesIndex(
		  meta={'id': self.id},
		  description="forsale",
		  product_id=self.product_id,
		  seller_pk=self.seller.pk,
		  #seller=self.seller.user.get_full_name(),
                  seller=self.seller.user.username,
		  seller_location=self.seller.city + ', ' + self.seller.country,
		  seller_country = self.seller.country,
		  seller_city = self.seller.city,
		  seller_phonenumber=self.seller.phone_number,
		  label=self.label,
		  price=self.price,
		  neworused=self.get_condition(self.condition),
		  currency=self.get_currency(self.seller.country),
		  status=self.status,
		  date=self.date,
		  actual_photos=self.get_actual_photos(),
		  image=self.get_image(),
		  brand=self.brand,
		  model=self.model,
		  category=self.category,
		  condition=self.condition,
		  warranty=self.warranty,
		  comment=self.comment
		)
		
		obj.save()
		return obj.to_dict(include_meta=True)


class SellDrone(models.Model):
	seller          = models.ForeignKey(Profile, related_name="drone_seller_profile", on_delete=models.CASCADE)
	label           = models.CharField(max_length=150, null=True, blank=True)
	slug            = models.SlugField(max_length=255, unique=True, blank=True)
	price           = models.FloatField(default=None, null=True, blank=True)
	brand  			= models.CharField(max_length=100, null=True, blank=True)
	model  			= models.CharField(max_length=150, null=True, blank=True)
	drone_type  	= models.CharField(max_length=100, null=True, blank=True)
	camera  		= models.CharField(max_length=100, null=True, blank=True)
	video_resolution= models.CharField(max_length=100, null=True, blank=True)
	megapixels 		= models.FloatField(default=None, help_text='megapixels', null=True, blank=True)
	operating_time  = models.CharField(max_length=100, null=True, blank=True)
	control_range  	= models.CharField(max_length=100, null=True, blank=True)
	condition       = models.TextField()
	actual_photos   = models.ManyToManyField(ForSaleActualPhoto, related_name="sale_drones_actual_photo", blank=True)
	date  		    = models.DateTimeField(auto_now_add=True)
	expiry			= models.DateTimeField(default=datetime.now() + timedelta(days=EXPIRATION))
	sold_on			= models.DateTimeField(null=True, blank=True)
	status			= models.CharField(max_length=10, choices=SELL_STATUS, default="Active")
	warranty		= models.TextField(default="No warranty")
	comment			= models.TextField(null=True, blank=True)
	product_id		= models.CharField(max_length=15)


	def __str__(self):
		return str(self.label)

	class Meta:
		verbose_name = 'Drons'
		verbose_name_plural = 'Drons'

	@models.permalink
	def get_absolute_url(self):
		return ('view_drone_sell', (), {'slug' :self.slug,})

	def get_markdown_comment(self):
		content = self.comment
		markdown_text = markdown(content)
		return mark_safe(markdown_text)

	def get_markdown_condition(self):
		content = self.condition
		markdown_text = markdown(content)
		return mark_safe(markdown_text)

	def get_markdown_warranty(self):
		content = self.warranty
		markdown_text = markdown(content)
		return mark_safe(markdown_text)

	def get_actual_photos(self):
		nest = []
		
		for n in self.actual_photos.all():
			nest.append(n.actual_photo)
		
		return "{0}".format([p.watermarked.url for p in self.actual_photos.all()])

	def get_image(self):
		images = self.actual_photos.all()
		
		if len(images) > 0:
			image = images[0].watermarked.url
		
			return image
		return None

	def get_currency(self, country):

		currency = ""
		if (country == "Saudi Arabia"):
			currency = "SAR"
		elif (country == "United Arab Emirates"):
			currency = "AED"
		elif (country == "Kuwait"):
			currency = "KWD"
		elif (country == "Bahrain"):
			currency = "BHD"
		elif (country == "Oman"):
			currency = "OMR"
		else:
			currency = "SAR"

		return currency

	def get_condition(self, condition):

		if (condition=="Brand new - not used"):
			return "Brand new - not used"
		else:
			return "Used"

	def indexing(self):
		print ('indexing drone models forsale')
		obj = ForSaleDronesIndex(
		  meta={'id': self.id},
		  description="forsale",
		  product_id=self.product_id,
		  seller_pk=self.seller.pk,
		  #seller=self.seller.user.get_full_name(),
                  seller=self.seller.user.username,
		  seller_location=self.seller.city + ', ' + self.seller.country,
		  seller_country = self.seller.country,
		  seller_city = self.seller.city,
		  seller_phonenumber=self.seller.phone_number,
		  label=self.label,
		  price=self.price,
		  neworused=self.get_condition(self.condition),
		  currency=self.get_currency(self.seller.country),
		  status=self.status,
		  date=self.date,
		  actual_photos=self.get_actual_photos(),
		  image=self.get_image(),
		  brand=self.brand,
		  model=self.model,
		  condition=self.condition,
		  warranty=self.warranty,
		  comment=self.comment,
		  megapixels=self.megapixels,
		  video_resolution=self.video_resolution,
		  camera=self.camera,
		  control_range=self.control_range,
		  operating_time=self.operating_time,
		  drone_type=self.drone_type
		)
		
		obj.save()
		return obj.to_dict(include_meta=True)



#CAMERA HELPER FUNCTIONS

def create_slug_cam(instance, new_slug=None):
	slug = slugify(instance.label)
	if new_slug is not None:
		slug = new_slug
	qs = SellCamera.objects.filter(slug=slug).order_by("-id")
	exists = qs.exists()
	if exists:
		new_slug = "%s-%s" %(slug, qs.first().id)
		return create_slug_cam(instance, new_slug=new_slug)
	return slug

#PRE SAVE SIGNALS
def pre_save_sellcamera_receiver(sender, instance, *args, **kwargs):
	label = str(instance.item.cam_type.brand.company) + " " + str(instance.item.cam_type.camera_type) + " " + str(instance.item.model) + " "+ str(instance.item.cam_type.kind.kind_of_camera)
	instance.label = label
	if not instance.slug:
		instance.slug = create_slug_cam(instance)

pre_save.connect(pre_save_sellcamera_receiver, sender=SellCamera)


#POST SAVE SIGNALS
def post_save_sellcamera_receiver(sender, instance, *args, **kwargs):
	instance.indexing()
	if instance.status == "Expiry":
		send_expired_notification(instance.seller)

post_save.connect(post_save_sellcamera_receiver, sender=SellCamera)


# VIDEO CAMERA HELPER FUNCTIONS
def create_slug_videocam(instance, new_slug=None):
	slug = slugify(instance.label)
	if new_slug is not None:
		slug = new_slug
	qs = SellVideoCamera.objects.filter(slug=slug).order_by("-id")
	exists = qs.exists()
	if exists:
		new_slug = "%s-%s" %(slug, qs.first().id)
		return create_slug_videocam(instance, new_slug=new_slug)
	return slug

#SIGNALS
def pre_save_sellvideocamera_receiver(sender, instance, *args, **kwargs):
	label = str(instance.item.cam_type.brand.company) + " " + str(instance.item.cam_type.camera_type) + " " + str(instance.item.model) + " "+ str(instance.item.cam_type.kind.kind_of_camera)
	instance.label = label
	if not instance.slug:
		instance.slug = create_slug_videocam(instance)

pre_save.connect(pre_save_sellvideocamera_receiver, sender=SellVideoCamera)

def post_save_sellvideocamera_receiver(sender, instance, *args, **kwargs):
	instance.indexing()
	if instance.status == "Expiry":
		send_expired_notification(instance.seller)

post_save.connect(post_save_sellvideocamera_receiver, sender=SellVideoCamera)


#LENSES HELPER FUNCTIONS

def create_slug_lens(instance, new_slug=None):
	slug = slugify(instance.label)
	if new_slug is not None:
		slug = new_slug
	qs = SellLenses.objects.filter(slug=slug).order_by("-id")
	exists = qs.exists()
	if exists:
		new_slug = "%s-%s" %(slug, qs.first().id)
		return create_slug_lens(instance, new_slug=new_slug)
	return slug

#SIGNALS
def pre_save_selllens_receiver(sender, instance, *args, **kwargs):
	print("lens pre save function")
	label = str(instance.item.type_of_lens.brand.company) + " " + str(instance.item.type_of_lens.lens_type) + " " + str(instance.item.model)
	instance.label = label
	if not instance.slug:
		instance.slug = create_slug_lens(instance)

pre_save.connect(pre_save_selllens_receiver, sender=SellLenses)

def post_save_selllens_receiver(sender, instance, *args, **kwargs):
	instance.indexing()
	if instance.status == "Expiry":
		send_expired_notification(instance.seller)

post_save.connect(post_save_selllens_receiver, sender=SellLenses)


#LIGHTING HELPER FUNCTIONS
def create_slug_lighting(instance, new_slug=None):
	slug = slugify(instance.label)
	if new_slug is not None:
		slug = new_slug
	qs = SellLighting.objects.filter(slug=slug).order_by("-id")
	exists = qs.exists()
	if exists:
		new_slug = "%s-%s" %(slug, qs.first().id)
		return create_slug_lighting(instance, new_slug=new_slug)
	return slug

#PRE SAVE SIGNALS
def pre_save_selllighting_receiver(sender, instance, *args, **kwargs):
	print("pre saving sell lighting")
	label = str(instance.brand) + " " + str(instance.model) + " " + str(instance.pk)
	instance.label = label
	if not instance.slug:
		instance.slug = create_slug_lighting(instance)

#pre_save.connect(pre_save_selllighting_receiver, sender=SellLighting)


def post_save_selllighting_receiver(sender, instance, *args, **kwargs):
	print("post saving sell lighting")
	instance.indexing()
	if instance.status == "Expiry":
		send_expired_notification(instance.seller)

post_save.connect(post_save_selllighting_receiver, sender=SellLighting)



#ACCESSORIES HELPER FUNCTIONS

def create_slug_accessories(instance, new_slug=None):
	slug = slugify(instance.label)
	if new_slug is not None:
		slug = new_slug
	qs = SellAccessories.objects.filter(slug=slug).order_by("-id")
	exists = qs.exists()
	if exists:
		new_slug = "%s-%s" %(slug, qs.first().id)
		return create_slug_accessories(instance, new_slug=new_slug)
	return slug

#SIGNALS
def pre_save_selllighting_receiver(sender, instance, *args, **kwargs):
	label = str(instance.brand) + " " + str(instance.model)+ " " + str(instance.pk)
	instance.label = label
	if not instance.slug:
		instance.slug = create_slug_accessories(instance)

#pre_save.connect(pre_save_selllighting_receiver, sender=SellAccessories)


def post_save_sellaccessories_receiver(sender, instance, *args, **kwargs):
	instance.indexing()
	if instance.status == "Expiry":
		send_expired_notification(instance.seller)

post_save.connect(post_save_sellaccessories_receiver, sender=SellAccessories)


#DRONES HELPER FUNCTIONS

def create_slug_drones(instance, new_slug=None):
	slug = slugify(instance.label)
	if new_slug is not None:
		slug = new_slug
	qs = SellDrone.objects.filter(slug=slug).order_by("-id")
	exists = qs.exists()
	if exists:
		new_slug = "%s-%s" %(slug, qs.first().id)
		return create_slug_drones(instance, new_slug=new_slug)
	return slug

#SIGNALS
def pre_save_selldrone_receiver(sender, instance, *args, **kwargs):
	label = str(instance.brand) + " " + str(instance.model)+ " " + str(instance.pk)
	instance.label = label
	if not instance.slug:
		instance.slug = create_slug_drones(instance)

#pre_save.connect(pre_save_selldrone_receiver, sender=SellDrone)


def post_save_selldrone_receiver(sender, instance, *args, **kwargs):
	instance.indexing()
	if instance.status == "Expiry":
		send_expired_notification(instance.seller)

post_save.connect(post_save_selldrone_receiver, sender=SellDrone)


def _delete_file(path):
   """ Deletes image file from filesystem. """
   if os.path.isfile(path):
	   os.remove(path)

@receiver(models.signals.post_delete, sender=ForSaleActualPhoto)
def delete_file(sender, instance, *args, **kwargs):
	""" Deletes image files on `post_delete` """
	if instance.actual_photo:
		_delete_file(instance.actual_photo.path)
	if instance.watermarked:
		_delete_file(instance.watermarked.path)

def delete_document(index, id):
 	s = Search(index=index).query('match', id=id)
 	response = s.delete()


@receiver(models.signals.pre_delete, sender=SellCamera)
def delete_still_document(sender, instance, *args, **kwargs):		
	instance.status = 'Deleted'
	instance.save()
	#delete_document('ForSaleIndex', instance.id)

@receiver(models.signals.pre_delete, sender=SellVideoCamera)
def delete_video_document(sender, instance, *args, **kwargs):		
	instance.status = 'Deleted'
	instance.save()
	#delete_document('ForSaleVideoIndex', instance.id)

@receiver(models.signals.pre_delete, sender=SellLenses)
def delete_lens_document(sender, instance, *args, **kwargs):		
	instance.status = 'Deleted'
	instance.save()
	#delete_document('ForSaleLensIndex', instance.id)

@receiver(models.signals.pre_delete, sender=SellAccessories)
def delete_acc_document(sender, instance, *args, **kwargs):		
	instance.status = 'Deleted'
	instance.save()
	#delete_document('ForSaleAccessoriesIndex', instance.id)

@receiver(models.signals.pre_delete, sender=SellLighting)
def delete_light_document(sender, instance, *args, **kwargs):		
	instance.status = 'Deleted'
	instance.save()
	#delete_document('ForSaleLightingIndex', instance.id)

@receiver(models.signals.pre_delete, sender=SellDrone)
def delete_dron_document(sender, instance, *args, **kwargs):		
	instance.status = 'Deleted'
	instance.save()
	#delete_document('ForSaleDronesIndex', instance.id)


