import django_tables2 as tables
from .models import SellCamera, SellVideoCamera, SellLenses, SellAccessories, SellLighting, SellDrone 
from django.utils.html import format_html
import itertools

class UserColumn(tables.Column):
	def render(self, value):
		return format_html('<a /admin/profiles/profile/{}/change/>{}</a>', value.id, value)

class PriceColumn(tables.Column):
    def render(self, value):
        return int(value)


class StillTable(tables.Table):
	row = tables.Column(attrs={'th': {'scope': 'col', 'class': 'rowcolumn'}}, empty_values=(), verbose_name= '', orderable=False)
	product_id = tables.Column(attrs={'th': {'scope': 'col'}})
	brand = tables.TemplateColumn('<a href="/api/reports/{{status}}/{{record.item.cam_type.brand.company}}/{{username}}/">{{record.item.cam_type.brand.company}}</a>', accessor='item.cam_type.brand.company', verbose_name= 'Brand', attrs={'th': {'scope': 'col'}})
	model = tables.Column(attrs={'th': {'scope': 'col'}}, accessor='item.model')
	country = tables.Column(attrs={'th': {'scope': 'col'}}, accessor='seller.country')
	city = tables.Column(attrs={'th': {'scope': 'col'}}, accessor='seller.city')
	price = PriceColumn(attrs={'th': {'scope': 'col'}})
	seller = tables.TemplateColumn('<a href="/api/reports/{{status}}/{{brand}}/{{record.seller}}/">{{record.seller}}</a>', attrs={'th': {'scope': 'col'}})
	condition = tables.Column(attrs={'th': {'scope': 'col'}})

	class Meta:
		model = SellCamera
		attrs = {"class": "table table-striped fixtablelayout table-90", "id": "reporttable", "id": "reporttable"}
		fields = ('product_id', 'price', 'seller', 'condition')
		sequence = ('row', 'product_id', 'brand', 'model', 'country', 'city', 'price', 'seller', 'condition')
		

	def __init__(self, *args, **kwargs):
		super(StillTable, self).__init__(*args, **kwargs)
		self.counter = itertools.count(start=1)

	def render_row(self):
		return '%d' % next(self.counter)


class VideoTable(tables.Table):
	row = tables.Column(attrs={'th': {'scope': 'col', 'class': 'rowcolumn'}}, empty_values=(), verbose_name= '',orderable=False)
	product_id = tables.Column(attrs={'th': {'scope': 'col'}})
	brand = tables.TemplateColumn('<a href="/api/reports/{{status}}/{{record.item.cam_type.brand.company}}/">{{record.item.cam_type.brand.company}}</a>', accessor='item.cam_type.brand.company', verbose_name= 'Brand', attrs={'th': {'scope': 'col'}})
	model = tables.Column(attrs={'th': {'scope': 'col'}}, accessor='item.model')
	country = tables.Column(attrs={'th': {'scope': 'col'}}, accessor='seller.country')
	city = tables.Column(attrs={'th': {'scope': 'col'}}, accessor='seller.city')
	price = PriceColumn(attrs={'th': {'scope': 'col'}})
	#seller = tables.TemplateColumn('<a href="/admin/profiles/profile/{{record.seller.id}}/change/" target="blank">{{record.seller}}</a>', attrs={'th': {'scope': 'col'}})
	seller = tables.TemplateColumn('<a href="/api/reports/{{status}}/{{brand}}/{{record.seller}}/">{{record.seller}}</a>', attrs={'th': {'scope': 'col'}})
	condition = tables.Column(attrs={'th': {'scope': 'col'}})


	class Meta:
		model = SellVideoCamera
		attrs = {"class": "table table-striped fixtablelayout table-90", "id": "reporttable"}
		fields = ('price', 'seller', 'condition')
		sequence = ('row', 'product_id', 'brand', 'model', 'country', 'city', 'price', 'seller', 'condition')
		

	def __init__(self, *args, **kwargs):
		super(VideoTable, self).__init__(*args, **kwargs)
		self.counter = itertools.count(start=1)

	def render_row(self):
		return '%d' % next(self.counter)


class LensTable(tables.Table):
	row = tables.Column(attrs={'th': {'scope': 'col', 'class': 'rowcolumn'}}, empty_values=(), verbose_name= '',orderable=False)
	product_id = tables.Column(attrs={'th': {'scope': 'col'}})
	brand = tables.TemplateColumn('<a href="/api/reports/{{status}}/{{record.item.type_of_lens.brand.company}}/">{{record.item.type_of_lens.brand.company}}</a>', accessor='item.type_of_lens.brand.company', verbose_name= 'Brand', attrs={'th': {'scope': 'col'}})
	model = tables.Column(attrs={'th': {'scope': 'col'}}, accessor='item.model')
	country = tables.Column(attrs={'th': {'scope': 'col'}}, accessor='seller.country')
	city = tables.Column(attrs={'th': {'scope': 'col'}}, accessor='seller.city')
	price = tables.Column(attrs={'th': {'scope': 'col'}})
	#seller = tables.TemplateColumn('<a href="/admin/profiles/profile/{{record.seller.id}}/change/" target="blank">{{record.seller}}</a>', attrs={'th': {'scope': 'col'}})
	seller = tables.TemplateColumn('<a href="/api/reports/{{status}}/{{brand}}/{{record.seller}}/">{{record.seller}}</a>', attrs={'th': {'scope': 'col'}})
	condition = tables.Column(attrs={'th': {'scope': 'col'}})


	class Meta:
		model = SellLenses
		attrs = {"class": "table table-striped fixtablelayout table-90", "id": "reporttable"}
		fields = ('price', 'seller', 'condition')
		sequence = ('row', 'product_id', 'brand', 'model', 'country', 'city', 'price', 'seller', 'condition')
		

	def __init__(self, *args, **kwargs):
		super(LensTable, self).__init__(*args, **kwargs)
		self.counter = itertools.count(start=1)

	def render_row(self):
		return '%d' % next(self.counter)


class AccessoriesTable(tables.Table):
	row = tables.Column(attrs={'th': {'scope': 'col', 'class': 'rowcolumn'}}, empty_values=(), verbose_name= '', orderable=False)
	product_id = tables.Column(attrs={'th': {'scope': 'col'}})
	country = tables.Column(attrs={'th': {'scope': 'col'}}, accessor='seller.country')
	city = tables.Column(attrs={'th': {'scope': 'col'}}, accessor='seller.city')
	model = tables.Column(attrs={'th': {'scope': 'col'}})
	price = tables.Column(attrs={'th': {'scope': 'col'}})
	price = PriceColumn(attrs={'th': {'scope': 'col'}})
	#seller = tables.TemplateColumn('<a href="/admin/profiles/profile/{{record.seller.id}}/change/" target="blank">{{record.seller}}</a>', attrs={'th': {'scope': 'col'}})
	seller = tables.TemplateColumn('<a href="/api/reports/{{status}}/{{brand}}/{{record.seller}}/">{{record.seller}}</a>', attrs={'th': {'scope': 'col'}})
	condition = tables.Column(attrs={'th': {'scope': 'col'}})
	category = tables.Column(attrs={'th': {'scope': 'col'}})


	class Meta:
		model = SellAccessories
		attrs = {"class": "table table-striped fixtablelayout", "id": "reporttable"}
		fields = ('brand', 'model', 'price', 'seller', 'condition', 'category')
		sequence = ('row', 'product_id', 'brand', 'model', 'country', 'city', 'price', 'seller', 'condition', 'category')
		

	def __init__(self, *args, **kwargs):
		super(AccessoriesTable, self).__init__(*args, **kwargs)
		self.counter = itertools.count(start=1)

	def render_row(self):
		return '%d' % next(self.counter)


class LightingTable(tables.Table):
	row = tables.Column(attrs={'th': {'scope': 'col', 'class': 'rowcolumn'}}, empty_values=(), verbose_name= '', orderable=False)
	product_id = tables.Column(attrs={'th': {'scope': 'col'}})
	country = tables.Column(attrs={'th': {'scope': 'col'}}, accessor='seller.country')
	city = tables.Column(attrs={'th': {'scope': 'col'}}, accessor='seller.city')
	model = tables.Column(attrs={'th': {'scope': 'col'}})
	#price = tables.Column(attrs={'th': {'scope': 'col'}})
	price = PriceColumn(attrs={'th': {'scope': 'col'}})
	#seller = tables.TemplateColumn('<a href="/admin/profiles/profile/{{record.seller.id}}/change/" target="blank">{{record.seller}}</a>',attrs={'th': {'scope': 'col'}})
	seller = tables.TemplateColumn('<a href="/api/reports/{{status}}/{{brand}}/{{record.seller}}/">{{record.seller}}</a>', attrs={'th': {'scope': 'col'}})
	condition = tables.Column(attrs={'th': {'scope': 'col'}})
	category = tables.Column(attrs={'th': {'scope': 'col'}})


	class Meta:
		model = SellLighting
		attrs = {"class": "table table-striped fixtablelayout", "id": "reporttable"}
		fields = ('brand', 'model', 'price', 'seller', 'condition', 'category')
		sequence = ('row', 'product_id', 'brand', 'model', 'country', 'city', 'price', 'seller', 'condition', 'category')
		

	def __init__(self, *args, **kwargs):
		super(LightingTable, self).__init__(*args, **kwargs)
		self.counter = itertools.count(start=1)

	def render_row(self):
		return '%d' % next(self.counter)


class DroneTable(tables.Table):
	row = tables.Column(attrs={'th': {'scope': 'col', 'class': 'rowcolumn'}}, empty_values=(), verbose_name= '', orderable=False)
	product_id = tables.Column(attrs={'th': {'scope': 'col'}})
	country = tables.Column(attrs={'th': {'scope': 'col'}}, accessor='seller.country')
	city = tables.Column(attrs={'th': {'scope': 'col'}}, accessor='seller.city')
	brand = tables.Column(attrs={'th': {'scope': 'col'}})
	model = tables.Column(attrs={'th': {'scope': 'col'}})
	price = PriceColumn(attrs={'th': {'scope': 'col'}})
	#seller = tables.TemplateColumn('<a href="/admin/profiles/profile/{{record.seller.id}}/change/" target="blank">{{record.seller}}</a>', attrs={'th': {'scope': 'col'}})
	seller = tables.TemplateColumn('<a href="/api/reports/{{status}}/{{brand}}/{{record.seller}}/">{{record.seller}}</a>', attrs={'th': {'scope': 'col'}})
	condition = tables.Column(attrs={'th': {'scope': 'col'}})
	drone_type = tables.Column(attrs={'th': {'scope': 'col'}}, verbose_name= 'Category')

	class Meta:
		model = SellDrone
		attrs = {"class": "table table-striped fixtablelayout", "id": "reporttable"}
		fields = ('brand', 'model', 'price', 'seller', 'condition', 'drone_type')
		sequence = ('row', 'product_id', 'brand', 'model', 'country', 'city', 'price', 'seller', 'condition', 'drone_type')
		

	def __init__(self, *args, **kwargs):
		super(DroneTable, self).__init__(*args, **kwargs)
		self.counter = itertools.count(start=1)

	def render_row(self):
		return '%d' % next(self.counter)