from django.apps import AppConfig


class ForsaleConfig(AppConfig):
    name = 'forsale'

    def ready(self):
        import forsale.signal