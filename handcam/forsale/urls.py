from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from forsale import views 
 
urlpatterns = [
	url(r'^camera/upload/$', views.UploadCameraView.as_view(), name='sell-camera-upload'),
	url(r'^lens_package/upload/$', views.UploadLensPackageView.as_view(), name='sell-camera-lens-package-upload'),
	url(r'^camera/lens_package/(?P<pk>[0-9]+)/$', views.GetLensPackageView.as_view(), name='sell-camera-lens-package'),
	url(r'^camera/accessories/upload/$', views.UploadStillAccessories.as_view(), name='sell-camera-accessories'),
	url(r'^camera/accessories/edit/$', views.EditStillAccessories.as_view(), name='edit-sell-camera-accessories'),
	url(r'^camera/accessories/(?P<pk>[0-9]+)/$', views.GetAccessoriesView.as_view(), name='get-camera-accessories'),
	url(r'^camera/edit/$', views.EditSaleCameraView.as_view(), name='edit-camera-upload'),
	url(r'^lens_package/edit/$', views.EditLensPackageView.as_view(), name='edit-lens-package'),
	url(r'^camera/remove_image/(?P<campk>[0-9]+)/(?P<photopk>[0-9]+)$', views.RemoveCameraPhoto.as_view(), name='edit-camera-remove-photo'),
	url(r'^camera/remove/(?P<pk>[0-9]+)$', views.RemoveSaleCameraView.as_view(), name='remove-camera-forsale'),


	url(r'^video-camera/upload/$', views.UploadVideoCameraView.as_view(), name='sell-video-camera-upload'),
	url(r'^video_lens_package/upload/$', views.UploadVideoLensPackageView.as_view(), name='sell-video-lens-package-upload'),
	url(r'^video/lens_package/(?P<pk>[0-9]+)/$', views.GetVideoLensPackageView.as_view(), name='sell-video-lens-package'),
	url(r'^video-camera/accessories/upload/$', views.UploadVideoAccessories.as_view(), name='sell-video-accessories'),
	url(r'^video-camera/accessories/edit/$', views.EditVideoAccessories.as_view(), name='edit-video-camera-accessories'),
	url(r'^video-camera/accessories/(?P<pk>[0-9]+)/$', views.GetVideoAccessoriesView.as_view(), name='get-video-camera-accessories'),
	url(r'^video-camera/edit/$', views.EditSaleVideoCameraView.as_view(), name='edit-video-camera-upload'),
	url(r'^video_lens_package/edit/$', views.EditVideoLensPackageView.as_view(), name='edit-video-lens-package'),
	url(r'^video-camera/remove_image/(?P<vidpk>[0-9]+)/(?P<photopk>[0-9]+)$', views.RemoveVideoCameraPhoto.as_view(), name='edit-video-camera-remove-photo'),
	url(r'^video-camera/remove/(?P<pk>[0-9]+)$', views.RemoveSaleVideoCameraView.as_view(), name='remove-video-camera-forsale'),

	url(r'^lens/upload/$', views.UploadLensView.as_view(), name='sell-lens-upload'),
	url(r'^lens/edit/$', views.EditSaleLensView.as_view(), name='edit-lens-upload'),
	url(r'^lens/remove_image/(?P<lenspk>[0-9]+)/(?P<photopk>[0-9]+)$', views.RemoveLensPhoto.as_view(), name='edit-lens-remove-photo'),
	url(r'^lens/remove/(?P<pk>[0-9]+)$', views.RemoveSaleLensView.as_view(), name='remove-lens-forsale'),

	url(r'^lighting/upload/$', views.UploadLightingView.as_view(), name='sell-lighting-upload'),
	url(r'^lighting/edit/$', views.EditSaleLightingView.as_view(), name='edit-lighting-upload'),
	url(r'^lighting/remove_image/(?P<lightpk>[0-9]+)/(?P<photopk>[0-9]+)$', views.RemoveLightingPhoto.as_view(), name='edit-lighting-remove-photo'),
	url(r'^lighting/remove/(?P<pk>[0-9]+)$', views.RemoveSaleLightingView.as_view(), name='remove-lighting-forsale'),

	url(r'^accessories/upload/$', views.UploadAccessoriesView.as_view(), name='sell-accessories-upload'),
	url(r'^accessories/edit/$', views.EditSaleAccessoriesView.as_view(), name='edit-accessories-upload'),
	url(r'^accessories/remove_image/(?P<accpk>[0-9]+)/(?P<photopk>[0-9]+)$', views.RemoveAccessoriesPhoto.as_view(), name='edit-accessories-remove-photo'),
	url(r'^accessories/remove/(?P<pk>[0-9]+)$', views.RemoveSaleAccessoriesView.as_view(), name='remove-accessories-forsale'),

	url(r'^drone/upload/$', views.UploadDroneView.as_view(), name='sell-drone-upload'),
	url(r'^drone/edit/$', views.EditSaleDroneView.as_view(), name='edit-drone-upload'),
	url(r'^drone/remove_image/(?P<dronepk>[0-9]+)/(?P<photopk>[0-9]+)$', views.RemoveDronePhoto.as_view(), name='edit-drone-remove-photo'),
	url(r'^drone/remove/(?P<pk>[0-9]+)$', views.RemoveSaleDroneView.as_view(), name='remove-drone-forsale'),
	
	url(r'^camera/index/$', views.load_forsaleindex, name='sell-camera-indexing'),
	url(r'^video/index/$', views.load_forsalevideoindex, name='sell-videocamera-indexing'),
	url(r'^lens/index/$', views.load_forsalelensindex, name='sell-lens-indexing'),

	url(r'^expiration-check/$', views.CheckForSaleExpirationCount.as_view(), name='check-forsale-expiration-count'),
	url(r'^reactivate-forsale/$', views.ActivateExpiredItem.as_view(), name='activate-forsale-item'),
	url(r'^sold-forsale/$', views.ChangeForsaleToSold.as_view(), name='change-sold-forsale'),
	url(r'^disable-forsale/$', views.DisableForSaleItem.as_view(), name='disable-forsale-item'),
	url(r'^enable-forsale/$', views.EnableForSaleItem.as_view(), name='enable-forsale-item'),
]