# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-11-23 00:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0010_profile_hide_phone'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='reset_passs',
            field=models.BooleanField(default=False),
        ),
    ]
