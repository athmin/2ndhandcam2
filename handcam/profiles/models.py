from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericRelation
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.db import models

from django.utils.safestring import mark_safe
from markdown_deux import markdown

from PIL import Image, ExifTags
from django.db.models.signals import post_save
import os
from django.conf import settings

from star_ratings.models import Rating


def upload_location(instance, filename):
	
	return "user_{0}/{1}".format(instance.user.username, filename)


def rotate_image(filepath):
	print(filepath)
	image = Image.open(filepath)
	print(image)
	for orientation in ExifTags.TAGS.keys():
		if ExifTags.TAGS[orientation] == 'Orientation':
			break
	if(image._getexif()):
		print("In EXIF")
		exif = dict(image._getexif().items())
		print(exif[orientation])
		if exif[orientation] == 3:
			image = image.rotate(180, expand=True)
		elif exif[orientation] == 6:
			image = image.rotate(270, expand=True)
		elif exif[orientation] == 8:
			image = image.rotate(90, expand=True)
		image.save(filepath)
		image.close()


class Profile(models.Model):
	user            = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
	date_of_birth	= models.DateTimeField(null=True, blank=True)
	city            = models.CharField(max_length=75, null=True, blank=True)
	country         = models.CharField(max_length=75, null=True, blank=True)
	phone_number	= models.CharField(max_length=20, null=True, blank=True)
	image           = models.ImageField(upload_to=upload_location, 
						null=True, 
						blank=True)
	reg_date        = models.DateTimeField(auto_now_add=True)
	verified		= models.BooleanField(default=False)
	hide_phone		= models.BooleanField(default=False)
	reset_pass		= models.BooleanField(default=False)
	

	def __str__(self):
		return str(self.user.username)


@receiver(post_save, sender=Profile, dispatch_uid="update_image_profile")
def update_image(sender, instance, **kwargs):
	if instance.image:
		#BASE_DIR = os.path.dirname(os.path.dirname(settings.BASE_DIR))
		fullpath = os.path.join(os.path.dirname(settings.BASE_DIR), instance.image.url)
		fullpath = fullpath.replace("/static/", "/static_cdn/")
		print(instance.image.url)
		#fullpath = BASE_DIR + instance.image.url
		print(fullpath)
		print(settings.MEDIA_ROOT)
		print(instance.image.url)
		rotate_image(fullpath)


class Review(models.Model):
	user 			= models.ForeignKey(Profile, related_name="user_review")
	reviewer		= models.ForeignKey(Profile, related_name="reviewer")
	comment			= models.TextField()
	ratings         = models.FloatField(default=None, null=True, blank=True)
	review_date		= models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return '%s' % self.review_date

	def get_markdown_comment(self):
		content = self.comment
		markdown_text = markdown(content)
		return mark_safe(markdown_text)


class Report(models.Model):
	reporter 		= models.ForeignKey(Profile, related_name="reporter_profile")
	seller			= models.ForeignKey(Profile, related_name="seller_report")
	issue			= models.TextField()
	review_date		= models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return '%s' % self.reporter

	def get_markdown_issue(self):
		content = self.issue
		markdown_text = markdown(content)
		return mark_safe(markdown_text)


class Block(models.Model):
	email = models.CharField(max_length=75, null=True, blank=True)
	date  = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return '%s' % self.email

	class Meta:
		verbose_name = 'Blocked Users'
		verbose_name_plural = 'Blocked Users'


@receiver(post_delete, sender=Profile)
def auto_delete_user_with_profile(sender, instance, **kwargs):
	instance.user.delete()
