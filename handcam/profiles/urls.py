from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views 
 
urlpatterns = [
	url(r'^dashboard$', views.ProfileDashboardView.as_view(), name='profile_dashboard'),
	url(r'^active-sales/(?P<pk>[0-9]+)/$', views.MoreFromSellerView.as_view(), name='seller_active_sales'),
	url(r'^public-profile/(?P<username>\w+)$', views.MoreFromSellerView.as_view(), name='seller_public_profile'),
	url(r'^post-review$', views.CreateSellerReview.as_view(), name='post-review'),
	url(r'^get-review/(?P<username>\w+)$', views.GetSellerReview.as_view(), name='get-review'),
	url(r'^post-report$', views.CreateSellerReport.as_view(), name='post-report'),
	url(r'^get-report/(?P<username>\w+)$', views.GetSellerReport.as_view(), name='get-report'),
	url(r'^user-credentials$', views.GetProfile.as_view(), name='get-profile'),
	url(r'^edit-user/$', views.EditUser.as_view(), name='edit-user'),
	url(r'^edit-profile/$', views.EditProfile.as_view(), name='edit-profile'),
	url(r'^seller_profile/(?P<pk>[0-9]+)$', views.GetSellerProfile.as_view(), name='seller-profile'),
	url(r'^delete-user$', views.DeleteUser.as_view(), name='delete-user'),
]
