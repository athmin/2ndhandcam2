from __future__ import (absolute_import, division, print_function, unicode_literals)
from rest_framework import serializers

from django.contrib.auth.models import User
from .models import Profile, Review, Report

class UserSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = User
		fields = [
			'pk',
			'username', 
			'first_name', 
			'last_name', 
			'email',
			'date_joined'
		]


class ProfileSerializer(serializers.Serializer):

	class Meta:
		models = Profile
		fields = [
			'user',
			'city',
			'country',
			'date_of_birth',
			'phone_number',
			'image',
		]


class ProfileDetailSerializer(serializers.ModelSerializer):
	user = UserSerializer()

	class Meta:
		model = Profile
		fields = [
			'pk',
			'user',
			'city',
			'country',
			'date_of_birth',
			'phone_number',
			'image',
			'hide_phone'
		]

	def get_user(self, obj):
		
		return obj.user.get_full_name()


class ReviewSerializer(serializers.ModelSerializer):
	user = ProfileDetailSerializer()
	reviewer = ProfileDetailSerializer()
	
	class Meta:
		model = Review
		fields = [
			'id',
			'user',
			'reviewer',
			'comment',
			'ratings',
			'review_date'
		]


class CreateReviewSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Review
		fields = [
			'user',
			'reviewer',
			'comment',
			'ratings'
		]

	def create(self, validated_data):
	
		# create review
		print (validated_data)
		review = Review.objects.create(
			user = validated_data['user'],
			reviewer = validated_data['reviewer'],
			comment = validated_data['comment'],
			ratings = validated_data['ratings'],
		)

		return review


class ReportSerializer(serializers.ModelSerializer):
	reporter = ProfileDetailSerializer()
	seller = ProfileDetailSerializer()
	
	class Meta:
		model = Report
		fields = [
			'id',
			'reporter',
			'seller',
			'issue',
			'date'
		]


class CreateReportSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Report
		fields = [
			'reporter',
			'seller',
			'issue',
		]

	def create(self, validated_data):
	
		# create report
		print (validated_data)
		report = Report.objects.create(
			reporter = validated_data['reporter'],
			seller = validated_data['seller'],
			issue = validated_data['issue']
		)

		return report