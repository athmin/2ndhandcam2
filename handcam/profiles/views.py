from django.contrib.auth.models import User
from django.http import Http404
from django.shortcuts import render
from django.utils.translation import ugettext_lazy as _

from rest_framework import generics, status
from rest_framework.decorators import permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from forsale.models import SellCamera, SellVideoCamera, SellLenses, ForSaleActualPhoto, SellLighting, SellAccessories, SellDrone
from .models import Profile, Review
from .serializers import ProfileSerializer, ReviewSerializer, ProfileDetailSerializer, CreateReviewSerializer, \
	CreateReportSerializer
from forsale.serializers import LensForSaleSerializer, GetActualPhotosSerializer, \
	GetCamForSaleSerializer, SellerDetailSerializer, CamForSaleSerializer, \
	GetVideoCamForSaleSerializer, VideoCamForSaleSerializer, GetLensForSaleSerializer, \
	LightingForSaleSerializer, GetLightingForSaleSerializer, AccessoriesForSaleSerializer, \
	GetAccessoriesForSaleSerializer, GetDroneForSaleSerializer, DroneForSaleSerializer

from messaging.utils import send_expired_notification

import datetime
import time
from dateutil import parser as date_parser


def get_user(userpk):
	try:
		user = User.objects.get(pk=userpk)
		return user
	except User.DoesNotExist:
		raise Http404 

def get_user_by_username(username):
	try:
		user = User.objects.get(username=username)
		print ("fetching user")
		print (user)
		return user
	except User.DoesNotExist:	
		status = {"status_code": 404, "detail": _("User does not exist!")}
		return Response(status)

def get_profile(user):
	try:
		profile = Profile.objects.get(user=user)
		return profile
	except Profile.DoesNotExist:	
		status = {"status_code": 404, "detail": _("Profile does not exist!")}
		return Response(status)

def delete_profile(pk):
	try:
		profile = Profile.objects.get(pk=pk)
		user = User.objects.get(pk=profile.user_id)
		user.is_active = False
		user.save()
		status = {"status_code": 200, "detail": _("User Profile Deleted Successfully")}
		return Response(status)
	except Profile.DoesNotExist:
		status = {"status_code": 404, "detail": _("User Profile does not exist!")}
		return Response(status)

def get_profile_by_pk(pk):
	try:
		profile = Profile.objects.get(pk=pk)
		return profile
	except Profile.DoesNotExist:	
		status = {"status_code": 404, "detail": _("Profile does not exist!")}
		return Http404

def get_reviews(profile):
	try:
		reviews = Review.objects.filter(user=profile).order_by('-review_date')
		return reviews
	except Review.DoesNotExist:
		status = {"status_code": 404, "detail": _("User has no reviews.")}
		return Response(status)


@permission_classes((IsAuthenticated, ))
class EditUser(generics.CreateAPIView):

	def post(self, request, format=None):
		pk = request.data.get('pk')
		username = request.data.get('username')
		first_name = request.data.get('first_name')
		last_name = request.data.get('last_name')
		email = request.data.get('email')

		user = User.objects.get(pk=pk)
		user.username = username
		user.first_name = first_name
		user.last_name = last_name
		user.email = email
		user.save()

		#get profile
		profile = get_profile(user=user)

		# get for sale items
		cameras = SellCamera.objects.filter(seller=profile)
		videos = SellVideoCamera.objects.filter(seller=profile)
		lenses = SellLenses.objects.filter(seller=profile)
		lightings = SellLighting.objects.filter(seller=profile)
		accessories = SellAccessories.objects.filter(seller=profile)
		drones = SellDrone.objects.filter(seller=profile)

		# update searchkit index
		if cameras:
			for cam in cameras:
				cam.indexing()
		if videos:
			for vid in videos:
				vid.indexing()
		if lenses:
			for lens in lenses:
				lens.indexing()
		if lightings:
			for light in lightings:
				light.indexing()
		if accessories:
			for acc in accessories:
				acc.indexing()
		if drones:
			for drone in drones:
				drone.indexing()


		status = {
			'status' : "Successfully save changes"
		}

		return Response(status)


@permission_classes((IsAuthenticated, ))
class EditProfile(generics.CreateAPIView):

	def post(self, request, format=None):
		pk = request.data.get('pk')
		city = request.data.get('city')
		country = request.data.get('country')
		phone_number = request.data.get('phone_number')
		date_of_birth = None
		print(request.data.get('date_of_birth'))
		print(type(request.data.get('date_of_birth')))
		
		if (request.data.get('date_of_birth') != 'null') and (request.data.get('date_of_birth') != ''):
			date_of_birth = date_parser.parse(request.data.get('date_of_birth'))

		if request.data.get('hide_phone') == 'true':
			hide_phone = True
		else:
			hide_phone = False
		image = request.FILES
		

		profile = Profile.objects.get(pk=pk)
		profile.city = city
		profile.country = country
		profile.phone_number = phone_number
		profile.date_of_birth = date_of_birth
		profile.hide_phone = hide_phone
		for ap in image.getlist('image'):
			if ap:
				
				profile.image = ap
				

		profile.save()

		status = {
			'status' : "Successfully save changes"
		}

		return Response(status)


@permission_classes((AllowAny, ))
class GetProfile(APIView):

	def get(self, request, format=None):
		
		user = request.user
		profile = get_profile(user)

		result = {
			'user_profile' : ProfileDetailSerializer(profile).data
		}

		return Response(result)


@permission_classes((AllowAny, ))
class ProfileDashboardView(APIView):

	def get_forsale_cam(self, profile):
		
		forsale_cam = SellCamera.objects.filter(seller=profile)
		return forsale_cam

	def get_expired_forsale_cam(self, profile):
		
		expired_cam = SellCamera.objects.filter(seller=profile, expiry__lte=datetime.datetime.now())
		for item in expired_cam:
			item.status = "Expired"
			item.save()

			#alert = send_expired_notification(item.seller)
		return expired_cam

	def get_forsale_videocam(self, profile):
		
		forsale_videocam = SellVideoCamera.objects.filter(seller=profile)
		return forsale_videocam

	def get_expired_forsale_videocam(self, profile):
		
		expired_videocam = SellVideoCamera.objects.filter(seller=profile, expiry__lte=datetime.datetime.now())
		for item in expired_videocam:
			item.status = "Expired"
			item.save()

			#alert = send_expired_notification(item.seller)
		return expired_videocam
		

	def get_forsale_lenses(self, profile):
		
		forsale_lenses = SellLenses.objects.filter(seller=profile)
		return forsale_lenses

	def get_expired_forsale_lens(self, profile):
		
		expired_lens = SellLenses.objects.filter(seller=profile, expiry__lte=datetime.datetime.now())
		for item in expired_lens:
			item.status = "Expired"
			item.save()

			#alert = send_expired_notification(item.seller)
		return expired_lens

	def get_forsale_lighting(self, profile):
			
		forsale_lighting = SellLighting.objects.filter(seller=profile)
		return forsale_lighting

	def get_expired_forsale_lighting(self, profile):
			
		expired_lighting = SellLighting.objects.filter(seller=profile, expiry__lte=datetime.datetime.now())
		for item in expired_lighting:
			item.status = "Expired"
			item.save()

			#alert = send_expired_notification(item.seller)
		return expired_lighting

	def get_forsale_accessories(self, profile):
			
		forsale_accessories = SellAccessories.objects.filter(seller=profile)
		return forsale_accessories

	def get_expired_forsale_accessories(self, profile):
			
		expired_accessories = SellAccessories.objects.filter(seller=profile, expiry__lte=datetime.datetime.now())
		for item in expired_accessories:
			item.status = "Expired"
			item.save()

			#alert = send_expired_notification(item.seller)
		return expired_accessories

	def get_forsale_drones(self, profile):
			
		forsale_drones = SellDrone.objects.filter(seller=profile)
		return forsale_drones

	def get_expired_forsale_drones(self, profile):
			
		expired_drones = SellDrone.objects.filter(seller=profile, expiry__lte=datetime.datetime.now())
		for item in expired_drones:
			item.status = "Expired"
			item.save()

			#alert = send_expired_notification(item.seller)
		return expired_drones
		

	def get(self, request, format=None):
		
		user = request.user
		profile = get_profile(user)
		reviews = get_reviews(profile)
		cameras = self.get_forsale_cam(profile)
		video_cam = self.get_forsale_videocam(profile)
		lenses = self.get_forsale_lenses(profile)
		accessories = self.get_forsale_accessories(profile)
		lightings = self.get_forsale_lighting(profile)
		drones = self.get_forsale_drones(profile)

		xcameras = self.get_expired_forsale_cam(profile)
		xvideo_cam = self.get_expired_forsale_videocam(profile)
		xlens = self.get_expired_forsale_lens(profile)
		xlightings = self.get_expired_forsale_lighting(profile)
		xaccessories = self.get_expired_forsale_accessories(profile)
		xdrones = self.get_expired_forsale_drones(profile)

		expired_items = len(xcameras) + len(xvideo_cam) + len(xlens) + len(xlightings) + len(xaccessories) + len(xdrones)

		result = {
			'cameras' : GetCamForSaleSerializer(cameras, many=True).data,
			'video_cameras' : GetVideoCamForSaleSerializer(video_cam, many=True).data,
			'lenses' : GetLensForSaleSerializer(lenses, many=True).data,
			'accessories' : GetAccessoriesForSaleSerializer(accessories, many=True).data,
			'lightings' : GetLightingForSaleSerializer(lightings, many=True).data,
			'drones' : GetDroneForSaleSerializer(drones, many=True).data,
			'expired_items' : expired_items,
			'user_profile' : ProfileDetailSerializer(profile).data
		}

		return Response(result)


@permission_classes((AllowAny, ))
class MoreFromSellerView(APIView):
	
	def get_active_forsale_cam(self, profile):
		
		forsale_cam = SellCamera.objects.filter(seller=profile, status="Active")
		return forsale_cam
		

	def get_active_forsale_lenses(self, profile):
		
		forsale_lenses = SellLenses.objects.filter(seller=profile, status="Active")
		return forsale_lenses

	def get_active_forsale_lighting(self, profile):
		
		forsale_lighting = SellLighting.objects.filter(seller=profile, status="Active")
		return forsale_lighting

	def get_active_forsale_accessories(self, profile):
		
		forsale_accessories = SellAccessories.objects.filter(seller=profile, status="Active")
		return forsale_accessories

	def get_active_forsale_drones(self, profile):
		
		forsale_drones = SellDrone.objects.filter(seller=profile, status="Active")
		return forsale_drones

	def get(self, request, username,format=None):
		print ("requesting profile for user: " + username)
		#user = get_user(username)
		profile = get_profile_by_pk(username)
		reviews = get_reviews(profile)
		cameras = self.get_active_forsale_cam(profile)
		lenses = self.get_active_forsale_lenses(profile)
		lightings = self.get_active_forsale_lighting(profile)
		accessories = self.get_active_forsale_accessories(profile)
		drones = self.get_active_forsale_drones(profile)

		result = {
			'profile' : ProfileDetailSerializer(profile).data,
			'reviews' : ReviewSerializer(reviews, many=True).data,
			'cameras' : GetCamForSaleSerializer(cameras, many=True).data,
			'lenses' : GetLensForSaleSerializer(lenses, many=True).data,
			'lightings' : GetDroneForSaleSerializer(drones, many=True).data,
			'accessories' : GetAccessoriesForSaleSerializer(accessories, many=True).data,
			'drones' : GetDroneForSaleSerializer(drones, many=True).data
		}

		return Response(result)


@permission_classes((AllowAny, ))
class GetSellerProfile(APIView):

	def get(self, request, pk,format=None):
		print ("requesting profile with pk: " + pk)
		
		profile = get_profile_by_pk(pk)
		

		result = {
			'hide_phone' : profile.hide_phone,
		}

		return Response(result)


@permission_classes((IsAuthenticated, ))
class CreateSellerReview(generics.CreateAPIView):

	def post(self, request, format=None):
		
		reviewer = get_profile(request.user) 
		seller_pk = request.data.get('seller', None)
		#print("seller username: " + seller_username)
		#seller_user = get_user(seller_username)
		seller_profile = get_profile_by_pk(seller_pk)
		review_data = {
			'user' : seller_pk,
			'reviewer' : reviewer.pk,
			'comment' : request.data.get('comment', None),
			'ratings' : request.data.get('ratings', None),
		}
		print (review_data)
		serializer = CreateReviewSerializer(data=review_data)
		if serializer.is_valid():
			
			serializer.save()
			reviews = get_reviews(seller_profile)
			seller_reviews = ReviewSerializer(reviews, many=True).data

			return Response(seller_reviews, status=status.HTTP_201_CREATED)
		print('invalid serializer')
		print(serializer.errors)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@permission_classes((AllowAny, ))
class GetSellerReview(APIView):

	def get(self, request, username, format=None):
		#user = get_user(username)
		#profile = get_profile_by_pk(username)
		reviews = get_reviews(username)
		
		serializer = ReviewSerializer(reviews, many=True)
		print (serializer.data)
		return Response(serializer.data)


@permission_classes((IsAuthenticated, ))
class CreateSellerReport(generics.CreateAPIView):

	def post(self, request, format=None):
		
		reporter = get_profile(request.user) 
		seller_pk = request.data.get('seller', None)
		#seller_user = get_user_by_username(seller_username)
		#seller_profile = get_profile(seller_user)
		seller_profile = get_profile_by_pk(seller_pk)
		report_data = {
			'reporter' : reporter.pk,
			'seller' : seller_profile.pk,
			'issue' : request.data.get('issue', None)
		}
	
		serializer = CreateReportSerializer(data=report_data)
		if serializer.is_valid():
			
			serializer.save()

			return Response(status=status.HTTP_201_CREATED)
		print('invalid serializer')
		print(serializer.errors)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@permission_classes((AllowAny, ))
class GetSellerReport(APIView):

	def get(self, request, username, format=None):
		user = get_user_by_username(username)
		profile = get_profile(user)
		reviews = get_reviews(profile.pk)
		for rev in reviews:
			print (rev.comment)
		serializer = ReviewSerializer(reviews, many=True)
		print (serializer.data)
		return Response(serializer.data)


@permission_classes((IsAuthenticated, ))
class DeleteUser(generics.CreateAPIView):

	def post(self, request, format=None):
		pk = request.data.get('pk')
		resp = delete_profile(pk)
		return resp
