from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib import messages
from .models import Profile, Review, Report, Block
from django.contrib.auth.models import Group



def block_user(modeladmin, request, queryset):
	for q in queryset:
		user = Block.objects.create(email=q.user.email)

	if queryset.count() == 1:
		message_bit = "1 email address was"
	else:
		message_bit = "%s stories were" % queryset.count()
	messages.add_message(request, messages.SUCCESS, "%s successfully added to blocked email database." % message_bit)
		
block_user.short_description = "Block this email address"

class ProfileDetailAdmin(admin.ModelAdmin):
	def email_address(self):
            if self.user:
                return self.user.email
            else:
                return ""

	list_display = ('user', email_address, 'city', 'country', 'phone_number')
	list_filter = ('user__username', 'city', 'country')
	search_fields = ('user__username', 'city', 'country')
	actions = [block_user]


class ReviewDetailAdmin(admin.ModelAdmin):

	list_display = ('user', 'reviewer', 'ratings', 'comment', 'review_date' )


class ReportDetailAdmin(admin.ModelAdmin):
	list_display = ('reporter', 'seller', 'issue', 'review_date')


class BlockDetailAdmin(admin.ModelAdmin):
	list_display = ('email', 'date')
	search_fields = ('email',)


admin.site.register(Profile, ProfileDetailAdmin)
admin.site.register(Review, ReviewDetailAdmin)
admin.site.register(Report, ReportDetailAdmin)
admin.site.register(Block, BlockDetailAdmin)
admin.site.unregister(Group)

