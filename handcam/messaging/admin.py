from django.contrib import admin
from .models import Message

class MessageDetailAdmin(admin.ModelAdmin):
	list_display = ('sender', 'recipient', 'subject', 'body', 'sent_at', 'read')

admin.site.register(Message, MessageDetailAdmin)	


