from __future__ import (absolute_import, division, print_function, unicode_literals)
from rest_framework import serializers
from .models import Message
from django.contrib.auth.models import User
from .models import Profile


class UserSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = User
		fields = [
			'username', 
			'first_name', 
			'last_name', 
			'email',
		]


class ProfileDetailSerializer(serializers.ModelSerializer):
	user = UserSerializer()

	class Meta:
		model = Profile
		fields = [
			'user',
			'city',
			'country',
			'phone_number',
			'image',
			'pk'
		]

	def get_user(self, obj):
		
		return obj.user.get_full_name()


class GetMessageSerializer(serializers.ModelSerializer):
	sender = ProfileDetailSerializer()
	recipient = ProfileDetailSerializer()

	class Meta:
		model = Message
		fields = [
			'parent',
			'id',
			'body',
			'subject',
			'sender',
			'recipient',
			'read',
			'sent_at',
			]


class CreateMessageSerializer(serializers.ModelSerializer):

	class Meta:
		model = Message
		fields = [
			'parent',
			'subject',
			'body',
			'sender',
			'recipient',
			]