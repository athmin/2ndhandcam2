from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from messaging import views 
 
urlpatterns = [
	url(r'^create/$', views.CreateMessageView.as_view(), name='create_message'),
	url(r'^get-messages/$', views.UserMessagesView.as_view(), name='get_message'),
	url(r'^change-status/(?P<pk>[0-9]+)$', views.ChangeMessageStatusView.as_view(), name='change_message_status'),
	url(r'^delete/(?P<parent>.+)$', views.DeleteMessagePkView.as_view(), name='delete_message'),
	url(r'^delete-all/$', views.DeleteAllMessages.as_view(), name='delete_all_messages'),
]