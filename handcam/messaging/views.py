from django.shortcuts import render
from django.db.models import Q
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import Message
from profiles.models import Profile

from .serializers import GetMessageSerializer, CreateMessageSerializer
from django.conf import settings
from socketIO_client import SocketIO
from urllib.parse import urlparse

#
# HELPER FUNCTIONS
#
def get_profile_by_user(user):
	profile = ''
	try:
		profile = Profile.objects.get(user=user)
	except Profile.DoesNotExist:
		status = {"status_code": 404, "detail": "User Profile does not exist!"}
		return Response(status)

	return profile	

def get_profile_by_username(username):
	profile = ''
	try:
		profile = Profile.objects.get(user__username=username)
	except Profile.DoesNotExist:
		status = {"status_code": 404, "detail": "User Profile does not exist!"}
		return Response(status)

	return profile	

def get_profile_by_pk(pk):
	profile = ''
	try:
		profile = Profile.objects.get(pk=pk)
	except Profile.DoesNotExist:
		status = {"status_code": 404, "detail": "User Profile does not exist!"}
		return Response(status)

	return profile	

# COUNT NUMBER OF UNREAD MESSAGES
def get_unread_messages_count(user_messages):

	count = 0
	for message in user_messages:
		if message.read == False:
			count += 1

	return count


def emit_newmessage_in_socket(pk):
	parsedurl = urlparse(settings.NODEJS_SOCKET_URL)
	baseurl = "%s://%s" % (parsedurl.scheme,parsedurl.hostname)
	baseport = parsedurl.port if parsedurl.port != None else 80
	# Publish record to node.js socket
	with SocketIO(baseurl, baseport) as socketIO:
		socketIO.emit('newMessage', {"update":{"user":pk}})

# 
# 	MESSAGE CREATE ENDPOINT
# 
@permission_classes((IsAuthenticated, ))
class CreateMessageView(generics.CreateAPIView):

	def post(self, request, *args, **kwargs):
		sender_profile = get_profile_by_user(request.user)
		recipient_profile = get_profile_by_pk(request.data.get('recipient'))

		if sender_profile == recipient_profile:
			status = {"status_code": 201, "detail": "Cannot send message to yourself."}
			return Response(status)

		else:
			message_data = {
				'sender' : sender_profile.pk,
				'recipient' : recipient_profile.pk,
				'body' : request.data.get('body'),
				'subject' : request.data.get('subject'),
				'parent' : request.data.get('parent')
			}

			serializer = CreateMessageSerializer(data=message_data)
			if serializer.is_valid():
				serializer.save()
				status = {"status_code": 201, "detail": "Message sent to seller."}

				#
				# EMIT MESSAGE TO WEBSOCKET
				#
				emit_newmessage_in_socket(recipient_profile.pk)
				return Response(status)

			
			status = {"status_code": 400, "detail": "Serializer error"}
			return Response(status)


# 
# 	MESSAGE RETRIEVE ENDPOINT
# 	/api/message/retrieve
@permission_classes((IsAuthenticated, ))
class UserMessagesView(APIView):

	def get_inbox_messages(self, user):
		messages = Message.objects.filter(Q(recipient=user) & Q(recipient_delete=False)).order_by('parent', '-sent_at').distinct('parent')
		return messages

	def get_sent_messages(self, user):
		messages = Message.objects.filter(Q(sender=user) & Q(sender_delete=False)).order_by('parent', '-sent_at').distinct('parent')
		return messages

	def get(self, request, format=None):
		user_profile = get_profile_by_user(request.user)
		user_messages = self.get_inbox_messages(user_profile)
		sent_messages = self.get_sent_messages(user_profile)

		
		#print (sent_messages)
		# COUNT NUMBER OF UNREAD MESSAGES
		count = get_unread_messages_count(user_messages)
		inbox_serializer = GetMessageSerializer(user_messages, many=True)
		sent_serializer = GetMessageSerializer(sent_messages, many=True)
		print("new message count: "+ str(count))
		inbox = {
			'unread_count' : count,
			'inbox_messages' : inbox_serializer.data,
			'sent_messages' : sent_serializer.data
		}
		return Response(inbox)


#
# 	CHANGE STATUS OF MESSAGE FROM UNREAD TO READ
#	/api/message/read
#   returns user messages and count of unread message
@permission_classes((IsAuthenticated, ))
class ChangeMessageStatusView(APIView):

	def get_message_pk(self, pk):
		try:
			message = Message.objects.get(pk=pk)
			return message
		except Message.DoesNotExist:
			status = {'error' : 'You are trying to retrieve a message that is not in the database.'}
			return Response(status)

	def get_inbox_messages(self, user):
		messages = Message.objects.filter(Q(recipient=user) & Q(recipient_delete=False)).order_by('-sent_at')
		return messages

	def get_sent_messages(self, user):
		messages = Message.objects.filter(Q(sender=user) & Q(sender_delete=False)).order_by('-sent_at')
		return messages

	def get_conversation(self, id):
		conversation = Message.objects.filter(Q(parent=id)).order_by('-sent_at')
		return conversation

	def get_conversation_id(self, parent):
		conversation = Message.objects.filter(parent=parent)
		return conversation

	def post(self, request, pk, format=None):
		inbox_message = self.get_message_pk(pk)
		inbox_message.read = True
		inbox_message.save()

		user_profile = get_profile_by_user(request.user)
		user_messages = self.get_inbox_messages(user_profile)
		sent_messages = self.get_sent_messages(user_profile)
		#conversation_id = self.get_conversation_id(inbox_message.parent)
		conversation = self.get_conversation(inbox_message.parent)
		
		# COUNT NUMBER OF UNREAD MESSAGES
		count = get_unread_messages_count(user_messages)
		print (str(count) + "unread messages")
		inbox_serializer = GetMessageSerializer(user_messages, many=True)
		sent_serializer = GetMessageSerializer(sent_messages, many=True)
		conversation_serializer = GetMessageSerializer(conversation, many=True)

		inbox = {
			'unread_count' : count,
			'inbox_messages' : inbox_serializer.data,
			'sent_messages' : sent_serializer.data,
			'conversation' : conversation_serializer.data,
			'subject' : inbox_message.subject
		}
		return Response(inbox)


#
#	DELETE SPECIFIC MESSAGE BY PK
#	/api/message/delete/pk
#
@permission_classes((IsAuthenticated, ))
class DeleteMessagePkView(APIView):

	def get_message_group(self, parent):
		try:
			message = Message.objects.filter(parent=parent)
			return message
		except Message.DoesNotExist:
			status = {'error' : 'You are trying to retrieve a message that is not in the database.'}
			return Response(status)

	def get_messages(self, user):
		messages = Message.objects.filter(Q(sender=user) | Q(recipient=user)).order_by('-sent_at')
		return messages

	def get_inbox_messages(self, user):
		messages = Message.objects.filter(Q(recipient=user) & Q(recipient_delete=False)).order_by('parent', '-sent_at').distinct('parent')
		return messages

	def get_sent_messages(self, user):
		messages = Message.objects.filter(Q(sender=user) & Q(sender_delete=False)).order_by('parent', '-sent_at').distinct('parent')
		return messages

	def post(self, request, parent, format=None):
		messages = self.get_message_group(parent)

		user = get_profile_by_user(request.user)
		print ("requesting to delete message")
		print (user)
		print (user.pk)

		for message in messages:
			print (message.sender)
			if message.sender == user:
				print ("user is sender")
				if message.recipient_delete:
					print ("should delete message")
					message.delete()
				else:
					message.sender_delete = True
					message.save()
			else:
				print ("user is recipient")
				if message.sender_delete:
					message.delete()
				else:
					message.recipient_delete = True
					message.save()
		

		user_profile = get_profile_by_user(request.user)
		user_messages = self.get_inbox_messages(user_profile)
		sent_messages = self.get_sent_messages(user_profile)

		# COUNT NUMBER OF UNREAD MESSAGES
		count = get_unread_messages_count(user_messages)
		print (str(count) + "unread messages")
		inbox_serializer = GetMessageSerializer(user_messages, many=True)
		sent_serializer = GetMessageSerializer(sent_messages, many=True)
		
		inbox = {
			'unread_count' : count,
			'inbox_messages' : inbox_serializer.data,
			'sent_messages' : sent_serializer.data,
			'status' : 'Message successfully deleted.'
		}
		return Response(inbox)


#
#	DELETE ALL INBOX MESSAGES OF USER
#	/api/message/delete-all/
#
@permission_classes((IsAuthenticated, ))
class DeleteAllMessages(APIView):

	def get_user_messages(self, user):
		try:
			message = Message.objects.filter(Q(sender=user) | Q(recipient=user))
			return message
		except Message.DoesNotExist:
			status = {'error' : 'You are trying to retrieve a message that is not in the database.'}
			return Response(status)

	def get_message_group(self, parent):
		try:
			message = Message.objects.filter(parent=parent)
			return message
		except Message.DoesNotExist:
			status = {'error' : 'You are trying to retrieve a message that is not in the database.'}
			return Response(status)

	def get_inbox_messages(self, user):
		messages = Message.objects.filter(Q(recipient=user) & Q(recipient_delete=False)).order_by('parent', '-sent_at').distinct('parent')
		return messages

	def get_sent_messages(self, user):
		messages = Message.objects.filter(Q(sender=user) & Q(sender_delete=False)).order_by('parent', '-sent_at').distinct('parent')
		return messages

	def post(self, request):
		data = self.request.data 

		selected_messages = data['selected']
		print (selected_messages)
		# user = get_profile_by_user(request.user)
		# messages = self.get_user_messages(user)

		# for message in messages:
		# 	print (message.sender)
		# 	if message.sender == user:
		# 		print ("user is sender")
		# 		if message.recipient_delete:
		# 			print ("should delete message")
		# 			message.delete()
		# 		else:
		# 			message.sender_delete = True
		# 			message.save()
		# 	else:
		# 		print ("user is recipient")
		# 		if message.sender_delete:
		# 			message.delete()
		# 		else:
		# 			message.recipient_delete = True
		# 			message.save()
		

		# user_messages = self.get_inbox_messages(user)
		# sent_messages = self.get_sent_messages(user)

		# # COUNT NUMBER OF UNREAD MESSAGES
		# count = get_unread_messages_count(user_messages)
		# print (str(count) + "unread messages")
		# inbox_serializer = GetMessageSerializer(user_messages, many=True)
		# sent_serializer = GetMessageSerializer(sent_messages, many=True)
		
		# inbox = {
		# 	'unread_count' : count,
		# 	'inbox_messages' : inbox_serializer.data,
		# 	'sent_messages' : sent_serializer.data,
		# 	'status' : 'Message successfully deleted.'
		# }
		for parent in selected_messages:
			messages = self.get_message_group(parent)

			user = get_profile_by_user(request.user)

			for message in messages:
				print (message.sender)
				if message.sender == user:
					print ("user is sender")
					if message.recipient_delete:
						print ("should delete message")
						message.delete()
					else:
						message.sender_delete = True
						message.save()
				else:
					print ("user is recipient")
					if message.sender_delete:
						message.delete()
					else:
						message.recipient_delete = True
						message.save()
		
		user_messages = self.get_inbox_messages(user)
		sent_messages = self.get_sent_messages(user)

		# COUNT NUMBER OF UNREAD MESSAGES
		count = get_unread_messages_count(user_messages)
		print (str(count) + "unread messages")
		inbox_serializer = GetMessageSerializer(user_messages, many=True)
		sent_serializer = GetMessageSerializer(sent_messages, many=True)
		
		inbox = {
			'unread_count' : count,
			'inbox_messages' : inbox_serializer.data,
			'sent_messages' : sent_serializer.data,
			'status' : 'Message successfully deleted.'
		}

		return Response(inbox)