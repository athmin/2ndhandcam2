from django.db import models
from django.db.models.signals import post_save
from django.conf import settings
from profiles.models import Profile
from socketIO_client import SocketIO
from urllib.parse import urlparse

class Message(models.Model):
	parent = models.CharField(null=True, blank=True, max_length=255)
	children = models.CharField(null=True, blank=True, max_length=255)
	subject = models.CharField(max_length=255)
	body = models.TextField(null=True, blank=True)
	sender = models.ForeignKey(Profile, related_name="sender_profile", on_delete=models.CASCADE)
	recipient = models.ForeignKey(Profile, related_name="recipient_profile", on_delete=models.CASCADE)
	read = models.BooleanField(default=False)
	sent_at = models.DateTimeField(auto_now_add=True)
	sender_delete = models.BooleanField(default=False)
	recipient_delete = models.BooleanField(default=False)

	# def save(self, *args, **kwargs):        
	# 	super(Message, self).save(*args, **kwargs)
	# 	parsedurl = urlparse(settings.NODEJS_SOCKET_URL)
	# 	baseurl = "%s://%s" % (parsedurl.scheme,parsedurl.hostname)
	# 	baseport = parsedurl.port if parsedurl.port != None else 80
	# 	# Publish record to node.js socket
	# 	with SocketIO(baseurl, baseport) as socketIO:
	# 		socketIO.emit('newMessage', {"update":{"user":self.recipient.user.pk}})


#POST SAVE SIGNALS
# def post_save_message_receiver(sender, instance, *args, **kwargs):
# 	print ("after saving the message -- emit newMessage")
# 	print (instance.pk)
# 	parsedurl = urlparse(settings.NODEJS_SOCKET_URL)
# 	baseurl = "%s://%s" % (parsedurl.scheme,parsedurl.hostname)
# 	baseport = parsedurl.port if parsedurl.port != None else 80
# 	# Publish record to node.js socket
# 	with SocketIO(baseurl, baseport) as socketIO:
# 		socketIO.emit('newMessage', {"update":{"user":instance.recipient.pk}})

	
# post_save.connect(post_save_message_receiver, sender=Message)

