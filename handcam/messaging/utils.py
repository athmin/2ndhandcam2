from .models import Message
from django.contrib.auth.models import User
from profiles.models import Profile
from .serializers import CreateMessageSerializer

def send_expired_notification(profile):
	admin_user = User.objects.get(username="2ndhandcam-Admin")
	admin_profile = Profile.objects.get(user=admin_user)

	receiver = profile
	message = "One of your for sale item has expired. Click Reactivate button in your dashboard to reactivate the item."
	sender = admin_profile

	message_data = {
		'sender' : sender.pk,
		'recipient' : receiver.pk,
		'body' : message,
	}

	serializer = CreateMessageSerializer(data=message_data)
	if serializer.is_valid():
		serializer.save()
		status = {"status_code": 201, "detail": "Message sent to seller."}
		return status

	print('invalid serializer')
	status = {"status_code": 400, "detail": "Serializer error"}
	return status