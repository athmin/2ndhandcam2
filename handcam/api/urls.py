from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from api import views
 
 
urlpatterns = [
    url(r'^backup/$', views.backup, name='backup'),
    url(r'^reports/(?P<filter_q>.+)/(?P<brand>.+)/(?P<username>.+)/$', views.Reporting, name='reports'),
    url(r'^maintenance-off/$', views.maintenance_mode_off, name='maintenance_mode_off'),
    url(r'^maintenance-on/$', views.maintenance_mode_on, name='maintenance_mode_on'),
    url(r'^get-maintenance-mode/$', views.GetMaintenanceMode.as_view(), name="get-maintenance-mode" ),
    url(r'^get-user-location/$', views.UserLocationView.as_view(), name='get-user-location'),
	url(r'^registration/$', views.RegisterProfileView.as_view(), name='registration'),
    url(r'^registration/clean-username$', views.cleanUsername.as_view(), name='clean-username'),
    url(r'^registration/clean-email$', views.cleanEmail.as_view(), name='clean-email'),
	url(r'^activate/(?P<key>.+)$', views.Activation.as_view(), name='activation'),
	url(r'^login/$', views.ApiLogin.as_view(), name='login'),
	url(r'^change-password/$', views.ChangePasswordView.as_view(), name='change-password'),
    url(r'^change-password-reset/$', views.ChangePasswordResetView.as_view(), name='change-password-reset'),
    url(r'^forgot-password/$', views.ForgotPassWordView.as_view(), name='forgot-password'),
    url(r'^suggestion/$', views.SuggestionView.as_view(), name='suggestion'),
    url(r'^new_model_notification/$', views.NewModelNotificationView.as_view(), name='new-model'),
    url(r'^customer_service/$', views.CustomerServiceView.as_view(), name='customer_services'),
    url(r'^commercial_account/$', views.CommercialAccountView.as_view(), name='commercial_account'),
    url(r'^notify_all/$', views.SendEmailToAll.as_view(), name='notify-all'),
    url(r'^notify_selected/$', views.SendEmailToFilterUser.as_view(), name='notify-selected'),
    url(r'^buy/camera/$', views.CameraForSaleList.as_view(), name='buy-camera-list'),
    url(r'^buy/camera/(?P<pk>[0-9]+)/', views.CameraForSaleDetail.as_view(), name='buy-camera-detail'),
    url(r'^buy/camera/search/type/(?P<cam_type>.+)/', views.CameraForSaleSearchType.as_view(), name='buy-camera-search-type'),
    url(r'^buy/camera/search/kind/(?P<kind>.+)/', views.CameraForSaleSearchKind.as_view(), name='buy-camera-search-kind'),
    url(r'^buy/camera/search/brand/(?P<brand>.+)/', views.CameraForSaleSearchBrand.as_view(), name='buy-camera-search-brand'),
    url(r'^buy/camera/search/model/(?P<model>.+)/', views.CameraForSaleSearchModel.as_view(), name='buy-camera-search-model'),
]