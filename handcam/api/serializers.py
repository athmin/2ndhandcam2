from __future__ import (absolute_import, division, print_function, unicode_literals)
from rest_framework import serializers
from camera.models import Feature, Specification, Kind, Brand, Type, CameraDetails
from forsale.models import ForSaleActualPhoto, SellCamera, SellLenses
from lens.models import LensFeature, LensSpecification, LensType, LensDetails
from profiles.models import Profile, Review

from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth.hashers import make_password

from rest_framework_elasticsearch.es_serializer import ElasticModelSerializer


class ChangePasswordSerializer(serializers.Serializer):
	old_password = serializers.CharField(required=True)
	new_password = serializers.CharField(required=True)

	# def validate_new_password(self, value):
	# 	validate_password(value)
	# 	return value


class ChangePasswordResetSerializer(serializers.Serializer):
	new_password = serializers.CharField(required=True)

	# def validate_new_password(self, value):
	# 	validate_password(value)
	# 	return value


class UserSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = User
		fields = ('username', 'first_name', 'last_name', 'email', 'password',)

	def create(self, validated_data):
		
		user = User.objects.create_user(
			username = validated_data['username'],
			first_name = validated_data['first_name'],
			last_name = validated_data['last_name'],
			email = validated_data['email'],
		)
		password = validated_data['password']
		user.set_password(password)
		user.save()

		return user


class ProfileSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Profile
		fields = [
			'user',
			'city',
			'country',
			'phone_number',
			'image',
		]

	def create(self, validated_data):
	
		# create profile
		profile = Profile.objects.create(
			user = validated_data['user'],
			city = validated_data['city'],
			country = validated_data['country'],
			phone_number = validated_data['phone_number'],
			image = validated_data['image'],
		  
		)

		return profile


class GetCamKindSerializers(serializers.ModelSerializer):
	class Meta:
		model = Kind
		fields = [
			'kind_of_camera',
			]


class GetCamBrandSerializers(serializers.ModelSerializer):
	class Meta:
		model = Brand
		fields = [
			'company',
			]

class GetCamTypeSerializers(serializers.ModelSerializer):
	kind = GetCamKindSerializers(read_only=True)
	brand = GetCamBrandSerializers(read_only=True)
	
	class Meta:
		model = Type
		fields = [
			'camera_type',
			'kind',
			'brand',
			]


class GetCamFeaturesSerializers(serializers.ModelSerializer):
	class Meta:
		model = Feature
		fields = [
			'camera_feature',
			]


class GetCamSpecsSerializers(serializers.ModelSerializer):
	class Meta:
		model = Specification
		fields = [
			'specs',
			]


class CameraDetailSerializer(serializers.ModelSerializer):
	cam_type = GetCamTypeSerializers(read_only=True)
	features = GetCamFeaturesSerializers(read_only=True)
	specifications = GetCamSpecsSerializers(read_only=True)

	class Meta:
		model = CameraDetails
		fields = [
			'cam_type',
			'model',
			'mb',
			'sensor_size',
			'sensor_type',
			'image_sensor_format',
			'lcd_size',
			'lcd_resolution',
			'weight',
			'iso_low',
			'iso_high',
			'video',
			'frames_per_sec',
			'af_points',
			'aperture',
			'max_aperture',
			'focal_length_from',
			'focal_length_to',
			'mirrorless',
			'wifi',
			'gps',
			'optical_zoom',
			'digital_zoom',
			'waterproof',
			'effective_resolution',
			'dynamic_range',
			'frame_rate',
			'view_finder',
			'lens_mount',
			'cam_features',
			'cam_specifications',
			'image',
		]



class GetLensBrandSerializers(serializers.ModelSerializer):
	class Meta:
		model = Brand
		fields = [
			'company',
			]


class GetLensFeaturesSerializers(serializers.ModelSerializer):
	class Meta:
		model = LensFeature
		fields = [
			'lens_feature',
			]


class GetLensSpecsSerializers(serializers.ModelSerializer):
	class Meta:
		model = LensSpecification
		fields = [
			'specs',
			]

class GetLensTypeSerializers(serializers.ModelSerializer):
	brand = GetLensBrandSerializers(read_only=True)
	
	class Meta:
		model = LensType
		fields = [
			'lens_type',
			'brand',
			]


class LensDetailSerializer(serializers.ModelSerializer):
	type_of_lens = GetLensTypeSerializers(read_only=True)
	features = GetLensFeaturesSerializers(read_only=True)
	specifications = GetLensSpecsSerializers(read_only=True)

	class Meta:
		model = LensDetails
		fields = [
			'type_of_lens',
			'model',
			'aperture',
			'max_aperture',
			'focal_length_from',
			'focal_length_to',
			'weight',
			'format_range',
			'lens_type',
			'lens_type_and',
			'fixed_focal',
			'lens_format',
			'focus_type',
			'features',
			'specifications',
			'image',
		]



