from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib import messages
from django.db.models import Sum, Count
from django.shortcuts import render
from django.http import Http404
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.utils.crypto import get_random_string
from django.utils.translation import ugettext_lazy as _

from rest_framework import generics
from rest_framework import status
from rest_framework_jwt.utils import jwt_encode_handler
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from .serializers import ChangePasswordSerializer, ChangePasswordResetSerializer, UserSerializer, ProfileSerializer, CameraDetailSerializer,\
	LensDetailSerializer 
from forsale.models import SellCamera, SellLenses, SellVideoCamera, SellLighting, SellAccessories, SellDrone, ForSaleActualPhoto
from camera.models import CameraDetails
from profiles.models import Profile, Review, Block
from registration.models import Registration
from registration.utils import token, email_handler
import datetime

from elasticsearch import Elasticsearch, RequestsHttpConnection
from rest_framework_elasticsearch import es_views, es_pagination, es_filters

from django.contrib.gis.geoip2 import GeoIP2

import subprocess
from maintenance_mode.decorators import force_maintenance_mode_off, force_maintenance_mode_on
from maintenance_mode.core import get_maintenance_mode, set_maintenance_mode
from django.conf import settings
from socketIO_client import SocketIO
from urllib.parse import urlparse
from django.core.mail import send_mail

from forsale.sorttable import StillTable, VideoTable, LensTable, AccessoriesTable, LightingTable, DroneTable
from django_tables2 import RequestConfig
from django_tables2.export.export import TableExport
import itertools as it
import xlwt

from django.http import HttpResponse

#
# GET USER LOCATION	
# /api/get-user-location
@permission_classes((AllowAny, ))
class UserLocationView(APIView):

	def get(self, request):
		ip = request.META.get('HTTP_X_FORWARDED_FOR')
		city = ''
		country = ''

		g = GeoIP2()
		
		try:
			result = g.country(ip)
			country = result['country_name']
			
		except:
			pass

		result = {
			'city' : city,
			'country' : country,
			'ip' : ip,
			# 'HTTP_X_FORWARDED_FOR' : request.META.get('HTTP_X_FORWARDED_FOR'), 
			# 'X_FORWARDED_FOR' : request.META.get('X_FORWARDED_FOR'),
   #   		'HTTP_CLIENT_IP' : request.META.get('HTTP_CLIENT_IP'),
   #   		'HTTP_X_REAL_IP' : request.META.get('HTTP_X_REAL_IP'),
   #   		'HTTP_X_FORWARDED' : request.META.get('HTTP_X_FORWARDED'),
   #   		'HTTP_X_CLUSTER_CLIENT_IP' : request.META.get('HTTP_X_CLUSTER_CLIENT_IP'),
   #   		'HTTP_FORWARDED_FOR' : request.META.get('HTTP_FORWARDED_FOR'),
   #   		'HTTP_FORWARDED' : request.META.get('HTTP_FORWARDED'),
   #   		'HTTP_VIA' : request.META.get('HTTP_VIA'),
   #   		'REMOTE_ADDR' : request.META.get('REMOTE_ADDR'),
		}

		return Response(result)


@permission_classes((AllowAny, ))
class cleanUsername(APIView):
	def post(self, request):
		data = self.request.data 
		self.username = data['username']
		self.email = data['email']

		try:
			block_user = Block.objects.get(email__iexact=self.email)
			email_block = True
		except Block.DoesNotExist:
			email_block = False

		
		try:
			findUser = User.objects.get(username__iexact=self.username)
			userError = True
			#{"error": "A user with that username already exists."}
		except User.DoesNotExist:
			userError = False
			#status = {"status": "username is not taken"}


		try:
			findUser = User.objects.get(email__iexact=self.email)
			emailError = True
			#status = {"error": "A user with that email already exists."}
		except User.DoesNotExist:
			emailError = False
			#status = {"status": "email is not taken"}

		if email_block:
			status = {"blockerror": "Email address is block by the 2ndhandcam Admin."}
			return Response(status)

		if userError and emailError and not email_block:
			status = {"usererror": "A user with that username already exists.", "emailerror": "A user with that email already exists."}
		if userError and not emailError and not email_block:
			status = {"usererror": "A user with that username already exists."}
		if not userError and emailError and not email_block:
			status = {"emailerror": "A user with that email already exists."}
		if not userError and not emailError and not email_block:
			status = {"status": "username and email is not taken"}

		return Response(status)


@permission_classes((AllowAny, ))
class cleanEmail(APIView):
	def post(self, request):
		data = self.request.data 
		self.email = data['email']
		
		try:
			findUser = User.objects.get(email=self.email)
			status = {"error": "A user with that email already exists."}
		except User.DoesNotExist:
			status = {"status": "email is not taken"}

		return Response(status)




# REGISTRATION ENDPOINT
# /api/registration
@permission_classes((AllowAny, ))
class RegisterProfileView(generics.CreateAPIView):
	permission_classes = (IsAuthenticated,)
	serializer_class = ProfileSerializer

	def post(self, request, format=None):

		language = request.data.get('email_template', None)

		try:
			block_user = Block.objects.get(email=request.data.get('email'))
			res = {'error': 'Email address is block by the 2ndhandcam Admin', 'status' : status.HTTP_400_BAD_REQUEST}
			return Response(res)
		except Block.DoesNotExist:
			print ("email is ok to register")

		user_data = {
			'username': request.data.get('username', None),
			'first_name': request.data.get('first_name', None),
			'last_name': request.data.get('last_name', None), 
			'email': request.data.get('email', None), 
			'password': request.data.get('password', None),
		}
		user_serializer = UserSerializer(data=user_data)
		if user_serializer.is_valid():
			user = user_serializer.save()
			#authenticates the user
			jwttoken = jwt_encode_handler({'username': user.username})
			data = {'token': jwttoken}

			print("telephone number")
			print(request.data.get('phone_number', None))
			profile, p = Profile.objects.get_or_create(user=user)
			profile.city =  request.data.get('city', None)
			profile.country = request.data.get('country', None)
			profile.phone_number = request.data.get('phone', None)
			profile.save()

			# 
			# Update Registration database 
			#
			user_email = user.email
			activation_key = token.generate_confirmation_token(user_email)
			link=settings.HTTP_SERVER_URL + "/activate/"+activation_key
			
			d = {'confirm_url' : link}
			if language == "rtl":
				html_message = render_to_string('activate-ar.html', d)
			else:	
				html_message = render_to_string('activate.html', d)

			

			reg, r = Registration.objects.get_or_create(user=user)
			reg.activation_key = activation_key
			reg.save()
			

			# 
			# send email activation 
			#
			email_handler.send_email_activation(user_email, html_message)
			res = {'data': data, 'status' : status.HTTP_201_CREATED}
			return Response(res)
		else:
			if 'username' in user_serializer.errors:
				res = {'error': user_serializer.errors['username'][0], 'status' : status.HTTP_400_BAD_REQUEST}
			else:
				res = {'error': "Error in registering account. Please check your inputs.", 'status' : status.HTTP_400_BAD_REQUEST}
			return Response(res)
		
		# profile_data = {
		# 	'user' : user.pk,
		# 	'city': request.data.get('city', None),
		# 	'country': request.data.get('country', None),
		# 	'phone_number': request.data.get('phone_number', None),
		# 	'image': request.data.get('image', None),
		# }
		
		# serializer = ProfileSerializer(data=profile_data)
		# if serializer.is_valid():
		# 	profile = serializer.save()

			
		
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# email activation ENDPOINT
# /api/activation/<key>
@permission_classes((AllowAny, ))
class Activation(APIView):
	#permission_classes = (IsAuthenticated,)
	def get(self, request, key):
		
		reg = Registration.objects.get(activation_key=key)
		
		if reg.acct_activated:
			
			status = {"status_code": 200, "detail": _("Account already confirmed. Please login to continue.")}
			logout(request)
			return Response(status)
		
		email = token.confirm_token(key)

		
		
		#user = User.objects.get(pk=request.user.pk)
		
		if reg.user.email == email:
			
			reg.acct_activated = True
			reg.date_activated = datetime.datetime.now()
			reg.save()

			profile = Profile.objects.get(user=reg.user)
			profile.verified = True
			profile.save()

			status = {"status_code": 200, "detail": _("Account already confirmed. Please login to continue.")}
			logout(request)
		
		else:
			
			status = {"status_code": 400, "detail": _("Account already confirmed. Please login to continue.")}

		return Response(status)


# Change Password ENDPOINT
# /api/change-password/<password>
@permission_classes((AllowAny, ))
class ChangePasswordView(APIView):

	permission_classes = (IsAuthenticated,)

	def get_object(self, queryset=None):
		obj = self.request.user
		return obj

	def post(self, request, *args, **kwargs):
		self.object = request.user
		print (request.user)
		serializer = ChangePasswordSerializer(data=request.data)

		if serializer.is_valid():
			#check old password
			old_password = serializer.data.get("old_password")
			if not self.object.check_password(old_password):
				status = {"status_code": 400, "detail": _("Old password incorrect")}
				return Response(status)
			self.object.set_password(serializer.data.get("new_password"))
			self.object.save()

			#update profile reset_pass to True
			profile = Profile.objects.get(user=self.object)
			profile.reset_pass = False
			profile.save()

			status = {"status_code": 200, "detail": _("Change password Successfull")}
			return Response(status)
		print (serializer.errors)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# Change Password ENDPOINT
# /api/change-password-reset/
@permission_classes((AllowAny, ))
class ChangePasswordResetView(APIView):

	permission_classes = (IsAuthenticated,)

	def get_object(self, queryset=None):
		obj = self.request.user
		return obj

	def post(self, request, *args, **kwargs):
		self.object = request.user
		serializer = ChangePasswordResetSerializer(data=request.data)

		if serializer.is_valid():
			
			self.object.set_password(serializer.data.get("new_password"))
			self.object.save()

			#update profile reset_pass to True
			profile = Profile.objects.get(user=self.object)
			profile.reset_pass = False
			profile.save()

			status = {"status_code": 200, "detail": _("Change password Successfull")}
			return Response(status)
		print (serializer.errors)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)		


# Reset Password ENDPOINT
# /api/reset-password/<password>
@permission_classes((AllowAny, ))
class ResetPasswordView(APIView):

	def get(self, request, uid, token):
		uid = self.kwargs['uid']
		token = self.kwargs['token']

		data = {'uid': uid, 'token': token}
		return Response(data)


# Forgot Password ENDPOINT
# /api/forgot-password/<password>
@permission_classes((AllowAny, ))
class ForgotPassWordView(APIView):

	def post(self, request):
		data = self.request.data

		self.email = data['email']

		# Get user base on submitted data
		try:
			user = User.objects.get(email=self.email)
		except User.DoesNotExist:
			status = {"status_code": 400, "detail": _("Email address does not exist!")}
			return Response(status)

		# Generate temporary password
		temp_pass = get_random_string(8)

		user.set_password(temp_pass)
		user.save()

		#update profile reset_pass to True
		profile = Profile.objects.get(user=user)
		profile.reset_pass = True
		profile.save()

		login = authenticate(username=user.username, password=temp_pass)
		token = jwt_encode_handler({'username': user.username})	

		# send notification in users email
		send_mail(
			'Request for password reset',
			'You\'re receiving this email because you requested a password reset for your user account at 2ndhandcam. \n\n Click this link to change your password : '+settings.HTTP_SERVER_URL+'/change_password_reset/:'+token+' \n\nThank you from 2ndhandcam team.',
			'support@2ndhandcam.com',
			[user.email],
			fail_silently=False,
			)

		status = {"status_code": 200, "detail": _("Password successfully reset. Follow the instructions sent to the email address you submitted.")}
		return Response(status)


@permission_classes((AllowAny, ))
class ApiLogin(APIView):

	def post(self, request):	
		data = self.request.data 

		self.username = data['username']
		self.password = data['password']
				
		#user = authenticate(username=self.username, password=self.password)

		try:
			findUser = User.objects.get(username__iexact=self.username)
		except User.DoesNotExist:
			try:
				findUser = User.objects.get(email__iexact=self.username)
			except User.DoesNotExist:
				findUser = None
			
		if findUser is not None:
			caseSensitiveUsername = findUser.username
			
			user = authenticate(username=caseSensitiveUsername, password=self.password)
			
			if user is not None:
				
				token = jwt_encode_handler({'username': caseSensitiveUsername})	
				#data = {'token': token}

				profile = Profile.objects.get(user=findUser)
				is_verified = profile.verified

				if is_verified:
					print ("user verified")
					if profile.reset_pass:
						data = {'token': token, 'redirect_to_change_pass': True}
						return Response(data)
					else:
						data = {'token': token, 'redirect_to_change_pass': False}
						return Response(data)
				else:
					print ("user not yet verified")
					status = {"status_code": 406, "detail": _("Account not yet verified. Please check your email and click on the activation link")}
					return Response(status)
			else:
				print("invalid user name or password")
				status = {"status_code": 405, "detail": _("Invalid username / password")}
				return Response(status)
		
		else:
			print("invalid user name or password")
			status = {"status_code": 405, "detail": _("Invalid username / password")}
			return Response(status)



# Suggestion ENDPOINT
# /api/suggestion/
@permission_classes((AllowAny, ))
class SuggestionView(APIView):

	def post(self, request, *args, **kwargs):

		data = self.request.data

		self.email = data["email"]
		self.name = data["name"]
		self.mobile = data["mobile"]
		self.country = data["country"]
		self.subject = data["subject"]
		self.suggestion = data["suggestion"]

		d = {'email' : self.email, 'name' : self.name, 'mobile' : self.mobile, 'country' : self.country, 'subject' : self.subject, 'suggestion' : self.suggestion }
		html_message = render_to_string('suggestion.html', d)
		
		# 
		# send email suggestion
		#
		email_handler.send_email_suggestion(html_message)
		
		status = {"status_code": 200, "detail": ("Suggestion successfully sent")}
		return Response(status)



# Customer service ENDPOINT
# /api/customer_services/
@permission_classes((AllowAny, ))
class CustomerServiceView(APIView):

	def post(self, request):
		data = self.request.data

		self.email = data["email"]
		self.name = data["name"]
		self.mobile = data["mobile"]
		self.country = data["country"]
		self.subject = data["subject"]
		self.message = data["message"]

		d = {'email' : self.email, 'name' : self.name, 'mobile' : self.mobile, 'country' : self.country, 'subject' : self.subject, 'message' : self.message }
		html_message = render_to_string('customer_services.html', d)

		# 
		# send email customer service
		#
		email_handler.send_email_customer_service(html_message)

		status = {"status_code": 200, "detail": ("Your message is successfully sent")}
		return Response(status)


# Customer service ENDPOINT
# /api/customer_services/
@permission_classes((AllowAny, ))
class NewModelNotificationView(APIView):

	def post(self, request):
		data = self.request.data

		self.email = data["email"]
		self.brand_name = data["brand_name"]
		self.model = data["model"]
		self.subject = data["subject"]
		self.message = data["message"]

		d = {'email' : self.email, 'brand_name' : self.brand_name, 'model' : self.model, 'message' : self.message }
		html_message = render_to_string('new_model_notification.html', d)

		# 
		# send email customer service
		#
		email_handler.send_email_new_models(self.subject, self.email, html_message)

		status = {"status_code": 200, "detail": ("Your message is successfully sent")}
		return Response(status)


# Commercial Account ENDPOINT
# /api/commercial_account/
@permission_classes((AllowAny, ))
class CommercialAccountView(APIView):

	def post(self, request):
		print ('sending email')
		data = self.request.data

		self.email = data["email"]
		self.name = data["name"]
		self.mobile = data["mobile"]
		self.country = data["country"]
		self.message = data["message"]

		d = {'email' : self.email, 'name' : self.name, 'mobile' : self.mobile, 'country' : self.country, 'message' : self.message }
		html_message = render_to_string('commercial_account.html', d)

		# 
		# send email customer service
		#
		email_handler.send_email_commercial_account(html_message)

		status = {"status_code": 200, "detail": ("Your message is successfully sent")}
		return Response(status)

# BUY ENDPOINT

# camera for sale list
# /api/buy/camera/
class CameraForSaleList(APIView):
	
	def get(self, request, format=None):
		cameras = SellCamera.objects.all()
		serializer = GetCamForSaleSerializer(cameras, many=True)
		return Response(serializer.data)


# filter camera with kind
# /api/buy/camera/search/kind/<kind>
class CameraForSaleSearchKind(APIView):
	def get_object(self, kind): 
		try: 
			return SellCamera.objects.filter(item__cam_type__kind__kind_of_camera=kind) 
		except SellCamera.DoesNotExist: 
			raise Http404 

	def get(self, request, kind): 
		camera = self.get_object(kind) 
		serializer = GetCamForSaleSerializer(camera, many=True) 
		return Response(serializer.data)		


# filter camera with type
# /api/buy/camera/search/type/<cam_type>
class CameraForSaleSearchType(APIView):
	def get_object(self, camtype): 
		try: 
			return SellCamera.objects.filter(item__cam_type__camera_type=camtype) 
		except SellCamera.DoesNotExist: 
			raise Http404 

	def get(self, request, cam_type): 
		camera = self.get_object(cam_type) 
		serializer = GetCamForSaleSerializer(camera, many=True) 
		return Response(serializer.data)


# filter camera with brand
# /api/buy/camera/search/brand/<brand>
class CameraForSaleSearchBrand(APIView):
	def get_object(self, brand): 
		try: 
			return SellCamera.objects.filter(item__cam_type__brand__company=brand) 
		except SellCamera.DoesNotExist: 
			raise Http404 

	def get(self, request, brand): 
		camera = self.get_object(brand) 
		serializer = GetCamForSaleSerializer(camera, many=True) 
		return Response(serializer.data)


# filter camera with model
# /api/buy/camera/search/model/<model>
class CameraForSaleSearchModel(APIView):
	def get_object(self, model): 
		try: 
			return SellCamera.objects.filter(item__model=model) 
		except SellCamera.DoesNotExist: 
			raise Http404 

	def get(self, request, model): 
		camera = self.get_object(model) 
		serializer = GetCamForSaleSerializer(camera, many=True) 
		return Response(serializer.data)


# detailed view of camera for sale
# /api/buy/camera/<pk>/
class CameraForSaleDetail(APIView):
	def get_object(self, pk): 
		try: 
			return SellCamera.objects.get(pk=pk) 
		except SellCamera.DoesNotExist: 
			raise Http404 

	def get(self, request, pk): 
		camera = self.get_object(pk) 
		serializer = GetCamForSaleSerializer(camera) 
		return Response(serializer.data)


def get_still_object(filter_q, brand, user):

	if filter_q == "all" and brand == "all" and user == "all":
		still = SellCamera.objects.all().order_by("item__cam_type__brand__company")
		return still
	elif filter_q != "all" and brand == "all" and user == "all":
		still = SellCamera.objects.filter(status=filter_q).order_by("item__cam_type__brand__company")
		return still
	elif filter_q != "all" and brand != "all" and user == "all":
		still = SellCamera.objects.filter(status=filter_q, item__cam_type__brand__company=brand).order_by("item__cam_type__brand__company")
		return still
	elif filter_q != "all" and brand == "all" and user != "all":
		still = SellCamera.objects.filter(seller__user__username=user, status=filter_q).order_by("item__cam_type__brand__company")
		return still
	elif filter_q == "all" and brand != "all" and user != "all":
		still = SellCamera.objects.filter(item__cam_type__brand__company=brand, seller__user__username=user).order_by("item__cam_type__brand__company")
		return still
	elif filter_q == "all" and brand != "all" and user == "all":
		still = SellCamera.objects.filter(item__cam_type__brand__company=brand).order_by("item__cam_type__brand__company")
		return still
	elif filter_q == "all" and brand == "all" and user != "all":
		still = SellCamera.objects.filter(seller__user__username=user).order_by("item__cam_type__brand__company")
		return still
	else:
		still = SellCamera.objects.filter(status=filter_q, item__cam_type__brand__company=brand, seller__user__username=user).order_by("item__cam_type__brand__company")
		return still

def get_video_object(filter_q, brand, user):

	if filter_q == "all" and brand == "all" and user == "all":
		still = SellVideoCamera.objects.all().order_by("item__cam_type__brand__company")
		return still
	elif filter_q != "all" and brand == "all" and user == "all":
		still = SellVideoCamera.objects.filter(status=filter_q).order_by("item__cam_type__brand__company")
		return still
	elif filter_q != "all" and brand != "all" and user == "all":
		still = SellVideoCamera.objects.filter(status=filter_q, item__cam_type__brand__company=brand).order_by("item__cam_type__brand__company")
		return still
	elif filter_q != "all" and brand == "all" and user != "all":
		still = SellVideoCamera.objects.filter(seller__user__username=user, status=filter_q).order_by("item__cam_type__brand__company")
		return still
	elif filter_q == "all" and brand != "all" and user != "all":
		still = SellVideoCamera.objects.filter(item__cam_type__brand__company=brand, seller__user__username=user).order_by("item__cam_type__brand__company")
		return still
	elif filter_q == "all" and brand != "all" and user == "all":
		still = SellVideoCamera.objects.filter(item__cam_type__brand__company=brand).order_by("item__cam_type__brand__company")
		return still
	elif filter_q == "all" and brand == "all" and user != "all":
		still = SellVideoCamera.objects.filter(seller__user__username=user).order_by("item__cam_type__brand__company")
		return still
	else:
		still = SellVideoCamera.objects.filter(status=filter_q, item__cam_type__brand__company=brand, seller__user__username=user).order_by("item__cam_type__brand__company")
		return still
	

def get_lens_object(filter_q, brand, user):

	if filter_q == "all" and brand == "all" and user == "all":
		still = SellLenses.objects.all().order_by("item__type_of_lens__brand__company")
		return still
	elif filter_q != "all" and brand == "all" and user == "all":
		still = SellLenses.objects.filter(status=filter_q).order_by("item__type_of_lens__brand__company")
		return still
	elif filter_q != "all" and brand != "all" and user == "all":
		still = SellLenses.objects.filter(status=filter_q, item__type_of_lens__brand__company=brand).order_by("item__type_of_lens__brand__company")
		return still
	elif filter_q != "all" and brand == "all" and user != "all":
		still = SellLenses.objects.filter(seller__user__username=user, status=filter_q).order_by("item__type_of_lens__brand__company")
		return still
	elif filter_q == "all" and brand != "all" and user != "all":
		still = SellLenses.objects.filter(item__type_of_lens__brand__company=brand, seller__user__username=user).order_by("item__type_of_lens__brand__company")
		return still
	elif filter_q == "all" and brand != "all" and user == "all":
		still = SellLenses.objects.filter(item__type_of_lens__brand__company=brand).order_by("item__type_of_lens__brand__company")
		return still
	elif filter_q == "all" and brand == "all" and user != "all":
		still = SellLenses.objects.filter(seller__user__username=user).order_by("item__type_of_lens__brand__company")
		return still
	else:
		still = SellLenses.objects.filter(status=filter_q, item__type_of_lens__brand__company=brand, seller__user__username=user).order_by("item__type_of_lens__brand__company")
		return still
	

def get_accessories_object(filter_q, brand, user):

	if filter_q == "all" and brand == "all" and user == "all":
		still = SellAccessories.objects.all().order_by("brand")
		return still
	elif filter_q != "all" and brand == "all" and user == "all":
		still = SellAccessories.objects.filter(status=filter_q).order_by("brand")
		return still
	elif filter_q != "all" and brand != "all" and user == "all":
		still = SellAccessories.objects.filter(status=filter_q, brand=brand).order_by("brand")
		return still
	elif filter_q != "all" and brand == "all" and user != "all":
		still = SellAccessories.objects.filter(seller__user__username=user, status=filter_q).order_by("brand")
		return still
	elif filter_q == "all" and brand != "all" and user != "all":
		still = SellAccessories.objects.filter(brand=brand, seller__user__username=user).order_by("brand")
		return still
	elif filter_q == "all" and brand != "all" and user == "all":
		still = SellAccessories.objects.filter(brand=brand).order_by("brand")
		return still
	elif filter_q == "all" and brand == "all" and user != "all":
		still = SellAccessories.objects.filter(seller__user__username=user).order_by("brand")
		return still
	else:
		still = SellAccessories.objects.filter(status=filter_q, brand=brand, seller__user__username=user).order_by("brand")
		return still
	

def get_lighting_object(filter_q, brand, user):

	if filter_q == "all" and brand == "all" and user == "all":
		still = SellLighting.objects.all().order_by("brand")
		return still
	elif filter_q != "all" and brand == "all" and user == "all":
		still = SellLighting.objects.filter(status=filter_q).order_by("brand")
		return still
	elif filter_q != "all" and brand != "all" and user == "all":
		still = SellLighting.objects.filter(status=filter_q, brand=brand).order_by("brand")
		return still
	elif filter_q != "all" and brand == "all" and user != "all":
		still = SellLighting.objects.filter(seller__user__username=user, status=filter_q).order_by("brand")
		return still
	elif filter_q == "all" and brand != "all" and user != "all":
		still = SellLighting.objects.filter(brand=brand, seller__user__username=user).order_by("brand")
		return still
	elif filter_q == "all" and brand != "all" and user == "all":
		still = SellLighting.objects.filter(brand=brand).order_by("brand")
		return still
	elif filter_q == "all" and brand == "all" and user != "all":
		still = SellLighting.objects.filter(seller__user__username=user).order_by("brand")
		return still
	else:
		still = SellLighting.objects.filter(status=filter_q, brand=brand, seller__user__username=user).order_by("brand")
		return still
	

def get_drone_object(filter_q, brand, user):

	if filter_q == "all" and brand == "all" and user == "all":
		still = SellDrone.objects.all().order_by("brand")
		return still
	elif filter_q != "all" and brand == "all" and user == "all":
		still = SellDrone.objects.filter(status=filter_q).order_by("brand")
		return still
	elif filter_q != "all" and brand != "all" and user == "all":
		still = SellDrone.objects.filter(status=filter_q, brand=brand).order_by("brand")
		return still
	elif filter_q != "all" and brand == "all" and user != "all":
		still = SellDrone.objects.filter(seller__user__username=user, status=filter_q).order_by("brand")
		return still
	elif filter_q == "all" and brand != "all" and user != "all":
		still = SellDrone.objects.filter(brand=brand, seller__user__username=user).order_by("brand")
		return still
	elif filter_q == "all" and brand != "all" and user == "all":
		still = SellDrone.objects.filter(brand=brand).order_by("brand")
		return still
	elif filter_q == "all" and brand == "all" and user != "all":
		still = SellDrone.objects.filter(seller__user__username=user).order_by("brand")
		return still
	else:
		still = SellDrone.objects.filter(status=filter_q, brand=brand, seller__user__username=user).order_by("brand")
		return still
	

#
# REPORTING
#
@staff_member_required
def Reporting(request, filter_q, brand, username):

	all_filter = None
	active_filter = None
	sold_filter = None
	expired_filter = None
	hidden_filter = None
	profile = None
	status = "all"
	# brand = brand
	# username = username

	if filter_q == "all":
		all_filter = "active_filter"
	elif filter_q == "Active":
		active_filter = "active_filter"
		status = "Active"
	elif filter_q == "Sold":
		sold_filter = "active_filter"
		status = "Sold"
	elif filter_q == "Expired":
		expired_filter = "active_filter"
		status = "Expired"
	else:
		hidden_filter = "active_filter"
		status = "Disabled"

	# if brand != "all":
	# 	brand = brand

	if username != "all":
		profile = Profile.objects.get(user__username=username)

	
	still = get_still_object(filter_q, brand, username)
	video = get_video_object(filter_q, brand, username)
	lenses = get_lens_object(filter_q, brand, username)
	accessories = get_accessories_object(filter_q, brand, username)
	lightings = get_lighting_object(filter_q, brand, username)
	drones = get_drone_object(filter_q, brand, username)

	still_table = StillTable(still)
	video_table = VideoTable(video)
	lens_table = LensTable(lenses)
	accessories_table = AccessoriesTable(accessories)
	lighting_table = LightingTable(lightings)
	drone_table = DroneTable(drones)

	all_table = [still_table, video_table, lens_table, accessories_table, lighting_table, drone_table]

	RequestConfig(request, paginate={'per_page': 15}).configure(still_table)
	RequestConfig(request, paginate={'per_page': 15}).configure(video_table)
	RequestConfig(request, paginate={'per_page': 15}).configure(lens_table)
	RequestConfig(request, paginate={'per_page': 15}).configure(accessories_table)
	RequestConfig(request, paginate={'per_page': 15}).configure(lighting_table)
	RequestConfig(request, paginate={'per_page': 15}).configure(drone_table)

	export_format = request.GET.get('_export', None)
	# if TableExport.is_valid_format(export_format):
	# 	exporter = TableExport(export_format, all_table)
	# 	return exporter.response('2ndhandcam-report.{}'.format(export_format))
	if export_format:

		still_sales = still.values('product_id', 'item__cam_type__brand__company', 'item__model', 'seller__country', 'seller__city', 'price', 'seller__user__username', 'condition').order_by().annotate(Count('item__cam_type__brand__company'))
		video_sales = video.values('product_id', 'item__cam_type__brand__company', 'item__model', 'seller__country', 'seller__city', 'price', 'seller__user__username', 'condition').order_by().annotate(Count('item__cam_type__brand__company'))
		lens_sales = lenses.values('product_id', 'item__type_of_lens__brand__company', 'item__model', 'seller__country', 'seller__city', 'price', 'seller__user__username', 'condition').order_by().annotate(Count('item__type_of_lens__brand__company'))
		acc_sales = accessories.values('product_id', 'brand', 'model', 'seller__country', 'seller__city', 'price', 'seller__user__username', 'condition', 'category').order_by().annotate(Count('brand'))
		light_sales = lightings.values('product_id', 'brand', 'model', 'seller__country', 'seller__city', 'price', 'seller__user__username', 'condition', 'category').order_by().annotate(Count('brand'))
		drons_sales = drones.values('product_id', 'brand', 'model', 'seller__country', 'seller__city', 'price', 'seller__user__username', 'condition', 'drone_type').order_by().annotate(Count('brand'))
	
		response = HttpResponse(content_type='application/ms-excel')
		response['Content-Disposition'] = 'attachment; filename="2ndhandcam_report.xls"'

		wb = xlwt.Workbook(encoding='utf-8')
		ws = wb.add_sheet('admin_reports')

		# Sheet header, first row
		row_num = 0

		font_style = xlwt.XFStyle()
		font_style.font.bold = True

		ws.set_panes_frozen(True)
		ws.set_horz_split_pos(1) 

		columns = ['', 'Product ID', 'Brand', 'Model', 'Country', 'City', 'Price', 'Seller', 'Condition', 'Category' ]

		for col_num in range(len(columns)):
			ws.write(row_num, col_num, columns[col_num], font_style)

		still_id = 0
		video_id = 0
		lens_id = 0
		acc_id = 0
		light_id = 0
		drone_id = 0

		row_num += 1
		font_style = xlwt.XFStyle()
		font_style.font.bold = True
		ws.write_merge(row_num, row_num, 0, 1, "Still Camera", font_style)

		# Sheet body, remaining rows
		font_style = xlwt.XFStyle()
		for row in still_sales:
			row_num += 1
			still_id += 1
			ws.write(row_num, 0, still_id, font_style)
			ws.write(row_num, 1, row['product_id'], font_style)
			ws.write(row_num, 2, row['item__cam_type__brand__company'], font_style)
			ws.write(row_num, 3, row['item__model'], font_style)
			ws.write(row_num, 4, row['seller__country'], font_style)
			ws.write(row_num, 5, row['seller__city'], font_style)
			ws.write(row_num, 6, row['price'], font_style)
			ws.write(row_num, 7, row['seller__user__username'], font_style)
			#ws.write(row_num, 8, row['status'], font_style)
			ws.write(row_num, 8, row['condition'], font_style)

		row_num += 5
		font_style = xlwt.XFStyle()
		font_style.font.bold = True
		ws.write_merge(row_num, row_num, 0,  1, "Video Camera", font_style)
		# Sheet body, remaining rows
		font_style = xlwt.XFStyle()
		for row in video_sales:
			row_num += 1
			video_id += 1
			ws.write(row_num, 0, video_id, font_style)
			ws.write(row_num, 1, row['product_id'], font_style)
			ws.write(row_num, 2, row['item__cam_type__brand__company'], font_style)
			ws.write(row_num, 3, row['item__model'], font_style)
			ws.write(row_num, 4, row['seller__country'], font_style)
			ws.write(row_num, 5, row['seller__city'], font_style)
			ws.write(row_num, 6, row['price'], font_style)
			ws.write(row_num, 7, row['seller__user__username'], font_style)
			#ws.write(row_num, 8, row['status'], font_style)
			ws.write(row_num, 8, row['condition'], font_style)

		row_num += 5
		font_style = xlwt.XFStyle()
		font_style.font.bold = True
		ws.write_merge(row_num, row_num, 0, 1, "Lenses", font_style)
		# Sheet body, remaining rows
		font_style = xlwt.XFStyle()
		for row in lens_sales:
			row_num += 1
			lens_id += 1
			ws.write(row_num, 0, lens_id, font_style)
			ws.write(row_num, 1, row['product_id'], font_style)
			ws.write(row_num, 2, row['item__type_of_lens__brand__company'], font_style)
			ws.write(row_num, 3, row['item__model'], font_style)
			ws.write(row_num, 4, row['seller__country'], font_style)
			ws.write(row_num, 5, row['seller__city'], font_style)
			ws.write(row_num, 6, row['price'], font_style)
			ws.write(row_num, 7, row['seller__user__username'], font_style)
			#ws.write(row_num, 8, row['status'], font_style)
			ws.write(row_num, 8, row['condition'], font_style)

		row_num += 5
		font_style = xlwt.XFStyle()
		font_style.font.bold = True
		ws.write_merge(row_num, row_num, 0, 1, "Accessories", font_style)
		# Sheet body, remaining rows
		font_style = xlwt.XFStyle()
		for row in acc_sales:
			row_num += 1
			acc_id += 1
			ws.write(row_num, 0, acc_id, font_style)
			ws.write(row_num, 1, row['product_id'], font_style)
			ws.write(row_num, 2, row['brand'], font_style)
			ws.write(row_num, 3, row['model'], font_style)
			ws.write(row_num, 4, row['seller__country'], font_style)
			ws.write(row_num, 5, row['seller__city'], font_style)
			ws.write(row_num, 6, row['price'], font_style)
			ws.write(row_num, 7, row['seller__user__username'], font_style)
			#ws.write(row_num, 8, row['status'], font_style)
			ws.write(row_num, 8, row['condition'], font_style)
			ws.write(row_num, 9, row['category'], font_style)

		row_num += 5
		font_style = xlwt.XFStyle()
		font_style.font.bold = True
		ws.write_merge(row_num, row_num, 0, 1, "Lightings", font_style)
		# Sheet body, remaining rows
		font_style = xlwt.XFStyle()
		for row in light_sales:
			row_num += 1
			light_id += 1
			ws.write(row_num, 0, light_id, font_style)
			ws.write(row_num, 1, row['product_id'], font_style)
			ws.write(row_num, 2, row['brand'], font_style)
			ws.write(row_num, 3, row['model'], font_style)
			ws.write(row_num, 4, row['seller__country'], font_style)
			ws.write(row_num, 5, row['seller__city'], font_style)
			ws.write(row_num, 6, row['price'], font_style)
			ws.write(row_num, 7, row['seller__user__username'], font_style)
			#ws.write(row_num, 8, row['status'], font_style)
			ws.write(row_num, 8, row['condition'], font_style)
			ws.write(row_num, 9, row['category'], font_style)

		row_num += 5
		font_style = xlwt.XFStyle()
		font_style.font.bold = True
		ws.write_merge(row_num, row_num, 0, 1, "Drones", font_style)
		# Sheet body, remaining rows
		font_style = xlwt.XFStyle()
		for row in drons_sales:
			row_num += 1
			drone_id += 1
			ws.write(row_num, 0, drone_id, font_style)
			ws.write(row_num, 1, row['product_id'], font_style)
			ws.write(row_num, 2, row['brand'], font_style)
			ws.write(row_num, 3, row['model'], font_style)
			ws.write(row_num, 4, row['seller__country'], font_style)
			ws.write(row_num, 5, row['seller__city'], font_style)
			ws.write(row_num, 6, row['price'], font_style)
			ws.write(row_num, 7, row['seller__user__username'], font_style)
			#ws.write(row_num, 8, row['status'], font_style)
			ws.write(row_num, 8, row['condition'], font_style)
			ws.write(row_num, 9, row['drone_type'], font_style)


		wb.save(response)
		return response


	context = {
		'still': still,
		'video': video,
		'lenses': lenses,
		'acc': accessories,
		'lightings': lightings,
		'drons': drones,
		'active_all': all_filter,
		'active_active': active_filter,
		'active_sold': sold_filter,
		'active_expired': expired_filter,
		'active_hidden': hidden_filter,
		'still_table': still_table,
		'video_table': video_table,
		'lens_table': lens_table,
		'accessories_table': accessories_table,
		'lighting_table': lighting_table,
		'drone_table': drone_table,
		'query_params': filter_q,
		'status': status,
		'brand': brand,
		'username': username,
		'profile': profile
	}

	return render(request, 'admin_reporting.html', context)


#
#	EMAIL 
#
@permission_classes((AllowAny, ))
class SendEmailToAll(APIView):

	def post(self, request):
		print ('sending email')
		data = self.request.data

		self.subject = data["subject"]
		self.message = data["message"]
		image = request.FILES

		email = []

		# users = User.objects.all()

		# for u in users:
		# 	email.append(u.email)

		email = []

		still_users = SellCamera.objects.all()
		video_users = SellVideoCamera.objects.all()
		lens_users = SellLenses.objects.all()
		acc_users = SellAccessories.objects.all()
		light_users = SellLighting.objects.all()
		drones_users = SellDrone.objects.all()

		for su in still_users:
			if su.seller.user.email not in email:
				email.append(su.seller.user.email)

		for su in video_users:
			if su.seller.user.email not in email:
				email.append(su.seller.user.email)

		for su in lens_users:
			if su.seller.user.email not in email:
				email.append(su.seller.user.email)

		for su in acc_users:
			if su.seller.user.email not in email:
				email.append(su.seller.user.email)

		for su in light_users:
			if su.seller.user.email not in email:
				email.append(su.seller.user.email)

		for su in drones_users:
			if su.seller.user.email not in email:
				email.append(su.seller.user.email)


		# 
		# send email notifications
		#
		
		email_handler.send_email_notify(email, self.subject, self.message, image)

		status = {"status_code": 200, "detail": ("Your message is successfully sent")}
		return Response(status)


#
#	EMAIL 
#
@permission_classes((AllowAny, ))
class SendEmailToFilterUser(APIView):

	def post(self, request):
		print ('sending email')
		data = self.request.data

		self.subject = data["subject"]
		self.message = data["message"]
		image = request.FILES
		filter_q = data["filter"]

		print ("filter: " + filter_q)

		email = []

		still_users = SellCamera.objects.filter(status=filter_q)
		video_users = SellVideoCamera.objects.filter(status=filter_q)
		lens_users = SellLenses.objects.filter(status=filter_q)
		acc_users = SellAccessories.objects.filter(status=filter_q)
		light_users = SellLighting.objects.filter(status=filter_q)
		drones_users = SellDrone.objects.filter(status=filter_q)

		for su in still_users:
			if su.seller.user.email not in email:
				email.append(su.seller.user.email)

		for su in video_users:
			if su.seller.user.email not in email:
				email.append(su.seller.user.email)

		for su in lens_users:
			if su.seller.user.email not in email:
				email.append(su.seller.user.email)

		for su in acc_users:
			if su.seller.user.email not in email:
				email.append(su.seller.user.email)

		for su in light_users:
			if su.seller.user.email not in email:
				email.append(su.seller.user.email)

		for su in drones_users:
			if su.seller.user.email not in email:
				email.append(su.seller.user.email)


		print (email)
		# 
		# send email notifications
		#
		
		email_handler.send_email_notify(email, self.subject, self.message, image)
		status = {"status_code": 200, "detail": ("Your message is successfully sent")}
		return Response(status)



def reports(request):

	still = SellCamera.objects.filter(status="Sold").order_by("item__cam_type__brand__company")
	video = SellVideoCamera.objects.filter(status="Sold").order_by("item__cam_type__brand__company")
	lens = SellLenses.objects.filter(status="Sold").order_by("item__type_of_lens__brand__company")
	acc = SellAccessories.objects.filter(status="Sold").order_by("brand")
	light = SellLighting.objects.filter(status="Sold").order_by("brand")
	drons = SellDrone.objects.filter(status="Sold").order_by("brand")

	# still_sales = still_sold.values('item__cam_type__brand__company', 'item__model', 'seller__country', 'status').order_by().annotate(Count('item__cam_type__brand__company'))
	# video_sales = video_sold.values('item__cam_type__brand__company', 'item__model', 'seller__country', 'status').order_by().annotate(Count('item__cam_type__brand__company'))
	# lens_sales = lens_sold.values('item__type_of_lens__brand__company', 'item__model', 'seller__country', 'status').order_by().annotate(Count('item__type_of_lens__brand__company'))
	# acc_sales = acc_sold.values('category', 'brand',  'model', 'seller__country', 'status').order_by().annotate(Count('brand'))
	# light_sales = light_sold.values('category', 'brand',  'model', 'seller__country', 'status').order_by().annotate(Count('brand'))
	# drons_sales = drons_sold.values('drone_type', 'brand',  'model', 'seller__country', 'status').order_by().annotate(Count('brand'))

	# print (video_sales)
	# print (lens_sales)
	# print (acc_sales)
	# print (light_sales)
	# print (drons_sales)
	context = {
		'still_brand_sales': still,
		'video_brand_sales': video,
		'lenses_brand_sales': lens,
		'acc_brand_sales': acc,
		'lightings_brand_sales': light,
		'drons_brand_sales': drons,
		'active_all': None,
		'active_sold': None,
		'active_expired': None,
		'active_hidden': None,
	}

	return render(request, 'admin_reports.html', context)


@staff_member_required
def backup(request):
	cmd = ['/home/jglydes/upload.sh', '--arg', 'value']
	subprocess.Popen(cmd).wait()
	print("requesting for database backup")
	messages.add_message(request, messages.SUCCESS, _('Database have successfully backup..'))
	return redirect("/admin/")


@staff_member_required
def maintenance_mode_off(request):
	print("request to turn off maintenance mode")
	print("maintenance mode: " + str(get_maintenance_mode()))

	if get_maintenance_mode():
		set_maintenance_mode(False)

	messages.add_message(request, messages.SUCCESS, _('Maintenance mode turned off..'))

	parsedurl = urlparse(settings.NODEJS_SOCKET_URL)
	baseurl = "%s://%s" % (parsedurl.scheme,parsedurl.hostname)
	baseport = parsedurl.port if parsedurl.port != None else 80
	# Publish record to node.js socket
	with SocketIO(baseurl, baseport) as socketIO:
		socketIO.emit('maintenance_mode', {"update":{"maintenance": False}})
	return redirect("/admin/")


@staff_member_required
def maintenance_mode_on(request):
	print("request to turn on maintenance mode")
	print("maintenance mode: " + str(get_maintenance_mode()))
	if not get_maintenance_mode():
		set_maintenance_mode(True)
	print("maintenance mode after: " + str(get_maintenance_mode()))
	messages.add_message(request, messages.SUCCESS, _('Maintenance mode turned on..'))
	parsedurl = urlparse(settings.NODEJS_SOCKET_URL)
	baseurl = "%s://%s" % (parsedurl.scheme,parsedurl.hostname)
	baseport = parsedurl.port if parsedurl.port != None else 80
	# Publish record to node.js socket
	with SocketIO(baseurl, baseport) as socketIO:
		socketIO.emit('maintenance_mode', {"update":{"maintenance": True}})
	return redirect("/admin/")


@permission_classes((AllowAny, ))
class GetMaintenanceMode(APIView):
	
	def get(self, request): 
		status = {"status": get_maintenance_mode()}
		return Response(status)
	
 

