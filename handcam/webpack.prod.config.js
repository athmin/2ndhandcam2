var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')
var config = require('./webpack.base.config.js')
var CompressionPlugin = require("compression-webpack-plugin");
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const PreloadWebpackPlugin = require('preload-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const StyleExtHtmlWebpackPlugin = require('style-ext-html-webpack-plugin');
const path = require('path');
const autoprefixer = require('autoprefixer');
const staticSourcePath = path.join(__dirname, 'static');
var ip = 'www.2ndhandcam.com'
console.log("Yes I am in web pack prod config");
//config.output.path = require('path').resolve('./handcam/static/bundles/prod/')
config.output.path = require('path').resolve('./static_cdn/bundles/prod/')
config.devtool = 'source-map'

config.plugins = config.plugins.concat([
   // removes a lot of debugging code in React
  new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify('production'),
      'BASE_API_URL': JSON.stringify('https://' + ip + '/api/'),
  }}),
  new webpack.optimize.ModuleConcatenationPlugin(),
  new webpack.optimize.CommonsChunkPlugin({
      name: 'vendors',
      filename: 'vendors.bundle.js', 
      minChunks (module) {
          return module.context && module.context.indexOf('node_modules') >= 0;
      }
  }),
  new BundleTracker({filename: 'handcam/webpack-stats-prod.json'}),

  // minifies your code
  new webpack.optimize.UglifyJsPlugin({
      compress: {
          warnings: false,
          screw_ie8: true,
          conditionals: true,
          unused: true,
          comparisons: true,
          sequences: true,
          dead_code: true,
          evaluate: true,
          if_return: true,
          join_vars: true
      },
      output: {
          comments: false
      }
  }),
  new webpack.LoaderOptionsPlugin({
      options: {
          postcss: [
              autoprefixer({
                  browsers: [
                      'last 3 version',
                      'ie >= 10'
                  ]
              })
          ],
          context: staticSourcePath
      }
  }),
  new webpack.HashedModuleIdsPlugin(),
  new PreloadWebpackPlugin({
      rel: 'preload',
      as: 'script',
      include: 'all',
      fileBlacklist: [/\.(css|map)$/, /base?.+/]
  }),
  new ScriptExtHtmlWebpackPlugin({
      defaultAttribute: 'defer'
  }),
  new ExtractTextPlugin({
      filename: "[name].[contenthash].css",
      allChunks: true
  }),
  new StyleExtHtmlWebpackPlugin({
      minify: true
  }),
  new CompressionPlugin({
    asset: "[path].gz[query]",
    algorithm: "gzip",
    //test: /\.js$|\.css$|\.html$/,
    test: /\.js$|\.jsx$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.svg?.+$/,
    threshold: 10240,
    minRatio: 0.8
  })
])

// Add a loader for JSX files  /\.(js|jsx)$/
config.module.loaders.push(
  { test: /\.(js|jsx)$/, exclude: /node_modules/, loader: 'babel-loader' },
  { test: /\.css$/, loaders: 'style-loader!css-loader' },
  { test: /\.json$/, loader: 'json-loader' },
  { test: /\.scss$/, loader: 'style-loader!css-loader!sass-loader' },
  { test: /\.(png|jpg|woff|woff2|eot|ttf|svg|gif)$/, loader: 'url-loader?limit=300000&name=[name]-[hash].[ext]' } 
)

module.exports = config
