from django.core.mail import send_mail
from django.core.mail import EmailMessage, EmailMultiAlternatives
from .decorators import async

@async
def send_email_activation(email, template):
	send_mail(
		'2ndhandcam email verification',
		'Here is the message.',
		'support@2ndhandcam.com',
		[email,],
		fail_silently=False,
		html_message=template
	)


@async
def send_email_forgotpass(email):
	temp_pass = "new_password"
	send_mail(
			'Request for password reset',
			'You\'re receiving this email because you requested a password reset for your user account at 2ndhandcam.com. \n\n Your temporary password is ' +temp_pass + '\n\nThank you from 2ndhandcam team.',
			'support@2ndhandcam.com',
			[email],
			fail_silently=False,
		)


@async
def send_email_notify(email, subject, message, files):
	
		email = EmailMessage(
			subject,
			message,
			'Admin@2ndhandcam.com',
			bcc=email,
		)
		
		if hasattr(files, 'getlist'):
			files = files.getlist('file')
			for _file in files:
				_file.open()
				
				email.attach(_file.name, _file.read(), _file.content_type)
				_file.close()
		email.send(fail_silently=True)

		# subject, from_email, to = 'hello', 'from@example.com', 'to@example.com'
		# text_content = 'This is an important message.'
		# html_content = '<p>This is an <strong>important</strong> message.</p>'
		# msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
		# msg.attach_alternative(html_content, "text/html")
		# msg.send()


@async
def send_email_customer_service(template):
	send_mail(
			'Customer Services',
			'',
			'support@2ndhandcam.com',
			["admin@2ndhandcam.com"],
			fail_silently=False,
			html_message=template
		)


@async
def send_email_suggestion(template):
	send_mail(
			'Suggestion for 2ndhandcam.com',
			'',
			'support@2ndhandcam.com',
			["admin@2ndhandcam.com"],
			fail_silently=False,
			html_message=template
		)


@async
def send_email_new_models(subject, email, template):
	send_mail(
			subject,
			'',
			'support@2ndhandcam.com',
			["admin@2ndhandcam.com"],
			fail_silently=False,
			html_message=template
		)

@async
def send_email_commercial_account(template):
	send_mail(
			'Request for Commercial Account',
			'',
			'support@2ndhandcam.com',
			["jgludo2181@gmail.com", "admin@2ndhandcam.com"],
			fail_silently=False,
			html_message=template
		)