from itsdangerous import URLSafeTimedSerializer

from django.conf import settings

SECRET_KEY = settings.SECRET_KEY
SECURITY_PASSWORD_SALT = settings.SECURITY_PASSWORD_SALT


def generate_confirmation_token(user_email):
    serializer = URLSafeTimedSerializer(SECRET_KEY)
    return serializer.dumps(user_email, salt=SECURITY_PASSWORD_SALT)


def confirm_token(token, expiration=3600):
    serializer = URLSafeTimedSerializer(SECRET_KEY)
    try:
        user_email = serializer.loads(
            token,
            salt=SECURITY_PASSWORD_SALT,
            max_age=expiration
        )
    except:
        return False
    return user_email