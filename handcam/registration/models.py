from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models

class Registration(models.Model):
    user 			= models.OneToOneField(User, related_name='profile_registration')
    activation_key 	= models.CharField(max_length=100)
    acct_activated	= models.BooleanField(default=False)
    date_activated	= models.DateTimeField(null=True, blank=True)
