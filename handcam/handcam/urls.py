"""handcam URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.views import generic
from rest_framework_jwt.views import obtain_jwt_token
from construction import views

urlpatterns = [
    #url(r'^', views.index, name='under-construction'),
    url(r'^admin/', admin.site.urls),
    url(r'^api/token/auth/', obtain_jwt_token),
    url(r'^api/', include('api.urls')),
    url(r'^api/camera/', include('camera.urls')),
    url(r'^api/lens/', include('lens.urls')),
    url(r'^api/sell/', include('forsale.urls')),
    url(r'^api/dashboard/', include('dashboard.urls')),
    url(r'^api/profile/', include('profiles.urls')),
    url(r'^api/message/', include('messaging.urls')),
    url(r'', generic.TemplateView.as_view(template_name='view.html')),
    url(r'^(?:.*)/?$', generic.TemplateView.as_view(template_name='view.html')),
    url(r'^api/maintenance-mode/', include('maintenance_mode.urls')),
] 

if settings.DEBUG:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    from django.conf.urls.static import static

    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

admin.site.site_header = '2ndhandcam Administration'