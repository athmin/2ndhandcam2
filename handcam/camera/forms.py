from django import forms
from .models import CameraDetails
from pagedown.widgets import PagedownWidget

class OpenCsvForm(forms.Form):

    filepath = forms.FileField(
        label = 'Choose a File',
        required = False
    )


class EditCameraForm(forms.ModelForm):
    features = forms.CharField(widget=PagedownWidget(show_preview=False))
    specifications = forms.CharField(widget=PagedownWidget(show_preview=False))
    

    class Meta:
        model = CameraDetails
        fields = [
            "cam_type",
            "model",
            "features",
            "specifications",
            "image",
        ]