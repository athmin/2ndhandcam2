from django.shortcuts import render
import csv
from .models import Brand, Kind, Type, CameraDetails, Feature, Specification
from django.http import HttpResponse
from .forms import OpenCsvForm, EditCameraForm
import codecs
from docx import Document
import os

from .serializers import CameraDetailSerializer, GetBrandModelSerializers
from rest_framework import generics
from rest_framework import status
from rest_framework_jwt.utils import jwt_encode_handler
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView


def clean_db_strings(request):

	cameras = CameraDetails.objects.all()

	for camera in cameras:
		if camera.sensor_size:
			camera.sensor_size = camera.sensor_size.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.sensor_type:
			camera.sensor_type = camera.sensor_type.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.lcd_size:
			camera.lcd_size = camera.lcd_size.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.image_sensor_format:	
			camera.image_sensor_format = camera.image_sensor_format.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.lcd_resolution:
			camera.lcd_resolution = camera.lcd_resolution.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.weight:	
			camera.weight = camera.weight.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.iso_low:
			camera.iso_low = camera.iso_low.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.iso_high:	
			camera.iso_high = camera.iso_high.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.video:	
			camera.video = camera.video.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.af_points:
			camera.af_points = camera.af_points.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.aperture:	
			camera.aperture = camera.aperture.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.max_aperture:	
			camera.max_aperture = camera.max_aperture.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.focal_length_from:
			camera.focal_length_from = camera.focal_length_from.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.focal_length_to:
			camera.focal_length_to	= camera.focal_length_to.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.optical_zoom:
			camera.optical_zoom = camera.optical_zoom.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.digital_zoom:
			camera.digital_zoom = camera.digital_zoom.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.effective_resolution:
			camera.effective_resolution = camera.effective_resolution.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.film_size:
			camera.film_size = camera.film_size.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.image_size:
			camera.image_size = camera.image_size.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')
		if camera.photo_capacity:
			camera.photo_capacity = camera.photo_capacity.strip('(\'\',)').replace("\t", "").replace(u'\xa0', u' ')

		camera.save()
		print ('finished cleaning ' + camera.model)

	return Response("Cleaning finished.")


def fill_aperture_strings(request):

	cameras = CameraDetails.objects.all()

	for camera in cameras:
		if camera.aperture:	
			camera.aperture = camera.aperture + ")"
		if camera.max_aperture:	
			camera.max_aperture = camera.max_aperture + ")"

		camera.save()

def load_db_cam(request):

	if request.method == "GET":
		form = OpenCsvForm()

	else:
		form = OpenCsvForm(request.POST, request.FILES)

		if form.is_valid():
			path = "/home/jong/Documents/project_files/2ndhandcam/ehabs_files/Canon/Still Camera.csv"
			filepath = form.cleaned_data['filepath']

			reader = csv.reader(codecs.iterdecode(filepath, 'utf-8'))
			for row in reader:
				_, brand = Brand.objects.get_or_create(
								company = row[0]
							)

				x, kind = Kind.objects.get_or_create(
								kind_of_camera = row[1]
							)

				y, camtype = Type.objects.get_or_create(
				 				camera_type = row[2],
				 				kind = x,
				 				brand = _
				 			)
				if row[1] == "Still Camera":
					# z, details = CameraDetails.objects.get_or_create(
					# 				cam_type = y,
					# 				model = row[3],
					# 				mb = row[4],
					# 				sensor_size	= row[5],
					# 				sensor_type = row[6],
					# 				image_sensor_format = row[7],
					# 				lcd_size = row[8],
					# 				lcd_resolution = row[9],
					# 				weight = row[10],
					# 				iso_low	= row[11],
					# 				iso_high = row[12],
					# 				video = row[13],
					# 				frames_per_sec = row[14],
					# 				af_points = row[15],
					# 				aperture = row[19],
					# 				max_aperture = row[20],
					# 				focal_length_from = row[23],
					# 				focal_length_to	= row[24],
					# 				mirrorless = row[16],
					# 				wifi = row[21],
					# 				gps = row[22],
					# 				optical_zoom = row[17],
					# 				digital_zoom = row[18],
					# 				waterproof = row[25],
					# 			)
					# z, details = CameraDetails.objects.get_or_create(
					# 				cam_type = y,
					# 				model = row[3],
					# 				sensor_size	= row[5],
					# 				sensor_type = row[6],
					# 				image_sensor_format = row[7],
					# 				lcd_resolution = row[9],
					# 				video = row[13],
					# 				af_points = row[15],
					# 				aperture = row[19],
					# 				max_aperture = row[20],
					# 				optical_zoom = row[17],
					# 				digital_zoom = row[18],
									
					# 			)
					# if row[21]:
					# 	z.wifi = str(row[21]).upper()
					# if row[22]:
					# 	z.gps = str(row[22]).upper()
					# if row[25]:
					# 	z.waterproof = str(row[25]).upper()
					# if row[16]:
					# 	z.mirrorless = str(row[16]).upper()
					
					# try:
					# 	z.lcd_size = float(row[8])
					# except ValueError:
					# 	pass
					# try:
					# 	z.weight = float(row[10])
					# except ValueError:
					# 	pass
					# try:
					# 	z.iso_low	= int(row[11])
					# except ValueError:
					# 	pass
					# try:
					# 	z.iso_high = int(row[12])
					# except ValueError:
					# 	pass
					# try:
					# 	z.frames_per_sec = float(row[14])
					# except ValueError:
					# 	pass
					# try:
					# 	z.focal_length_from = float(row[23])
					# except ValueError:
					# 	pass
					# try:
					# 	z.focal_length_to	= float(row[24])
					# except ValueError:
					# 	pass
					cam_model = row[0]+" " + row[3]
					z = CameraDetails.objects.get(model=cam_model)
					
					try:
						z.megapixels = float(row[4])
					except ValueError:
						pass
					
					z.sensor_size = row[5],
					z.sensor_type = row[6],
					z.image_sensor_format = row[7],
					z.lcd_size = row[8],
					z.lcd_resolution = row[9],
					z.weight = row[10],
					z.iso_low = row[11],
					z.iso_high = row[12],
					z.video = row[13],
					z.af_points = row[15],
					z.aperture = row[19],
					z.max_aperture = row[20],
					z.focal_length_from = row[23],
					z.focal_length_to	= row[24],
					z.optical_zoom = row[17],
					z.digital_zoom = row[18],

					# z.sensor_size = sensor_size,
					# z.sensor_type = sensor_type,
					# z.image_sensor_format = image_sensor_format,
					# z.lcd_size = lcd_size,
					# z.lcd_resolution = lcd_resolution,
					# z.weight = weight,
					# z.iso_low = iso_low,
					# z.iso_high = iso_high,
					# z.video = video,
					try:
						z.frames_per_sec = float(row[14])
					except ValueError:
						pass
					
					# z.af_points = af_points,
					# z.aperture = aperture,
					# z.max_aperture = max_aperture,
					# z.focal_length_from = focal_length_from,
					# z.focal_length_to	= focal_length_to,
					# z.optical_zoom = optical_zoom,
					# z.digital_zoom = digital_zoom,
					
					if row[21]:
						z.wifi = str(row[21]).upper()
					if row[22]:
						z.gps = str(row[22]).upper()
					if row[25]:
						z.waterproof = str(row[25]).upper()
					if row[16]:
						z.mirrorless = str(row[16]).upper()

					z.effective_resolution = row[26]
					z.film_size = row[27]
					z.image_size = row[28]
					z.photo_capacity = row[29]
					
					
					
					
					#if cd.features:
					# 	cd.cam_features = cd.features.camera_feature
					# if cd.specifications:
					# 	cd.cam_specifications = cd.specifications.specs
					z.save()

				else:
					# zz, details = CameraDetails.objects.get_or_create(
					# 				cam_type = y,
					# 				model = row[3], 
					# 				sensor_size = row[4],
					# 				video = row[5],
					# 				effective_resolution = row[6],
					# 				dynamic_range = row[7],
					# 				iso_low	= row[8],
					# 				iso_high = row[9],
					# 				frame_rate = row[10],
					# 				lcd_size = row[11],
					# 				lcd_resolution = row[12],
					# 				view_finder = row[13],
					# 				weight = row[14],
					# 				lens_mount = row[15],
					# 				optical_zoom = row[16],
					# 				digital_zoom = row[17],
					# 				focal_length_from = row[18],
					# 				focal_length_to	= row[19],
					# 				aperture = row[20],
					# 			)
					
					
					# if row[21]:
					# 	zz.wifi = str(row[21]).upper()
					# if row[22]:
					# 	zz.gps = str(row[22]).upper()
					# try:
					# 	zz.lcd_size = float(row[11])
					# except ValueError:
					# 	pass
					# try:
					# 	zz.weight = float(row[14])
					# except ValueError:
					# 	pass
					# try:
					# 	zz.iso_low	= int(row[8])
					# except ValueError:
					# 	pass
					# try:
					# 	zz.iso_high = int(row[9])
					# except ValueError:
					# 	pass
					# try:
					# 	zz.frame_rate = float(row[10])
					# except ValueError:
					# 	pass
					# try:
					# 	zz.dynamic_range = float(row[7])
					# except ValueError:
					# 	pass
					# try:
					# 	zz.focal_length_from = float(row[18])
					# except ValueError:
					# 	pass
					# try:
					# 	zz.focal_length_to	= float(row[19])
					# except ValueError:
					# 	pass
					
					
					# if cd.features:
					# 	cd.cam_features = cd.features.camera_feature
					# if cd.specifications:
					# 	cd.cam_specifications = cd.specifications.specs
					cam_model = row[0]+" " + row[3]
					if (row[0]=="Red"):
						cam_model = str(row[0]).upper() +" " + row[3]
					
					z = CameraDetails.objects.get(model=cam_model)
					z.frames_per_sec = 0
					z.sensor_size = row[4]
					z.video = row[5]
					z.lcd_size = row[11]
					z.lcd_resolution = row[12]
					z.effective_resolution = row[6]
					z.dynamic_range = row[7]
					z.frame_rate = row[10]
					z.view_finder = row[13]
					z.weight = row[14]
					z.lens_mount = row[15]
					z.optical_zoom = row[16]
					z.digital_zoom = row[17]
					z.focal_length_from = row[18]
					z.focal_length_to	= row[19]
					z.aperture = row[20]

					if row[21]:
						z.wifi = str(row[21]).upper()
					if row[22]:
						z.gps = str(row[22]).upper()

					z.save()
				
				print ("camera model " + str(row[3]) + " saved to db.." )

	context = {
		'form' : form,
	}

	return render(request, 'loaddb.html', context)





def get_features_specs(request):
	if request.method == "GET":
		form = OpenCsvForm()

	else:
		form = OpenCsvForm(request.POST, request.FILES)

		if form.is_valid():
			
			filepath = "/home/jong/Documents/project_files/2ndhandcam/ehabs_files/Nikon/Camera _ Lenses/Still Camera/"

			for typefolder in os.listdir(filepath):
				
				type_dir = filepath + str(typefolder)

				if os.path.isdir(type_dir):
					for model in os.listdir(type_dir):
						
						model_dir = type_dir +"/"+ model

						if os.path.isdir(model_dir):
							
							camdetails = CameraDetails.objects.get(model=model)

							for filename in os.listdir(model_dir):

								if (filename.endswith(".png")) and not (filename.startswith("._")):
									print (model_dir + "/" + str(filename))
									print (filename)

								if (filename.endswith(".docx")) and not (filename.startswith("._")):
									if filename == "Specifications.docx":
										path = model_dir + "/" + filename
										doc = Document(path)
										fullSpecs = []
										for para in doc.paragraphs:
											fullSpecs.append(para.text)
										x, specs = Specification.objects.get_or_create(specs=fullSpecs)
										camdetails.specifications = x
										del fullSpecs[:]

									elif filename == "Features.docx":
										path = model_dir + "/" + filename
										doc = Document(path)
										fullFeats = []
										for para in doc.paragraphs:
											fullFeats.append(para.text)
										y, feats = Feature.objects.get_or_create(camera_feature=fullFeats)
										camdetails.features = y
										del fullFeats[:]
									else:
										print ("invalid filename")

									
									
									camdetails.save()
							print ( str(model) + " specs and feature saved to db..")

	context = {
		'form' : form,
	}
	return render(request, 'loaddb.html', context)


def index(request):

	camlist = CameraDetails.objects.all()

	context = {
		'cameras' : camlist       
	}
	return render(request, 'camera_index.html', context)


def edit_camera_details(request, slug=None):

	if slug:
		post = CameraDetails.objects.get(slug=slug)
	else:
		post = CameraDetails(author=request.user)
	

	if request.method == "POST":
		form = EditCameraForm(request.POST or None, request.FILES or None, instance=post)

		if form.is_valid():

			instance = form.save(commit=False)
			instance.save()
			   
			return HttpResponseRedirect(instance.get_absolute_url())

	else:
		form = EditCameraForm(instance=post)
	
	context = {
		"form": form,
	}
	return render(request, "edit_camera.html", context)


#
# DRF ENDPOINT
# /api/camera/list
@permission_classes((AllowAny, ))
class CameraList(APIView):
	
	def get(self, request, format=None):
		cameras = CameraDetails.objects.all()[:10]
		serializer = CameraDetailSerializer(cameras, many=True)
		return Response(serializer.data)

@permission_classes((AllowAny, ))
class CameraDetailView(APIView):
	def get_object(self, model): 
		try: 
			return CameraDetails.objects.get(model=model) 
		except CameraDetails.DoesNotExist: 
			status = {'error' : 'You are trying to retrieve an item that is not in the database.'}
			return Response(status) 

	def get(self, request, model, format=None): 
		camera = self.get_object(model) 
		serializer = CameraDetailSerializer(camera) 
		return Response(serializer.data)


@permission_classes((AllowAny, ))
class CameraBrandModels(APIView):

	def get_models(self, brand):
		models = CameraDetails.objects.filter(cam_type__brand__company=brand, cam_type__kind__kind_of_camera="Still Camera").order_by('model')
		return models

	def get(self, request, brand, format=None):
		camera_models = self.get_models(brand)
		serializer = GetBrandModelSerializers(camera_models, many=True)
		
		return Response(serializer.data)


@permission_classes((AllowAny, ))
class VideoCamBrandModels(APIView):

	def get_models(self, brand):
		models = CameraDetails.objects.filter(cam_type__brand__company=brand, cam_type__kind__kind_of_camera="Video Camera").order_by('model')
		return models

	def get(self, request, brand, format=None):
		camera_models = self.get_models(brand)
		serializer = GetBrandModelSerializers(camera_models, many=True)
		
		return Response(serializer.data)		
