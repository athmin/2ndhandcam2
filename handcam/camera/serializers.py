from __future__ import (absolute_import, division, print_function, unicode_literals)
from rest_framework import serializers
from .models import Feature, Specification, Type, Kind, Brand, CameraDetails
from django.utils.safestring import mark_safe
from markdown_deux import markdown


class GetCamKindSerializers(serializers.ModelSerializer):
	class Meta:
		model = Kind
		fields = [
			'kind_of_camera',
			]


class GetCamBrandSerializers(serializers.ModelSerializer):
	class Meta:
		model = Brand
		fields = [
			'company',
			]

class GetCamTypeSerializers(serializers.ModelSerializer):
	kind = GetCamKindSerializers(read_only=True)
	brand = GetCamBrandSerializers(read_only=True)
	
	class Meta:
		model = Type
		fields = [
			'camera_type',
			'kind',
			'brand',
			]


class GetCamFeaturesSerializers(serializers.ModelSerializer):
	class Meta:
		model = Feature
		fields = [
			'camera_feature',
			]


class GetCamSpecsSerializers(serializers.ModelSerializer):
	class Meta:
		model = Specification
		fields = [
			'specs',
			]


class GetBrandModelSerializers(serializers.ModelSerializer):
	cam_type = GetCamTypeSerializers(read_only=True)
	
	class Meta:
		model = CameraDetails
		fields = [
			'id',
			'model',
			'cam_type',
			'image',
			'image_thumbnail'
		]


class CameraDetailSerializer(serializers.ModelSerializer):
	cam_type = GetCamTypeSerializers(read_only=True)
	#features = GetCamFeaturesSerializers(read_only=True)
	#specifications = GetCamSpecsSerializers(read_only=True)

	class Meta:
		model = CameraDetails
		fields = [
			'id',
			'cam_type',
			'model',
			'megapixels',
			'sensor_size',
			'sensor_type',
			'image_sensor_format',
			'lcd_size',
			'lcd_resolution',
			'weight',
			'iso_low',
			'iso_high',
			'video',
			'frames_per_sec',
			'af_points',
			'aperture',
			'max_aperture',
			'focal_length_from',
			'focal_length_to',
			'mirrorless',
			'wifi',
			'gps',
			'optical_zoom',
			'digital_zoom',
			'waterproof',
			'effective_resolution',
			'dynamic_range',
			'frame_rate',
			'view_finder',
			'lens_mount',
			'features',
			'specifications',
			'cam_features',
			'cam_specifications',
			'image',
		]

	def get_markdown_specs(self):
		content = self.cam_specifications
		markdown_text = markdown(content)
		return mark_safe(markdown_text)

	def get_markdown_features(self):
		content = self.cam_features
		markdown_text = markdown(content)
		return mark_safe(markdown_text)