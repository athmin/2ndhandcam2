from django.db import models
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.template.defaultfilters import slugify
from django.utils.safestring import mark_safe
from markdown_deux import markdown



def upload_location(instance, filename):
	
	return "{0}/{1}/{2}/{3}/{4}".format(instance.cam_type.brand.company, instance.cam_type.kind.kind_of_camera, instance.cam_type.camera_type, instance.model, filename)

def upload_location_thumbnail(instance, filename):
	thumb_name = "thumb.png"
	return "{0}/{1}/{2}/{3}/{4}".format(instance.cam_type.brand.company, instance.cam_type.kind.kind_of_camera, instance.cam_type.camera_type, instance.model, thumb_name)

# models containing camera features
# to get from word document
class Feature(models.Model):
	camera_feature 		= models.TextField()


# models containing camera specifications
# 
class Specification(models.Model):
	specs 				= models.TextField()


# model containing what kind of camera
# still, video
class Kind(models.Model):
	kind_of_camera 		= models.CharField(max_length=20)

	def __str__(self):
		return self.kind_of_camera


# model containing brand of the camera
# Canon, Nikon, etc
class Brand(models.Model):
	company 			= models.CharField(max_length=50)

	def __str__(self):
		return self.company


# model containing camera types
# dslr, compact, etc
class Type(models.Model):
	camera_type 		= models.CharField(max_length=50)
	kind 				= models.ForeignKey(Kind)
	brand 				= models.ForeignKey(Brand)

	def __str__(self):
		return str(self.brand) + "-" + str(self.kind) + "-" + str(self.camera_type)

# models containing full camera details
# will be decoded from excel files
class CameraDetails(models.Model):
	cam_type 			= models.ForeignKey(Type)
	model  				= models.CharField(max_length=80)
	slug            	= models.SlugField(max_length=255, unique=True, blank=True)
	megapixels 			= models.FloatField(default=None, help_text='megapixels', null=True, blank=True)
	sensor_size			= models.CharField(max_length=30, null=True, blank=True)
	sensor_type 		= models.CharField(max_length=30, null=True, blank=True)
	image_sensor_format = models.CharField(max_length=30, null=True, blank=True)
	lcd_size 			= models.CharField(max_length=30, help_text='in inches', null=True, blank=True)
	lcd_resolution 		= models.CharField(max_length=80, null=True, blank=True)
	weight 				= models.CharField(max_length=30, help_text='grams', null=True, blank=True)
	iso_low				= models.CharField(max_length=30, help_text='iso low', null=True, blank=True)
	iso_high 			= models.CharField(max_length=30, help_text='iso high', null=True, blank=True)
	video 				= models.CharField(max_length=20, help_text='4k / HD / Full HD', null=True, blank=True)
	frames_per_sec 		= models.FloatField(default=None, help_text='fps', null=True, blank=True)
	af_points 			= models.CharField(max_length=20, help_text='yes / no', null=True, blank=True)
	aperture 			= models.CharField(max_length=50, help_text='f-numbers', null=True, blank=True)
	max_aperture 		= models.CharField(max_length=20, help_text='f-numbers', null=True, blank=True)
	focal_length_from 	= models.CharField(max_length=30, help_text='focal length from', null=True, blank=True)
	focal_length_to		= models.CharField(max_length=30, help_text='focal length to', null=True, blank=True)
	mirrorless 			= models.CharField(max_length=10, help_text='yes / no', null=True, blank=True)
	wifi 				= models.CharField(max_length=10, help_text='yes / no', null=True, blank=True)
	gps 				= models.CharField(max_length=10, help_text='yes / no', null=True, blank=True)
	optical_zoom 		= models.CharField(max_length=20, help_text='x', null=True, blank=True)
	digital_zoom 		= models.CharField(max_length=20, help_text='x', null=True, blank=True)
	waterproof 			= models.CharField(max_length=10, help_text='yes / no', null=True, blank=True)
	effective_resolution= models.CharField(max_length=50, help_text='for video camera', null=True, blank=True)
	dynamic_range 		= models.CharField(max_length=30, help_text='stops / for video camera', null=True, blank=True)
	frame_rate 			= models.CharField(max_length=30, help_text='Hz / for video camera', null=True, blank=True)
	view_finder 		= models.CharField(max_length=50, help_text='for video camera', null=True, blank=True)
	lens_mount 			= models.CharField(max_length=50, help_text='for video camera', null=True, blank=True)
	film_size			= models.CharField(max_length=50, help_text='film size', null=True, blank=True)
	image_size			= models.CharField(max_length=50, help_text='image size', null=True, blank=True)
	photo_capacity		= models.CharField(max_length=50, help_text='photo capacity', null=True, blank=True)
	effective_pixels	= models.CharField(max_length=50, help_text='effective pixels', null=True, blank=True)
	features 			= models.ForeignKey(Feature, on_delete=models.CASCADE, null=True, blank=True)
	specifications 		= models.ForeignKey(Specification, related_name="still_cam_specs", on_delete=models.CASCADE, null=True, blank=True)
	cam_features		= models.TextField(null=True, blank=True)
	cam_specifications  = models.TextField(null=True, blank=True)
	image 				= models.ImageField(upload_to=upload_location, 
							null=True, 
							blank=True,
							max_length=255)
	image_thumbnail		= models.ImageField(upload_to=upload_location_thumbnail, 
							null=True, 
							blank=True,
							max_length=255)
	date 				= models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name_plural = "Camera Details"

	def __str__(self):

		return str( self.slug )


	@models.permalink
	def get_absolute_url(self):
		return ('view_camera_details', (), {'slug' :self.slug,})

	def get_markdown_specs(self):
		content = self.specifications
		markdown_text = markdown(content)
		return mark_safe(markdown_text)

	def get_markdown_features(self):
		content = self.features
		markdown_text = markdown(content)
		return mark_safe(markdown_text)


#HELPER FUNCTIONS

def create_slug(instance, new_slug=None):
	slug = slugify(instance.model)
	if new_slug is not None:
		slug = new_slug
	qs = CameraDetails.objects.filter(slug=slug).order_by("-id")
	exists = qs.exists()
	if exists:
		new_slug = "%s-%s" %(slug, qs.first().id)
		return create_slug(instance, new_slug=new_slug)
	return slug

#SIGNALS
def pre_save_camera_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_slug(instance)

pre_save.connect(pre_save_camera_receiver, sender=CameraDetails)