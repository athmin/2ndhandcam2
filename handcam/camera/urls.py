from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from camera import views
import lens.views 
 
urlpatterns = [
	url(r'^list/$', views.CameraList.as_view(), name='camera_list'),
    url(r'^detail/(?P<model>.+)/$', views.CameraDetailView.as_view(), name='camera_detail'),
    url(r'^models/(?P<brand>.+)$', views.CameraBrandModels.as_view(), name='camera_brand_models'),
    url(r'^video-models/(?P<brand>.+)$', views.VideoCamBrandModels.as_view(), name='videocam_brand_models'),
    url(r'^load-db-cam/$', views.load_db_cam, name='load_database_cam'),
    url(r'^clean-db-cam/$', views.clean_db_strings, name='clean_database_cam'),
    url(r'^fill-cam-aperture/$', views.fill_aperture_strings, name='fill_cam_aperture'),
]