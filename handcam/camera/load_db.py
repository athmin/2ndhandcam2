import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "secondhandcam.settings")

import csv
from django.conf import settings
from django.db import models
from secondhandcam.camera.models import Brand, Kind, Type, CameraDetails

path = "/home/jong/Documents/project_files/2ndhandcam/ehabs_files/Canon/Canon Excel Sheet/Still Camera.csv"
with open(path) as f:
	reader = csv.reader(f)
	for row in reader:
		_, brand = Brand.get_or_create(
						company = row[0]
					)

		x, kind = Kind.get_or_create(
						kind_of_camera = row[1]
					)

		y, camtype = Type.get_or_create(
						camera_type = row[2],
						kind = kind,
						brand = brand
					)

		z, details = CameraDetails.get_or_create(
						cam_type = camtype,
						model = row[3],
						mb = row[4],
						sensor_size	= row[5],
						sensor_type = row[6],
						image_sensor_format = row[7],
						lcd_size = row[8],
						lcd_resolution = row[9],
						weight = row[10],
						iso_low	= row[11],
						iso_high = row[12],
						video = row[13],
						frames_per_sec = row[14],
						af_points = row[15],
						aperture = row[19],
						max_aperture = row[20],
						focal_length_from = row[23],
						focal_length_to	= row[24],
						mirrorless = row[16],
						wifi = row[21],
						gps = row[22],
						optical_zoom = row[17],
						digital_zoom = row[18],
						waterproof = row[25],
					)
