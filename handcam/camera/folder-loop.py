#from .models import Image
import os


main_directory = "/home/jong/Documents/project_files/2ndhandcam/ehabs_files/Nikon/Camera _ Lenses/Still Camera/DSLR Cameras/"
images_count = 0

for folder in os.listdir(main_directory):
	
	sub_dir = main_directory + folder

	if os.path.isdir(sub_dir):
		
		dirc = main_directory.split('/')

		model = dirc[-1]
		cam_type = dirc[-2]
		cam_kind = dirc[-3]
		cam_brand = dirc[-4]

		# print (cam_kind)
		# print (cam_brand)
		# print (model)
		# print (cam_type)

		for filename in os.listdir(sub_dir):
			if (filename.endswith(".png")) and not (filename.startswith("._")):
				print (sub_dir + "/" + str(filename))
				images_count += 1

print ("total images " + str(images_count))


CAMERA CSV
column		description
0			Brand
1 			Kind	
2 			Type	
3			Model	
4 			MB	
5  			Sensor Size	
6 			Sensor Type	
7 			Image Sensor Format	
8 			LCD size	
9 			LCD Resolution	
10 			Weight 	
11 			ISO Low 	
12 			ISO High	
13 			Video (4k/full HD/HD)	
14 			Frames per Sec	
15 			AF points 	
16 			Mirrorless 	
17 			optical zoom 	
18 			Digital zoom	
19 			Aperture	
20 			Max Apeture	
21 			WiFi 	
22 			GPS	
23			Focal Length From	
24			Focal Lenth TO	
25  		Water Proof

		
LENS CSV
column		description
0 			Brand 	
1 			Type	
2			Model	
3 			Focal Length From	
4			Focal Length TO	
5 			Aperture Max	
6			Aperture to 	
7			Weight 	
8			Format range 	
9 			Lens Type	
10   		and	
11 			Lens Format	
12 			Focus Type	
13			Fixed Focal

VIDEO CAMERA
0			Brand
1 			Kind	
2 			Type	
3 			Model	
4 			Sensor Size	
5 			Video	
6			Effective Resolution	
7			Dynamic Range	
8			ISO Hi	
9			ISO Low	
10			Frame Rate	
11			LCD Size	
12			LCD Resolution	
13			View Finder	
14			Weight	
15			Lens Mount	
16			Optical Zoom	
17			Digital Zoom	
18			Focal Length From	
19			Focal Length To	
10			Aperture	
21			Wifi	
22			GPS

cam_type
model 
sensor_size
video
effective_resolution
dynamic_range
iso_low
iso_high
frame_rate
lcd_size
lcd_resolution
view_finder
weight
lens_mount
optical_zoom
digital_zoom
focal_length_from
focal_length_to
aperture
wifi
gps



