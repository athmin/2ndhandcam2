from django.contrib import admin
from django.contrib.auth.models import User
from .models import Brand, Kind, Type, Feature, Specification, CameraDetails
from django.db import models
from pagedown.widgets import AdminPagedownWidget

class CameraDetailAdmin(admin.ModelAdmin):
	def cam_image(self):
		if self.image:
			return '<img src="/%s" width="50" height="50" />' % self.image.url
		else:
			return "No Image to Display"

	cam_image.allow_tags = True
	
	list_display = ('cam_type', 'model', cam_image)
	list_filter = ('cam_type__kind__kind_of_camera', 'cam_type__brand__company','cam_type__camera_type')
	search_fields = ('cam_type__camera_type', 'cam_type__brand__company', 'model')
	ordering = ('cam_type__brand__company', )

	formfield_overrides = {
        models.TextField: {'widget': AdminPagedownWidget },
    }


class CameraTypeAdmin(admin.ModelAdmin):

	list_display = ('camera_type', 'kind', 'brand')
	list_filter = ('brand__company', 'kind__kind_of_camera', 'camera_type')
	search_fields = ('brand__company', 'kind__kind_of_camera', 'camera_type')


class AlbumAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': AdminPagedownWidget },
    }

# Register your models here.
#admin.site.register(Brand)
#admin.site.register(Kind)
#admin.site.register(Type, CameraTypeAdmin)
admin.site.register(CameraDetails, CameraDetailAdmin)
#admin.site.register(Specification, AlbumAdmin)
#admin.site.register(Feature, AlbumAdmin)


