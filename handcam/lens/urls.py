from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from lens import views
 
 
urlpatterns = [
    url(r'^load-db-cam/$', views.load_db_lens, name='load_database_lens'),
    url(r'^models/(?P<brand>.+)$', views.LensBrandModels.as_view(), name='lens_brand_models'),
    url(r'^detail/(?P<model>.+)/$', views.LensDetailView.as_view(), name='lens_detail'),
]