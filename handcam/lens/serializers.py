from __future__ import (absolute_import, division, print_function, unicode_literals)
from rest_framework import serializers
from .models import LensFeature, LensSpecification, LensType, LensDetails
from camera.models import Brand
from django.utils.safestring import mark_safe
from markdown_deux import markdown


class GetCamBrandSerializers(serializers.ModelSerializer):
	class Meta:
		model = Brand
		fields = [
			'company',
			]

class GetLensTypeSerializers(serializers.ModelSerializer):
	brand = GetCamBrandSerializers(read_only=True)
	
	class Meta:
		model = LensType
		fields = [
			'lens_type',
			'brand',
			]


class GetLensModelSerializers(serializers.ModelSerializer):
	type_of_lens = GetLensTypeSerializers(read_only=True)
	
	class Meta:
		model = LensDetails
		fields = [
			'id',
			'model',
			'type_of_lens',
			'image',
			'image_thumbnail'
		]


class LensDetailSerializer(serializers.ModelSerializer):
	id = serializers.IntegerField(required=False)
	type_of_lens = GetLensTypeSerializers(read_only=True)

	class Meta:
		model = LensDetails
		fields = [
			'id',
			'type_of_lens',
			'model',
			'slug',
			'aperture',
			'max_aperture',
			'focal_length_from',
			'focal_length_to',
			'weight',
			'format_range',
			'lens_type',
			'lens_type_and',
			'fixed_focal',
			'lens_format',
			'focus_type',
			'mounts',
			'magnification',
			'features',
			'specifications',
			'image'
		]

	def get_markdown_specs(self):
		content = self.specifications
		markdown_text = markdown(content)
		return mark_safe(markdown_text)

	def get_markdown_features(self):
		content = self.features
		markdown_text = markdown(content)
		return mark_safe(markdown_text)