from django.shortcuts import render
from django.http import HttpResponse
from camera.forms import OpenCsvForm
from camera.models import Brand
from .models import LensDetails, LensFeature, LensSpecification, LensType
from .serializers import GetLensModelSerializers, LensDetailSerializer

from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

import csv
import codecs

def load_db_lens(request):

	if request.method == "GET":
		form = OpenCsvForm()

	else:
		form = OpenCsvForm(request.POST, request.FILES)

		if form.is_valid():
			filepath = form.cleaned_data['filepath']

			reader = csv.reader(codecs.iterdecode(filepath, 'utf-8'))
			for row in reader:
				# _, brand = Brand.objects.get_or_create(
				# 				company = row[0]
				# 			)

				# y, camtype = LensType.objects.get_or_create(
				# 				lens_type = row[1],
				# 				brand = _
				# 			)

				# z, details = LensDetails.objects.get_or_create(
				# 				type_of_lens = y,
				# 				model = row[2],
				# 				focal_length_from = row[3],
				# 				focal_length_to = row[4],
				# 				aperture = row[5],
				# 				max_aperture = row[6],
				# 				weight = row[7],
				# 				format_range = row[8],
				# 				lens_type = row[9],
				# 				lens_type_and = row[10],
				# 				lens_format = row[11],
				# 				focus_type = row[12],
				# 				fixed_focal	= row[13],
				# 				mounts = row[14],
				# 				magnification = row[15]
				# 			)
				# if row[3] != '':
				# 	try:
				# 		z.focal_length_from = float(row[3])
				# 	except ValueError:
				# 		pass
				# if row[4] != '':			
				# 	try:
				# 		z.focal_length_to = float(row[4])
				# 	except ValueError:
				# 		pass
				# if row[6] != '':
				# 	try:
				# 		z.max_aperture	= float(row[6])
				# 	except ValueError:
				# 		pass
				# if row[7] != '':
				# 	try:
				# 		z.weight = float(row[7])
				# 	except ValueError:
				# 		pass
				# if row[13] != '':
				# 	try:
				# 		z.fixed_focal	= float(row[13])
				# 	except ValueError:
				# 		pass
				# z.save()

				lens = LensDetails.objects.get(model=row[2])
				# if row[3] != '':
				# 	try:
				# 		lens.focal_length_from = float(row[3])
				# 	except ValueError:
				# 		pass
				# if row[4] != '':			
				# 	try:
				# 		lens.focal_length_to = float(row[4])
				# 	except ValueError:
				# 		pass
				lens.fixed_focal = row[13]
				lens.magnification = row[15]
				lens.save()

				print ("lens model " + str(row[2]) + " saved to db.." )

	context = {
		'form' : form,
	}

	return render(request, 'load_db_lens.html', context)


@permission_classes((AllowAny, ))
class LensBrandModels(APIView):

	def get_models(self, brand):
		models = LensDetails.objects.filter(type_of_lens__brand__company=brand).order_by('model')
		return models

	def get(self, request, brand, format=None):
		print ("requesting lens brands")
		print (brand)
		lens_models = self.get_models(brand)
		serializer = GetLensModelSerializers(lens_models, many=True)
		
		return Response(serializer.data)


@permission_classes((AllowAny, ))
class LensDetailView(APIView):
	def get_object(self, model): 
		try: 
			return LensDetails.objects.get(model=model) 
		except LensDetails.DoesNotExist: 
			raise Http404 

	def get(self, request, model, format=None): 
		print (model)
		camera = self.get_object(model) 
		serializer = LensDetailSerializer(camera) 
		#print(serializer.data)
		return Response(serializer.data)
