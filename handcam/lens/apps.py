from django.apps import AppConfig


class LenseConfig(AppConfig):
    name = 'lens'
