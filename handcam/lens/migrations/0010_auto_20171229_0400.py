# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-12-29 04:00
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lens', '0009_auto_20171207_1323'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lensdetails',
            name='focal_length_from',
        ),
        migrations.RemoveField(
            model_name='lensdetails',
            name='focal_length_to',
        ),
    ]
