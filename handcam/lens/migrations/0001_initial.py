# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-30 01:19
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import lens.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('camera', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LensDetails',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('model', models.CharField(max_length=80)),
                ('slug', models.SlugField(blank=True, unique=True)),
                ('aperture', models.CharField(blank=True, help_text='f-numbers', max_length=30, null=True)),
                ('max_aperture', models.CharField(blank=True, help_text='f-numbers', max_length=30, null=True)),
                ('focal_length_from', models.CharField(blank=True, help_text='focal length from', max_length=30, null=True)),
                ('focal_length_to', models.CharField(blank=True, help_text='focal length to', max_length=30, null=True)),
                ('weight', models.CharField(blank=True, help_text='grams', max_length=30, null=True)),
                ('format_range', models.CharField(blank=True, help_text='format range', max_length=50, null=True)),
                ('lens_type', models.CharField(blank=True, help_text='lens type', max_length=50, null=True)),
                ('lens_type_and', models.CharField(blank=True, help_text='lens type and', max_length=50, null=True)),
                ('fixed_focal', models.CharField(blank=True, help_text='grams', max_length=30, null=True)),
                ('lens_format', models.CharField(blank=True, help_text='lens format', max_length=50, null=True)),
                ('focus_type', models.CharField(blank=True, help_text='focus type', max_length=50, null=True)),
                ('image', models.ImageField(blank=True, null=True, upload_to=lens.models.upload_location)),
                ('date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name_plural': 'Lens Details',
            },
        ),
        migrations.CreateModel(
            name='LensFeature',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('camera_feature', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='LensSpecification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('specs', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='LensType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lens_type', models.CharField(max_length=50)),
                ('brand', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='camera.Brand')),
            ],
        ),
        migrations.AddField(
            model_name='lensdetails',
            name='features',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='lens.LensFeature'),
        ),
        migrations.AddField(
            model_name='lensdetails',
            name='specifications',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='still_cam_specs', to='lens.LensSpecification'),
        ),
        migrations.AddField(
            model_name='lensdetails',
            name='type_of_lens',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lens.LensType'),
        ),
    ]
