from camera.models import Brand
from django.db import models
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.template.defaultfilters import slugify
from django.utils.safestring import mark_safe
from markdown_deux import markdown


def upload_location(instance, filename):
	
	return "{0}/{1}/{2}/{3}".format(instance.type_of_lens.brand.company, instance.type_of_lens.lens_type, instance.model, filename)

def upload_location_thumbnail(instance, filename):
	thumb_name = "thumb.png"
	return "{0}/{1}/{2}/{3}".format(instance.type_of_lens.brand.company, instance.type_of_lens.lens_type, instance.model, thumb_name)


# models containing camera features
# to get from word document
class LensFeature(models.Model):
	lens_feature 		= models.TextField()


# models containing camera specifications
# 
class LensSpecification(models.Model):
	specs 				= models.TextField()


# model containing camera types
# dslr, compact, etc
class LensType(models.Model):
	lens_type 			= models.CharField(max_length=50)
	brand 				= models.ForeignKey(Brand)

	def __str__(self):
		return str(self.brand) + "-" + str(self.lens_type)
		

# models containing full camera details
# will be decoded from excel files
class LensDetails(models.Model):
	type_of_lens 		= models.ForeignKey(LensType)
	model  				= models.CharField(max_length=80)
	slug            	= models.SlugField(max_length=255, unique=True, blank=True)
	aperture 			= models.CharField(max_length=30, help_text='f-numbers', null=True, blank=True)
	max_aperture 		= models.CharField(max_length=30, help_text='f-numbers', null=True, blank=True)
	focal_length_from 	= models.FloatField(default=None, help_text='focal length from', null=True, blank=True)
	focal_length_to		= models.FloatField(default=None, help_text='focal length to', null=True, blank=True)
	weight 				= models.CharField(max_length=30, help_text='grams', null=True, blank=True)
	format_range		= models.CharField(max_length=50, help_text='format range', null=True, blank=True)
	lens_type			= models.CharField(max_length=50, help_text='lens type', null=True, blank=True)
	lens_type_and		= models.CharField(max_length=50, help_text='lens type and', null=True, blank=True)
	fixed_focal			= models.CharField(max_length=30, help_text='mm', null=True, blank=True)
	lens_format			= models.CharField(max_length=50, help_text='lens format', null=True, blank=True)
	focus_type			= models.CharField(max_length=50, help_text='focus type', null=True, blank=True)
	mounts				= models.CharField(max_length=250, help_text='mounts', null=True, blank=True)
	magnification		= models.CharField(max_length=50, help_text='magnification', null=True, blank=True)
	features			= models.TextField(null=True, blank=True)
	specifications  	= models.TextField(null=True, blank=True)
	image 				= models.ImageField(upload_to=upload_location, 
							null=True, 
							blank=True,
							max_length=255)
	image_thumbnail		= models.ImageField(upload_to=upload_location_thumbnail, 
							null=True, 
							blank=True,
							max_length=255)
	date 				= models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name_plural = "Lens Details"


	def __str__(self):
		return str(self.type_of_lens.brand.company) +" "+ str(self.model) +" "+ str(self.lens_type)


	@models.permalink
	def get_absolute_url(self):
		return ('view_lens_details', (), {'slug' :self.slug,})

	def get_markdown_specs(self):
		content = self.specifications
		markdown_text = markdown(content)
		return mark_safe(markdown_text)

	def get_markdown_features(self):
		content = self.features
		markdown_text = markdown(content)
		return mark_safe(markdown_text)


#HELPER FUNCTIONS

def create_slug(instance, new_slug=None):
	slug = slugify(instance.model)
	if new_slug is not None:
		slug = new_slug
	qs = LensDetails.objects.filter(slug=slug).order_by("-id")
	exists = qs.exists()
	if exists:
		new_slug = "%s-%s" %(slug, qs.first().id)
		return create_slug(instance, new_slug=new_slug)
	return slug

#SIGNALS
def pre_save_lens_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = create_slug(instance)

pre_save.connect(pre_save_lens_receiver, sender=LensDetails)