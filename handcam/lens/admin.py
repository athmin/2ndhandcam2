from django.contrib import admin
from django.contrib.auth.models import User
from .models import Brand, LensType, LensFeature, LensSpecification, LensDetails
from django.db import models
from pagedown.widgets import AdminPagedownWidget

class LensDetailAdmin(admin.ModelAdmin):
	def lens_image(self):
		if self.image:
			return '<img src="/%s" width="50" height="50" />' % self.image.url
			#return "<img src='{}'  width='40' height='40' />".format(self.image.url) 
		else:
			return "No Image to Display"

	lens_image.allow_tags = True
 
	list_display = ('type_of_lens', 'model', lens_image)
	list_filter = ('type_of_lens__brand__company', 'type_of_lens__lens_type')
	search_fields = ('type_of_lens__lens_type', 'type_of_lens__brand__company', 'model')
	ordering = ['type_of_lens__brand__company',]

	formfield_overrides = {
        models.TextField: {'widget': AdminPagedownWidget },
    }

class LensTypeAdmin(admin.ModelAdmin):

	list_display = ('lens_type', 'brand')
	list_filter = ('brand__company', 'lens_type')
	search_fields = ('brand__company', 'lens_type')


# Register your models here.
#admin.site.register(LensType)
admin.site.register(LensDetails, LensDetailAdmin)
#admin.site.register(LensSpecification)
#admin.site.register(LensFeature)

