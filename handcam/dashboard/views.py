from django.shortcuts import render

from rest_framework import generics
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from forsale.models import SellCamera, SellLenses
from forsale.serializers import GetCamForSaleSerializer

from profiles.models import Profile

class dashboard(APIView):
	def get_profile(user):
		try:
			prof = Profile.objects.get(user=request.user)
		except Profile.DoesNotExist:
			status = {"status_code": 404, "detail": _("Profile does not exist!")}
			return Response(status)

	def get(self, request, format=None):
		profile = get_profile(request.user)
		
		saleitems = SellCamera.objects.filter(profile=profile)
		serializer = GetCamForSaleSerializer(saleitems, many=True)
		return Response(serializer.data)
		
