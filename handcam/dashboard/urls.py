from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from dashboard import views 
 
urlpatterns = [
	url(r'^', views.dashboard.as_view(), name='profile-dashboard'),
]