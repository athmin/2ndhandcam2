import * as types from './types'
import { authHeader } from '../lib'
import { history } from '../store/configureStore'
let CONFIG = require('../lib/config.json')


export const editUserAction = {
	edituser
}

export function edituser(data){
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/profile/edit-user/', {
			method: 'post',
			headers: authHeader(),
			body: data
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(useredit({ user : responseData }))
		}).catch((error) => {
			console.log('error: ' + error)
		});
	}	
}


export function useredit({ user }){
	return {
		type: types.USER_EDIT,
		user
	}
}