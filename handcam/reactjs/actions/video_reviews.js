import * as types from './types'
import { credentials } from '../lib/youtube-api-key';
const API_KEY = credentials.key;

export const getVideoReviewsAction = {
	getVideoReviews
}


export function getVideoReviews(model){
	
	return (dispatch, getState) => {
		return fetch('https://www.googleapis.com/youtube/v3/search?key='+API_KEY+'&part=snippet&q='+model+' review&maxResults=5')
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(videoReviews({reviews: responseData}))
		}).catch((error) => {
			console.log(error);
		});
	}	
}


export function videoReviews({ reviews }){
	
	return {
		type: types.GET_VIDEO_REVIEWS,
		reviews,
	}
}
