import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const sellVideoLensPackageActions = {
    sellVideoLensPackage
};


export function sellVideoLensPackage(data) {
		
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/sell/video_lens_package/upload/', {
			method: 'Post',
		    headers: authHeader(),
		    body: data
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
			  	if (responseData.error){
			  		dispatch(lensVideoPackage({sell: responseData}));
			  	}else{
			  		dispatch(lensVideoPackage({sell: responseData}));
					history.push("/dashboard")
			  	}
				
		  }).catch( (error) => {
		      	console.log('error' +' '+ error);
		  });
	}
}

export function lensVideoPackage({ sell }) {
  return {
    type: types.VIDEO_SELL,
    sell,
  }
}