import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const getUserProfileAction = {
    userCredentials,
};


export function userCredentials() {
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/profile/user-credentials', {
			method: 'get',
		    headers: authHeader(),
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  		
		  		dispatch(user_cred({credentials: responseData}));
		  }).catch( (error) => {
		      console.log(error);
		  });
	}
}


export function user_cred({ credentials }) {
	return {
	    type: types.USER_CREDENTIALS,
	    credentials,
	}
}