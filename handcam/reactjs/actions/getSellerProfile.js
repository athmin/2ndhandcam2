import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const getSellerProfileAction = {
    sellerProfile,
};


export function sellerProfile(pk) {
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/profile/seller_profile/' + pk, {
			method: 'get',
		    headers: authHeader(),
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  		
		  		dispatch(hide_contact({hide_contact: responseData}));
		  }).catch( (error) => {
		      console.log(error);
		  });
	}
}


export function hide_contact({ hide_contact }) {
	return {
	    type: types.SELLER_HIDE_CONTACT,
	    hide_contact,
	}
}