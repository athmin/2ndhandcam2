import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader_json } from '../lib'
let CONFIG = require('../lib/config.json')


export const contactUsActions = {
    suggestion,
    customer_service,
    new_model,
    commercial_account
};


export function suggestion(data) {

	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/suggestion/', {
			method: 'Post',
			headers: authHeader_json(),
			body: JSON.stringify({
		      email: data.email,
		      name: data.name,
		      mobile: data.mobile,
		      country: data.country,
		      subject: data.subject,
		      suggestion: data.suggestion,
		    })
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(sendSuggestion({message: responseData}));
		}).catch((error) => {
			console.log('error: ' + error)
		});
	}
}

export function sendSuggestion({ message }){
	return {
		type: types.SEND_SUGGESTION,
		message,
	}
}


export function customer_service(data) {

	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/customer_service/', {
			method: 'Post',
			headers: authHeader_json(),
			body: JSON.stringify({
		      email: data.email,
		      name: data.name,
		      mobile: data.mobile,
		      country: data.country,
		      subject: data.subject,
		      message: data.text,
		    })
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(sendCustomerService({message: responseData}));
		}).catch((error) => {
			console.log('error: ' + error)
		});
	}
}

export function sendCustomerService({ message }){
	return {
		type: types.SEND_CUSTOMER_SERVICE,
		message,
	}
}


export function new_model(data) {

	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/new_model_notification/', {
			method: 'Post',
			headers: authHeader_json(),
			body: JSON.stringify({
		      email: data.email,
		      brand_name: data.brand_name,
		      model: data.model,
		      message: data.text,
		      subject: data.subject,
		    })
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(sendNewModel({message: responseData}));
		}).catch((error) => {
			console.log('error: ' + error)
		});
	}
}

export function sendNewModel({ message }){
	return {
		type: types.SEND_CUSTOMER_SERVICE,
		message,
	}
}


export function commercial_account(data) {
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/commercial_account/', {
			method: 'Post',
			headers: authHeader_json(),
			body: JSON.stringify({
		      email: data.email,
		      name: data.username,
		      mobile: data.mobile,
		      country: data.country,
		      message: data.text,
		    })
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(comAcc({message: responseData}));
		}).catch((error) => {
			console.log('error: ' + error)
		});
	}
}

export function comAcc({ message }){
	return {
		type: types.SEND_CUSTOMER_SERVICE,
		message,
	}
}