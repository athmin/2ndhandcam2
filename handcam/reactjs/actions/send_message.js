import * as types from './types'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')

export const sendMessageAction = {
	sendMessageToSeller
};

export function sendMessageToSeller(data){

	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/message/create/', {
			method: 'post',
			headers: authHeader(),
			body: data
		})
		.then((response) => response.json())
		.then((responseData) => {
			
			dispatch(sendMessage({message: responseData}));
		}).catch((error) => {
			console.log('error: ' + error)
		});
	}
} 

export function sendMessage({ message }){
	return {
		type: types.SEND_SELLER_MESSAGE,
		message,
	}
}