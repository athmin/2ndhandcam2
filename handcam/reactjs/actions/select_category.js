import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')

export const getBrandModelsAction = {
	selectCategory,
}

export function selectCategory(brand){
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/camera/models/' + brand, {
			method: 'get',
			headers: authHeader(),
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(cam_models({camera_models: responseData}));
			//history.push('/select_category')
			
		}).catch( (error) => {
			console.log('error ' + error )
		});
	}
}

export function cam_models({ camera_models }){
	return{
		type: types.CAMERA_BRAND_MODELS,
		camera_models
	}
}
