import * as types from './types'
import { authHeader } from '../lib'
let CONFIG = require ('../lib/config.json')


export const getReviewsAction = {
	getSellerReviews
}


export function getSellerReviews(username){
	
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/profile/get-review/' + username, {
			method: 'get',
			headers: authHeader()
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(getReviews({reviews: responseData}));
		}).catch((error) => {
			console.log(error);
		});
	}	
}


export function getReviews({ reviews }){
	
	return {
		type: types.GET_SELLER_REVIEWS,
		reviews,
	}
}
