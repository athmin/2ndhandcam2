import * as types from './types'
import { authHeader } from '../lib'
import { history } from '../store/configureStore'
let CONFIG = require('../lib/config.json')


export const editLightingForsaleAction = {
	editlightingforsale
}

export function editlightingforsale(data){
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/sell/lighting/edit/', {
			method: 'post',
			headers: authHeader(),
			body: data
		})
		.then((response) => response.json())
		.then((responseData) => {
			if (responseData.error){
				dispatch(editlightingError({sell: responseData}));
			}else{
				dispatch(editlighting({ dashboard : responseData }))
				history.push("/dashboard")
			}
			
		}).catch((error) => {
			console.log('error: ' + error)
		});
	}	
}


export function editlighting({ dashboard }){
	return {
		type: types.USER_DASHBOARD,
		dashboard
	}
}

export function editlightingError({ sell }) {
  return {
    type: types.LIGHTING_SELL,
    sell,
  }
}