import * as types from './types'
import { authHeader } from '../lib'
import { history } from '../store/configureStore'
let CONFIG = require('../lib/config.json')


export const editCameraForsaleAction = {
	editcameraforsale
}

export function editcameraforsale(data){
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/sell/camera/edit/', {
			method: 'post',
			headers: authHeader(),
			body: data
		})
		.then((response) => response.json())
		.then((responseData) => {
			if (responseData.error){
				dispatch(editcameraError({sell: responseData}));
			}else{
				dispatch(editcamera({ dashboard : responseData }))
				history.push("/dashboard")
			}
		}).catch((error) => {
			console.log('error: ' + error)
		});
	}	
}


export function editcamera({ dashboard }){
	return {
		type: types.USER_DASHBOARD,
		dashboard
	}
}

export function editcameraError({ sell }) {
  return {
    type: types.CAMERA_SELL,
    sell,
  }
}