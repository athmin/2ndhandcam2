import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const sellDroneActions = {
    sellDrone
};


export function sellDrone(data) {
		
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/sell/drone/upload/', {
			method: 'Post',
		    headers: authHeader(),
		    body: data
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  		if (responseData.error){
					dispatch(droneSell({sell: responseData}));
				}else{
					dispatch(droneSell({sell: responseData}));
					history.push("/dashboard")
				}
				
		  }).catch( (error) => {
		      	console.log('error' +' '+ error);
		  });
	}
}

export function droneSell({ sell }) {
  return {
    type: types.DRONE_SELL,
    sell,
  }
}