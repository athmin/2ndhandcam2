import * as types from './types'
import { authHeader } from '../lib'
import { history } from '../store/configureStore'
let CONFIG = require('../lib/config.json')


export const createSellerReportAction = {
	createSellerReport
}

export function createSellerReport(data){
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/profile/post-report', {
			method: 'post',
			headers: authHeader(),
			body: data
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(sellerReport({ review : responseData }))
		}).catch((error) => {
			console.log('error: ' + error)
		});
	}	
}


export function sellerReport({ review }){
	return {
		type: types.CREATE_USER_REPORT,
		review
	}
}