import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const maintenanceModeActions = {
    setMaintenanceModeON, setMaintenanceModeOFF
};


export function setMaintenanceModeON(data) {
		
	return (dispatch, getState) => {
		dispatch(modeUpdate({mode: {status: true}}));
	}
}

export function setMaintenanceModeOFF(data) {
		
	return (dispatch, getState) => {
		dispatch(modeUpdate({mode: {status: false}}));
	}
}

export function modeUpdate({ mode }) {
  return {
    type: types.MAINTENANCE_MODE,
    mode,
  }
}