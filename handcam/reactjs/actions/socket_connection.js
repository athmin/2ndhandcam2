import { getMessagesAction } from './get_messages';
import { maintenanceModeActions } from './maintenance_mode';
import * as types from './types' 
import { history } from '../store/configureStore'
import io from 'socket.io-client';
let CONFIG = require('../lib/config.json')


export const socketIOReduxMiddleware = {
    listener
};


export function listener() {
    
    const socket = io(CONFIG.server_url + ':3100', {  secure: true, reconnect: true, pingInterval: 3000, pingTimeout: 10000,transports: ["websocket", "polling"]
      });
    
    return (dispatch, getState) => {
        socket.on('inboxUpdate', payload =>{

            let profile_pk = localStorage.getItem('profile_pk')
            if (payload.message.hasOwnProperty('update')) {
                if (payload.message.update.user == profile_pk){
                    dispatch(getMessagesAction.getUserInbox())    
                }
            }
        });

        socket.on('maintenance', payload =>{

            if (payload.message.update.maintenance){
                dispatch(maintenanceModeActions.setMaintenanceModeON())    
            }else{
                dispatch(maintenanceModeActions.setMaintenanceModeOFF())    
            }
        });
    }
}

export function sockOn({ payload }){
    
    return {
        type: types.SOCKET_CONNECTED,
        payload,
    }
}
