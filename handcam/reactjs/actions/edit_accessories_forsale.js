import * as types from './types'
import { authHeader } from '../lib'
import { history } from '../store/configureStore'
let CONFIG = require('../lib/config.json')


export const editAccessoriesForsaleAction = {
	editaccessoriesforsale
}

export function editaccessoriesforsale(data){
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/sell/accessories/edit/', {
			method: 'post',
			headers: authHeader(),
			body: data
		})
		.then((response) => response.json())
		.then((responseData) => {
			if (responseData.error){
				dispatch(editaccessoriesError({sell: responseData}));
			}else{
				dispatch(editaccessories({ dashboard : responseData }))
				history.push("/dashboard")
			}
			
		}).catch((error) => {
			console.log('error: ' + error)
		});
	}	
}


export function editaccessories({ dashboard }){
	return {
		type: types.USER_DASHBOARD,
		dashboard
	}
}

export function editaccessoriesError({ sell }) {
  return {
    type: types.ACCESSORIES_SELL,
    sell,
  }
}