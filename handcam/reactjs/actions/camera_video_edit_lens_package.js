import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const editVideoLensPackageAction = {
    editVideoPackage
};


export function editVideoPackage(data) {
		
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/sell/video_lens_package/edit/', {
			method: 'Post',
		    headers: authHeader(),
		    body: data
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  		if (responseData.error){
			  		dispatch(editVideoLensPackage({sell: responseData}));
			  	}else{
			  		dispatch(editVideoLensPackage({sell: responseData}));
				history.push("/dashboard")
			  	}
		  }).catch( (error) => {
		      	console.log('error' +' '+ error);
		  });
	}
}

export function editVideoLensPackage({ sell }) {
  return {
    type: types.CAMERA_SELL,
    sell,
  }
}