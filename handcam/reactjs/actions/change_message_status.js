import * as types from './types'
import { authHeader } from '../lib'
let CONFIG = require ('../lib/config.json')


export const changeMessageStatusAction = {
	changeStatus
}


export function changeStatus(pk){
	
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/message/change-status/'+pk, {
			method: 'post',
			headers: authHeader()
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(getNewMessages({messages: responseData}));
		}).catch((error) => {
			console.log('error: ' + error);
		});
	}	
}


export function getNewMessages({ messages }){
	
	return {
		type: types.GET_SELLER_MESSAGE,
		messages,
	}
}
