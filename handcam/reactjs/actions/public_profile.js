import * as types from './types'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const GetSellerProfileAction = {
	getSellerProfile
}


export function getSellerProfile(username) {
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/profile/public-profile/' + username, {
			method: 'get',
			headers: authHeader(),
		})
		.then((response) => response.json())
		.then((responseData) => {
			
			dispatch(get_profile({ public_profile: responseData}));
		}).catch( (error) => {
		    console.log('error:'+error);
		});
	}
}


export function get_profile({ public_profile }) {
	return {
		type: types.PUBLIC_PROFILE,
		public_profile
	}
}