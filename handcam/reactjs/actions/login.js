import * as types from './types'
import { history } from '../store/configureStore'
let CONFIG = require('../lib/config.json')


export const userActions = {
    fetchUserToken,
    clearAlert,
    registerUser,
    cleanUsername,
    cleanEmail,
    forgotPassword,
    clearForgetPass,
    forgetPassUserToken
};


export function fetchUserToken(data) {
	
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/login/', {
			method: 'post',
		    headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
		    body: JSON.stringify({
		      username: data.username,
		      password: data.password
		    })
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  	if (responseData.token) {
		  		
		  		dispatch(setLoginToken({token: responseData.token}));
		  		if (responseData.redirect_to_change_pass){
		  			history.push("/change_password")
		  		}else{
		  			history.push("/")
		  		}
		  	}else{
		  		console.log(responseData)
		  		console.log(responseData.detail)
		  		dispatch(setLoginTokenError({message: responseData.detail}));
		  	}
		  }).catch( (error) => {
		      console.log(error);
		  });
	}
}

export function forgetPassUserToken(token) {
	
	return (dispatch, getState) => {
	 	console.log(token)	
		dispatch(setLoginToken({token: token}));
		  		
	}
}

export function clearAlert() {
	
	return (dispatch, getState) => {
		return dispatch(setLoginTokenError({message: ''}))
	}
}

export function setLoginToken({ token }) {
  return {
    type: types.GET_LOGIN_TOKEN,
    token,
  }
}

export function setLoginTokenError({ message }) {
  return {
    type: types.GET_LOGIN_TOKEN_ERROR,
    message,
  }
}

export function registerUser(data) {
	
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/registration/', {
			method: 'post',
		    headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
		    body: JSON.stringify({
		      username: data.username,
		      first_name: data.firstname,
		      last_name: data.lastname,
		      email: data.email,
		      password: data.password,
		      country:data.country,
		      city:data.city,
		      phone:data.phone_number,
		      email_template:data.email_template
		    })
		  })
		  .then((response) => response.json())
		  .then((responseData) => {	
		  	if (responseData.error){
		  		dispatch(setRegistrationError({message: responseData.error}));
		  	}else{
	  			dispatch(setRegister({reg: responseData}));
	  			history.push("/confirm")
	  		}
		  }).catch( (error) => {
		      console.log('error' +' '+ error);
		  });
	}
}

export function setRegister({ reg }) {
  return {
    type: types.USER_REGISTRATION,
    reg,
  }
}

export function setRegistrationError({ message }) {
  return {
    type: types.GET_REGISTRATION_ERROR,
    message,
  }
}

export function cleanUsername(data){
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/registration/clean-username', {
			method: 'post',
			headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
		    body: JSON.stringify({
		      username: data.username,
		      email: data.email,
		    })
		  })
		  .then((response) => response.json())
		  .then((responseData) => {	
		  	if (responseData.blockerror){
		  		console.log("user error")
		  		dispatch(setBlockError({message: responseData.blockerror}));
		  	}else{
		  		dispatch(setBlockError({message: ""}));
		  	}
		  	if (responseData.usererror){
		  		console.log("user error")
		  		dispatch(setUsernameError({message: responseData.usererror}));
		  	}else{
		  		dispatch(setUsernameError({message: ""}));
		  	}
		  	if (responseData.emailerror){
		  		console.log("email error")
		  		dispatch(setEmailError({message: responseData.emailerror}));
		  	}else{
		  		dispatch(setEmailError({message: ""}));
		  	}
		  	
		  }).catch( (error) => {
		      console.log('error' +' '+ error);
		  }); 
		}
	}

export function setUsernameError({ message }) {
  return {
    type: types.GET_USERNAME_ERROR,
    message,
  }
}

export function setBlockError({ message }) {
  return {
    type: types.GET_BLOCK_ERROR,
    message,
  }
}


export function cleanEmail(data){
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/registration/clean-email', {
			method: 'post',
			headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
		    body: JSON.stringify({
		      email: data.email,
		    })
		  })
		  .then((response) => response.json())
		  .then((responseData) => {	
		  	if (responseData.error){
		  		dispatch(setEmailError({message: responseData.error}));
		  	}else{
		  		dispatch(setEmailError({message: "OK"}));
		  	}
		  }).catch( (error) => {
		      console.log('error' +' '+ error);
		  }); 
		}
	}

export function setEmailError({ message }) {
  return {
    type: types.GET_EMAIL_ERROR,
    message,
  }
}

export function forgotPassword(data){
	console.log("inside forget password action")
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/forgot-password/', {
			method: 'post',
			headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
			body: JSON.stringify({
		      email: data.email,
		    })
		  })
		  .then((response) => response.json())
		  .then((responseData) => {	
		  	dispatch(setForgetPass({message: responseData}));
		  }).catch( (error) => {
		      console.log('error' +' '+ error);
		})
	}
}

export function clearForgetPass(){
	return(dispatch, getState) => {
		dispatch(setForgetPass({}))
	}
}

export function setForgetPass({ message }) {
  return {
    type: types.FORGET_PASS,
    message,
  }
}