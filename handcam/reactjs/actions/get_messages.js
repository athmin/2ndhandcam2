import * as types from './types'
import { authHeader } from '../lib'
let CONFIG = require ('../lib/config.json')


export const getMessagesAction = {
	getUserInbox
}


export function getUserInbox(){
	
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/message/get-messages/', {
			method: 'get',
			headers: authHeader()
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(getMessages({messages: responseData}));
		}).catch((error) => {
			console.log('error: ' + error);
		});
	}	
}


export function getMessages({ messages }){
	
	return {
		type: types.GET_SELLER_MESSAGE,
		messages,
	}
}
