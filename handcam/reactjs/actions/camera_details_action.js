import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const camDetails = {
    cameraDetail,
};


export function cameraDetail(model) {
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/camera/detail/' + model + '/', {
			method: 'get',
		    headers: authHeader(),
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  		//console.log("camera detail action");
		  		//console.log(responseData);
		  		dispatch(camDetail({detail: responseData}));
		  		//history.push("/camera_detail")
		  }).catch( (error) => {
		      console.log('error' +' '+ error);
		  });
	}
}


export function camDetail({ detail }) {
  return {
    type: types.CAMERA_DETAILS,
    detail,
  }
}
