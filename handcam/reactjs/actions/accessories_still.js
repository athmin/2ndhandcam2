import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const stillAccessoriesActions = {
    sellStillAccessories
};


export function sellStillAccessories(data) {
		
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/sell/camera/accessories/upload/', {
			method: 'Post',
		    headers: authHeader(),
		    body: data
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  	if (responseData.error){
		  		dispatch(stillAccessoriesSell({sell: responseData}));
		  	}else{
		  		dispatch(stillAccessoriesSell({sell: responseData}));
				history.push("/dashboard")
		  	}
		  }).catch( (error) => {
		      	console.log('error' +' '+ error);
		  });
	}
}

export function stillAccessoriesSell({ sell }) {
  return {
    type: types.CAMERA_SELL,
    sell,
  }
}