import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const getStillAccessoriesAction = {
    getStillAccessories,
};


export function getStillAccessories(pk) {
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/sell/camera/accessories/' + pk + '/', {
			method: 'get',
		    headers: authHeader(),
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  		dispatch(stillAccessories({detail: responseData}));
		  }).catch( (error) => {
		      console.log('error' +' '+ error);
		  });
	}
}


export function stillAccessories({ detail }) {
  return {
    type: types.ACCESSORIES_PACKAGE,
    detail,
  }
}
