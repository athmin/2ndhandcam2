import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const lensVideoPackageAction = {
    getVideoPackage,
};


export function getVideoPackage(pk) {
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/sell/video/lens_package/' + pk + '/', {
			method: 'get',
		    headers: authHeader(),
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  		
		  		dispatch(vlPackage({detail: responseData}));
		  }).catch( (error) => {
		      console.log('error' +' '+ error);
		  });
	}
}


export function vlPackage({ detail }) {
  return {
    type: types.LENS_PACKAGE,
    detail,
  }
}
