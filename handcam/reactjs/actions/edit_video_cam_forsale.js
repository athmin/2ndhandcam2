import * as types from './types'
import { authHeader } from '../lib'
import { history } from '../store/configureStore'
let CONFIG = require('../lib/config.json')


export const editVideoCameraForsaleAction = {
	editvideocameraforsale
}

export function editvideocameraforsale(data){
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/sell/video-camera/edit/', {
			method: 'post',
			headers: authHeader(),
			body: data
		})
		.then((response) => response.json())
		.then((responseData) => {
			if (responseData.error){
				dispatch(editvideocameraError({sell: responseData}));
			}else{
				dispatch(editvideocamera({ dashboard : responseData }))
				history.push("/dashboard")
			}
			
		}).catch((error) => {
			console.log('error: ' + error)
		});
	}	
}


export function editvideocamera({ dashboard }){
	return {
		type: types.USER_DASHBOARD,
		dashboard
	}
}

export function editvideocameraError({ sell }) {
  return {
    type: types.VIDEO_SELL,
    sell,
  }
}