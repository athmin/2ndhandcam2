import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const getVideoAccessoriesAction = {
    getVideoAccessories,
};


export function getVideoAccessories(pk) {
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/sell/video-camera/accessories/' + pk + '/', {
			method: 'get',
		    headers: authHeader(),
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  		dispatch(videoAccessories({detail: responseData}));
		  }).catch( (error) => {
		      console.log('error' +' '+ error);
		  });
	}
}


export function videoAccessories({ detail }) {
  return {
    type: types.ACCESSORIES_PACKAGE,
    detail,
  }
}
