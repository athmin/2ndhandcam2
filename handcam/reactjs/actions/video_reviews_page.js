import * as types from './types'
import { credentials } from '../lib/youtube-api-key';
const API_KEY = credentials.key;

export const getVideoReviewsPageAction = {
	getVideoReviewsPage
}


export function getVideoReviewsPage(model,page){
	
	return (dispatch, getState) => {
		return fetch('https://www.googleapis.com/youtube/v3/search?key='+API_KEY+'&part=snippet&q='+model+' review&maxResults=5&pageToken='+page)
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(videoReviewsPage({reviews: responseData}))
		}).catch((error) => {
			console.log(error);
		});
	}	
}


export function videoReviewsPage({ reviews }){
	
	return {
		type: types.GET_VIDEO_REVIEWS,
		reviews,
	}
}
