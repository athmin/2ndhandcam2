import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')

export const getLensBrandModelsAction = {
	selectLensCategory,
}

export function selectLensCategory(brand){
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/lens/models/' + brand, {
			method: 'get',
			headers: authHeader(),
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(lens_models({lens_models: responseData}));
			//history.push('/select_category')
			//console.log("lens models response: " + responseData);
		}).catch( (error) => {
			console.log('error ' + error )
		});
	}
}

export function lens_models({ lens_models }){
	return{
		type: types.LENS_BRAND_MODELS,
		lens_models
	}
}
