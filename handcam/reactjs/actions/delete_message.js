import * as types from './types'
import { authHeader_json, authHeader } from '../lib'
let CONFIG = require ('../lib/config.json')


export const deleteMessageAction = {
	deleteMessage, deleteAllMessage
}


export function deleteMessage(parent){
	
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/message/delete/'+parent, {
			method: 'post',
			headers: authHeader()
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(messageAfterDeletion({messages: responseData}));
		}).catch((error) => {
			console.log('error: ' + error);
		});
	}	
}


export function deleteAllMessage(deleteArray){
	
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/message/delete-all/', {
			method: 'post',
			headers: authHeader_json(),
			body: JSON.stringify({
		      selected: deleteArray,
		    })
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(messageAfterDeletion({messages: responseData}));
		}).catch((error) => {
			console.log('error: ' + error);
		});
	}	
}


export function messageAfterDeletion({ messages }){
	
	return {
		type: types.GET_SELLER_MESSAGE,
		messages,
	}
}
