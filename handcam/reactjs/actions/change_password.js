import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader_json } from '../lib'
let CONFIG = require('../lib/config.json')


export const changePasswordActions = {
    changePassword,
    changePasswordReset
};


export function changePassword(data) {
	
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/change-password/', {
			method: 'post',
		    headers: authHeader_json(),
		    body: JSON.stringify({
		      old_password: data.old_password,
		      new_password: data.new_password
		    })
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  		
		  		dispatch(changePass({newpassstat: responseData}));
		  		history.push("/logout")
		  }).catch( (error) => {
		      console.log(error);
		  });
	}
}


export function changePasswordReset(data) {
	
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/change-password-reset/', {
			method: 'post',
		    headers: authHeader_json(),
		    body: JSON.stringify({
		      new_password: data.new_password
		    })
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  		
		  		dispatch(changePass({newpassstat: responseData}));
		  		history.push("/logout")
		  }).catch( (error) => {
		      console.log(error);
		  });
	}
}


export function changePass({ newpassstat }) {
  return {
    type: types.CHANGE_PASSWORD,
    newpassstat,
  }
}

