import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const sellActions = {
    sellCamera
};


export function sellCamera(data) {
		
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/sell/camera/upload/', {
			method: 'Post',
		    headers: authHeader(),
		    body: data
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
				dispatch(camSell({sell: responseData}));
				//history.push("/dashboard")
		  }).catch( (error) => {
		      	console.log('error' +' '+ error);
		  });
	}
}

export function camSell({ sell }) {
  return {
    type: types.CAMERA_SELL,
    sell,
  }
}