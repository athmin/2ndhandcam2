import * as types from './types'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const removeForsaleCameraAction = {
	removeForsaleCamera
};


export function removeForsaleCamera(pk, type){
	if (type=="Camera") {
		return (dispatch) => {
			return fetch(CONFIG.server_url + '/api/sell/camera/remove/' + pk, {
				method: 'post',
				headers: authHeader()
			})
			.then((response) => response.json())
			.then((responseData) => {
				dispatch(removeCamera({ dashboard: responseData }))
			}).catch((error) => {
				console.log('error: ' + error)
			});
		}
	}
	if (type=="Video") {
		return (dispatch) => {
			return fetch(CONFIG.server_url + '/api/sell/video-camera/remove/' + pk, {
				method: 'post',
				headers: authHeader()
			})
			.then((response) => response.json())
			.then((responseData) => {
				dispatch(removeCamera({ dashboard: responseData }))
			}).catch((error) => {
				console.log('error: ' + error)
			});
		}
	}
	if (type=="Lens") {
		return (dispatch) => {
			return fetch(CONFIG.server_url + '/api/sell/lens/remove/' + pk, {
				method: 'post',
				headers: authHeader()
			})
			.then((response) => response.json())
			.then((responseData) => {
				dispatch(removeCamera({ dashboard: responseData }))
			}).catch((error) => {
				console.log('error: ' + error)
			});
		}
	}
	if (type=="Lighting") {
		return (dispatch) => {
			return fetch(CONFIG.server_url + '/api/sell/lighting/remove/' + pk, {
				method: 'post',
				headers: authHeader()
			})
			.then((response) => response.json())
			.then((responseData) => {
				dispatch(removeCamera({ dashboard: responseData }))
			}).catch((error) => {
				console.log('error: ' + error)
			});
		}
	}
	if (type=="Accessories") {
		return (dispatch) => {
			return fetch(CONFIG.server_url + '/api/sell/accessories/remove/' + pk, {
				method: 'post',
				headers: authHeader()
			})
			.then((response) => response.json())
			.then((responseData) => {
				dispatch(removeCamera({ dashboard: responseData }))
			}).catch((error) => {
				console.log('error: ' + error)
			});
		}
	}
	if (type=="Drone") {
		return (dispatch) => {
			return fetch(CONFIG.server_url + '/api/sell/drone/remove/' + pk, {
				method: 'post',
				headers: authHeader()
			})
			.then((response) => response.json())
			.then((responseData) => {
				dispatch(removeCamera({ dashboard: responseData }))
			}).catch((error) => {
				console.log('error: ' + error)
			});
		}
	}
}


export function removeCamera({ dashboard }){
	
	return {
		type: types.USER_DASHBOARD,
		dashboard
	}

}