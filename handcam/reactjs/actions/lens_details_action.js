import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const lensDetails = {
    lensDetail,
};


export function lensDetail(model) {
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/lens/detail/' + model + '/', {
			method: 'get',
		    headers: authHeader(),
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  		
		  		dispatch(details_lens({detail: responseData}));
		  		//history.push("/camera_detail")
		  }).catch( (error) => {
		      console.log('error' +' '+ error);
		  });
	}
}


export function details_lens({ detail }) {
  return {
    type: types.LENS_DETAILS,
    detail,
  }
}
