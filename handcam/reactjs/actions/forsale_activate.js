import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader_json } from '../lib'
let CONFIG = require('../lib/config.json')


export const activateForSaleAction = {
    activateItems,
};


export function activateItems(model, pk) {
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/sell/reactivate-forsale/', {
			method: 'post',
		    headers: authHeader_json(),
		    body: JSON.stringify({
		      model: model,
		      pk: pk
		    })
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  		dispatch(activate_dash({dashboard: responseData}));
		  }).catch( (error) => {
		      console.log(error);
		  });
	}
}


export function activate_dash({ dashboard }) {
	return {
	    type: types.USER_DASHBOARD,
	    dashboard,
	}
}