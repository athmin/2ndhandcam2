import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader_json } from '../lib'
let CONFIG = require('../lib/config.json')


export const enableForSaleAction = {
    enableItems,
};


export function enableItems(model, pk) {
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/sell/enable-forsale/', {
			method: 'post',
		    headers: authHeader_json(),
		    body: JSON.stringify({
		      model: model,
		      pk: pk
		    })
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  		dispatch(enable_dash({dashboard: responseData}));
		  }).catch( (error) => {
		      console.log(error);
		  });
	}
}


export function enable_dash({ dashboard }) {
	return {
	    type: types.USER_DASHBOARD,
	    dashboard,
	}
}