import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')

export const getVideoBrandModelsAction = {
	selectVideoCategory,
}

export function selectVideoCategory(brand){
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/camera/video-models/' + brand, {
			method: 'get',
			headers: authHeader(),
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(vid_models({video_models: responseData}));
			//history.push('/select_category')
			
		}).catch( (error) => {
			console.log('error ' + error )
		});
	}
}

export function vid_models({ video_models }){
	return{
		type: types.VIDEO_BRAND_MODELS,
		video_models
	}
}
