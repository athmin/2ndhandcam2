import * as types from './types'
import { authHeader } from '../lib'
import { history } from '../store/configureStore'
let CONFIG = require('../lib/config.json')


export const createSellerReviewAction = {
	createSellerReview
}

export function createSellerReview(data){
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/profile/post-review', {
			method: 'post',
			headers: authHeader(),
			body: data
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(sellerReview({ review : responseData }))
		}).catch((error) => {
			console.log('error: ' + error)
		});
	}	
}


export function sellerReview({ review }){
	return {
		type: types.CREATE_USER_REVIEW,
		review
	}
}