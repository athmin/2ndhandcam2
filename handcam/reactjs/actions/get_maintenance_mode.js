import * as types from './types'
import { authHeader } from '../lib'
let CONFIG = require ('../lib/config.json')


export const getMaintenanceAction = {
	getMode
}


export function getMode(){
	
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/get-maintenance-mode/', {
			method: 'get',
			headers: authHeader()
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(setMode({mode: responseData}));
		}).catch((error) => {
			console.log('error: ' + error);
		});
	}	
}


export function setMode({ mode }) {
	console.log("setting maintenance mode reducer")
  	return {
    	type: types.MAINTENANCE_MODE,
    	mode,
  	}
}
