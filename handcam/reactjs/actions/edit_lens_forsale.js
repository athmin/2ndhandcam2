import * as types from './types'
import { authHeader } from '../lib'
import { history } from '../store/configureStore'
let CONFIG = require('../lib/config.json')


export const editLensForsaleAction = {
	editlensforsale
}

export function editlensforsale(data){
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/sell/lens/edit/', {
			method: 'post',
			headers: authHeader(),
			body: data
		})
		.then((response) => response.json())
		.then((responseData) => {
			if (responseData.error){
				dispatch(editlensError({sell: responseData}));
			}else{
				dispatch(editlens({ dashboard : responseData }))
				history.push("/dashboard")
			}
		}).catch((error) => {
			console.log('error: ' + error)
		});
	}	
}


export function editlens({ dashboard }){
	return {
		type: types.USER_DASHBOARD,
		dashboard
	}
}

export function editlensError({ sell }) {
  return {
    type: types.LENS_SELL,
    sell,
  }
}