import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const sellLightingActions = {
    sellLighting
};


export function sellLighting(data) {
		
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/sell/lighting/upload/', {
			method: 'Post',
		    headers: authHeader(),
		    body: data
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
				dispatch(lightingSell({sell: responseData}));
				history.push("/dashboard")
		  }).catch( (error) => {
		      	console.log('error' +' '+ error);
		  });
	}
}

export function lightingSell({ sell }) {
  return {
    type: types.LIGHTING_SELL,
    sell,
  }
}