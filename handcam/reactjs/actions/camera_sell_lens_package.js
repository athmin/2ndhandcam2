import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const sellLensPackageActions = {
    sellCameraLensPackage
};


export function sellCameraLensPackage(data) {
		
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/sell/lens_package/upload/', {
			method: 'Post',
		    headers: authHeader(),
		    body: data
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  		if (responseData.error){
			  		dispatch(lensPackage({sell: responseData}));
			  	}else{
			  		dispatch(lensPackage({sell: responseData}));
					history.push("/dashboard")
			  	}
				
		  }).catch( (error) => {
		      	console.log('error' +' '+ error);
		  });
	}
}

export function lensPackage({ sell }) {
  return {
    type: types.CAMERA_SELL,
    sell,
  }
}