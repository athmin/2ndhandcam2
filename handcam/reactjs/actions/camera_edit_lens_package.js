import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const editLensPackageAction = {
    editPackage
};


export function editPackage(data) {
		
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/sell/lens_package/edit/', {
			method: 'Post',
		    headers: authHeader(),
		    body: data
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  		if (responseData.error){
			  		dispatch(editlPackage({sell: responseData}));
			  	}else{
			  		dispatch(editlPackage({sell: responseData}));
					history.push("/dashboard")
			  	}
				
		  }).catch( (error) => {
		      	console.log('error' +' '+ error);
		  });
	}
}

export function editlPackage({ sell }) {
  return {
    type: types.CAMERA_SELL,
    sell,
  }
}