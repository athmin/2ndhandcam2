import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const getUserDashboardAction = {
    userDashboard,
};


export function userDashboard() {
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/profile/dashboard', {
			method: 'get',
		    headers: authHeader(),
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  		dispatch(user_dash({dashboard: responseData}));
		  		history.push("/dashboard")
		  }).catch( (error) => {
		      console.log(error);
		  });
	}
}


export function user_dash({ dashboard }) {
	return {
	    type: types.USER_DASHBOARD,
	    dashboard,
	}
}