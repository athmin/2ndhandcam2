import * as types from './types'

export const rtlconvert = {
    convertrtlltr,
};

export function convertrtlltr(data) {
	
	return (dispatch, getState) => {
		dispatch(setrtl({rtlstate: data.direct}));
	}
}

export function setrtl({ rtlstate }) {
  return {
    type: types.RTLCONVERT,
    rtlstate,
  }
}