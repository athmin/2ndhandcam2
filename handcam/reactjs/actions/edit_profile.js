import * as types from './types'
import { authHeader } from '../lib'
import { history } from '../store/configureStore'
let CONFIG = require('../lib/config.json')


export const editProfileAction = {
	editprofile
}

export function editprofile(data){
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/profile/edit-profile/', {
			method: 'post',
			headers: authHeader(),
			body: data
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(profileedit({ profile : responseData }))
		}).catch((error) => {
			console.log('error: ' + error)
		});
	}	
}


export function profileedit({ profile }){
	return {
		type: types.PROFILE_EDIT,
		profile
	}
}