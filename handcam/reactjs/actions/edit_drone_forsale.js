import * as types from './types'
import { authHeader } from '../lib'
import { history } from '../store/configureStore'
let CONFIG = require('../lib/config.json')


export const editDroneForsaleAction = {
	editdroneforsale
}

export function editdroneforsale(data){
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/sell/drone/edit/', {
			method: 'post',
			headers: authHeader(),
			body: data
		})
		.then((response) => response.json())
		.then((responseData) => {
			if (responseData.error){
				dispatch(editdroneError({sell: responseData}));
			}else{
				dispatch(editdrone({ dashboard : responseData }))
				history.push("/dashboard")
			}
			
		}).catch((error) => {
			console.log('error: ' + error)
		});
	}	
}


export function editdrone({ dashboard }){
	return {
		type: types.USER_DASHBOARD,
		dashboard
	}
}

export function editdroneError({ sell }) {
  return {
    type: types.DRONE_SELL,
    sell,
  }
}