import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const sellAccessoriesActions = {
    sellAccessories
};


export function sellAccessories(data) {
		
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/sell/accessories/upload/', {
			method: 'Post',
		    headers: authHeader(),
		    body: data
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  		if (responseData.error){
					dispatch(accessoriesSell({sell: responseData}));
				}else{
					dispatch(accessoriesSell({sell: responseData}));
					history.push("/dashboard")
				}
		  }).catch( (error) => {
		      	console.log('error' +' '+ error);
		  });
	}
}

export function accessoriesSell({ sell }) {
  return {
    type: types.ACCESSORIES_SELL,
    sell,
  }
}