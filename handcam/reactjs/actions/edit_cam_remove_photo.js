import * as types from './types'
import { authHeader } from '../lib'
import { history } from '../store/configureStore'
let CONFIG = require('../lib/config.json')


export const removeCameraPhotoAction = {
	removecameraphoto
}

export function removecameraphoto(did, pid){
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/sell/camera/remove_image/'+did+'/'+pid, {
			method: 'post',
			headers: authHeader(),
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(removecamphoto({ status : responseData }))
		}).catch((error) => {
			console.log('error: ' + error)
		});
	}	
}


export function removecamphoto({ status }){
	return {
		type: types.REMOVE_PHOTO_FORSALE,
		status
	}
}