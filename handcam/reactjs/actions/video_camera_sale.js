import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const sellVideoActions = {
    sellVideo
};


export function sellVideo(data) {
		
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/sell/video-camera/upload/', {
			method: 'Post',
		    headers: authHeader(),
		    body: data
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
				dispatch(videoSell({sell: responseData}));
				//history.push("/dashboard")
		  }).catch( (error) => {
		      	console.log('error' +' '+ error);
		  });
	}
}

export function videoSell({ sell }) {
  return {
    type: types.VIDEO_SELL,
    sell,
  }
}