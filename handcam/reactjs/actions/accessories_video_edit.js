import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const editVideoAccessoriesActions = {
    editVideoAccessories
};


export function editVideoAccessories(data) {
		
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/sell/video-camera/accessories/edit/', {
			method: 'Post',
		    headers: authHeader(),
		    body: data
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
				if (responseData.error){
			  		dispatch(videoAccessoriesSell({sell: responseData}));
			  	}else{
			  		dispatch(videoAccessoriesSell({sell: responseData}));
					history.push("/dashboard")
			  	}
		  }).catch( (error) => {
		      	console.log('error' +' '+ error);
		  });
	}
}

export function videoAccessoriesEdit({ sell }) {
  return {
    type: types.CAMERA_SELL,
    sell,
  }
}