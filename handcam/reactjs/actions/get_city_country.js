import * as types from './types'
import { authHeader } from '../lib'
let CONFIG = require ('../lib/config.json')


export const getUserLocation = {
	getLocation
}


export function getLocation(){
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/get-user-location/', {
			method: 'get',
			headers: authHeader()
		})
		.then((response) => response.json())
		.then((responseData) => {
			dispatch(userLocation({location: responseData}));
		}).catch((error) => {
			console.log('error: ' + error);
		});
	}	
}


export function userLocation({ location }){
	return {
		type: types.GET_USER_LOCATION,
		location,
	}
}
