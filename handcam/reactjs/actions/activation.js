import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const getUserActivation = {
    activateUser
};


export function activateUser(data) {
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api' + data, {
			method: 'get',
		    headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
				dispatch(actiUser({act: responseData}));
		  }).catch( (error) => {
		      	console.log('error' +' '+ error);
		  });
	}
}

export function actiUser({ act }) {
  return {
    type: types.USER_ACTIVATED,
    act,
  }
}