import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const lensPackageAction = {
    getPackage,
};


export function getPackage(pk) {
	return (dispatch) => {
		return fetch(CONFIG.server_url + '/api/sell/camera/lens_package/' + pk + '/', {
			method: 'get',
		    headers: authHeader(),
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
		  		
		  		dispatch(lPackage({detail: responseData}));
		  }).catch( (error) => {
		      console.log('error' +' '+ error);
		  });
	}
}


export function lPackage({ detail }) {
  return {
    type: types.LENS_PACKAGE,
    detail,
  }
}
