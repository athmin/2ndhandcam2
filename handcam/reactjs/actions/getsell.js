import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const getUserSellActions = {
    sellUserCamera
};


export function sellUserCamera(data) {
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/sell/camera/upload/', {
			method: 'Get',
		    headers: authHeader(),
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
				dispatch(camUserSell({usersell: responseData}));
		  }).catch( (error) => {
		      	console.log('error' +' '+ error);
		  });
	}
}

export function camUserSell({ usersell }) {
  return {
    type: types.USER_CAMERA_SELL,
    usersell,
  }
}