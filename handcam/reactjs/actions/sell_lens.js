import * as types from './types'
import { history } from '../store/configureStore'
import { authHeader } from '../lib'
let CONFIG = require('../lib/config.json')


export const sellLensActions = {
    sellLens
};


export function sellLens(data) {
		
	return (dispatch, getState) => {
		return fetch(CONFIG.server_url + '/api/sell/lens/upload/', {
			method: 'Post',
		    headers: authHeader(),
		    body: data
		  })
		  .then((response) => response.json())
		  .then((responseData) => {
				if (responseData.error){
					dispatch(lensSell({sell: responseData}));
				}else{
					dispatch(lensSell({sell: responseData}));
					history.push("/dashboard")
				}
		  }).catch( (error) => {
		      	console.log('error' +' '+ error);
		  });
	}
}

export function lensSell({ sell }) {
  return {
    type: types.LENS_SELL,
    sell,
  }
}