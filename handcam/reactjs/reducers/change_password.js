import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const changePassReducer = createReducer({}, {
	[types.CHANGE_PASSWORD](state, action){
		return action.newpassstat
	}
});
