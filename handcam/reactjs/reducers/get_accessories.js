import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const accessoriesPackage = createReducer({}, {
	[types.ACCESSORIES_PACKAGE](state, action) {
	    return action.detail;
  	}, 
});
