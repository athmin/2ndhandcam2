import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const publicProfile = createReducer({}, {
	[types.PUBLIC_PROFILE](state, action) {
	    return action.public_profile;
  	}, 
});