import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const hideContact = createReducer({}, {
	[types.SELLER_HIDE_CONTACT](state, action){
		return action.hide_contact
	}
});