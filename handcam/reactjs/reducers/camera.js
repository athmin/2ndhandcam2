export default function () {
    return [
        {
            id: 1,
            name: "EOS 5D Mark IV",
            type: "DSLR",
            brand: "Cannon",
            mb: "30",
            sensor_size: "36.0mm x 24.0mm",
            sensor_type: "Full Frame",
            image: 'EOS_5d_mark_4.png'
        },
        {
            id: 2,
            name: "EOS 77D",
            type: "DSLR",
            brand: "Cannon",
            mb: "24",
            sensor_size: "22.3 x 14.9mm",
            sensor_type: "APS-C",
            image: 'EOS_77d.png'
        },
        {
            id: 3,
            name: "PowerShot G9 X Mark II",
            type: "Compact/PowerShot",
            brand: "Cannon",
            mb: "20.1",
            sensor_size: "1",
            sensor_type: "CMOS",
            image: 'powershot_g9x_mark_2.png'
        },
        {
            id: 4,
            name: "PowerShot G7 X Mark II",
            type: "Compact/PowerShot",
            brand: "Cannon",
            mb: "20.1",
            sensor_size: "1",
            sensor_type: "CMOS",
            image: 'powershot_g7x_mark_2.png'
        },
        {
            id: 5,
            name: "EOS M 5",
            type: "EOS M Series",
            brand: "Cannon",
            mb: "24.2",
            sensor_size: "22.3 x 14.9mm",
            sensor_type: "APS-C",
            image: 'EOS_M5.png'
        },
        {
            id: 6,
            name: "EOS M 6",
            type: "EOS M Series",
            brand: "Cannon",
            mb: "24.2",
            sensor_size: "22.3 x 14.9mm",
            sensor_type: "APS-C",
            image: 'EOS_M6.png'
        },
        {
            id: 7,
            name: "D7500",
            type: "DSLR",
            brand: "Nikon",
            mb: "20.9",
            sensor_size: "23.5mm x 15.6mm",
            sensor_type: "CMOS",
            image: 'D7500.png'
        },
        {
            id: 8,
            name: "D5600",
            type: "DSLR",
            brand: "Nikon",
            mb: "24.2",
            sensor_size: "23.5mm x 15.6mm",
            sensor_type: "CMOS",
            image: 'D5600.png'
        },
        {
            id: 9,
            name: "COOLPIX L340",
            type: "Compact Camera",
            brand: "Nikon",
            mb: "20.2",
            sensor_size: "1 / 2.3",
            sensor_type: "CCD",
            image: 'COOLPIX_L340.png'
        },
        {
            id: 10,
            name: "COOLPIX W100",
            type: "Compact Camera",
            brand: "Nikon",
            mb: "13.2",
            sensor_size: "1 / 3.1",
            sensor_type: "CMOS",
            image: 'COOLPIX_W100.png'
        },
        {
            id: 11,
            name: "Nikon 1 V3",
            type: "Nikon 1 Performance",
            brand: "Nikon",
            mb: "18.4",
            sensor_size: "13.2mm x 8.8mm",
            sensor_type: "CMOS",
            image: 'Nikon_1_v3.png'
        },
        {
            id: 12,
            name: "Nikon 1 AW1",
            type: "Nikon 1 Performance",
            brand: "Nikon",
            mb: "14.2",
            sensor_size: "13.2mm x 8.8mm",
            sensor_type: "CMOS",
            image: 'Nikon_1_AW1.png'
        }
    ]
}