import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const editprofile = createReducer({}, {
	[types.PROFILE_EDIT](state, action) {
	    return action.profile;
  	}, 
});
