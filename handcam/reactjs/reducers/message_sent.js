import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const messageSent = createReducer({}, {
	[types.SEND_SELLER_MESSAGE](state, action) {
		
	    return action.message;
  	}, 
});