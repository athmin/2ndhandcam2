import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const createSellerReport = createReducer({}, {
	[types.CREATE_USER_REPORT](state, action) {
	    return action.report;
  	}, 
});