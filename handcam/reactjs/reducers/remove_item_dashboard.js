import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const dash_remove = createReducer({}, {
	[types.USER_REMOVE_DASHBOARD](state, action) {
	    return action.new_dashboard;
  	}, 
});
