import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const dash = createReducer({}, {
	[types.USER_DASHBOARD](state, action) {
	    return action.dashboard;
  	}, 
});

/*export default function (state = null, action) {
    switch (action.type) {
        case 'USER_DASHBOARD':
            return action.dashboard;
            break;
    }
    return state;
}*/