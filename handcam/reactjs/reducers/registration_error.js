import createReducer from '../lib/createReducers'
import * as types from '../actions/types'


export const registrationError = createReducer({}, {
	[types.GET_REGISTRATION_ERROR](state, action) {
	    return action;
  	}, 
});
