import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const userLocationReducer = createReducer({}, {
	[types.GET_USER_LOCATION](state, action) {
	    return action.location;
  	}, 
});
