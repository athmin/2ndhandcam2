import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const edituser = createReducer({}, {
	[types.USER_EDIT](state, action) {
	    return action.user;
  	}, 
});
