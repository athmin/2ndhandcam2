import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const sellerReview = createReducer({}, {
	[types.GET_SELLER_REVIEWS](state, action) {
	    return action.reviews;
  	}, 
});