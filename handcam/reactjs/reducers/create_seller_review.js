import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const createSellerReview = createReducer({}, {
	[types.CREATE_USER_REVIEW](state, action) {
	    return action.review;
  	}, 
});