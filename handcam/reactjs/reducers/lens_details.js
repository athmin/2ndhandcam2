import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const lensdetails = createReducer({}, {
	[types.LENS_DETAILS](state, action) {
	    return action.detail;
  	}, 
});
