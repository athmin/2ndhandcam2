import createReducer from '../lib/createReducers'
import * as types from '../actions/types'


export const loginTokenError = createReducer({}, {
	[types.GET_LOGIN_TOKEN_ERROR](state, action) {
	    return action;
  	}, 
});

/*export default function (state = false, action) {
    switch (action.type) {
        case 'GET_LOGIN_TOKEN_ERROR':
            return action.message;
            break;
    }
    return state;
}*/