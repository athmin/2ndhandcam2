import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const usernameError = createReducer({}, {
	[types.GET_USERNAME_ERROR](state, action) {
	    return action;
  	}, 
});
