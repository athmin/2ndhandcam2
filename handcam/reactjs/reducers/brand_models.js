import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const camera_models = createReducer({}, {
	[types.CAMERA_BRAND_MODELS](state, action) {
		return action.camera_models;
	}
});