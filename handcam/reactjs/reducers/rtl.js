import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const rtlconv = createReducer({}, {
	[types.RTLCONVERT](state, action) {
		localStorage.setItem('direction', JSON.stringify(action.rtlstate))
	    return action.rtlstate;
  	}, 
});