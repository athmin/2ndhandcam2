import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const lenspackage = createReducer({}, {
	[types.LENS_PACKAGE](state, action) {
	    return action.detail;
  	}, 
});
