import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const cameradetail = createReducer({}, {
	[types.CAMERA_DETAILS](state, action) {
	    return action.detail;
  	}, 
});
