import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const blockError = createReducer({}, {
	[types.GET_BLOCK_ERROR](state, action) {
	    return action;
  	}, 
});