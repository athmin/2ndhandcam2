import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const inboxReducer = createReducer({}, {
	[types.GET_SELLER_MESSAGE](state, action){
		return action.messages
	}
});
