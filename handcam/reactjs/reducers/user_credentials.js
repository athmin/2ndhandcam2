import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const credentialsReducer = createReducer({}, {
	[types.USER_CREDENTIALS](state, action){
		localStorage.setItem('profile', action.credentials.user_profile.user.first_name +" "+ action.credentials.user_profile.user.last_name)
		localStorage.setItem('profile_pk', action.credentials.user_profile.pk)
		return action.credentials
	}
});