import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const lens_models = createReducer({}, {
	[types.LENS_BRAND_MODELS](state, action) {
		return action.lens_models;
	}
});