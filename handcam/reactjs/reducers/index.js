import { createStore, combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import * as loginReducer from './login'
import * as loginErrorReducer from './login_error'
import * as registrationErrorReducer from './registration_error'
import * as usernameErrorReducer from './username_error'
import * as emailErrorReducer from './email_error'
import * as blockErrorReducer from './block_error'
import * as cameraReducer from './camera'
import * as cameraDetailReducer from './camera_detail'
import * as lensPackageReducer from './lens_package'
import * as cameraSellReducer from './camera_sell'
import * as videoCameraSellReducer from './video_camera_sell'
import * as lightingSellReducer from './lighting_sell'
import * as lensSellReducer from './lens_sell'
import * as lensDetailReducer from './lens_details'
import * as accessoriesSellReducer from './accessories_sell'
import * as droneSellReducer from './drone_sell'
import * as accessoriesPackage from './get_accessories'
import * as userDashboardReducer from './user_dashboard'
import * as userDashboardRemoveReducer from './remove_item_dashboard'
import * as cameraBrandModelReducer from './brand_models'
import * as videoBrandModelReducer from './video_brand_models'
import * as lensBrandModelReducer from './lens_brand_models'
import * as sellerMessage from './message_sent'
import * as inboxMessages from './inbox_messages'
import * as publicProfileReducer from './public_profile'
import * as userCredentials from './user_credentials'
import * as changePassword from './change_password'
import * as createSellerReview from './create_seller_review'
import * as getSellerReview from './get_seller_review'
import * as createSellerReport from './create_seller_report'
import * as editUser from './edit_user'
import * as editProfile from './edit_profile'
import * as userLocation from './get_user_location'
import * as rtl from './rtl'
import * as hideContact from './get_seller_profile'
import * as videoReview from './video_reviews'
import * as maintenanceMode from './maintenance'
import * as forget_pass from './forget_pass'
import * as removephoto from './remove_forsale_photo'


export default combineReducers(Object.assign(
	loginReducer,
	loginErrorReducer,
	registrationErrorReducer,
	usernameErrorReducer,
	emailErrorReducer,
	blockErrorReducer,
	routing,
	userDashboardReducer,
	userDashboardRemoveReducer,
	cameraBrandModelReducer,
	videoBrandModelReducer,
	lensBrandModelReducer,
	sellerMessage,
	inboxMessages,
	publicProfileReducer,
	videoCameraSellReducer,
	lightingSellReducer,
	lensSellReducer,
	accessoriesSellReducer,
	accessoriesPackage,
	cameraDetailReducer,
	lensDetailReducer,
	cameraSellReducer,
	lensPackageReducer,
	userCredentials,
	changePassword,
	createSellerReview,
	getSellerReview,
	createSellerReport,
	editUser,
	editProfile,
	userLocation,
	rtl,
	hideContact,
	droneSellReducer,
	videoReview,
	maintenanceMode,
	forget_pass,
	removephoto
));


