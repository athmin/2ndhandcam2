import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const maintenanceMode = createReducer({}, {
	[types.MAINTENANCE_MODE](state, action) {
	    return action;
  	}, 
});