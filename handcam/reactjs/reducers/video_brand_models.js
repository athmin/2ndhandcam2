import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const video_models = createReducer({}, {
	[types.VIDEO_BRAND_MODELS](state, action) {
		return action.video_models;
	}
});