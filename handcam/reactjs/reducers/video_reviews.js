import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const videoReview = createReducer({}, {
	[types.GET_VIDEO_REVIEWS](state, action){
		return action.reviews
	}
});