import createReducer from '../lib/createReducers'
import * as types from '../actions/types'

export const loginToken = createReducer({}, {
	[types.GET_LOGIN_TOKEN](state, action) {
		localStorage.setItem('token', JSON.stringify(action.token))
	    return action.token;
  	}, 
});


/*export default function (state = null, action) {
    switch (action.type) {
        case 'GET_LOGIN_TOKEN':
        	localStorage.setItem('token', JSON.stringify(action.token))
            return action.token;
            break;
    }
    return state;
}*/
