export function authHeader() {
    // return authorization header with jwt token
    let token = JSON.parse(localStorage.getItem('token'));
    //console.log('auth' + token)
    if (token) {
        return { 'Authorization': 'JWT ' + token };
    } else {
        return {};
    }
}