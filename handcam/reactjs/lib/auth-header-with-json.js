export function authHeader_json() {
    // return authorization header with jwt token
    let token = JSON.parse(localStorage.getItem('token'));
    //console.log('auth' + token)
    if (token) {
        return { 'Authorization': 'JWT ' + token, 'Accept': 'application/json', 'Content-Type': 'application/json' };
    } else {
        return { 'Accept': 'application/json', 'Content-Type': 'application/json' };
    }
}