import React from 'react'
import { history } from '../store/configureStore'
import { connect } from 'react-redux'
import {Link} from 'react-router-dom'
import { strings } from '../lib/strings';


class MaintenanceMode extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			videobrandmodels: []
		}
	}


  render() {
    return (
		<div className="secondhandlogo">
            <div className="brand-header text-center">
	            <span>We are currently in Maintenance Mode</span>
	        </div>
			<br />
			<br />
			<br />
			<br />
		</div>
    )
  }
}


export default connect()(MaintenanceMode);