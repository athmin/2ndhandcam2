import React from 'react'
import { history } from '../store/configureStore'
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'
import { getMaintenanceAction } from '../actions';
let CONFIG = require('../lib/config.json')
import { strings } from '../lib/strings';

const images_url = CONFIG.server_url + "/static/media_cdn"
const camStyle = {
	  width: 200,
    height: 150
};


class Sell extends React.Component {

	componentWillMount(){
		const { dispatch } = this.props;
        if (!this.props.maintenanceMode.hasOwnProperty('mode')){
            dispatch(getMaintenanceAction.getMode()).then( () => {
                if (this.props.maintenanceMode){
                    if (this.props.maintenanceMode.mode.status){

                        history.push('/maintenance')
                    }
                } 
            })
        }else{
            if (this.props.maintenanceMode.mode.status){
                history.push('/maintenance')
            }
        }
    }

	clickPhotographyHandle(){
		history.push('/sell_brand')
	}

	clickVideoHandle(){
		history.push('/sell_video_brand')
	}

	clickLightingHandle(){
		history.push('/sell_lighting')
	}

	clickLensHandle(){
		history.push('/sell_lens_brand')
	}

	clickAccessoriesHandle = (e) => {
        e.preventDefault();
        history.push('/sell_accessories')
    }

    clickDroneHandle = (e) => {
        e.preventDefault();
        history.push('/sell_drone')
    }

  render() {
    return (
		<div>
			<ul className="breadcrumb">
				  <li><Link to="/">{strings.home}</Link></li>
				  <li>{strings.sell}</li>
			</ul>
			<div className="card m-2">
				<h2 className="text-center card-title">{strings.what_item_would_you_like_to_sell}</h2>
				<div className={"row lighting-category" + (this.props.rtlconv == "rtl" ? ' center_by_margin' : '')}  dir="ltr">
					<div className="col text-center">
						<a href="#" onClick={this.clickPhotographyHandle} ><img src={images_url + "/images/EOS_5d_mark_4.png"} style={camStyle} /></a><br />
						<button className="btn btn-warning" onClick={this.clickPhotographyHandle}>{strings.photography_cameras}</button>
					</div>
					<div className="col text-center">
						<a href="#" onClick={this.clickLensHandle} ><img src={images_url + "/images/lens.png"} style={camStyle} /></a><br />
						<button className="btn btn-warning" onClick={this.clickLensHandle}>   {strings.lenses}   </button>
					</div>
					<div className="col text-center">
						<a href="#" onClick={this.clickVideoHandle} ><img src={images_url + "/images/video.png"} style={camStyle} /></a><br />
						<button className="btn btn-warning" onClick={this.clickVideoHandle}>{strings.video_cameras}</button>
					</div>
					<div className="col text-center">
						<a href="#" onClick={this.clickLightingHandle} ><img src={images_url + "/images/lights.png"} style={camStyle} /></a><br />
						<button className="btn btn-warning" onClick={this.clickLightingHandle}>   {strings.lighting}   </button>
					</div>
				</div>	
				<div className={"row lighting-category" + (this.props.rtlconv == "rtl" ? ' center_by_margin' : '')} dir="ltr">
					<div className="col text-center">
						<a href="#" onClick={this.clickAccessoriesHandle} ><img src={images_url + "/images/accessories.png"} style={camStyle} /></a><br />
						<button className="btn btn-warning" onClick={this.clickAccessoriesHandle}>{strings.accessories}</button>
					</div>
					<div className="col text-center">
						<a href="#" onClick={this.clickDroneHandle} ><img src={images_url + "/images/drone.png"} style={camStyle} /></a><br />
						<button className="btn btn-warning" onClick={this.clickDroneHandle}>{strings.drone}</button>
					</div>
				</div>
			</div>

		<div className="card m-2 how-it-works">	
		    <div className="page-header">
		        <h3 id="timeline">{strings.how_it_works_c}</h3>
		    </div>
		    <ul className="timeline">
		        <li className={this.props.rtlconv == "rtl" ? 'timeline-inverted' : ''}>
		          	<div className="timeline-badge success"><span className="badge-text">1</span></div>
		          	<div className="timeline-panel">
			            <div className={"timeline-heading" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
			              	<h5 className="timeline-title">{strings.create_an_account}</h5>
			            </div>
			            <div className={"timeline-body" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
			              	<p>{strings.it_takes_only_a_minute_to_register_an_account}</p>
			              	<p>{strings.all_you_need_is_a_valid_email_address}</p>
			            </div>
		          	</div>
		        </li>
		        <li className={this.props.rtlconv == "rtl" ? '' : 'timeline-inverted'}>
		          	<div className="timeline-badge success"><span className="badge-text">2</span></div>
		          	<div className="timeline-panel">
		            	<div className={"timeline-heading" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
		              		<h5 className="timeline-title">{strings.fill_up_the_form}</h5>
		            	</div>
			            <div className={"timeline-body" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
			              	<p>{strings.choose_an_item_you_want_to_sell}</p>
			              	<p>{strings.name_your_price_the_condition_of_the_item_and_the_actual_photo}</p>
			            </div>
		          	</div>
		        </li>
		        <li className={this.props.rtlconv == "rtl" ? 'timeline-inverted' : ''}>
			        <div className="timeline-badge success"><span className="badge-text">3</span></div>
			        <div className="timeline-panel">
			            <div className={"timeline-heading" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
			              	<h5 className="timeline-title">{strings.get_connected_with_the_potential_buyer}</h5>
			            </div>
			            <div className={"timeline-body" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
			              	<p>{strings.receive_messages_via_the_website_messaging_system}</p>
			              	<p>{strings.or_directly_to_your_registered_mobile_number}</p>
			            </div>
		          	</div>
		        </li>
		    </ul>
		</div>
		<br />
		<br />
		<br />
		<br />
		</div>
    )
  }
}

function mapStatetoProps(state){
    return {
        userlocation: state.userLocationReducer,
        rtlconv: state.rtlconv,
        maintenanceMode: state.maintenanceMode
    }
}

export default connect(mapStatetoProps)(Sell);