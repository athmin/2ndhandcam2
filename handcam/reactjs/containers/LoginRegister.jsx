import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import { userActions } from '../actions';
import { strings } from '../lib/strings';
import Modal from 'react-modal';

let CONFIG = require('../lib/config.json')
const images_url = CONFIG.server_url + "/static/media_cdn"

const divStyle = {
	  marginTop: '150px'
	};

const arrowStyle = {
    height: 150
    };

const customStyles = {
    content: {
        'border-left': '5px solid #f4a72e',
        'border-top': '5px solid #f4a72e',
        'border-right': '5px solid #3d4041',
        'border-bottom': '5px solid #3d4041',
        borderRadius: '4px',
        bottom: 'auto',
        minHeight: '10rem',
        left: '50%',
        padding: '2rem',
        position: 'fixed',
        right: 'auto',
        top: '50%',
        transform: 'translate(-50%,-50%)',
        minWidth: '20rem',
        width: '60%',
        maxWidth: '60rem',
        padding: '0'
      },
    overlay: {
        backgroundColor: "rgba(255, 255, 255, 0.25)"
    }  
};


class LoginRegister extends React.Component {
    constructor(props) {
        super(props);


        this.state = {
            username: '',
            password: '',
            submitted: false,
            modalIsOpen: false,
            forget_pass_email: '',
            submitted_forget: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.forgotPassword = this.forgotPassword.bind(this);
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.closeModalCancel = this.closeModalCancel.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        
    }


    componentWillMount(){
        this.setState({submitted: false})       
    }


    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { username, password } = this.state;
        const { dispatch } = this.props;
        const data = {
        	'username': username,
        	'password': password,
        }
        if (username && password) {
            dispatch(userActions.fetchUserToken(data));
            //this.props.dispatch(fetchUserToken(data))
        }
    }

    forgotPassword(){
        this.setState({ submitted_forget: true });
        const { forget_pass_email } = this.state;
        const { dispatch } = this.props;
        const data = {
            'email': forget_pass_email,
        }
       
        if (forget_pass_email) {
            
            dispatch(userActions.forgotPassword(data));
        }
    }

    openModal() {
        this.setState({modalIsOpen: true});
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        //this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({modalIsOpen: false});
        
        this.forgotPassword();
    }

    closeModalCancel() {
        this.setState({modalIsOpen: false});
    }

    renderForgetPassMessage(){
        
        if (typeof(this.props.forget_pass.message) != "undefined"){
           
            if (this.props.forget_pass.message.status_code == 200){
                return (
                    <div className="alert alert-success text-center" role="alert">
                        <strong>{this.props.forget_pass.message.detail}</strong>
                    </div> 
                )
            }else{
                return(
                    <div className="alert alert-danger text-center" role="alert">
                        <strong>{this.props.forget_pass.message.detail}</strong>
                    </div> 
                )
            }
        }
    }


    render() {
        const { loginTokenError, login } = this.props
        const { username, password, submitted, forget_pass_email, submitted_forget} = this.state;
        return (
            <div className="row homelogo50" dir="ltr">
                <div className="mt-4 mx-auto col-md-3">
                    
                </div>
                <div className="card mt-4 mx-auto col-md-5 loginregcontainer" style={{height:'55%'}} dir="ltr">
                    {this.renderForgetPassMessage()}
                    <div className="row marginTB-20">
                        
                          <div className="col-md-6">
                              <div className="well">
                                    <form name="form" onSubmit={this.handleSubmit} className="login-form">
                                        {loginTokenError.message && submitted &&
                                            <div className="alert alert-danger text-center" role="alert">
                                                <strong>{loginTokenError.message}</strong>
                                            </div> 
                                        }
                                        <div className={'form-group' + (submitted && !username ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                                            <label htmlFor="username">{strings.username}</label>
                                            <input type="text" className={"form-control input92" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')} name="username" value={username} onChange={this.handleChange} />
                                            {submitted && !username &&
                                                <div className="help-block">{strings.username_is_required}</div>
                                            }
                                        </div>
                                        <div className={'form-group' + (submitted && !password ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                                            <label htmlFor="password">{strings.password}</label>
                                            <input type="password" className={"form-control input92" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')} name="password" value={password} onChange={this.handleChange} />
                                            <div onClick={(e)=>{e.preventDefault(); this.openModal();}}>
                                            <span className="spannote spanhowto"  >{strings.forgot_password}</span>
                                            </div>
                                            {submitted && !password &&
                                                <div className="help-block">{strings.password_is_required}</div>
                                            }
                                        </div>
                                        
                                        <div className="form-group ta-center">
                                            <button className="btn btn-info">{strings.login}</button>
                                        </div>
                                    </form>
                              </div>
                          </div>
                          <div className="col-md-4 ta-center">
                              <p className="register-header-text">{strings.create_new_account}</p>
                              {/*<p className="register-header-text"><span className="text-success">for FREE</span></p>
                              <ul className={"list ul-margin" + (this.props.rtlconv == "rtl" ? ' ta-right' : '')} dir={this.props.rtlconv}>
                                  <li><span className="fa fa-check text-success"></span> View all items for sale</li>
                                  <li><span className="fa fa-check text-success"></span> Upload items for everyone to see</li>
                              </ul>*/}
                              <img src={images_url + "/images/arrow-down.png"} style={arrowStyle}/>
                              <Link to="/register" className="btn btn-info btn-block"  style={{marginBottom:'30%',marginTop:'10%'}}>{strings.register}</Link>
                          </div>
                    </div>
                   
                </div>
                <div className="mt-4 mx-auto col-md-3">
                    
                </div>
                <br />
                <br />
                <br />
                <br />
                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    style={customStyles}
                    contentLabel={strings.add_lens}
                    ariaHideApp={false}
                >
                    
                    <div className="forget-frame arabicfont"> 
                        <div className="scroll"> 
                            <div className={this.props.rtlconv == "rtl" ? ' righttoleft' : ''} dir={this.props.rtlconv}>
                                <div className="card-header registration-header">
                                    <h6 className="mb-0">{strings.forgot_password}</h6>
                                </div>
                                
                                <div className="card-body">

                                    <p>{strings.forgot_label_1}</p>

                                    <p>{strings.forgot_label_2}</p>
                                    <form name="forget-form">
                                        
                                        <div className={'form-group' + (submitted_forget && !forget_pass_email ? ' has-error' : '')}>
                                            <label htmlFor="username">{strings.enter_your_email_address}</label>
                                            <input type="text" className={"form-control" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')} name="forget_pass_email" value={forget_pass_email} onChange={this.handleChange} />
                                            {submitted_forget && !forget_pass_email &&
                                                <div className="help-block">{strings.this_field_is_required}</div>
                                            }
                                        </div>

                                        <button className="btn btn-sm btn-primary float-center margin-right" onClick={(e)=>{e.preventDefault(); this.closeModal()}}>{strings.submit}</button>
                                        <button className="btn btn-sm float-center" onClick={(e)=>{e.preventDefault(); this.closeModalCancel()}}>{strings.cancel}</button>
                                        
                                    </form>
                                    
                                </div>
                            </div>
                            <br />
                            <br />
                        </div>
                    </div>
                    
                </Modal>
            </div>
        );

    }
}

function mapStateToProps(state){
    return{
        loginToken: state.loginToken,
        loginTokenError: state.loginTokenError,
        rtlconv: state.rtlconv,
        forget_pass: state.forget_pass
    }
}


export default connect(mapStateToProps)(LoginRegister)
