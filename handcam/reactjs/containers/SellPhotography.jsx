import React from 'react'
import { connect } from 'react-redux';
import { history } from '../store/configureStore'
let CONFIG = require('../lib/config.json')
import { strings } from '../lib/strings';

const images_url = CONFIG.server_url + "/static/media_cdn"

const camStyle = {
      width: 200,
    height: 68
};



class SellPhotography extends React.Component {

    clickHandle(){
        history.push('/cannon_sell')
    }

    componentWillMount(){
        let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }   
    }

  render() {
    return (
        <div className='container'>
            <div className="card mt-1">
                <h2 className="text-center card-title mt-2">{strings.choose_from_any_brand_you_like}</h2>
                <div className="row mt-4">
                    <div className="col-md-3 text-center">
                        <button onClick={this.clickHandle}><img src={images_url + "/images/logo-Canon.png"} style={camStyle} /></button>
                    </div>
                    <div className="col-md-3 text-center">
                        <img src={images_url + "/images/nikon.jpg"} style={camStyle} />
                    </div>
                    <div className="col-md-3 text-center">
                        <img src={images_url + "/images/logo-Sony.png"} style={camStyle} />
                    </div>
                    <div className="col-md-3 text-center">
                        <img src={images_url + "/images/logo-Pentax.png"} style={camStyle} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3 text-center">
                        <img src={images_url + "/images/logo-Hasselblad.png"} style={camStyle} />
                    </div>
                    <div className="col-md-3 text-center">
                        <img src={images_url + "/images/logo-Olympus.png"} style={camStyle} />
                    </div>
                    <div className="col-md-3 text-center">
                        <img src={images_url + "/images/logo-Panasonic.png"} style={camStyle} />
                    </div>
                    <div className="col-md-3 text-center">
                        <img src={images_url + "/images/logo-Leica.png"} style={camStyle} />
                    </div>
                    
                </div>
                <div className="row">
                    <div className="col-md-4 text-center">
                        <img src={images_url + "/images/logo-Sigma.png"} style={camStyle} />
                    </div>
                    <div className="col-md-4 text-center">
                        <img src={images_url + "/images/logo-Fujifilm.png"} style={camStyle} />
                    </div>
                    <div className="col-md-4 text-center">
                        <img src={images_url + "/images/logo-Samsung.png"} style={camStyle} />
                    </div>
                </div>
            </div>
        </div>
    )
  }
}

export default connect()(SellPhotography);