import React from 'react'
import { connect } from 'react-redux'
import { camDetails } from '../actions'
import { sellActions } from '../actions';
import ImageUploader from 'react-images-upload';
import SellForm from '../components/SellForm'




import Dropzone from 'react-dropzone';


const camStyle = {
      width: 400,
    height: 267
    };



class CameraDetail extends React.Component {

	constructor(props) {
        super(props);
        this.state = {
            item: this.props.location.state.id,
            price: "",
            condition: "",
            shutter_count: "",
            status: 'Active',
            warranty: "",
            actual_photos: [],
            with_kit: false,
            submitted: false
        }

        this.onDrop = this.onDrop.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);


    }

    componentWillMount(){
        let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }   
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        
    }

    toggleChange = () => {
        this.setState({
          with_kit: !this.state.with_kit,
        });
        
    }

    onDrop(picture) {
        
        this.setState({
            actual_photos: picture
        });
    }



    handleSubmit = (e) => {
        e.preventDefault()
        const { item, price, with_kit, condition, shutter_count, warranty, actual_photos, submitted } = this.state
        const { dispatch } = this.props;
        
        let body = new FormData()
        body.append('item', item)
        body.append('price', price)
        body.append('condition', condition)
        body.append('shutter_count', shutter_count)
        body.append('warranty', warranty) 
        body.append('with_kit', with_kit)
        body.append('status', 'Active')
        Object.keys(actual_photos).forEach(( key, i ) => {
            body.append('actual_photos', actual_photos[key]);
        });

        if (price && actual_photos){ 
            for (var pair of body.entries()){
                
            }
            //dispatch(sellActions.sellCamera(body))
            
        }
    }

    

    renderSellForm(){
        let token = localStorage.getItem('token')
        const { price, condition, shutter_count, warranty, actual_photos, submitted, file} = this.state;
        if(!token){
            return <p>Please login to sell.</p>
        }else{
           return  <form name="form" onSubmit={this.handleSubmit}>
                                <div className={'form-group' + (submitted && !price ? ' has-error' : '')}>
                                    <label htmlFor="price">Price</label>
                                    <input type="text" className="form-control" name="price" value={price} onChange={this.handleChange} />
                                    {submitted && !price &&
                                        <div className="help-block">Price is required</div>
                                    }
                                </div>
                                <div className={'form-group' + (submitted && !shutter_count ? ' has-error' : '')}>
                                    <label htmlFor="shutter_count">Shutter count</label>
                                    <input type="text" className="form-control" name="shutter_count" value={shutter_count} onChange={this.handleChange} />
                                    {submitted && !shutter_count &&
                                        <div className="help-block">First name is required</div>
                                    }
                                </div>
                                <div className={'form-group' + (submitted && !warranty ? ' has-error' : '')}>
                                    <label htmlFor="warranty">Warranty</label>
                                    <input type="text" className="form-control" name="warranty" value={warranty} onChange={this.handleChange} />
                                    {submitted && !warranty &&
                                        <div className="help-block">Last name is required</div>
                                    }
                                </div>
                                <div className={'form-group' + (submitted && !condition ? ' has-error' : '')}>
                                    <label htmlFor="condition">Condition</label>
                                    <input type="text" className="form-control" name="condition" value={condition} onChange={this.handleChange} />
                                    {submitted && !condition &&
                                        <div className="help-block">Condition is required</div>
                                    }
                                </div>
                                <div className={'form-group' + (submitted && !with_kit ? ' has-error' : '')}>
                                    <label htmlFor="with_kit">With kit</label>
                                    <input type="checkbox" className="form-control" name="with_kit" checked={this.state.with_kit} onChange={this.toggleChange} />
                                    {submitted && !with_kit &&
                                        <div className="help-block">With kit is required</div>
                                    }
                                </div>
                                    <ImageUploader
                                        withIcon={true}
                                        buttonText='Choose images'
                                        onChange={this.onDrop}
                                        imgExtension={['.jpg', '.gif', '.png', '.gif']}
                                        maxFileSize={5242880}/>
                                <div className="form-group">
                                    <button className="btn btn-primary">Submit</button>
                                </div>
                            </form> 
        }
    }


    

  render() {
        const { state } = this.props.location
        
        return (
                <div className="card m-2">
                    <div className="card-header">
                        <h3 className="mb-0" >{state.company} {state.model} {state.camera_type}</h3>
                    </div>
                    <div className="card-body">
                        <div className="row">
                            <div className="col">
                                <img className="card-img-left" src={'/static/media_cdn/' + state.image} style={camStyle}/>
                            </div>
                            <div className="col">
                                <h3>Product Highlights</h3>
                                <ul>
                                {state.megapixels ? <li>{state.megapixels +' '+ 'Megapixel'}</li> : null}
                                {state.sensor_size ? <li>{state.sensor_size +' '+ 'Sensor size'}</li> : null}
                                {state.lcd_size ? <li>{state.lcd_size +'" '+ state.lcd_resolution +' '+ 'LCD Monitor'}</li> : null}
                                </ul>
                            </div>
                            <div className="col">
                            
                            { this.renderSellForm() }
                            </div>
                        </div>
                    </div>
            	</div>
        )
  }
}



export default connect()(CameraDetail)