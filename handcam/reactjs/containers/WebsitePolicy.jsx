import React from 'react'
import { history } from '../store/configureStore'
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'
import { strings } from '../lib/strings';


class WebsitePolicy extends React.Component {

    componentWillMount(){
         
    }

    clickStillHandle(){
        history.push('/buy_item')
    }

    clickVideoHandle(){
        history.push('/buy_video_cameras')
    }

    clickLensHandle(){
        history.push('/buy_lens')
    }

    clickLightingHandle(){
        history.push('/buy_lighting')
    }

    clickAccessoriesHandle(){
        history.push('/buy_accessories')
    }

    clickDroneHandle(){
        history.push('/buy_drones')
    }

    goBack() {
        history.goBack();
    }

    closeCustomerServices = (e) => {
        e.preventDefault();
        this.goBack();
    }

  render() {
    return (
        <div className="row homelogo" dir="ltr">
            <div className="mt-4 mx-auto col-md-3">
                
            </div>
            <div className="card mt-4 col-md-5 infoForms secondhandlogo" dir={this.props.rtlconv}>
                <button className="btn btn-danger btn-sm margintop10 modalButton float-right" onClick={this.closeCustomerServices}> X </button>
                <div className="card-header registration-header">
                    <h6 className="mb-0">{strings.website_policies}</h6>
                </div>
                
                <div className={"card-body" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
       

                    <h1>{strings.privacy_policy}</h1>


                    <p>{strings.effective_date}</p>


                    <p>{strings.privacy_policy1}</p>

                    <p>{strings.privacy_policy2}</p>

                    <p>{strings.privacy_policy3}</p>


                    <h2>{strings.information_collection_and_use}</h2>

                    <p>{strings.information1}</p>

                    <h3>{strings.types_of_data_collected}</h3>

                    <h4>{strings.personal_data}</h4>

                    <p>{strings.personal_data1}</p>

                    <ul>
                    <li>{strings.personal_data_li1}</li><li>{strings.personal_data_li2}</li><li>{strings.personal_data_li3}</li><li>{strings.personal_data_li4}</li><li>{strings.personal_data_li5}</li>
                    </ul>

                    <h4>{strings.usage_data}</h4>

                    <p>{strings.usage_data1}</p>

                    <h4>{strings.tracking_cookies_data}</h4>
                    <p>{strings.tracking_cookies_data1}</p>
                    <p>{strings.tracking_cookies_data2}</p>
                    <p>{strings.tracking_cookies_data3}</p>
                    <p>{strings.tracking_cookies_data4}</p>
                    <ul>
                        <li><strong>{strings.tracking_cookies_data4_li1a}</strong> {strings.tracking_cookies_data4_li1b}</li>
                        <li><strong>{strings.tracking_cookies_data4_li2a}</strong> {strings.tracking_cookies_data4_li2b}</li>
                        <li><strong>{strings.tracking_cookies_data4_li3a}</strong> {strings.tracking_cookies_data4_li3b}</li>
                    </ul>

                    <h2>{strings.use_of_data}</h2>
                        
                    <p>{strings.use_of_data_p1}</p>    
                    <ul>
                        <li>{strings.use_of_data_l1}</li>
                        <li>{strings.use_of_data_l2}</li>
                        <li>{strings.use_of_data_l3}</li>
                        <li>{strings.use_of_data_l4}</li>
                        <li>{strings.use_of_data_l5}</li>
                        <li>{strings.use_of_data_l6}</li>
                        <li>{strings.use_of_data_l7}</li>
                    </ul>

                    <h2>{strings.transfer_of_data}</h2>
                    <p>{strings.transfer_of_data_p1}</p>
                    <p>{strings.transfer_of_data_p2}</p>
                    <p>{strings.transfer_of_data_p3}</p>
                    <p>{strings.transfer_of_data_p4}</p>

                    <h2>{strings.disclosure_of_data}</h2>

                    <h3>{strings.legal_requirements}</h3>
                    <p>{strings.legal_requirements_p1}</p>
                    <ul>
                        <li>{strings.legal_requirements_l1}</li>
                        <li>{strings.legal_requirements_l2}</li>
                        <li>{strings.legal_requirements_l3}</li>
                        <li>{strings.legal_requirements_l4}</li>
                        <li>{strings.legal_requirements_l5}</li>
                    </ul>

                    <h2>{strings.security_of_data}</h2>
                    <p>{strings.security_of_data_p1}</p>

                    <h2>{strings.service_providers}</h2>
                    <p>{strings.service_providers_p1}</p>
                    <p>{strings.service_providers_p2}</p>



                    <h2>{strings.links_to_other_sites}</h2>
                    <p>{strings.links_to_other_sites_p1}</p>
                    <p>{strings.links_to_other_sites_p2}</p>


                    <h2>{strings.childrens_privacy}</h2>
                    <p>{strings.childrens_privacy_p1}</p>
                    <p>{strings.childrens_privacy_p2}</p>


                    <h2>{strings.changes_to_this_privacy_policy}</h2>
                    <p>{strings.changes_to_this_privacy_policy_p1}</p>
                    <p>{strings.changes_to_this_privacy_policy_p2}</p>
                    <p>{strings.changes_to_this_privacy_policy_p3}</p>


                    <h2>{strings.contact_us}</h2>
                    <p>{strings.contact_us_p1}</p>
                    <ul>
                            <li>{strings.contact_us_l1}</li>
                            <li>{strings.contact_us_l2}</li>
                          
                    </ul> 
                </div>
                <br />
                <br />
                <br />
                <br />
            </div> 
            <div className="mt-4 mx-auto col-md-3">
                
            </div>
        </div>      
    )
  }
}

function mapStatetoProps(state){
    return {
        userlocation: state.userLocationReducer,
        rtlconv: state.rtlconv
    }
}

export default connect(mapStatetoProps)(WebsitePolicy);