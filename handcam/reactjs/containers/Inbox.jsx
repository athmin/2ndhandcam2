import React from 'react'
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'
import { getMessagesAction, deleteMessageAction, getMaintenanceAction } from '../actions';
import moment from 'moment'
import { history } from '../store/configureStore'
import Pagination from '../components/Pagination'
import { strings } from '../lib/strings';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
import { FaTrashO } from "react-icons/lib/fa";

const divStyle = {
	  marginTop: '10px'
	};

const cord = {
	  border: '1px solid gray'
	};

const camStyle = {
	  width: 200,
    height: 150
	};

class Inbox extends React.Component {

	constructor(props) {
        super(props)
        this.state = {
            messages: [],
            inboxMessages: [],
            sentMessages: [],
            renderedMessages: [],
            to_delete: [],
            page: 1,
            total: 0,
            unread_count : 0,
            fetching: false,
            flashMessage: null,
            message_state: "inbox"
        }; 
        this.handlePageChange = this.handlePageChange.bind(this);
    }

    handlePageChange(page) {
	    const renderedMessages = this.state.inboxMessages.slice((page - 1) * 10, (page - 1) * 10 + 10);
	    this.setState({ page, renderedMessages });
	}

    componentWillMount(){
        const { dispatch } = this.props;
        if (!this.props.maintenanceMode.hasOwnProperty('mode')){
            dispatch(getMaintenanceAction.getMode()).then( () => {
                if (this.props.maintenanceMode){
                    if (this.props.maintenanceMode.mode.status){
                        history.push('/maintenance')
                    }
                } 
            })
        }else{
            if (this.props.maintenanceMode.mode.status){
                history.push('/maintenance')
            }
        }
    }

    componentDidMount(){
    	const { dispatch } = this.props;
        let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }

        dispatch(getMessagesAction.getUserInbox()).then( () => {
        	this.setState({fetching: true})
        	var inbox = this.props.inboxReducer.inbox_messages
        	var sent = this.props.inboxReducer.sent_messages

            this.setState({ inboxMessages: inbox.sort(function(a, b){ return (new Date(b.sent_at).getTime() - new Date(a.sent_at).getTime())}) })
            this.setState({ sentMessages: sent.sort(function(a, b){ return (new Date(b.sent_at).getTime() - new Date(a.sent_at).getTime())}) })
            this.setState({ unread_count: this.props.inboxReducer.unread_count})

            this.setState({ messages: this.props.inboxReducer.inbox_messages})

            this.setState({ renderedMessages: this.state.inboxMessages.slice(0,10)})
            this.setState({ total: this.state.inboxMessages.length})
            
        })
    	
    }

    renderFlashMessages(){

        if(this.state.flashMessage){
            return(
                <div className="alert alert-success">
                    { this.state.flashMessage }
                </div>
            )
        }
    }

    displayMessages(kind){
    	
    	if (kind=="inbox"){
    		this.setState({ message_state: "inbox" })
    		this.setState({ messages: this.props.inboxReducer.inbox_messages})
    		this.setState({ renderedMessages: this.state.inboxMessages.slice(0,10)})
            this.setState({ total: this.state.inboxMessages.length})
    	}else{
    		this.setState({ message_state: "sent" })
    		this.setState({ messages: this.props.inboxReducer.sent_messages})
    		this.setState({ renderedMessages: this.state.sentMessages.slice(0,10)})
            this.setState({ total: this.state.sentMessages.length})
    	}
    	
    }

    deleteMessageButton(parent) {
    	const { dispatch } = this.props;
    	let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }

        dispatch(deleteMessageAction.deleteMessage(parent)).then( () => {
        	this.setState({fetching: true})

        	var inbox = this.props.inboxReducer.inbox_messages
        	var sent = this.props.inboxReducer.sent_messages
        	
            this.setState({ inboxMessages: inbox.sort(function(a, b){ return (new Date(b.sent_at).getTime() - new Date(a.sent_at).getTime())}) })
            this.setState({ sentMessages: sent.sort(function(a, b){ return (new Date(b.sent_at).getTime() - new Date(a.sent_at).getTime())}) })
            this.setState({ unread_count: this.props.inboxReducer.unread_count })
            this.setState({ flashMessage : this.props.inboxReducer.status })
            if (this.state.message_state == "inbox"){
            	this.setState({ renderedMessages: this.state.inboxMessages.slice(0,10)})
            }else{
            	this.setState({ renderedMessages: this.state.sentMessages.slice(0,10)})
            }
        })
    }

    deleteAllMessages() {
    	const { dispatch } = this.props;
    	let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }

        dispatch(deleteMessageAction.deleteAllMessage(this.state.to_delete)).then( () => {
        	this.setState({fetching: true})

        	var inbox = this.props.inboxReducer.inbox_messages
        	var sent = this.props.inboxReducer.sent_messages
        	
            this.setState({ inboxMessages: inbox.sort(function(a, b){ return (new Date(b.sent_at).getTime() - new Date(a.sent_at).getTime())}) })
            this.setState({ sentMessages: sent.sort(function(a, b){ return (new Date(b.sent_at).getTime() - new Date(a.sent_at).getTime())}) })
            this.setState({ unread_count: this.props.inboxReducer.unread_count })
            this.setState({ flashMessage : this.props.inboxReducer.status })
            this.setState({ to_delete: [] })
            if (this.state.message_state == "inbox"){
            	this.setState({ renderedMessages: this.state.inboxMessages.slice(0,10)})
            }else{
            	this.setState({ renderedMessages: this.state.sentMessages.slice(0,10)})
            }
        })
    }

    renderConfirmationModal(parent){
        confirmAlert({
          title: strings.confirm,                        
          message: strings.are_you_sure_you_want_to_delete,               
          confirmLabel: strings.yes,                           
          cancelLabel: strings.no,                             
          onConfirm: () => {
            this.deleteMessageButton(parent)
          },    // Action after Confirm
          onCancel: () => {} ,      // Action after Cancel
        })

    }

    renderConfirmationAllModal(){
        confirmAlert({
          title: strings.confirm,                        
          message: strings.delete_all_confirmation,               
          confirmLabel: strings.yes,                           
          cancelLabel: strings.no,                             
          onConfirm: () => {
            this.deleteAllMessages()
          },    // Action after Confirm
          onCancel: () => {} ,      // Action after Cancel
        })

    }

    handleSelectForDeletion(e){

        const {name, value} = e.target;
        var deleteArray = this.state.to_delete

        if (deleteArray.includes(value)){
            var index = deleteArray.indexOf(value)
            if (index > -1){
                deleteArray.splice(index, 1)
                this.setState({ to_delete: deleteArray }, () => {
                })
            }
        }else{
            this.setState(prevState => ({
                to_delete: [...prevState.to_delete, value]
            }), () => {
                console.log(this.state.to_delete)
            })
        }
        
    }

    renderDeleteSelectedButton(){
        if (this.state.to_delete.length > 0){
            return(
                <button className="btn-xs" onClick={(e)=>{e.preventDefault(); this.renderConfirmationAllModal() }}>
                    <FaTrashO />
                </button>
            )
        }else{
            return(
                <button className="btn-xs" disabled="true">
                    <FaTrashO />
                </button>
            )
        }
    }


	renderInbox(){
		const { renderedMessages } = this.state 

		return (
			<div className={'row messageContainer' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
				<div className="col-2">
					<div className="box box-solid">
			            <div className="box-header with-border inboxtableheaders">
			              <h3 className="box-title">{strings.folders}</h3>
			            </div>
			            <div className="box-body no-padding">
			              <div className="side-menu">
							    <a href="#" className={(this.state.message_state == "inbox" ? 'active' : '')} onClick={(e)=>{e.preventDefault(); this.displayMessages("inbox")}}> 
							    	<div className={this.props.rtlconv == "rtl" ? 'righttoleft' : ''}>
							    		<span>{strings.inbox}</span>  
			                  			<span className={this.props.rtlconv == "rtl" ? 'span-left' : 'span-right'}>{this.state.unread_count > 0 ? this.state.unread_count : '' }</span>
			                  		</div>
			                  	</a>
							    <hr />
							    <a href="#" className={(this.state.message_state == "sent" ? 'active' : '')} onClick={(e)=>{e.preventDefault(); this.displayMessages("sent")}}>
							     
							     	<div className={this.props.rtlconv == "rtl" ? 'righttoleft' : ''}>
							     		<span>{strings.sent}</span>
							     	</div>
							    </a>
							</div>
			            </div>
			          </div>
				</div>
				<div className="col mr-10">
					
					<table className="table table-hover table-striped">
						<thead className="inboxtableheaders">
							<tr>
								<th>{this.state.message_state == "inbox" ? strings.sender : strings.recipient}</th>
								<th>{strings.message}</th>
								<th>{strings.sent}</th>
								<th></th>
                                <th  className="ta-center">{this.renderDeleteSelectedButton()}</th>
							</tr>	
						</thead>
						<tbody>
							{this.state.fetching && this.state.renderedMessages.map((inbox, i) => {
								let message = ''
								//console.log(inbox);
								if(inbox.body.length>50){
									message = inbox.subject.substring(0,50) + '...'
								}else{
									message = inbox.subject
								}
								return (
			                    	
				                    <tr key={inbox.id} className={inbox.read == false ? 'message-unread' : 'message'}>
				                        <td >{ this.state.message_state == "inbox" ? inbox.sender.user.username : inbox.recipient.user.username}</td>
				                        <td >
				                        	<a href="" onClick={(e)=>{e.preventDefault();  
				                        		history.push({
				                        			'pathname': '/view_message',
				                        			'state':{
				                        			'id': inbox.id,
				                        			'sender': inbox.sender.pk,
				                        			'body': inbox.body,
				                        			'subject': inbox.subject,
				                        			'sent_at': inbox.sent_at,
				                        			'parent' : inbox.parent
				                        			}
				                        		})
				                        	}}>{message}
				                        	</a>
				                        </td>
				                        <td>{moment(inbox.sent_at).format('MMMM Do YYYY, h:mm:ss a')}</td>
				                        <td>
				                        	<button className="btn-default" onClick={(e)=>{e.preventDefault(); this.renderConfirmationModal(inbox.parent) }}>
		                                      {strings.del}
		                                    </button>
				                        </td>
                                        <td className="ta-center">
                                            <input type="checkbox" value={inbox.parent} onChange={(e)=>{this.handleSelectForDeletion(e) }} />
                                        </td>
				                    </tr>    
			                    )  
			                })}
			                
						</tbody>	
					</table>
					{this.state.total > 10 ? 
					<Pagination
						margin={10}
						page={this.state.page}
						count={Math.ceil(this.state.total / 10)}
						onPageChange={this.handlePageChange}
					/>
					: null }
				</div>
			</div>
		)
	}

	render() {
		

		return (
			<div>
				<div>
	                <ul className="breadcrumb">
	                      <li><Link to="/">{strings.home}</Link></li>
	                      <li>{strings.inbox}</li>
	                </ul>
	            </div>
	            
				<div className={"container" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
					<h2 style={divStyle}>{strings.inbox}</h2>
					<p>{strings.you_have} {this.state.unread_count} {strings.new_message}</p>

					{ this.renderFlashMessages() }

					

					{ this.renderInbox() }

					<br />
					<br />
					<br />
					<br />
				</div>
			</div>
		)
	}


}

function mapStateToProps(state) {
	
    return {
        inboxReducer: state.inboxReducer,
        rtlconv: state.rtlconv,
        maintenanceMode: state.maintenanceMode
    };
}


export default connect(mapStateToProps)(Inbox)