import React from 'react'
import { connect } from 'react-redux'
import {Link} from 'react-router-dom'
import { camDetails } from '../actions'
import { sellVideoActions } from '../actions';
import { sellVideoLensPackageActions, videoAccessoriesActions } from '../actions';
import { getLensBrandModelsAction } from '../actions';
import ImageUploader from 'react-images-upload';
import SellForm from '../components/SellForm'
import { history } from '../store/configureStore'
import Modal from 'react-modal';
import OverlayLoader from 'react-loading-indicator-overlay/lib/OverlayLoader'
import { strings } from '../lib/strings';

let CONFIG = require('../lib/config.json')
import Dropzone from 'react-dropzone';
const images_url = CONFIG.server_url + "/static/media_cdn"
const lensStyle = {
    width: 105,
    height: 60,
};
const logoStyle = {
    width: 100,
    height: 35,
};
const camStyle = {
    "max-width": "100%",
    height: "auto"
    };

const customStyles = {
    content: {
        'border-left': '5px solid #f4a72e',
        'border-top': '5px solid #f4a72e',
        'border-right': '5px solid #3d4041',
        'border-bottom': '5px solid #3d4041',
        borderRadius: '4px',
        bottom: 'auto',
        minHeight: '10rem',
        left: '50%',
        padding: '2rem',
        position: 'fixed',
        right: 'auto',
        top: '50%',
        transform: 'translate(-50%,-50%)',
        minWidth: '20rem',
        width: '80%',
        maxWidth: '60rem',
        padding: '0'
      },
    overlay: {
        backgroundColor: "rgba(255, 255, 255, 0.25)"
    }  
};

const customStylesForAcc = {
    content: {
        'border-left': '5px solid #f4a72e',
        'border-top': '5px solid #f4a72e',
        'border-right': '5px solid #3d4041',
        'border-bottom': '5px solid #3d4041',
        borderRadius: '4px',
        bottom: 'auto',
        minHeight: '10rem',
        left: '50%',
        padding: '2rem',
        position: 'fixed',
        right: 'auto',
        top: '50%',
        transform: 'translate(-50%,-50%)',
        minWidth: '20rem',
        width: '60%',
        maxWidth: '60rem',
        padding: '0'
      },
    overlay: {
        backgroundColor: "rgba(255, 255, 255, 0.25)"
    }  
};



class CameraDetail extends React.Component {

	constructor(props) {
        super(props);
        this.state = {
            item: this.props.location.state.id,
            category: this.props.location.state.type,
            price: "",
            condition: "",
            comments: "",
            description: "",
            shutter_count: 0,
            status: 'Active',
            warranty: "",
            actual_photos: [],
            camera_package: "Body Only",
            submitted: false,
            modalIsOpen: false,     // state that opens / closes the modal
            selectBrand: true,      // if true display the brand selection for lenses
            selectModel: false,      // if true display lens models for the selected brand
            selectPhotoAndComment: false, // if tru display comments and imageuploader for lens

            accModalIsOpen: false,
            accessoriesDescription: [],

            howtoModalIsOpen: false,

            lensbrandmodels: [],
            models: [],
            fetching: false,
            selected: 'all',

            priceError: false,
            warrantyError: false,
            conditionError: false,
            actualPhotoError: false,
            packagePhotoError: false,
            bothPhotoError: false,

            lens: "",
            lens_id: "",
            lens_comments: "",
            lens_photos: [],
            camerasell: this.props.camerasell,

            lensPackage: [],
            lensPackageIDS: [],
            spinner: false,
            showSizeError: false,

        }

        this.onDrop = this.onDrop.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSelect = this.handleSelect.bind(this);

        this.onLensDrop = this.onLensDrop.bind(this);
        this.handleLensComments = this.handleLensComments.bind(this);
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);

        this.openAccModal = this.openAccModal.bind(this);
        this.afterOpenAccModal = this.afterOpenAccModal.bind(this);
        this.closeAccModal = this.closeAccModal.bind(this);

        this.openHowToModal = this.openHowToModal.bind(this);
        this.afterOpenHowToModal = this.afterOpenHowToModal.bind(this);
        this.closeHowToModal = this.closeHowToModal.bind(this);

    }

    componentWillMount(){
        let token = localStorage.getItem('token')
        if (!token){
          history.push('/login_register')
        }   
    }

    componentDidMount(){
        window.scrollTo(0, 0)
    }


    handleSelectCondition(e) {
        this.setState({condition: e.target.value});
        
    }

    handleSelectWarranty(e) {
        this.setState({warranty: e.target.value});
        
    }

    handleSelect(e) {
        this.setState({camera_package: e.target.value});
        
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        
    }

    handleLensComments(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        
    }

    openModal() {
        this.setState({modalIsOpen: true});
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        //this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({modalIsOpen: false});
        this.setState({selectBrand: true})
        this.setState({selectModel: false})
        this.setState({selectPhotoAndComment: false})
        this.setState({lensbrandmodels: []})
    }    

    openAccModal() {
        this.setState({accModalIsOpen: true});
    }

    afterOpenAccModal() {
        // references are now sync'd and can be accessed.
        //this.subtitle.style.color = '#f00';
    }

    closeAccModal() {
        this.setState({accModalIsOpen: false});
        this.setState({description: []})
    }

    openHowToModal() {
        this.setState({howtoModalIsOpen: true});
    }

    afterOpenHowToModal() {
        // references are now sync'd and can be accessed.
        //this.subtitle.style.color = '#f00';
    }

    closeHowToModal() {
        this.setState({howtoModalIsOpen: false});
    }

    backToSelectLensModel() {
        this.setState({selectBrand: false})
        this.setState({selectModel: true})
        this.setState({selectPhotoAndComment: false})
        this.setState({lens: ""})
    }

    addLensToPackage() {
        
        var newArray = this.state.lensPackage.slice();
        var newIDArray = this.state.lensPackage.slice();      
        newArray.push({model:this.state.lens, id:this.state.lens_id});   
        newIDArray.push(this.state.lens_id)
        this.setState({lensPackage: newArray})
        this.setState({lensPackageIDS: newIDArray})
        
        this.closeModal()
        this.setState({selectBrand: true})
        this.setState({selectModel: false})
        this.setState({selectPhotoAndComment: false})

        // Reset the values of lens
        this.setState({lens: ""})
        //this.setState({lens_comments: ""})
        //this.setState({lens_photos: []})
    }

    addAccessories() {
        var accArray = this.state.accessoriesDescription.slice();
        accArray.push(this.state.description)
        
        this.setState({ accessoriesDescription: accArray })
        this.closeAccModal()
    }


    removeAccToPackage(acc) {
        
        var accArray = this.state.accessoriesDescription
        var index = accArray.indexOf(acc)
        var newArray = []
        if (index > -1) {
            newArray = accArray.splice(index, 1);
        }

        this.setState({ accessoriesDescription: accArray })

    }

    selectedLensModel(model, id) {
        this.setState({lens: model})
        this.setState({lens_id: id})
        this.setState({selectBrand: false})
        this.setState({selectModel: false})
        this.setState({selectPhotoAndComment: true})
    }

    removeLensToPackage(model) {
        
        var lensArray = this.state.lensPackage
        var index = lensArray.indexOf(model)
        var newArray = []
        if (index > -1) {
            newArray = lensArray.splice(index, 1);
        }

        this.setState({ lensPackage: lensArray })

    }

    SelectLensModel(brand) {
        const { dispatch } = this.props;
        this.setState({selectBrand: false})
        this.setState({selectModel: true})
        dispatch(getLensBrandModelsAction.selectLensCategory(brand)).then( () => {
            this.setState({fetching: true})
            this.setState({ lensbrandmodels: this.props.lens_models })
            this.allLenses()
        })
    }

    setSelected(category) {
        this.setState({selected  : category})
    }

    isActive(value){
        return 'col choose-category '+((value===this.state.selected) ?'selected':'default');
    }

    renderAllLensButton(category) {
        if (category.length>1){
            return (
                    <div id='choose-category' className={this.isActive('all')}>
                        <span onClick={(e) =>{ this.allLenses(); this.setSelected('all') }}>{strings.all_lenses}</span>
                    </div>    
            )
        }else{
            return <p></p>
        }
    }

    renderCategory() {
        const { lensbrandmodels } = this.state
        let category = []

        for (let i = 0; i < lensbrandmodels.length; i++){
            if (category.indexOf(lensbrandmodels[i].type_of_lens.lens_type) === -1 ) category.push(lensbrandmodels[i].type_of_lens.lens_type);     
        }
        
        return (
            <div className="row">
                { category.map((cat) => {
                    
                    
                    return (
                        <div id='choose-category' className={this.isActive(cat)}>
                            <span key={cat} onClick={(e) =>{ this.filterModel(cat); this.setSelected(cat) }}>{cat}</span>
                        </div>    
                    )
                        
                })}
                { this.renderAllLensButton(category)}
            </div>    
        )

    }

    allLenses() {
        const { lensbrandmodels } = this.state
        //this.setState({models: lensbrandmodels})

        let newmodels = []
        
        for (let i = 0; i < lensbrandmodels.length; i++){
            newmodels.push({'id' : lensbrandmodels[i].id, 'model' : lensbrandmodels[i].model, 'image' : lensbrandmodels[i].image, 'type' : lensbrandmodels[i].type_of_lens.lens_type, 'brand' : lensbrandmodels[i].type_of_lens.brand.company});     
        }

        if (newmodels){
            this.setState({models: newmodels})
        }
    }

    filterModel(lensCategory) {
        
        const { lensbrandmodels } = this.state
        let newmodels = []
        
        for (let i = 0; i < lensbrandmodels.length; i++){
            if ( lensbrandmodels[i].type_of_lens.lens_type == lensCategory ) newmodels.push({'id' : lensbrandmodels[i].id, 'model' : lensbrandmodels[i].model, 'image' : lensbrandmodels[i].image, 'type' : lensbrandmodels[i].type_of_lens.lens_type, 'brand' : lensbrandmodels[i].type_of_lens.brand.company});     
        }

        if (newmodels){
            
            this.setState({models: newmodels})
        }
            
        
    }

    searchModel(event) {
        const { lensbrandmodels } = this.state
        let searchResult = lensbrandmodels
        let newmodels = []

        searchResult = searchResult.filter(function(item){
            
            return item.model.toString().toLowerCase().search(
                event.target.value.toLowerCase()) !== -1;
        });

        for (let i = 0; i < searchResult.length; i++){
            newmodels.push({'id' : searchResult[i].id, 'model' : searchResult[i].model, 'image' : searchResult[i].image, 'image_thumbnail' : searchResult[i].image_thumbnail, 'type' : searchResult[i].type_of_lens.lens_type, 'brand' : searchResult[i].type_of_lens.brand.company});     
        }

        this.setState({models: newmodels})
    }

    onDrop(picture) {
        
        this.setState({
            actual_photos: picture,
            actualPhotoError: false,
            bothPhotoError: false,
            showSizeError: false
        });
        
    }

    onLensDrop(picture) {
        
        this.setState({
            lens_photos: picture,
            packagePhotoError: false,
            bothPhotoError: false,
        });
        
    }


    clickStillHandle = (e) => {
        e.preventDefault();
        history.push('/sell_brand')
    }

    clickVideoHandle = (e) => {
        e.preventDefault();
        history.push('/sell_video_brand')
    }

    clickLensHandle = (e) => {
        e.preventDefault();
        history.push('/sell_lens_brand')
    }

    clickLightingHandle = (e) => {
        e.preventDefault();
        history.push('/sell_lighting')
    }

    clickAccessoriesHandle = (e) => {
        e.preventDefault();
        history.push('/sell_accessories')
    }

    clickDroneHandle = (e) => {
        e.preventDefault();
        history.push('/sell_drone')
    }



    handleSubmit = (e) => {
        e.preventDefault()
        const { item, price, camera_package, condition, shutter_count, warranty, actual_photos, submitted, comments, lensPackage, accessoriesDescription, lens_photos, description} = this.state
        const { dispatch } = this.props;
        let cam_package = ""
        let body = new FormData()

        if (lensPackage.length > 0 && accessoriesDescription.length > 0){
            cam_package = "With Lens and Accessories"
            
        }else if (lensPackage.length > 0 && accessoriesDescription.length == 0){
            cam_package = "With Lens"
            
        }else if (lensPackage.length == 0 && accessoriesDescription.length > 0){
            cam_package = "With Accessories"
           
        }else{
            cam_package = "Body Only"
            
        }
        
        body.append('item', item)
        body.append('price', price)
        body.append('condition', condition)
        body.append('description', description)
        if (description==""){
            body.append('description', "body only")
        }else{
            body.append('description', description)
        }
        body.append('comments', comments)
        body.append('shutter_count', shutter_count)
        body.append('warranty', warranty) 
        body.append('camera_package', cam_package)
        body.append('status', 'Active')
        Object.keys(actual_photos).forEach(( key, i ) => {
            body.append('actual_photos', actual_photos[key]);
        });

        if (price && warranty && condition && actual_photos){ 
            for (var pair of body.entries()){
                
            }
            //dispatch(sellVideoActions.sellVideo(body))
        }

        if (cam_package == "With Lens"){
            if (price && actual_photos.length > 0 && lens_photos.length > 0){ 
                for (var pair of body.entries()){
                }
                this.setState({ spinner: true})
                dispatch(sellVideoActions.sellVideo(body)).then( () => {
                    
                    
                    this.setState({ showSizeError: true })
                    this.handleLensPackages()
                })     
            }
            if (price && condition && warranty && actual_photos.length == 0 && lens_photos.length > 0){
                this.setState({actualPhotoError: true})
            }
            else if (price && condition && warranty && actual_photos.length > 0 && lens_photos.length == 0){
                this.setState({packagePhotoError: true})
            }else if (price && condition && warranty && actual_photos.length == 0 && lens_photos.length == 0){
                this.setState({bothPhotoError: true})
            }
        }

        if (cam_package == "With Lens and Accessories"){
            if (price && actual_photos.length > 0 && lens_photos.length > 0){ 
                for (var pair of body.entries()){
                }
                this.setState({ spinner: true})
                dispatch(sellVideoActions.sellVideo(body)).then( () => {
                    
                    
                    this.setState({ showSizeError: true })
                    this.handleLensPackages()
                    this.handleAccessoriesDescription()
                })     
            }
            if (price && condition && warranty && actual_photos.length == 0 && lens_photos.length > 0){
                this.setState({actualPhotoError: true})
            }
            else if (price && condition && warranty && actual_photos.length > 0 && lens_photos.length == 0){
                this.setState({packagePhotoError: true})
            }else if (price && condition && warranty && actual_photos.length == 0 && lens_photos.length == 0){
                this.setState({bothPhotoError: true})
            }
        }

        if (cam_package == "With Accessories"){
            if (price && actual_photos.length > 0 && lens_photos.length > 0){ 
                for (var pair of body.entries()){
                }
                this.setState({ spinner: true})
                dispatch(sellVideoActions.sellVideo(body)).then( () => {
                    
                    
                    this.setState({ showSizeError: true })
                    this.handleAccessoriesDescription()
                    
                })     
            }
            if (price && condition && warranty && actual_photos.length == 0 && lens_photos.length > 0){
                this.setState({actualPhotoError: true})
            }
            else if (price && condition && warranty && actual_photos.length > 0 && lens_photos.length == 0){
                this.setState({packagePhotoError: true})
            }else if (price && condition && warranty && actual_photos.length == 0 && lens_photos.length == 0){
                this.setState({bothPhotoError: true})
            }
        }

        if (cam_package == "Body Only"){
            if (price && actual_photos.length > 0 && condition && warranty){ 
                ;
                for (var pair of body.entries()){
                }
                this.setState({ spinner: true})
                dispatch(sellVideoActions.sellVideo(body)).then( () => {
                    history.push('/dashboard')    
                })
                
            }
            if (price && actual_photos.length == 0 && condition && warranty){
                this.setState({actualPhotoError: true})
            }

            
            if (price=="" && condition == "" && warranty == ""){
                this.setState({actualPhotoError: true})   
            }
        }


        if (price == ""){
            this.setState({ priceError: true})
        }else{
            this.setState({ priceError: false})
        }

        if (condition == ""){
            this.setState({ conditionError: true})
        }else{
            this.setState({ conditionError: false})
        }

        if (warranty == ""){
            this.setState({ warrantyError: true})
        }else{
            this.setState({ warrantyError: false})
        }
    }

    handleLensPackages() {
        const { lensPackageIDS, lens_photos, submitted } = this.state
        const { dispatch } = this.props;

        let body = new FormData()
        body.append('lensPackage', JSON.stringify(this.state.lensPackage))
        /*Object.keys(lensPackageIDS).forEach(( key, i ) => {
            body.append('lensPackage', lensPackageIDS[key]);
        });*/
        Object.keys(lens_photos).forEach(( key, i ) => {
            body.append('lens_photos', lens_photos[key]);
        });

        let camera = this.props.camerasell
       
        body.append('cam_id', camera.sell.data.id)
        dispatch(sellVideoLensPackageActions.sellVideoLensPackage(body))
    }

    handleAccessoriesDescription() {
        const { accessoriesDescription } = this.state;
        const { dispatch } = this.props;

        let body = new FormData()
        let camera = this.props.camerasell

        body.append('accessories_package', JSON.stringify(accessoriesDescription))
        body.append('cam_id', camera.sell.data.id)
        dispatch(videoAccessoriesActions.sellVideoAccessories(body))

    }

    renderPackage() {
        const { category, submitted, campackage } = this.state

        
        return (
            <div className={'form-group' + (submitted && !campackage ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                <label htmlFor="campackage">{strings.add_lens_and_accessories}</label>
                <span className="spannote spanhowto" onClick={(e)=>{e.preventDefault(); this.openHowToModal() }}>
                    ({strings.please_read_for_more_info_upload_accessories_and_lenses})
                </span>
                <select className="form-control form-package" 
                    name="campackage" 
                    
                    onChange={(e) => this.handleSelect(e)}
                >
                    <option value="Body Only">{strings.body_only}</option>
                    <option value="With Lens">{strings.with_lens}</option>
                    <option value="With Accessories">{strings.with_accessories}</option>
                    <option value="With Lens and Accessories">{strings.with_lens_and_accessories}</option>
                </select>
            </div>
        )
        

    }

    renderModels() {    
        const { models } = this.state    
        if (models) {        
            return (
                <div className="sk-hits-grid m-4">
                    { models.map((model) => {
                        return (
                            <div className="sk-hits-grid-hit sk-hits-grid__item">
                                <a href="" className="centerLensImage" key={model.id} onClick={(e)=>{e.preventDefault(); 
                                    this.selectedLensModel(model.brand +" "+ model.model +" "+ model.type, model.id)
                                }}>
                                    <img src={ model.image } style={lensStyle} />
                                    <div className="sk-hits-grid-hit__title text-center">{ model.model }</div>
                                </a>
                            </div>    
                        )    
                    })}
                </div>
            )
        }else {
            return <h4>{strings.choose_lens_category}</h4>
        }
    }

    renderLensPackage() {
        //model.brand +" "+ model.model +" "+ model.type, model.id
        var count = 0
        if (this.state.lensPackage.length > 0 ) {
            return (
                <div className={this.props.rtlconv == "rtl" ? 'righttoleft' : ''}>
                    <span>Lenses</span>
                    { this.state.lensPackage.map((lens, index) => {
                       
                        return (
                            <p className="blue-font">{ index + 1 }. {lens.model} <a href="" onClick={(e)=>{e.preventDefault(); this.removeLensToPackage(lens)}} >  x </a></p>
                        )
                        
                    })}
                </div>
            )
        }
    }


    renderAccessoriesDescription() {
        
        var count = 0
        if (this.state.accessoriesDescription.length > 0 ) {
            return (
                <div>
                    <span>{strings.accessories_description}</span>
                    { this.state.accessoriesDescription.map((acc, index) => {
                       
                        return (
                            <p className="blue-font">{ index + 1 }. {acc} <a href="" onClick={(e)=>{e.preventDefault(); this.removeAccToPackage(acc)}} >  x </a></p>
                        )
                        
                    })}
                </div>
            )
        }
    }


    renderDescription() {
        const { camera_package, description, submitted } = this.state
        //<div><FaPlusCircle /> Add Lens</div>
        if (camera_package == "With Accessories"){
            return (
                <div className={this.props.rtlconv == "rtl" ? ' righttoleft' : ''}>
                    <button className="addPackageButton" onClick={(e)=>{e.preventDefault(); this.openAccModal() }}> + {strings.add_accessories_description}</button>
                </div>
            )
        }

        if (camera_package == "With Lens and Accessories"){
            if (this.state.lensPackage.length < 20) { // Maximum number of Lenses in a package
                return (
                    <div className={this.props.rtlconv == "rtl" ? ' righttoleft' : ''}>
                        <button className="addPackageButton" onClick={(e)=>{e.preventDefault(); this.openModal() }}> + {strings.add_lens}</button>
                        <br />
                        <br />
                        <button className="addPackageButton" onClick={(e)=>{e.preventDefault(); this.openAccModal() }}> + {strings.add_accessories_description}</button>
                    </div>
                )
            }
        }

        if (camera_package == "With Lens"){
            if (this.state.lensPackage.length < 20) { // Maximum number of Lenses in a package
                return (
                    <div className={this.props.rtlconv == "rtl" ? ' righttoleft' : ''}>
                        <button className="addPackageButton" onClick={(e)=>{e.preventDefault(); this.openModal() }}> + {strings.add_lens}</button>
                    </div>
                )
            }
        }
    }

    renderAddLensModal(){
        const { lensbrandmodels } = this.state

        // 1st window (Lens Brand selection)
        if (this.state.selectBrand) {
            return (
                <div>
                    <h2 className="text-center card-title">{strings.choose_lens_brand}</h2>
                        <div className="m-2" id="selectBrand">
                            <div className="row category">
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Canon')}} >
                                        <img src={images_url + "/images/lens-Canon.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Canon.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Fujifilm')}}>
                                        <img src={images_url + "/images/lens-Fujifilm.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Fujifilm.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Hasselblad')}}>
                                        <img src={images_url + "/images/lens-Hasselblad.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Hasselblad.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Leica')}}>
                                        <img src={images_url + "/images/lens-Leica.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Leica.png"} style={logoStyle} />
                                    </a>
                                </div>
                            </div>
                            <div className="row category">
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Nikon')}}>
                                        <img src={images_url + "/images/lens-Nikon.png"} style={lensStyle} />
                                        <img src={images_url + "/images/nikon.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Olympus')}}>
                                        <img src={images_url + "/images/lens-Olympus.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Olympus.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Panasonic')}}>
                                        <img src={images_url + "/images/lens-Panasonic.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Panasonic.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Pentax')}}>
                                        <img src={images_url + "/images/lens-Pentax.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Pentax.png"} style={logoStyle} />
                                    </a>
                                </div>
                            </div>
                            <div className="row justify-content-md-center category">
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Phaseone')}}>
                                        <img src={images_url + "/images/lens-Phaseone.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Phaseone.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Samsung')}}>
                                        <img src={images_url + "/images/lens-Samsung.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Samsung.png"} style={logoStyle} />
                                    </a>
                                </div>
                                
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Samyang')}}>
                                        <img src={images_url + "/images/lens-Samyang.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Samyang.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Sigma')}}>
                                        <img src={images_url + "/images/lens-Sigma.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Sigma.png"} style={logoStyle} />
                                    </a>
                                </div>
                            </div>
                            <div className="row justify-content-md-center category">
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Sony')}}>
                                        <img src={images_url + "/images/lens-Sony.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Sony.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Tamron')}}>
                                        <img src={images_url + "/images/lens-Tamron.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Tamron.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Tokina')}}>
                                        <img src={images_url + "/images/lens-Tokina.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Tokina.png"} style={logoStyle} />
                                    </a>
                                </div>
                                
                                
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Zeiss')}}>
                                        <img src={images_url + "/images/lens-Zeiss.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Zeiss.png"} style={logoStyle} />
                                    </a>
                                </div>
                            </div>
                        </div>
                    <hr />    
                    <button className="btn btn-lg btn-primary float-right" onClick={this.closeModal}>{strings.cancel}</button>
                </div>
            )
        }

        // 2nd window (Lens model selection)
        if (this.state.selectModel){
            if (lensbrandmodels.length>0){
                return (
                    <div>
                        <div className="text-center">
                            <img className="brandimgheight" src={images_url + "/images/logo-"+lensbrandmodels[0].type_of_lens.brand.company+".png"} style={{logoStyle}} />
                        </div>
                        <div>
                            { this.renderCategory() }
                        </div>
                        <hr/>
                        <div className="filter-list search">
                            <form onSubmit={e => { e.preventDefault(); }}>
                                <fieldset className="form-group">
                                    <input type="text" className="form-control form-control-lg searchModel" placeholder={strings.search_model} onChange={(e) =>{ this.searchModel(e);}}/>
                                </fieldset>
                            </form>
                        </div>
                        
                        <div>
                            { this.renderModels() }
                        </div>  
                    </div>
                )
            }else{
                return (
                    <div>
                        <div className="loading">{strings.loading_lens_models}</div>
                    </div>
                )
            }
        }

        // 3rd window (Lens comments and actual photos)
        if (this.state.selectPhotoAndComment) {
            /*<div>
                <label htmlFor="comments">Comments</label>
                <input type="text" className="comment-input" name="lens_comments" value={this.state.lens_comments} onChange={this.handleLensComments} />
            </div>

            <ImageUploader
                withIcon={true}
                buttonText='Upload Lens Photos'
                onChange={this.onLensDrop}
                imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                maxFileSize={5242880}/>*/
            return (
                <div className="thirdWindow">
                    <h4>{this.state.lens}</h4>
                    <hr />

                    <div className="thirdWindowButton">
                        <button className="btn btn-lg btn-primary" onClick={(e)=>{e.preventDefault(); this.backToSelectLensModel()}}>
                            {strings.back}
                        </button>
                        <button className="btn btn-lg btn-primary addButtonMargin" onClick={(e)=>{e.preventDefault(); this.addLensToPackage()}}>
                            {strings.add}
                        </button>
                    </div>
                </div>
            )
        }


        
    }


    renderAddAccessoriesModal() {

        const { camera_package, description, submitted } = this.state

        return (

            <div className={"topspace"+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>

                <div className={'form-group' + (submitted && !description ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <label htmlFor="description">{strings.accessories_description}</label>
                    <textarea className="form-control replyBox" name="description" value={description} onChange={this.handleChange} />
                </div>

                <button className="btn btn-lg btn-primary addButtonMargin" onClick={(e)=>{e.preventDefault(); this.addAccessories()}}>
                    {strings.add}
                </button>

            </div>

            )
    }


    renderHowToModal() {

        return (

            <div className={"topspace" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>

                <div>
                    <h2>{strings.info_upload_accessories_and_lenses}</h2>
                    <hr />
                    <p>{strings.paragraph_one}</p>
                    <p>{strings.paragraph_two}</p>
                </div>

            </div>

            )
    }



    renderImageUploader() {
        if (this.state.camera_package == "With Lens"){
            return (
                <div>
                <div className="wrapper">
                    <div className="uploadCam">
                        <ImageUploader
                            withIcon={true}
                            buttonText={strings.upload_camera_photo}
                            onChange={this.onDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                            maxFileSize={5242880}
                            buttonType='button'
                            withPreview={true}/>
                    </div>
                    <div className="uploadLens">
                        <ImageUploader
                            withIcon={true}
                            buttonText={strings.upload_lens_photo}
                            onChange={this.onLensDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                            maxFileSize={5242880}
                            buttonType='button'
                            withPreview={true}/>
                    </div>
                </div>
                {this.state.actualPhotoError ? <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_camera_photos}</div> : null}
                {this.state.packagePhotoError ? <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_lens_photos}</div> : null}
                {this.state.bothPhotoError ? <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_camera_and_lens_photos}</div> : null}
                </div>
            )
        }else if ( this.state.camera_package == "With Lens and Accessories"){
            return (
                <div>
                <div className="wrapper">
                    <div className="uploadCam">
                        <ImageUploader
                            withIcon={true}
                            buttonText={strings.upload_camera_photo}
                            onChange={this.onDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                            maxFileSize={5242880}
                            buttonType='button'
                            withPreview={true}/>
                    </div>
                    <div className="uploadLens">
                        <ImageUploader
                            withIcon={true}
                            buttonText={strings.upload_lenses_accessories_photo}
                            onChange={this.onLensDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                            maxFileSize={5242880}
                            buttonType='button'
                            withPreview={true}/>
                    </div>
                </div>
                {this.state.actualPhotoError ? <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_camera_photos}</div> : null}
                {this.state.packagePhotoError ? <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_lens_photos}</div> : null}
                {this.state.bothPhotoError ? <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_camera_and_lens_photos}</div> : null}
                </div>
            )
        }else if ( this.state.camera_package == "With Accessories"){
            return (
                <div>
                <div className="wrapper">
                    <div className="uploadCam">
                        <ImageUploader
                            withIcon={true}
                            buttonText={strings.upload_camera_photo}
                            onChange={this.onDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                            maxFileSize={5242880}
                            buttonType='button'
                            withPreview={true}/>
                    </div>
                    <div className="uploadLens">
                        <ImageUploader
                            withIcon={true}
                            buttonText={strings.upload_accessories_photo}
                            onChange={this.onLensDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                            maxFileSize={5242880}
                            buttonType='button'
                            withPreview={true}/>
                    </div>
                </div>
                {this.state.actualPhotoError ? <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_camera_photos}</div> : null}
                {this.state.packagePhotoError ? <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_lens_photos}</div> : null}
                {this.state.bothPhotoError ? <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_camera_and_lens_photos}</div> : null}
                </div>
            )
        } else {
            return (
                <div>
                    <ImageUploader
                        withIcon={true}
                        buttonText={strings.upload_camera_photo}
                        onChange={this.onDrop}
                        imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                        maxFileSize={5242880}
                        buttonType='button'
                        withPreview={true}/>
                        {this.state.actualPhotoError ? <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_camera_photos}.</div> : null}
                </div>
            )
        }
    }


    renderPhotoSizeError(){
        
        if (this.state.showSizeError){
            if (Object.keys(this.props.video_sell).length > 0) {
                   
                return(
                    <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_decent_photo_size}</div>
                )
            }
        }
    }


    renderSellForm(){
        let token = localStorage.getItem('token')
        const { price, condition, shutter_count, warranty, actual_photos, submitted, file, camera_package, comments} = this.state;
        if(!token){
            return <p>{strings.please_login_to_sell}</p>
        }else{
           return  <form name="form" onSubmit={this.handleSubmit}>
                        <div className={'form-group' + (submitted && !price ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="price">{strings.price}</label><span className="spannote required">({strings.should_be_in_number})</span>
                            <input type="number" inputMode="numeric" className="form-control form-price" name="price" value={price} onChange={this.handleChange} />
                            {this.state.priceError ?
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                                : null
                            }
                        </div>
                        
                        <div className={'form-group' + (submitted && !warranty ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="warranty" className="required">{strings.warranty}</label>
                            <select className="form-control form-warranty" 
                                name="warranty" 
                                
                                onChange={(e) => this.handleSelectWarranty(e)}
                            >
                                <option value="">{strings.sellect_warranty}</option>
                                <option value="No Warranty">{strings.no_warranty}</option>
                                <option value="One (1) Week">{strings.one_week}</option>
                                <option value="Two (2) Weeks">{strings.two_weeks}</option>
                                <option value="One (1) Month">{strings.one_month}</option>
                            </select>
                            {this.state.warrantyError ?
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                                : null
                            }
                        </div>
                        <div className={'form-group' + (submitted && !condition ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="condition" className="required">{strings.condition}</label>
                            <select className="form-control form-condition" 
                                name="condition" 
                                
                                onChange={(e) => this.handleSelectCondition(e)}
                            >
                                <option value="">{strings.select_camera_condition}</option>
                                <option value="Considered new - barely used">{strings.considered_new_barely_used}</option>
                                <option value="Brand new - not used">{strings.brand_new_not_used}</option>
                                <option value="Used for demo only">{strings.used_for_demo_only}</option>
                                <option value="Show signs of use but works perfectly">{strings.show_sign_of_use_but_works_perfectly}</option>
                                <option value="Shows some wear">{strings.shows_some_wear}</option>
                                <option value="Manufacturer Defect">{strings.manufacturer_defect}</option>
                                <option value="Broken but still works fine">{strings.broken_but_still_works_fine}</option>
                                <option value="Broken and need some maintenance">{strings.broken_and_need_some_maintenance}</option>
                                <option value="Other - please specify in the comment box below">{strings.others_please_specify}</option>
                            </select>
                            {this.state.conditionError ?
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                                : null
                            }
                        </div>

                        { this.renderPackage() }

                        { this.renderLensPackage() }

                        { this.renderAccessoriesDescription() }

                        { this.renderDescription() }

                        <div className={'form-group' + (submitted && !comments ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="comments">{strings.comments}</label>
                            <textarea className="form-control replyBox" name="comments" value={comments} onChange={this.handleChange} />
                        </div>
                        
                        { this.renderImageUploader() }

                        { this.state.spinner ?
                        
                            <OverlayLoader 
                              color={'blue'} // default is white
                              loader="ScaleLoader" // check below for more loaders
                              text={strings.uploading_your_item_please_wait}
                              active={this.state.spinner} 
                              backgroundColor={'white'} // default is black
                              opacity=".4" // default is .9  
                            >
                                <div className="spinner">
                                </div>
                            </OverlayLoader>
                        
                        : null}

                        <div className="form-group text-center">
                            <button className="btn btn-lg btn-primary">{strings.submit}</button>
                        </div>
                    </form> 
        }
    }


    

  render() {
        const { state } = this.props.location
        
        return (
            <div>
                <ul className="breadcrumb">
                    <li><Link to="/">{strings.home}</Link></li>
                    <li><Link to="/sell">{strings.sell}</Link></li>
                    <li><Link to="/sell_brand">{strings.video_cameras}</Link></li>
                    <li><a href="#" onClick={(e)=>{e.preventDefault(); history.push(
                        {'pathname':'/select_video_category', 
                        'state':{'brand':state.brand}
                        })}}>{state.brand}</a></li>
                    <li>{state.model} - {strings.video_camera_sell_form}</li>
                </ul>
                <div className=" bg-company-orange">
                    <ul className="d-flex mx-auto justify-content-center nomarge zeroleftpadding">
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickStillHandle}><span>{strings.photography_cameras}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickLensHandle}><span>{strings.lenses}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4 active">
                            <a className="awhite" href="" onClick={this.clickVideoHandle}><span>{strings.video_cameras}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickLightingHandle}><span>{strings.lighting}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickAccessoriesHandle}><span>{strings.accessories}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickDroneHandle}><span>{strings.drone}</span></a>
                        </li>
                    </ul>
                </div>
                <div className="card m-2">
                    <div className={"card-header" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                        <h3 className="mb-0" >{state.model} {state.type}</h3>
                    </div>
                    <div className="card-body" dir="ltr">
                        <div className="row">
                            <div className="col-4">
                                <img className="card-img-left" src={state.image} style={camStyle}/><br />
                            </div>
                            <div className="col mr-5" dir={this.props.rtlconv}>
                                <p className="text-center">{strings.please_fill_up_the_form_and_the_system_will_autogenerate_other_details}..</p>
                                { this.renderSellForm() }
                            </div>
                        </div>
                    </div>
            	</div>
                <br />
                <br />
                <br />
                <br />

                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    style={customStyles}
                    contentLabel={strings.add_lens}
                    ariaHideApp={false}
                >
                    
                    <div className="frame arabicfont"> 
                        <button className="btn btn-danger btn-sm float-right fixedclosed modalButton" onClick={this.closeModal}> X </button>
                        <div className="scroll"> 
                              {this.renderAddLensModal()}
                        </div>
                    </div>
                    
                </Modal>

                <Modal
                    isOpen={this.state.accModalIsOpen}
                    onAfterOpen={this.afterOpenAccModal}
                    onRequestClose={this.closeAccModal}
                    style={customStylesForAcc}
                    contentLabel={strings.add_accessories}
                    ariaHideApp={false}
                >
                    
                    <div className="acc-frame arabicfont"> 
                        <button className="btn btn-danger btn-sm float-right fixedclosed modalButton" onClick={this.closeAccModal}> X </button>
                        <div className="scroll"> 
                              {this.renderAddAccessoriesModal()}
                        </div>
                    </div>
                    
                </Modal>

                <Modal
                    isOpen={this.state.howtoModalIsOpen}
                    onAfterOpen={this.afterOpenHowToModal}
                    onRequestClose={this.closeHowToModal}
                    style={customStyles}
                    contentLabel={strings.info_upload_accessories_and_lenses}
                    ariaHideApp={false}
                >
                    
                    <div className="addinglensaccnote-frame arabicfont"> 
                        <button className="btn btn-danger btn-sm float-right fixedclosed modalButton" onClick={this.closeHowToModal}> X </button>
                        <div className="scroll"> 
                              {this.renderHowToModal()}
                        </div>
                    </div>
                    
                </Modal>
            </div>
        )
  }
}

function mapStateToProps(state){
    
    return {
        lens_models: state.lens_models,
        camerasell: state.camerasell,
        video_sell: state.camerasell,
        rtlconv: state.rtlconv
    };
}


export default connect(mapStateToProps)(CameraDetail)