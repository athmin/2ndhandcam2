import React from 'react'
import { connect } from 'react-redux'
import {Link} from 'react-router-dom'
import { camDetails } from '../actions'
import { editVideoCameraForsaleAction } from '../actions'
import { sellLensPackageActions, stillAccessoriesActions, removeVideoCameraPhotoAction, editVideoAccessoriesActions, editVideoLensPackageAction } from '../actions';
import { getLensBrandModelsAction } from '../actions';
import ImageUploader from 'react-images-upload';
import SellForm from '../components/SellForm'
import { history } from '../store/configureStore'
import Modal from 'react-modal';
import FaPlusCircle from 'react-icons/lib/fa'
import OverlayLoader from 'react-loading-indicator-overlay/lib/OverlayLoader'
import { strings } from '../lib/strings';

let CONFIG = require('../lib/config.json')
import Dropzone from 'react-dropzone';
const images_url = CONFIG.server_url + "/static/media_cdn"
const lensStyle = {
    width: 105,
    height: 60,
};
const logoStyle = {
    width: 100,
    height: 35,
};
const camStyle = {
    "maxWidth": "100%",
    height: "auto"
    };

const uploadStyle = {
    "position": "relative", 
    "display": "flex", 
    "alignItems": "center", 
    "justifyContent": "center", 
    "flexWrap": "wrap", 
    width: "100%",
};

const customStyles = {
    content: {
        'border-left': '5px solid #f4a72e',
        'border-top': '5px solid #f4a72e',
        'border-right': '5px solid #3d4041',
        'border-bottom': '5px solid #3d4041',
        borderRadius: '4px',
        bottom: 'auto',
        minHeight: '10rem',
        left: '50%',
        padding: '2rem',
        position: 'fixed',
        right: 'auto',
        top: '50%',
        transform: 'translate(-50%,-50%)',
        minWidth: '20rem',
        width: '80%',
        maxWidth: '60rem',
        padding: '0'
      },
    overlay: {
        backgroundColor: "rgba(255, 255, 255, 0.25)"
    }  
};

const customStylesForAcc = {
    content: {
        border: '5px solid dodgerblue',
        borderRadius: '4px',
        bottom: 'auto',
        minHeight: '10rem',
        left: '50%',
        padding: '2rem',
        position: 'fixed',
        right: 'auto',
        top: '50%',
        transform: 'translate(-50%,-50%)',
        minWidth: '20rem',
        width: '60%',
        maxWidth: '60rem',
        padding: '0'
      },
    overlay: {
        backgroundColor: "rgba(255, 255, 255, 0.25)"
    }  
};


class EditCamForSale extends React.Component{
	
	constructor(props) {
		super(props);
		this.state = {
            accessoriesDescription: [],
			id : this.props.location.state.id,
            item: this.props.location.state.item,
	        brand : this.props.location.state.brand,
	        type : this.props.location.state.type,
	        model : this.props.location.state.model,
	        price : this.props.location.state.price,
	        status : this.props.location.state.status,
	        condition : this.props.location.state.condition,
            accessoriesDescription : this.props.location.state.description,
	        shutter_count : this.props.location.state.shutter_count,
	        warranty : this.props.location.state.warranty,
	        camera_package : this.props.location.state.cam_package,
	        image : this.props.location.state.image,
            //actual_photos: this.props.location.state.actual_photos,
            comments: this.props.location.state.comments,
            uploaded_photos: this.props.location.state.actual_photos ? this.props.location.state.actual_photos : [],
            uploaded_lens_photos: this.props.location.state.lens_photos ? this.props.location.state.lens_photos : [],
            actual_photos:[],
            flashMessage: null,

            submitted: false,
            modalIsOpen: false,     // state that opens / closes the modal
            selectBrand: true,      // if true display the brand selection for lenses
            selectModel: false,      // if true display lens models for the selected brand
            selectPhotoAndComment: false, // if tru display comments and imageuploader for lens

            accModalIsOpen: false,

            lensbrandmodels: [],
            models: [],
            fetching: false,
            selected: 'all',

            lens: "",
            lens_id: "",
            lens_comments: "",
            lens_photos: [],

            lensPackage: this.props.location.state.lens_package ? this.props.location.state.lens_package : [] ,
            lensPackageIDS: [],

            priceError: false,
            warrantyError: false,
            conditionError: false,
            spinner: false,

            edit_accessories: false,
            edit_lens: false,

            removed_lens: [],
            removed_acc: [],

            add_lens: [],
            add_acc: [],
            showSizeError: false,


		}
		
        
        this.onDrop = this.onDrop.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        
        this.onLensDrop = this.onLensDrop.bind(this);
        this.handleLensComments = this.handleLensComments.bind(this);
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);

        this.openAccModal = this.openAccModal.bind(this);
        this.afterOpenAccModal = this.afterOpenAccModal.bind(this);
        this.closeAccModal = this.closeAccModal.bind(this);

    }

    componentWillMount(){
        let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }   

        let viewportmeta = document.querySelector('meta[name="viewport"]');
        if(viewportmeta===null){
            viewportmeta = document.createElement("meta");
            viewportmeta.setAttribute("name","viewport");
            document.head.appendChild(viewportmeta);
              
            viewportmeta = document.querySelector('meta[name="viewport"]');
        }
        viewportmeta.setAttribute('content', 'width=1470, initial-scale=0.3, maximum-scale=1.0');
    }

    componentDidMount(){
        window.scrollTo(0, 0)
    }

    handleSelect(e) {
        this.setState({camera_package: e.target.value});
       
    }

    handleSelectCondition(e) {
        this.setState({condition: e.target.value});
        
    }

    handleSelectWarranty(e) {
        this.setState({warranty: e.target.value});
        
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        
    }

    handleLensComments(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        
    }

    openModal() {
        this.setState({modalIsOpen: true});
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        //this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({modalIsOpen: false});
        this.setState({selectBrand: true})
        this.setState({selectModel: false})
        this.setState({selectPhotoAndComment: false})
        this.setState({lensbrandmodels: []})
    }

    openAccModal() {
        this.setState({accModalIsOpen: true});
    }

    afterOpenAccModal() {
        // references are now sync'd and can be accessed.
        //this.subtitle.style.color = '#f00';
    }

    closeAccModal() {
        this.setState({accModalIsOpen: false});
        this.setState({description: []})
    }

    backToSelectLensModel() {
        this.setState({selectBrand: false})
        this.setState({selectModel: true})
        this.setState({selectPhotoAndComment: false})
        this.setState({lens: ""})
    }

    addLensToPackage() {

        this.setState({edit_lens: true})
        
        var newArray = this.state.add_lens.slice();
        var newIDArray = this.state.add_lens.slice();      
        newArray.push({model:this.state.lens, id:this.state.lens_id});   
        newIDArray.push(this.state.lens_id)

        this.setState({add_lens: newArray})
        this.setState({lensPackageIDS: newIDArray})
        
        this.closeModal()
        this.setState({selectBrand: true})
        this.setState({selectModel: false})
        this.setState({selectPhotoAndComment: false})

        // Reset the values of lens
        this.setState({lens: ""})
    }

    addAccessories() {
        this.setState({edit_accessories: true})

        var accArray = this.state.add_acc.slice();
        accArray.push(this.state.description)
        
        this.setState({ add_acc: accArray })
        
        this.closeAccModal()
    }


    removeNewAccToPackage(acc) {
        this.setState({edit_accessories: true})

        var accArray = this.state.add_acc
        var index = accArray.indexOf(acc)
        var newArray = []
        if (index > -1) {
            newArray = accArray.splice(index, 1);
        }

        this.setState({ add_acc: accArray })

        var remArray = this.state.removed_acc
        remArray.push(acc)
        this.setState({ removed_acc: remArray})

    }

    removeOldAccToPackage(acc) {
        this.setState({edit_accessories: true})

        var accArray = this.state.accessoriesDescription
        var index = accArray.indexOf(acc)
        var newArray = []
        if (index > -1) {
            newArray = accArray.splice(index, 1);
        }

        this.setState({ accessoriesDescription: accArray })

        var remArray = this.state.removed_acc
        remArray.push(acc)
        this.setState({ removed_acc: remArray})

    }

    selectedLensModel(model, id) {
        this.setState({lens: model})
        this.setState({lens_id: id})
        this.setState({selectBrand: false})
        this.setState({selectModel: false})
        this.setState({selectPhotoAndComment: true})
    }

    removeLensToPackage(model) {
        
        var lensArray = this.state.lensPackage
        var index = lensArray.indexOf(model)
        var newArray = []
        if (index > -1) {
            newArray = lensArray.splice(index, 1);
        }

        this.setState({ lensPackage: lensArray })

        this.setState({ edit_lens: true })
        var removeArray = this.state.removed_lens.slice();
        removeArray.push(model)
        this.setState({ removed_lens: removeArray })

    }

    SelectLensModel(brand) {
        
        const { dispatch } = this.props;
        this.setState({selectBrand: false})
        this.setState({selectModel: true})
        dispatch(getLensBrandModelsAction.selectLensCategory(brand)).then( () => {
            this.setState({fetching: true})
            this.setState({ lensbrandmodels: this.props.lens_models })
            this.allLenses()
        })
    }

    setSelected(category) {
        this.setState({selected  : category})
    }

    isActive(value){
        return 'col choose-category '+((value===this.state.selected) ?'selected':'default');
    }

    renderAllLensButton(category) {
        if (category.length>1){
            return (
                    <div id='choose-category' className={this.isActive('all')}>
                        <span onClick={(e) =>{ this.allLenses(); this.setSelected('all') }}>{strings.all_lenses}</span>
                    </div>    
            )
        }else{
            return <p></p>
        }
    }

    renderCategory() {
        const { lensbrandmodels } = this.state
        let category = []

        for (let i = 0; i < lensbrandmodels.length; i++){
            if (category.indexOf(lensbrandmodels[i].type_of_lens.lens_type) === -1 ) category.push(lensbrandmodels[i].type_of_lens.lens_type);     
        }
       
        return (
            <div className="row">
                { category.map((cat) => {
                    
                    
                    return (
                        <div id='choose-category' className={this.isActive(cat)}>
                            <span key={cat} onClick={(e) =>{ this.filterModel(cat); this.setSelected(cat) }}>{cat}</span>
                        </div>    
                    )
                        
                })}
                { this.renderAllLensButton(category)}
            </div>    
        )

    }

    allLenses() {
        const { lensbrandmodels } = this.state
        //this.setState({models: lensbrandmodels})

        let newmodels = []
        
        for (let i = 0; i < lensbrandmodels.length; i++){
            newmodels.push({'id' : lensbrandmodels[i].id, 'model' : lensbrandmodels[i].model, 'image' : lensbrandmodels[i].image, 'type' : lensbrandmodels[i].type_of_lens.lens_type, 'brand' : lensbrandmodels[i].type_of_lens.brand.company});     
        }

        if (newmodels){
            this.setState({models: newmodels})
        }
    }

    filterModel(lensCategory) {
        
        const { lensbrandmodels } = this.state
        let newmodels = []
        
        for (let i = 0; i < lensbrandmodels.length; i++){
            if ( lensbrandmodels[i].type_of_lens.lens_type == lensCategory ) newmodels.push({'id' : lensbrandmodels[i].id, 'model' : lensbrandmodels[i].model, 'image' : lensbrandmodels[i].image, 'type' : lensbrandmodels[i].type_of_lens.lens_type, 'brand' : lensbrandmodels[i].type_of_lens.brand.company});     
        }

        if (newmodels){
            
            this.setState({models: newmodels})
        }
            
        
    }

    searchModel(event) {
        const { lensbrandmodels } = this.state
        let searchResult = lensbrandmodels
        let newmodels = []

        searchResult = searchResult.filter(function(item){
            
            return item.model.toString().toLowerCase().search(
                event.target.value.toLowerCase()) !== -1;
        });

        for (let i = 0; i < searchResult.length; i++){
            newmodels.push({'id' : searchResult[i].id, 'model' : searchResult[i].model, 'image' : searchResult[i].image, 'image_thumbnail' : searchResult[i].image_thumbnail, 'type' : searchResult[i].type_of_lens.lens_type, 'brand' : searchResult[i].type_of_lens.brand.company});     
        }

        this.setState({models: newmodels})
    }


    onDrop(picture) {
        
        this.setState({
            actual_photos: picture,
            showSizeError: false
        });
        
    }

    onLensDrop(picture) {
        
        this.setState({
            lens_photos: picture,
            showSizeError: false
        });

        this.setState({
            edit_lens: true,
        });

        
    }

    handleSubmit = (e) => {
        e.preventDefault()
        const { id, price, item, camera_package, condition, description, shutter_count, warranty, status, comments, actual_photos, lensPackage, lens_photos, submitted, edit_lens, removed_acc, removed_lens  } = this.state
        const { dispatch } = this.props;
        
        let body = new FormData()
        
        body.append('pk', id)
        body.append('price', price)
        body.append('condition', condition)
        body.append('warranty', warranty) 
        body.append('camera_package', camera_package)
        body.append('status', status)
        body.append('comments', comments)
        body.append('item', item)

        if (shutter_count==""){
            body.append('shutter_count', 0)
        }else{
            body.append('shutter_count', shutter_count)
        }
        
    
        Object.keys(actual_photos).forEach(( key, i ) => {
            body.append('actual_photos', actual_photos[key]);
        });

        if (camera_package == "With Lens"){
            if (price){ 
                for (var pair of body.entries()){
                }
                this.setState({ spinner: true})
                dispatch(editVideoCameraForsaleAction.editvideocameraforsale(body)).then( () => {
                    if(this.state.edit_lens){
                        this.setState({ showSizeError: true })
                        this.setState({ spinner: false })
                        this.handleLensPackages()
                    }
                })     
            }
        }

        if (camera_package == "With Lens and Accessories"){
            if (price){ 
                for (var pair of body.entries()){
                }
                this.setState({ spinner: true})
                dispatch(editVideoCameraForsaleAction.editvideocameraforsale(body)).then( () => {
                    this.setState({ showSizeError: true })
                    this.setState({ spinner: false })
                    if(this.state.edit_lens){
                        this.handleLensPackages()
                    }
                    if(this.state.edit_accessories){
                        this.handleAccessoriesDescription()
                    }

                })     
            }
        }

        if (camera_package == "With Accessories"){
            if (price){ 
                for (var pair of body.entries()){
                }
                this.setState({ spinner: true})
                dispatch(editVideoCameraForsaleAction.editvideocameraforsale(body)).then( () => {
                    this.setState({ showSizeError: true })
                    this.setState({ spinner: false })
                    if(this.state.edit_accessories){
                        this.handleAccessoriesDescription()
                    }
                    
                })     
            }
        }

        if (camera_package == "Body Only"){
            if (price && condition && warranty){ 
                
                for (var pair of body.entries()){
                }
                this.setState({ spinner: true})
                dispatch(editVideoCameraForsaleAction.editvideocameraforsale(body)).then( () => {
                    history.push('/dashboard')    
                })
                
            }
        }


        if (price == ""){
            this.setState({ priceError: true})
        }else{
            this.setState({ priceError: false})
        }

        if (condition == ""){
            this.setState({ conditionError: true})
        }else{
            this.setState({ conditionError: false})
        }

        if (warranty == ""){
            this.setState({ warrantyError: true})
        }else{
            this.setState({ warrantyError: false})
        }
    }

    handleLensPackages() {
        const { lensPackageIDS, lens_photos, submitted, id, edit_lens, removed_lens, add_lens } = this.state
        const { dispatch } = this.props;
        
        let body = new FormData()
        body.append('lensPackage', JSON.stringify(this.state.lensPackage))
        body.append('edit', edit_lens)
        body.append('removed_lens', JSON.stringify(removed_lens))
        body.append('add_lens', JSON.stringify(add_lens))
       
        
        Object.keys(lens_photos).forEach(( key, i ) => {
            body.append('lens_photos', lens_photos[key]);
        });
    
        body.append('cam_id', id)
        dispatch(editVideoLensPackageAction.editVideoPackage(body))
        
    }

    handleAccessoriesDescription() {

        const { accessoriesDescription, lens_photos, submitted, camera_package, id, add_acc, edit_accessories, removed_acc } = this.state;
        const { dispatch } = this.props;

        if (this.state.edit_accessories){
            let body = new FormData()
            let camera = this.props.camera_sell

            body.append('accessories_package', JSON.stringify(accessoriesDescription))
            body.append('edit', edit_accessories)
            body.append('removed_acc', JSON.stringify(removed_acc))
            body.append('add_acc', JSON.stringify(add_acc))
            body.append('edit_accessories', edit_accessories)

            if (camera_package == "With Accessories"){
                Object.keys(lens_photos).forEach(( key, i ) => {
                    body.append('acc_photos', lens_photos[key]);
                });
            }
            body.append('cam_id', id)
            dispatch(editVideoAccessoriesActions.editVideoAccessories(body))
        }
    }


    renderShutterCountField() {
        const { category, submitted, shutter_count, type } = this.state

        if ((type == 'DSLR Cameras') || (type == 'Medium Format Cameras') || (type == 'Mirrorless Cameras') || (type == 'EOS M Series Cameras')){
            return (
                <div className={'form-group' + (submitted && !shutter_count ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <label htmlFor="shutter_count">Shutter count</label>
                    <input type="text" className="form-control form-shuttercount" name="shutter_count" value={shutter_count} onChange={this.handleChange} />
                    {submitted && !shutter_count &&
                        <div className="help-block">Shutter count is required</div>
                    }
                </div>
            )
        }
    }


    renderPackage() {
        const { category, submitted, camera_package } = this.state

        return (
            <div className={'form-group' + (submitted && !campackage ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                <label htmlFor="campackage">{strings.add_lens_and_accessories}</label>
                <select className="form-control form-package" 
                    name="campackage" 
                    value={camera_package}
                    onChange={(e) => this.handleSelect(e)}
                >
                    <option value="Body Only">{strings.body_only}</option>
                    <option value="With Lens">{strings.with_lens}</option>
                    <option value="With Accessories">{strings.with_accessories}</option>
                    <option value="With Lens and Accessories">{strings.with_lens_and_accessories}</option>
                </select>
            </div>
        )
        
    }

    renderModels() {    
        const { models } = this.state    
        if (models) {        
            return (
                <div className="sk-hits-grid m-4">
                    { models.map((model) => {
                        return (
                            <div className="sk-hits-grid-hit sk-hits-grid__item">
                                <a href="" className="centerLensImage" key={model.id} onClick={(e)=>{e.preventDefault(); 
                                    this.selectedLensModel(model.brand +" "+ model.model +" "+ model.type, model.id)
                                }}>
                                    <img src={ model.image } style={lensStyle} />
                                    <div className="sk-hits-grid-hit__title text-center">{ model.model }</div>
                                </a>
                            </div>    
                        )    
                    })}
                </div>
            )
        }else {
            return <h4>{strings.choose_lens_category}</h4>
        }
    }


    renderLensPackage() {
        //model.brand +" "+ model.model +" "+ model.type, model.id
        var count = 0
        if (this.state.lensPackage.length > 0 && this.state.add_lens.length > 0) {
            return (
                <div>
                    <span>{strings.lenses}</span>
                    { this.state.lensPackage.map((lens, index) => {
                        
                        return (
                            <p className="blue-font" key={index}>{ index + 1 }. {lens.lens_package.model} <a href="" onClick={(e)=>{e.preventDefault(); this.removeLensToPackage(lens)}} >  x </a></p>
                        )
                        
                    })}

                    { this.state.add_lens.map((lens, index) => {
                        
                        return (
                            <p className="blue-font" key={index}>{ index + 1 }. {lens.model} <a href="" onClick={(e)=>{e.preventDefault(); this.removeLensToPackage(lens)}} >  x </a></p>
                        )
                        
                    })}
                </div>
            )
        }else if(this.state.lensPackage.length > 0 && this.state.add_lens.length == 0) {
            return (
                <div>
                    <span>{strings.lenses}</span>
                    { this.state.lensPackage.map((lens, index) => {
                        
                        return (
                            <p className="blue-font" key={index}>{ index + 1 }. {lens.lens_package.model} <a href="" onClick={(e)=>{e.preventDefault(); this.removeLensToPackage(lens)}} >  x </a></p>
                        )
                        
                    })}
                </div>
            )
        }else if(this.state.lensPackage.length == 0 && this.state.add_lens.length > 0) {
            return (
                <div>
                    <span>{strings.lenses}</span>
                    { this.state.add_lens.map((lens, index) => {
                        
                        return (
                            <p className="blue-font" key={index}>{ index + 1 }. {lens.model} <a href="" onClick={(e)=>{e.preventDefault(); this.removeLensToPackage(lens)}} >  x </a></p>
                        )
                        
                    })}
                </div>
            )
        }else{
            console.log("no added lenses")
        }
    }


    renderAccessoriesDescription() {
        //model.brand +" "+ model.model +" "+ model.type, model.id
        var count = 0
        
        if (this.state.accessoriesDescription.length > 0 && this.state.add_acc.length > 0) {
            
            return (
                <div className={this.props.rtlconv == "rtl" ? 'righttoleft' : ''}>
                    <span>{strings.accessories_description}</span>
                    { this.state.accessoriesDescription.map((acc, index) => {
                       
                        return (
                            <p className="blue-font" key={index}>{ index + 1 }. {acc.description} <a href="" onClick={(e)=>{e.preventDefault(); this.removeOldAccToPackage(acc)}} >  x </a></p>
                        )
                        
                    })}

                    { this.state.add_acc.map((acc, index) => {
                        
                        return (
                            <p className="blue-font" key={index}>{ index + 1 }. {acc} <a href="" onClick={(e)=>{e.preventDefault(); this.removeNewAccToPackage(acc)}} >  x </a></p>
                        )
                        
                    })}
                </div>
            )
        }else if (this.state.accessoriesDescription.length > 0 && this.state.add_acc.length == 0) {
            return (
                <div className={this.props.rtlconv == "rtl" ? 'righttoleft' : ''}>
                    <span>{strings.accessories_description}</span>
                    { this.state.accessoriesDescription.map((acc, index) => {
                       
                        return (
                            <p className="blue-font" key={index}>{ index + 1 }. {acc.description} <a href="" onClick={(e)=>{e.preventDefault(); this.removeOldAccToPackage(acc)}} >  x </a></p>
                        )
                        
                    })}
                </div>
            )
        }else if (this.state.accessoriesDescription.length == 0 && this.state.add_acc.length > 0) {
            return (
                <div className={this.props.rtlconv == "rtl" ? 'righttoleft' : ''}>
                    <span>{strings.accessories_description}</span>
                    { this.state.add_acc.map((acc, index) => {
                       
                        return (
                            <p className="blue-font" key={index}>{ index + 1 }. {acc} <a href="" onClick={(e)=>{e.preventDefault(); this.removeNewAccToPackage(acc)}} >  x </a></p>
                        )
                        
                    })}
                </div>
            )
        }else{
            console.log("no added accessories")
        }
    }


    renderAddLensModal(){
        const { lensbrandmodels } = this.state

        // 1st window (Lens Brand selection)
        if (this.state.selectBrand) {
            return (
                <div>
                    <h2 className="text-center card-title">{strings.choose_lens_brand}</h2>
                        <div className="m-2" id="selectBrand">
                            <div className="row category">
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Canon')}} >
                                        <img src={images_url + "/images/lens-Canon.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Canon.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Fujifilm')}}>
                                        <img src={images_url + "/images/lens-Fujifilm.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Fujifilm.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Hasselblad')}}>
                                        <img src={images_url + "/images/lens-Hasselblad.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Hasselblad.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Leica')}}>
                                        <img src={images_url + "/images/lens-Leica.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Leica.png"} style={logoStyle} />
                                    </a>
                                </div>
                            </div>
                            <div className="row category">
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Nikon')}}>
                                        <img src={images_url + "/images/lens-Nikon.png"} style={lensStyle} />
                                        <img src={images_url + "/images/nikon.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Olympus')}}>
                                        <img src={images_url + "/images/lens-Olympus.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Olympus.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Panasonic')}}>
                                        <img src={images_url + "/images/lens-Panasonic.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Panasonic.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Pentax')}}>
                                        <img src={images_url + "/images/lens-Pentax.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Pentax.png"} style={logoStyle} />
                                    </a>
                                </div>
                            </div>
                            <div className="row justify-content-md-center category">
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Phaseone')}}>
                                        <img src={images_url + "/images/lens-Phaseone.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Phaseone.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Samsung')}}>
                                        <img src={images_url + "/images/lens-Samsung.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Samsung.png"} style={logoStyle} />
                                    </a>
                                </div>
                                
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Samyang')}}>
                                        <img src={images_url + "/images/lens-Samyang.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Samyang.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Sigma')}}>
                                        <img src={images_url + "/images/lens-Sigma.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Sigma.png"} style={logoStyle} />
                                    </a>
                                </div>
                            </div>
                            <div className="row justify-content-md-center category">
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Sony')}}>
                                        <img src={images_url + "/images/lens-Sony.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Sony.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Tamron')}}>
                                        <img src={images_url + "/images/lens-Tamron.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Tamron.png"} style={logoStyle} />
                                    </a>
                                </div>
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Tokina')}}>
                                        <img src={images_url + "/images/lens-Tokina.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Tokina.png"} style={logoStyle} />
                                    </a>
                                </div>
                                
                                
                                <div className="col text-center" id="choose-brand">
                                    <a href="" onClick={(e)=>{e.preventDefault(); this.SelectLensModel('Zeiss')}}>
                                        <img src={images_url + "/images/lens-Zeiss.png"} style={lensStyle} />
                                        <img src={images_url + "/images/logo-Zeiss.png"} style={logoStyle} />
                                    </a>
                                </div>
                            </div>
                        </div>
                    <hr />    
                    <button className="btn btn-lg btn-primary float-right" onClick={this.closeModal}>{strings.cancel}</button>
                </div>
            )
        }

        // 2nd window (Lens model selection)
        if (this.state.selectModel){
            if (lensbrandmodels.length>0){
                return (
                    <div>
                        <div className="text-center">
                            <img className="brandimgheight" src={images_url + "/images/logo-"+lensbrandmodels[0].type_of_lens.brand.company+".png"} style={{logoStyle}} />
                        </div>
                        <div>
                            { this.renderCategory() }
                        </div>
                        <hr/>
                        <div className="filter-list search">
                            <form onSubmit={e => { e.preventDefault(); }}>
                                <fieldset className="form-group">
                                    <input type="text" className="form-control form-control-lg searchModel" placeholder={strings.search_model} onChange={(e) =>{ this.searchModel(e);}}/>
                                </fieldset>
                            </form>
                        </div>
                        
                        <div>
                            { this.renderModels() }
                        </div>  
                    </div>
                )
            }else{
                return (
                    <div>
                        <div className="loading">{strings.loading_lens_models}</div>
                    </div>
                )
            }
        }

        // 3rd window (Lens comments and actual photos)
        if (this.state.selectPhotoAndComment) {
            
            return (
                <div className="thirdWindow">
                    <h4>{this.state.lens}</h4>
                    <hr />

                    <div className="thirdWindowButton">
                        <button className="btn btn-lg btn-primary" onClick={(e)=>{e.preventDefault(); this.backToSelectLensModel()}}>
                            {strings.back}
                        </button>
                        <button className="btn btn-lg btn-primary addButtonMargin" onClick={(e)=>{e.preventDefault(); this.addLensToPackage()}}>
                            {strings.add}
                        </button>
                    </div>
                </div>
            )
        }


        
    }


    renderAddAccessoriesModal() {

        const { camera_package, description, submitted } = this.state

        return (

            <div className="topspace">

                <div className={'form-group' + (submitted && !description ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <label htmlFor="description">{strings.accessories_description}</label>
                    
                    <textarea className="form-control replyBox" name="description" value={description} onChange={this.handleChange} />
                </div>

                <button className="btn btn-lg btn-primary addButtonMargin" onClick={(e)=>{e.preventDefault(); this.addAccessories()}}>
                    Add
                </button>

            </div>

            )
    }

    renderDescription() {
        const { camera_package, description, submitted } = this.state
        //<div><FaPlusCircle /> Add Lens</div>
        if (camera_package == "With Accessories"){
            return (
                <button className="addPackageButton" onClick={(e)=>{e.preventDefault(); this.openAccModal() }}> + {strings.add_accessories_description}</button>
            )
        }

        if (camera_package == "With Lens and Accessories"){
            if (this.state.lensPackage.length < 20) { // Maximum number of Lenses in a package
                return (
                    <div>
                        <button className="addPackageButton" onClick={(e)=>{e.preventDefault(); this.openModal() }}> + {strings.add_lens}</button>
                        <br />
                        <br />
                        <button className="addPackageButton" onClick={(e)=>{e.preventDefault(); this.openAccModal() }}> + {strings.add_accessories_description}</button>
                        
                    </div>
                )
            }
        }

        if (camera_package == "With Lens"){
            if (this.state.lensPackage.length < 20) { // Maximum number of Lenses in a package
                return (
                
                    <button className="addPackageButton" onClick={(e)=>{e.preventDefault(); this.openModal() }}> + {strings.add_lens}</button>
                    
                )
            }
        }
    }

    renderImageUploader() {
        if (this.state.camera_package == "With Lens"){
            return (
                <div className="wrapper">
                    
                    <div className="uploadCam">
                        <ImageUploader
                            withIcon={true}
                            buttonText={strings.upload_camera_photo}
                            onChange={this.onDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                            maxFileSize={5242880}
                            buttonType='button'
                            withPreview={true}/>
                    </div>
                    <div className="uploadLens">
                        <ImageUploader
                            withIcon={true}
                            buttonText={strings.upload_lens_photo}
                            onChange={this.onLensDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                            maxFileSize={5242880}
                            buttonType='button'
                            withPreview={true}/>
                    </div>
                </div>
            )
        }else if ( this.state.camera_package == "With Lens and Accessories"){
            return (
                <div className="wrapper">
                    
                    <div className="uploadCam">
                        <ImageUploader
                            withIcon={true}
                            buttonText={strings.upload_camera_photo}
                            onChange={this.onDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                            maxFileSize={5242880}
                            buttonType='button'
                            withPreview={true}/>
                    </div>
                    <div className="uploadLens">
                        <ImageUploader
                            withIcon={true}
                            buttonText={strings.upload_lenses_accessories_photo}
                            onChange={this.onLensDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                            maxFileSize={5242880}
                            buttonType='button'
                            withPreview={true}/>
                    </div>
                </div>
            )
        }else if ( this.state.camera_package == "With Accessories"){
            return (
                <div className="wrapper">
                    
                    <div className="uploadCam">
                        <ImageUploader
                            withIcon={true}
                            buttonText={strings.upload_camera_photo}
                            onChange={this.onDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                            maxFileSize={5242880}
                            buttonType='button'
                            withPreview={true}/>
                    </div>
                    <div className="uploadLens">
                        <ImageUploader
                            withIcon={true}
                            buttonText={strings.upload_accessories_photo}
                            onChange={this.onLensDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                            maxFileSize={5242880}
                            buttonType='button'
                            withPreview={true}/>
                    </div>
                </div>
            )
        }else {
            return (
                <div>
                    
                    <ImageUploader
                        withIcon={true}
                        buttonText={strings.upload_camera_photo}
                        onChange={this.onDrop}
                        imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                        maxFileSize={5242880}
                        buttonType='button'
                        withPreview={true}/>
                </div>
            )
        }
    }

    removeUploadedPhoto(photo){
        const { dispatch } = this.props;
        dispatch(removeVideoCameraPhotoAction.removevideocameraphoto(this.state.id, photo.id)).then( () => {
            this.setState({ flashMessage: this.props.removephoto.status}) 
            var array = this.state.uploaded_photos.filter(function(s) { return s != photo });
            this.setState({uploaded_photos: array });

            var lens_array = this.state.uploaded_lens_photos.filter(function(s) { return s != photo });
            this.setState({uploaded_lens_photos: lens_array });
        });
    }

    renderUploadedPhotos() {
        return (
            <div>
                {this.renderFlashMessages()}

                <h4>Uploaded Photos</h4>
                <div className="fileContainer">
                    <div className="uploadPicturesWrapper">
                        <div style={uploadStyle}>
                            { this.state.uploaded_photos.map((photo, index) => {
                                return (
                                    <div className="uploadPictureContainer" key={index}>
                                        <div className="deleteUploadedImage" onClick={() => { this.removeUploadedPhoto(photo) }}>X</div>
                                        <img src={photo.watermarked} className="uploadPicture" alt="preview"/>
                                    </div>
                                )
                            })}

                            { this.state.uploaded_lens_photos.map((photo, index) => {
                                return (
                                    <div className="uploadPictureContainer" key={index}>
                                        <div className="deleteUploadedImage" onClick={() => { this.removeUploadedPhoto(photo) }}>X</div>
                                        <img src={photo.watermarked} className="uploadPicture" alt="preview"/>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderFlashMessages(){

        if(this.state.flashMessage){
            return(
                <div className="alert alert-success">
                    { this.state.flashMessage }
                </div>
            )
        }
    }

    renderPhotoSizeError(){
        
        if (this.state.showSizeError){
            if (Object.keys(this.props.video_sell).length > 0) {
                   
                return(
                    <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_decent_photo_size}</div>
                )
            }
        }
    }


    renderSellForm(){
        let token = localStorage.getItem('token')
        const { price, condition, shutter_count, warranty, submitted, camera_package, status, actual_photos, comments } = this.state;
        if(!token){
            return <p>{strings.please_login_to_sell}.</p>
        }else{
           return  <form name="form" onSubmit={this.handleSubmit}>
                        <div className={'form-group' + (submitted && !price ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="price">{strings.price} ({strings.should_be_in_number})</label>
                            <input type="number" inputMode="numeric" className="form-control form-price" name="price" value={price} onChange={this.handleChange} />
                            {this.state.priceError ?
                                <div className="errorMessage">{strings.price_is_required}</div>
                                : null
                            }
                        </div>

                        

                        <div className={'form-group' + (submitted && !warranty ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="warranty">{strings.warranty}</label>
                            <select className="form-control form-warranty" 
                                name="warranty" 
                                value={warranty}
                                onChange={(e) => this.handleSelectWarranty(e)}
                            >
                                <option value="">{strings.sellect_warranty}</option>
                                <option value="No Warranty">{strings.no_warranty}</option>
                                <option value="One (1) Week">{strings.one_week}</option>
                                <option value="Two (2) Weeks">{strings.two_weeks}</option>
                                <option value="One (1) Month">{strings.one_month}</option>
                            </select>
                            {this.state.warrantyError ?
                                <div className="errorMessage">{strings.warranty_is_required}</div>
                                : null
                            }
                        </div>

                        <div className={'form-group' + (submitted && !condition ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="condition">{strings.condition}</label>
                            <select className="form-control form-condition" 
                                name="condition" 
                                value={condition}
                                onChange={(e) => this.handleSelectCondition(e)}
                            >
                                <option value="">{strings.select_camera_condition}</option>
                                <option value="Considered new - barely used">{strings.considered_new_barely_used}</option>
                                <option value="Brand new - not used">{strings.brand_new_not_used}</option>
                                <option value="Used for demo only">{strings.used_for_demo_only}</option>
                                <option value="Show signs of use but works perfectly">{strings.show_sign_of_use_but_works_perfectly}</option>
                                <option value="Shows some wear">{strings.shows_some_wear}</option>
                                <option value="Manufacturer Defect">{strings.manufacturer_defect}</option>
                                <option value="Broken but still works fine">{strings.broken_but_still_works_fine}</option>
                                <option value="Broken and need some maintenance">{strings.broken_and_need_some_maintenance}</option>
                                <option value="Other - please specify in the comment box below">{strings.others_please_specify}</option>
                            </select>
                            {this.state.conditionError ?
                                <div className="errorMessage">{strings.condition_is_required}</div>
                                : null
                            }
                        </div>

                        { this.renderPackage() }

                        { this.renderAccessoriesDescription() }

                        { this.renderLensPackage() }

                        { this.renderDescription() }

                        <div className={'form-group' + (submitted && !comments ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="comments">{strings.comments}</label>
                            
                            <textarea className="form-control replyBox" name="comments" value={comments} onChange={this.handleChange} />
                        </div>

                        {this.renderUploadedPhotos()}

                        { this.renderImageUploader() }
                        { this.state.spinner ?
                        
                            <OverlayLoader 
                              color={'blue'} // default is white
                              loader="ScaleLoader" // check below for more loaders
                              text={strings.uploading_your_item_please_wait}
                              active={this.state.spinner} 
                              backgroundColor={'white'} // default is black
                              opacity=".4" // default is .9  
                            >
                                <div className="spinner">
                                </div>
                            </OverlayLoader>
                        
                        : null}
                        
                        <div className="form-group text-center">
                            <button className="btn btn-lg btn-primary">{strings.submit}</button>
                        </div>
                    </form>
        }
    }


    

  render() {
        const { state } = this.props.location
        
        return (
            <div>
                <ul className="breadcrumb">
                    <li><Link to="/">{strings.home}</Link></li>
                    <li><Link to="/dashboard">{strings.dashboard}</Link></li>
                    <li>{strings.edit} - {state.brand} {state.model} {state.type}</li>
                </ul>
                
                <div className="card m-2">
                    <div className={"card-header" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                        <h3 className="mb-0" >{state.brand} {state.model} {state.type}</h3>
                    </div>
                    <div className="card-body">
                        <div className="row">
                            <div className="col-4">
                                <img className="card-img-left" src={state.image} style={camStyle}/><br />
                            </div>
                            <div className="col mr-5">
                                <p className="text-center">{strings.please_fill_up_the_form_and_the_system_will_autogenerate_other_details}..</p>
                                { this.renderSellForm() }
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <br />
                <br />
                <br />

                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    style={customStyles}
                    contentLabel={strings.add_lens}
                    ariaHideApp={false}
                >
                    
                    <div className="frame"> 
                        <button className="btn btn-danger btn-sm float-right fixedclosed" onClick={this.closeModal}> X </button>
                        <div className="scroll"> 
                              {this.renderAddLensModal()}
                        </div>
                    </div>
                    
                </Modal>

                <Modal
                    isOpen={this.state.accModalIsOpen}
                    onAfterOpen={this.afterOpenAccModal}
                    onRequestClose={this.closeAccModal}
                    style={customStylesForAcc}
                    contentLabel={strings.add_accessories}
                    ariaHideApp={false}
                >
                    
                    <div className="acc-frame"> 
                        <button className="btn btn-danger btn-sm float-right fixedclosed" onClick={this.closeAccModal}> X </button>
                        <div className="scroll"> 
                              {this.renderAddAccessoriesModal()}
                        </div>
                    </div>
                    
                </Modal>
            </div>
        )
  }
}

function mapStateToProps(state){
    
    return {
        lens_models: state.lens_models,
        camera_sell: state.camera_sell,
        video_sell: state.camerasell,
        rtlconv: state.rtlconv,
        removephoto: state.removephoto
    };
}

export default connect(mapStateToProps)(EditCamForSale)