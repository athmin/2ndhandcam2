import React from 'react'
import { history } from '../store/configureStore'
import { connect } from 'react-redux';
import { strings } from '../lib/strings';


class ConfirmEmail extends React.Component {

	

  render() {
    return (
    	<div className="card mt-4 mx-auto col-md-6 col-md-offset-3">
                <div className="card-body" style={{textAlign: "right"}}>
                    <p style={{color: "blue"}}>{strings.activation_mail_sent1}</p>
                    <p style={{color: "red"}}>{strings.activation_mail_sent2}</p>
                </div>
            </div>
    )
  }
}

export default ConfirmEmail;
