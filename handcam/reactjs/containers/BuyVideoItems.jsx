import * as _ from "lodash/core";
import * as React from "react";
import { connect } from 'react-redux';
import { history } from '../store/configureStore'
import { strings } from '../lib/strings';



const VideoGridItem = (props)=> {
  const {bemBlocks, result} = props
  const source:any = _.extend({}, result._source, result.highlight)
  var _a = props, bemBlock = _a.bemBlock, hasFilters = _a.hasFilters;
  return (
    <div className={bemBlocks.item().mix(bemBlocks.container("item"))} data-qa="hit">
      <a href="" onClick={(e)=>{e.preventDefault(); 
        history.push({'pathname':'/buy_video-camera_detail', 
          'state':{'id':result._id,
          'product_id':result._source.product_id,
          'label':result._source.label,
          'model':result._source.model,
          'seller':result._source.seller,
          'seller_pk':result._source.seller_pk,
          'seller_country':result._source.seller_country,
          'seller_city':result._source.seller_city,
          'seller_location':result._source.seller_location,
          'seller_phonenumber':result._source.seller_phonenumber,
          'condition':result._source.condition,
          'lens_description':result._source.lens_description,
          'comments':result._source.comments,
          'lens_photos':result._source.lens_photos,
          'lens_package':result._source.lens_package,
          'price':result._source.price,
          'currency':result._source.currency,
          'shutter_count':result._source.shutter_count,
          'actual_photos':result._source.actual_photos,
          'status':result._source.status,
          'warranty':result._source.warranty,
          'package':result._source.package,
          'image':result._source.image,
          'company':result._source.company,
          'megapixels':result._source.megapixels,
          'sensor_size':result._source.sensor_size,
          'sensor_type':result._source.sensor_type,
          'image_sensor_format':result._source.image_sensor_format,
          'lcd':result._source.lcd,
          'lcd_size':result._source.lcd_size,
          'lcd_resolution':result._source.lcd_resolution,
          'weight':result._source.weight,
          'iso':result._source.iso,
          'iso_low':result._source.iso_low,
          'iso_high':result._source.iso_high,
          'video':result._source.video,
          'frames_per_sec':result._source.frames_per_sec,
          'af_points':result._source.af_points,
          'aperture':result._source.aperture,
          'max_aperture':result._source.max_aperture,
          'mirrorless':result._source.mirrorless,
          'wifi':result._source.wifi,
          'gps':result._source.gps,
          'focal_length':result._source.focal_length,
          'focal_length_from':result._source.focal_length_from,
          'focal_length_to':result._source.focal_length_to,
          'waterproof':result._source.waterproof,
          'optical_zoom':result._source.optical_zoom,
          'digital_zoom':result._source.digital_zoom,
          'effective_resolution':result._source.effective_resolution,
          'dynamic_range':result._source.dynamic_range,
          'view_finder':result._source.view_finder,
          'lens_mount':result._source.lens_mount,
          'frame_rate':result._source.frame_rate,
          'effective_pixels':result._source.effective_pixels,
          'features':result._source.cam_features,
          'specifications':result._source.cam_specifications
        }})}}>
        <img data-qa="poster" className={bemBlocks.item("poster")} src={result._source.image} width="200" height="133"/>
        <div data-qa="title" className={bemBlocks.item("title")} dangerouslySetInnerHTML={{__html:source.label}}>
        </div>
      </a>
    </div>
  )
}

const VideoListItem = (props)=> {
  const direction = localStorage.getItem('direction')
  const {bemBlocks, result} = props
  const source:any = _.extend({}, result._source, result.highlight)
  var _a = props, bemBlock = _a.bemBlock, hasFilters = _a.hasFilters;

  var transCondition = ""

  if (result._source.condition=="Considered new - barely used"){
      transCondition = strings.considered_new_barely_used
  }else if (result._source.condition=="Brand new - not used"){
      transCondition = strings.brand_new
  }else if (result._source.condition=="Used for demo only"){
      transCondition = strings.used_for_demo_only
  }else if (result._source.condition=="Show signs of use but works perfectly"){
      transCondition = strings.show_sign_of_use_but_works_perfectly
  }else if (result._source.condition=="Shows some wear"){
      transCondition = strings.shows_some_wear
  }else if (result._source.condition=="Manufacturer defect"){
      transCondition = strings.manufacturer_defect
  }else if (result._source.condition=="Broken but still works fine"){
      transCondition = strings.broken_but_still_works_fine
  }else if (result._source.condition=="Broken and need some maintenance"){
      transCondition = strings.broken_and_need_some_maintenance
  }else {
      transCondition = strings.others_please_specify
  }

  return ( 
    <div className={bemBlocks.item().mix(bemBlocks.container("item"))} data-qa="hit">
      <div className={bemBlocks.item("poster")}>
        <a href="" onClick={(e)=>{e.preventDefault(); 
        history.push({'pathname':'/buy_video-camera_detail', 
          'state':{'id':result._id,
          'product_id':result._source.product_id,
          'label':result._source.label,
          'model':result._source.model,
          'seller':result._source.seller,
          'seller_pk':result._source.seller_pk,
          'seller_country':result._source.seller_country,
          'seller_city':result._source.seller_city,
          'seller_location':result._source.seller_location,
          'seller_phonenumber':result._source.seller_phonenumber,
          'condition':result._source.condition,
          'lens_description':result._source.lens_description,
          'comments':result._source.comments,
          'lens_photos':result._source.lens_photos,
          'lens_package':result._source.lens_package,
          'price':result._source.price,
          'currency':result._source.currency,
          'shutter_count':result._source.shutter_count,
          'actual_photos':result._source.actual_photos,
          'status':result._source.status,
          'warranty':result._source.warranty,
          'package':result._source.package,
          'image':result._source.image,
          'company':result._source.company,
          'megapixels':result._source.megapixels,
          'sensor_size':result._source.sensor_size,
          'sensor_type':result._source.sensor_type,
          'image_sensor_format':result._source.image_sensor_format,
          'lcd':result._source.lcd,
          'lcd_size':result._source.lcd_size,
          'lcd_resolution':result._source.lcd_resolution,
          'weight':result._source.weight,
          'iso':result._source.iso,
          'iso_low':result._source.iso_low,
          'iso_high':result._source.iso_high,
          'video':result._source.video,
          'frames_per_sec':result._source.frames_per_sec,
          'af_points':result._source.af_points,
          'aperture':result._source.aperture,
          'max_aperture':result._source.max_aperture,
          'mirrorless':result._source.mirrorless,
          'wifi':result._source.wifi,
          'gps':result._source.gps,
          'focal_length':result._source.focal_length,
          'focal_length_from':result._source.focal_length_from,
          'focal_length_to':result._source.focal_length_to,
          'waterproof':result._source.waterproof,
          'optical_zoom':result._source.optical_zoom,
          'digital_zoom':result._source.digital_zoom,
          'effective_resolution':result._source.effective_resolution,
          'dynamic_range':result._source.dynamic_range,
          'view_finder':result._source.view_finder,
          'lens_mount':result._source.lens_mount,
          'frame_rate':result._source.frame_rate,
          'effective_pixels':result._source.effective_pixels,
          'features':result._source.cam_features,
          'specifications':result._source.cam_specifications
        }})}}><img data-qa="poster" src={result._source.image} width="200" height="auto"/></a>
      </div>
      <div className={bemBlocks.item("details") + (direction == '"rtl"' ? ' righttoleft' : '')}>
        <a href="" onClick={(e)=>{e.preventDefault(); 
        history.push({'pathname':'/buy_video-camera_detail', 
          'state':{'id':result._id,
          'product_id':result._source.product_id,
          'label':result._source.label,
          'model':result._source.model,
          'seller':result._source.seller,
          'seller_location':result._source.seller_location,
          'seller_pk':result._source.seller_pk,
          'seller_country':result._source.seller_country,
          'seller_city':result._source.seller_city,
          'seller_phonenumber':result._source.seller_phonenumber,
          'condition':result._source.condition,
          'lens_description':result._source.lens_description,
          'comments':result._source.comments,
          'lens_photos':result._source.lens_photos,
          'lens_package':result._source.lens_package,
          'price':result._source.price,
          'currency':result._source.currency,
          'shutter_count':result._source.shutter_count,
          'actual_photos':result._source.actual_photos,
          'status':result._source.status,
          'warranty':result._source.warranty,
          'package':result._source.package,
          'image':result._source.image,
          'company':result._source.company,
          'megapixels':result._source.megapixels,
          'sensor_size':result._source.sensor_size,
          'sensor_type':result._source.sensor_type,
          'image_sensor_format':result._source.image_sensor_format,
          'lcd':result._source.lcd,
          'lcd_size':result._source.lcd_size,
          'lcd_resolution':result._source.lcd_resolution,
          'weight':result._source.weight,
          'iso':result._source.iso,
          'iso_low':result._source.iso_low,
          'iso_high':result._source.iso_high,
          'video':result._source.video,
          'frames_per_sec':result._source.frames_per_sec,
          'af_points':result._source.af_points,
          'aperture':result._source.aperture,
          'max_aperture':result._source.max_aperture,
          'mirrorless':result._source.mirrorless,
          'wifi':result._source.wifi,
          'gps':result._source.gps,
          'focal_length':result._source.focal_length,
          'focal_length_from':result._source.focal_length_from,
          'focal_length_to':result._source.focal_length_to,
          'waterproof':result._source.waterproof,
          'optical_zoom':result._source.optical_zoom,
          'digital_zoom':result._source.digital_zoom,
          'effective_resolution':result._source.effective_resolution,
          'dynamic_range':result._source.dynamic_range,
          'view_finder':result._source.view_finder,
          'lens_mount':result._source.lens_mount,
          'frame_rate':result._source.frame_rate,
          'effective_pixels':result._source.effective_pixels,
          'features':result._source.cam_features,
          'specifications':result._source.cam_specifications
        }})}}><h2 className={bemBlocks.item("title")} dangerouslySetInnerHTML={{__html:source.model}}></h2></a>
        <hr className="bigHr" />
        <h4 className={bemBlocks.item("subtitle")} dangerouslySetInnerHTML={{__html:strings.top_highlights}}></h4>
        {source.video ? <div className={bemBlocks.item("text")} dangerouslySetInnerHTML={{__html:'- '+source.video +' '+'Video Resolution'}}></div> : null}
        {source.sensor_size ? <div className={bemBlocks.item("text")} dangerouslySetInnerHTML={{__html:'- '+source.sensor_size +' '+ 'Sensor size.'}}></div> :null}
        {source.effective_resolution ? <div className={bemBlocks.item("text")} dangerouslySetInnerHTML={{__html:'- '+source.effective_resolution +' '+ 'Sensor type.'}}></div> : null}
        {source.lcd ? <div className={bemBlocks.item("text")} dangerouslySetInnerHTML={{__html:source.lcd + ' Optical Zoom.'}}></div> : null}
        <hr className="bigHr" />
        <div>
          <span className={bemBlocks.item("condition")} dangerouslySetInnerHTML={{__html:strings.item_condition+': '}}></span>
          <span className={bemBlocks.item("conditiontext")} dangerouslySetInnerHTML={{__html:transCondition}}></span>
        </div>
      </div>
      <div className={bemBlocks.item("details") + (direction == '"rtl"' ? ' righttoleft' : '')}>
        <div className={bemBlocks.item("detailprice price-border")}>
          <div className="row">
            <div className="col-md-4">
              <span className={bemBlocks.item("sem")} dangerouslySetInnerHTML={{__html:strings.price +':'}}></span>
            </div>  
            <div className={direction == '"rtl"' ? 'zeropadleftright' : ''}>
              <span className={bemBlocks.item("seme") + (direction == '"rtl"' ? ' span-left' : '')} dangerouslySetInnerHTML={{__html:source.currency + " " }}></span>
              <span className={bemBlocks.item("seme") + (direction == '"rtl"' ? ' span-left' : '')} dangerouslySetInnerHTML={{__html:source.price + " " }}></span>
            </div>
          </div>
          <hr />
          <div>
              <table width="100%">
                <tr>
                  <td width="20%">
                      <span className="black-font14px" dangerouslySetInnerHTML={{__html:strings.seller +':'}}></span>
                  </td>
                  <td>
                      <span className={bemBlocks.item("condition")} dangerouslySetInnerHTML={{__html:source.seller}}></span>
                  </td>
                </tr>
                <tr>
                  <td width="20%">
                      <span className="black-font14px" dangerouslySetInnerHTML={{__html:strings.location +':'}}></span>
                  </td>
                  <td>
                      <span className={bemBlocks.item("condition")} dangerouslySetInnerHTML={{__html:source.seller_location}}></span>
                  </td>
                </tr>
              </table>
          </div>
        </div>
      </div>
    </div>
  )
}


export {VideoGridItem, VideoListItem}