import React from 'react'
import { connect } from 'react-redux'
import {Link} from 'react-router-dom'
import { sellDroneActions } from '../actions';
import ImageUploader from 'react-images-upload';
import SellForm from '../components/SellForm'
import { history } from '../store/configureStore'
import OverlayLoader from 'react-loading-indicator-overlay/lib/OverlayLoader'
import { strings } from '../lib/strings';



import Dropzone from 'react-dropzone';


const camStyle = {
      width: 400,
    height: 267
    };



class DroneDetails extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            brand: "",
            model: "",
            comment: "",
            price: "",
            condition: "",
            status: 'Active',
            warranty: "",
            drone_type: this.props.location.state.drone_type,
            camera: "",
            video_resolution:"",
            megapixels:"",
            operating_time:"",
            control_range:"",
            control_range_unit:"Feet",
            actual_photos: [],
            submitted: false,
            priceError: false,
            brandError: false,
            modelError: false,
            conditionError: false,
            warrantyError: false,
            actualPhotoError: false,
            IsNanError: false,
            drone_typeError: false,
            cameraError: false,
            operating_timeError: false,
            control_rangeError: false,
            video_resolutionError: false,
            megapixelsError: false,
            spinner: false,
            showSizeError: false,
        }

        this.onDrop = this.onDrop.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeEnglishonly = this.handleChangeEnglishonly.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSelectWarranty = this.handleSelectWarranty.bind(this)
    }

    componentWillMount(){
        let token = localStorage.getItem('token')
        if (!token){
          history.push('/login_register')
        }   
    }

    componentDidMount(){
        window.scrollTo(0, 0)
    }

    isInt(n){
        return Number(n) === n && n % 1 === 0;
    }

    isFloat(n){
        return Number(n) === n && n % 1 !== 0;
    }

    handleChange(e) {
        const { name, value } = e.target;

        this.setState({ [name]: value });
    
    }

    handleChangeEnglishonly(e) {
        const { name, value } = e.target;
        let regEx = /^[ A-Za-z_0-9@./#&+-]*$/;

        if (regEx.test(value)) {
            this.setState({ [name]: value });
        }
    }

    handleSelectWarranty(e) {
        this.setState({warranty: e.target.value});
        
    }

    handleSelectCondition(e) {
        this.setState({condition: e.target.value});
        
    }

    handleSelectDroneType(e) {
        this.setState({drone_type: e.target.value});
        
    }

    handleSelectCamera(e) {
        this.setState({camera: e.target.value});
        
    }

    handleSelectOperatingTime(e) {
        this.setState({operating_time: e.target.value});
        
    }

    handleSelectControlRange(e) {
        this.setState({control_range: e.target.value});
        
    }

    handleSelectControlRangeUnit(e) {
        this.setState({control_range_unit: e.target.value});
        
    }

    handleSelectVideoResolution(e) {
        this.setState({video_resolution: e.target.value});
        
    }


    onDrop(picture) {
        
        this.setState({
            actual_photos: picture,
            actualPhotoError: false,
            showSizeError: false
        });
    }


    clickStillHandle = (e) => {
        e.preventDefault();
        history.push('/sell_brand')
    }

    clickVideoHandle = (e) => {
        e.preventDefault();
        history.push('/sell_video_brand')
    }

    clickLensHandle = (e) => {
        e.preventDefault();
        history.push('/sell_lens_brand')
    }

    clickLightingHandle = (e) => {
        e.preventDefault();
        history.push('/sell_lighting')
    }

    clickAccessoriesHandle = (e) => {
        e.preventDefault();
        history.push('/sell_accessories')
    }

    clickDroneHandle = (e) => {
        e.preventDefault();
        history.push('/sell_drone')
    }



    handleSubmit = (e) => {
        e.preventDefault()
        const { item, price, brand, model, category, condition, warranty, comment, actual_photos, drone_type, camera, megapixels, video_resolution, control_range, control_range_unit, operating_time, submitted } = this.state
        const { dispatch } = this.props;

        let body = new FormData()
        body.append('item', item)
        body.append('price', price)
        body.append('brand', brand)
        body.append('model', model)
        body.append('category', category)
        body.append('condition', condition)
        body.append('warranty', warranty) 
        body.append('comment', comment)
        body.append('drone_type', drone_type)
        body.append('camera', camera)
        body.append('video_resolution', video_resolution)
        if (control_range){
            body.append('control_range', control_range.toString() +" "+ control_range_unit)
        }
        body.append('operating_time', operating_time)
        body.append('megapixels', megapixels)
        body.append('status', 'Active')
        Object.keys(actual_photos).forEach(( key, i ) => {
            body.append('actual_photos', actual_photos[key]);
        });

        if (price && brand && condition && actual_photos.length > 0 && drone_type && camera){ 
            for (var pair of body.entries()){
                
            }

            if (isNaN(brand)){
                this.setState({ spinner: true})
                dispatch(sellDroneActions.sellDrone(body)).then(() => {
                    this.setState({ showSizeError: true })
                });
                
            }else{
                this.setState({ IsNanError: true})
            }
        }

        if (price == ""){
            this.setState({ priceError: true})
        }else{
            this.setState({ priceError: false})
        }

        if (brand == ""){
            this.setState({ brandError: true})
        }else{
            this.setState({ brandError: false})
        }

        if (condition == ""){
            this.setState({ conditionError: true})
        }else{
            this.setState({ conditionError: false})
        }

        if (warranty == ""){
            this.setState({ warrantyError: true})
        }else{
            this.setState({ warrantyError: false})
        }

        if (actual_photos.length == 0){
            this.setState({ actualPhotoError: true})
        }else{
            this.setState({ actualPhotoError: false})
        }

        if (camera == ""){
            this.setState({ cameraError: true})
        }else{
            this.setState({ cameraError: false})
        }

        if (video_resolution == ""){
            //this.setState({ video_resolutionError: true})
        }else{
            this.setState({ video_resolutionError: false})
        }

        /*if (megapixels == ""){
            this.setState({ megapixelsError: true})
        }else{
            this.setState({ megapixelsError: false})
        }

        if (control_range == ""){
            this.setState({ control_rangeError: true})
        }else{
            this.setState({ control_rangeError: false})
        }

        if (operating_time == ""){
            this.setState({ operating_timeError: true})
        }else{
            this.setState({ operating_timeError: false})
        }*/

    }

    renderDroneType(cat) {

        if (cat=="Quadcopter"){
            return (strings.quadcopter)
        }else if(cat=="Hexacopter"){
            return (strings.hexacopter)
        }else if(cat=="Octacopter"){
            return (strings.octacopter)
        }else if(cat=="Helicopter"){
            return (strings.helicopter)
        }else{
            return (strings.airplane)
        } 
    }

    renderPhotoSizeError(){
        
        if (this.state.showSizeError){
            if (Object.keys(this.props.drone_sell).length > 0) {
                   
                return(
                    <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_decent_photo_size}</div>
                )
            }
        }
    }


    renderSellForm(){
        let token = localStorage.getItem('token')
        const { price, condition, warranty, actual_photos, submitted, file, brand, model, drone_type, megapixels, camera, video_resolution, control_range, control_range_unit ,operating_time, comment} = this.state;
        if(!token){
            return <p>{strings.please_login_to_sell}</p>
        }else{
           return   <form name="form" onSubmit={this.handleSubmit}>
                        <div className={'form-group' + (this.state.priceError ? ' has-error' : '') + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="price">{strings.price}</label><span className="spannote required">({strings.should_be_in_number})</span>
                            <input type="number" inputMode="numeric" className="form-control form-price" name="price" value={price} onChange={this.handleChange} />
                            {this.state.priceError ?
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                                : null
                            }
                        </div>
                        
                        <div className={'form-group' + (this.state.submitted && !this.state.brand ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="brand" className="required">{strings.brand}</label>{this.props.rtlconv == "rtl" ? <span className="spannote">({strings.english_only})</span> : null}
                            <input type="text" className="form-control form-brand" name="brand" value={brand} onChange={this.handleChangeEnglishonly} />
                            {this.state.brandError ?
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                                : null
                            }
                            {this.state.IsNanError ?
                                <div className="errorMessage">{strings.should_not_be_a_number}</div>
                                : null
                            }
                        </div>

                        <div className={'form-group' + (this.state.submitted && !this.state.model ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="brand">{strings.model_name}</label>{this.props.rtlconv == "rtl" ? <span className="spannote">({strings.english_only})</span> : null}
                            <input type="text" className="form-control form-model" name="model" value={model} onChange={this.handleChangeEnglishonly} />
                            
                            {this.state.IsNanError ?
                                <div className="errorMessage">{strings.should_not_be_a_number}</div>
                                : null
                            }
                        </div>

                        {/*<div className={'form-group' + (this.state.submitted && !this.state.drone_type ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="drone_type">{strings.drone_type}</label>
                            <select className="form-control form-brand" 
                                name="drone_type" 
                                
                                onChange={(e) => this.handleSelectDroneType(e)}
                            >
                                <option value="">{strings.select_drone_type}</option>
                                <option value="Quadcopter">{strings.quadcopter}</option>
                                <option value="Hexacopter">{strings.hexacopter}</option>
                                <option value="Octacopter">{strings.octacopter}</option>
                                <option value="Helicopter">{strings.helicopter}</option>
                                <option value="Airplane">{strings.airplane}</option>
                            </select>
                            {this.state.drone_typeError ?
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                                : null
                            }
                        </div>*/}
                        
                        <div className={'form-group' + (this.state.submitted && !this.state.camera ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="camera"  className="required">{strings.camera}</label>
                            <select className="form-control form-brand" 
                                name="camera" 
                                
                                onChange={(e) => this.handleSelectCamera(e)}
                            >
                                <option value="">{strings.select_camera}</option>
                                <option value="Camera Included">{strings.camera_included}</option>
                                <option value="Camera not Included">{strings.camera_not_included}</option>
                            </select>
                            {this.state.cameraError ?
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                                : null
                            }
                        </div>

                        <div className={'form-group' + (this.state.submitted && !this.state.video_resolution ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="video_resolution">{strings.camera_video_resolution}</label>
                            <select className="form-control form-brand" 
                                name="video_resolution" 
                                
                                onChange={(e) => this.handleSelectVideoResolution(e)}
                            >
                                <option value="">{strings.select_video_resolution}</option>
                                <option value="4K">4K</option>
                                <option value="1080p">1080p</option>
                                <option value="720p">720p</option>
                                <option value="420p">420p</option>
                            </select>
                            {this.state.video_resolutionError ?
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                                : null
                            }
                        </div>

                        <div className={'form-group' + (this.state.submitted && !this.state.megapixels ? ' has-error' : '') + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="price">{strings.camera_still_resolution}</label>
                            <input type="number" inputMode="numeric" className="form-control form-price" name="megapixels" value={megapixels} onChange={this.handleChange} />
                        </div>

                        <div className={'form-group' + (this.state.submitted && !this.state.operating_time ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="operating_time">{strings.operating_time}</label>
                            <select className="form-control form-brand" 
                                name="operating_time" 
                                
                                onChange={(e) => this.handleSelectOperatingTime(e)}
                            >
                                <option value="">{strings.select_operating_time}</option>
                                <option value="less than 10 min">less than 10 min</option>
                                <option value="Up to 10 min">Up to 10 min</option>
                                <option value="20 min">20 min</option>
                                <option value="30 min">30 min</option>
                                <option value="40 min">40 min</option>
                                <option value="50 min">50 min</option>
                                <option value="60 min">60 min</option>
                                <option value="70 min">70 min</option>
                                <option value="80 min">80 min</option>
                                <option value="90 min">90 min</option>
                                <option value="100 min">100 min</option>
                                <option value="110 min">110 min</option>
                                <option value="120 min">120 min</option>
                                <option value="130 min">130 min</option>
                                <option value="140 min">140 min</option>
                                <option value="150 min">150 min</option>
                                <option value="160 min">160 min</option>
                                <option value="170 min">170 min</option>
                                <option value="180 min">180 min</option>
                                <option value="190 min">190 min</option>
                                <option value="200 min">200 min</option>

                            </select>
                            
                        </div>

                        <div className={'form-group' + (this.state.submitted && !this.state.control_range ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="control_range">{strings.control_range}</label>
                            <div className="row">
                                
                                    <div className="col-md-4">
                                        <select className="form-control" 
                                            name="control_range" 
                                            
                                            onChange={(e) => this.handleSelectControlRange(e)}
                                        >
                                            <option value="">{strings.select_control_range}</option>
                                            <option value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="30">30</option>
                                            <option value="40">40</option>
                                            <option value="50">50</option>
                                            <option value="60">60</option>
                                            <option value="70">70</option>
                                            <option value="80">80</option>
                                            <option value="90">90</option>
                                            <option value="100">100</option>
                                            <option value="200">200</option>
                                            <option value="300">300</option>
                                            <option value="400">400</option>
                                            <option value="500">500</option>
                                            <option value="600">600</option>
                                            <option value="700">700</option>
                                            <option value="800">800</option>
                                            <option value="900">900</option>
                                            <option value="1000">1000</option>
                                            <option value="2000">2000</option>
                                            <option value="3000">3000</option>
                                            <option value="4000">4000</option>
                                            <option value="5000">5000</option>
                                            <option value="6000">6000</option>
                                            <option value="7000">7000</option>
                                            <option value="8000">8000</option>
                                            <option value="9000">9000</option>
                                            <option value="10000">10000</option>
                                            <option value="20000">20000</option>
                                            <option value="30000">30000</option>
                                            <option value="40000">40000</option>
                                            <option value="50000">50000</option>
                                            <option value="60000">60000</option>
                                            <option value="70000">70000</option>
                                            <option value="80000">80000</option>
                                            <option value="90000">90000</option>
                                            <option value="100000">100000</option>
                                            
                                        </select>
                                    </div>
                                    <div className="col-md-2">
                                        <select className="form-control form-brand" 
                                            name="control_range_unit" 
                                            value={this.state.control_range_unit}
                                            onChange={(e) => this.handleSelectControlRangeUnit(e)}
                                        >
                                            <option value="Feet">{strings.feet}</option>
                                            <option value="Meter">{strings.meter}</option>
                                        </select>
                                    </div>
                                
                            </div>
                            
                        </div>

                        <div className={'form-group' + (this.state.submitted && !this.state.warranty ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="warranty" className="required">{strings.warranty}</label>
                            <select className="form-control form-warranty" 
                                name="warranty" 
                                
                                onChange={(e) => this.handleSelectWarranty(e)}
                            >
                                <option value="">{strings.sellect_warranty}</option>
                                <option value="No Warranty">{strings.no_warranty}</option>
                                <option value="One (1) Week">{strings.one_week}</option>
                                <option value="Two (2) Weeks">{strings.two_weeks}</option>
                                <option value="One (1) Month">{strings.one_month}</option>
                            </select>
                            {this.state.warrantyError ?
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                                : null
                            }
                        </div>

                        <div className={'form-group' + (this.state.submitted && !this.state.condition ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="condition" className="required">{strings.condition}</label>
                            <select className="form-control form-condition" 
                                name="condition" 
                                
                                onChange={(e) => this.handleSelectCondition(e)}
                            >
                                <option value="">{strings.select_conditions}</option>
                                <option value="Considered new - barely used">{strings.considered_new_barely_used}</option>
                                <option value="Brand new - not used">{strings.brand_new_not_used}</option>
                                <option value="Used for demo only">{strings.used_for_demo_only}</option>
                                <option value="Show signs of use but works perfectly">{strings.show_sign_of_use_but_works_perfectly}</option>
                                <option value="Shows some wear">{strings.shows_some_wear}</option>
                                <option value="Manufacturer Defect">{strings.manufacturer_defect}</option>
                                <option value="Broken but still works fine">{strings.broken_but_still_works_fine}</option>
                                <option value="Broken and need some maintenance">{strings.broken_and_need_some_maintenance}</option>
                                <option value="Other - please specify in the comment box below">{strings.others_please_specify}</option>
                            </select>
                            {this.state.conditionError ?
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                                : null
                            }
                        </div>

                        <div className={'form-group' + (this.state.submitted && !this.state.comment ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="comment">{strings.comments}</label>
                            <textarea className="form-control replyBox" name="comment" value={comment} onChange={this.handleChange} />
                        </div>
                        

                        <ImageUploader
                            withIcon={true}
                            buttonText={strings.choose_images}
                            onChange={this.onDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                            maxFileSize={5242880}
                            buttonType='button'
                            withPreview={true}/>

                        {this.state.actualPhotoError ? <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_accessories_photo}</div> : null}    

                        
                        { this.state.spinner ?
                        
                            <OverlayLoader 
                              color={'blue'} // default is white
                              loader="ScaleLoader" // check below for more loaders
                              text={strings.uploading_your_item_please_wait}
                              active={this.state.spinner} 
                              backgroundColor={'white'} // default is black
                              opacity=".4" // default is .9  
                            >
                                <div className="spinner">
                                </div>
                            </OverlayLoader>
                        
                        : null}
                        
                        <div className="form-group text-center">
                            <button className="btn btn-lg btn-primary">{strings.submit}</button>
                        </div>

                    </form> 

        }
    }


    

  render() {
        const { state } = this.props.location
        
        return (
            <div>
                <ul className="breadcrumb">
                    <li><Link to="/">{strings.home}</Link></li>
                    <li><Link to="/sell">{strings.sell}</Link></li>
                    <li><Link to="/sell_drone">{strings.drone}</Link></li>
                    <li>{strings.sell_drone_form}</li>
                </ul>
                <div className=" bg-company-orange">
                    <ul className="d-flex mx-auto justify-content-center nomarge zeroleftpadding">
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickStillHandle}><span>{strings.photography_cameras}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickLensHandle}><span>{strings.lenses}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickVideoHandle}><span>{strings.video_cameras}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickLightingHandle}><span>{strings.lighting}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickAccessoriesHandle}><span>{strings.accessories}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4 active">
                            <a className="awhite" href="" onClick={this.clickDroneHandle}><span>{strings.drone}</span></a>
                        </li>
                    </ul>
                </div>
                
                <div className="card m-2">
                    
                    <div className={"card-header" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    
                        <h3 className="mb-0" >{strings.drone} - {this.renderDroneType(this.state.drone_type)}</h3>
                     
                    </div>

                    <div className="card-body">
                    
                        <div className="form-container">

                            <div className="form-width">
                                
                                    <p className="text-center">{strings.please_fill_up_the_form}</p>
                                                    
                                          { this.renderSellForm() }
                                    
                            </div>
                           
                        </div>
                    
                    </div>
                    
                </div>
                
                <br />
                <br />
                <br />
                <br />
                <br />

            </div>
        )
  }
}

function mapStateToProps(state){
    
    return {
        rtlconv: state.rtlconv,
        drone_sell: state.dronesell
    };
}

export default connect(mapStateToProps)(DroneDetails)
