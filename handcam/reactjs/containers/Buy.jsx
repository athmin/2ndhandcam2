import React from 'react'
import { history } from '../store/configureStore'
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'
import { getMaintenanceAction } from '../actions';
let CONFIG = require('../lib/config.json')
import { strings } from '../lib/strings';

const images_url = CONFIG.server_url + "/static/media_cdn"

const camStyle = {
	  width: 200,
    height: 150
};

class Buy extends React.Component {

	componentWillMount(){
		const { dispatch } = this.props;
        if (!this.props.maintenanceMode.hasOwnProperty('mode')){
	      	dispatch(getMaintenanceAction.getMode()).then( () => {
	            if (this.props.maintenanceMode){
	                if (this.props.maintenanceMode.mode.status){
	                    history.push('/maintenance')
	                }
	            } 
	        })
	    }else{
	      	if (this.props.maintenanceMode.mode.status){
	            history.push('/maintenance')
	         }
	    }
    }

	clickStillHandle(){
		history.push('/buy_item')
	}

	clickVideoHandle(){
		history.push('/buy_video_cameras')
	}

	clickLensHandle(){
		history.push('/buy_lens')
	}

	clickLightingHandle(){
		history.push('/buy_lighting')
	}

	clickAccessoriesHandle(){
		history.push('/buy_accessories')
	}

	clickDroneHandle(){
		history.push('/buy_drones')
	}

  render() {
    return (
    	<div>
	    	<ul className="breadcrumb">
				  <li><Link to="/">{strings.home}</Link></li>
				  <li>{strings.buy}</li>
			</ul>
			<div className="card m-2">
				<h2 className="text-center card-title">{strings.what_item_would_you_like_to_buy}</h2>
				<div className="row category" dir="ltr">
					<div className={"row lighting-category" + (this.props.rtlconv == "rtl" ? ' center_by_margin' : '')}>
						<div className="col text-center">
							<a href="#" onClick={this.clickStillHandle}><img src={images_url + "/images/EOS_5d_mark_4.png"} style={camStyle} /></a><br />
							<button className="btn btn-warning" onClick={this.clickStillHandle}>{strings.photography_cameras}</button>
						</div>
						<div className="col text-center">
							<a href="#" onClick={this.clickLensHandle}><img src={images_url + "/images/lens.png"} style={camStyle} /></a><br />
							<button className="btn btn-warning" onClick={this.clickLensHandle}>{strings.lenses}</button>
						</div>
						<div className="col text-center">
							<a href="#" onClick={this.clickVideoHandle}><img src={images_url + "/images/video.png"} style={camStyle} /></a><br />
							<button className="btn btn-warning" onClick={this.clickVideoHandle}>{strings.video_cameras}</button>
						</div>
						<div className="col text-center">
							<a href="#" onClick={this.clickLightingHandle}><img src={images_url + "/images/lights.png"} style={camStyle} /></a><br />
							<button className="btn btn-warning" onClick={this.clickLightingHandle}>{strings.lighting}</button>
						</div>
					</div>
					<div className={"row lighting-category" + (this.props.rtlconv == "rtl" ? ' center_by_margin' : '')}>
						<div className="col text-center">
							<a href="#" onClick={this.clickAccessoriesHandle}><img src={images_url + "/images/accessories.png"} style={camStyle} /></a><br />
							<button className="btn btn-warning" onClick={this.clickAccessoriesHandle}>{strings.accessories}</button>
						</div>
						<div className="col text-center">
							<a href="#" onClick={this.clickDroneHandle} ><img src={images_url + "/images/drone.png"} style={camStyle} /></a><br />
							<button className="btn btn-warning" onClick={this.clickDroneHandle}>{strings.drone}</button>
						</div>
					</div>
				</div>
				<br />
				<br />
				<br />
				<br />
			</div>
		</div>
    )
  }
}

function mapStatetoProps(state){
    return {
        userlocation: state.userLocationReducer,
        rtlconv: state.rtlconv,
        maintenanceMode: state.maintenanceMode
    }
}

export default connect(mapStatetoProps)(Buy);