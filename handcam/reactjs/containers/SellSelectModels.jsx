import React from 'react';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'
import { history } from '../store/configureStore'
import LoadingIndicator from '../components/LoadingIndicator';
import { getBrandModelsAction } from '../actions';
import { strings } from '../lib/strings';
let CONFIG = require('../lib/config.json')

const images_url = CONFIG.server_url + "/static/media_cdn"
const divStyle = {
    marginTop: '150px'
  };

const camStyle = {
    width: 175,
    height: "auto"
    };
const logoStyle = {
    width: 150,
    height: 50,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
};


class SelectCategory extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          brandmodels: [],
          models: [],
          fetching: false,
          brand: this.props.location.state.brand,
          selected: 'all',
          searchKeyword: "",
        }
    }

    componentWillMount(){
        const { brand } = this.state
        const { dispatch } = this.props;
        let token = localStorage.getItem('token')
        /*if (!token){
          history.push('/login')
        }*/
        
        dispatch(getBrandModelsAction.selectCategory(brand)).then( () => {
            this.setState({fetching: true})
            this.setState({ brandmodels: this.props.camera_models })
            this.allCameras();
            //this.setState({ models: this.props.camera_models })

           
        })
        
    }

    moveToSellForm() {

    }

    clickStillHandle = (e) => {
        e.preventDefault();
        history.push('/sell_brand')
      }

    clickVideoHandle = (e) => {
        e.preventDefault();
        history.push('/sell_video_brand')
    }

    clickLensHandle = (e) => {
        e.preventDefault();
        history.push('/sell_lens_brand')
    }

    clickLightingHandle(){
        history.push('/sell_lighting')
    }

    clickAccessoriesHandle = (e) => {
        e.preventDefault();
        history.push('/sell_accessories')
    }

    clickDroneHandle = (e) => {
        e.preventDefault();
        history.push('/sell_drone')
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        
    }

    setSelected(category) {
        this.setState({selected  : category})
    }

    isActive(value){
        return 'col choose-category '+((value===this.state.selected) ?'selected-cat':'default');
    }

    renderAllCameraButton(category) {
        if (category.length>1){
            return (
                    <div id='choose-category' className={this.isActive('all')}>
                        <span onClick={(e) =>{ this.allCameras(); this.setSelected('all') }}>{strings.all_cameras}</span>
                    </div>    
            )
        }else{
            return <p></p>
        }
    }

    renderCategory() {
        const { brandmodels } = this.state
        let category = []

        for (let i = 0; i < brandmodels.length; i++){
            if (category.indexOf(brandmodels[i].cam_type.camera_type) === -1 ) category.push(brandmodels[i].cam_type.camera_type);     
        }
        
        return (
            <div className="row">
                { this.renderAllCameraButton(category)}
                
                { category.map((cat) => {
                    
                    
                    return (
                        <div id='choose-category' className={this.isActive(cat)}>
                            <span key={cat} onClick={(e) =>{ this.filterModel(cat); this.setSelected(cat) }}>{cat}</span>
                        </div>    
                    )
                        
                })}
                
            </div>    
        )

    }

    allCameras() {
        const { brandmodels } = this.state
        //this.setState({models: brandmodels})
        let newmodels = []
        
        for (let i = 0; i < brandmodels.length; i++){
            newmodels.push({'id' : brandmodels[i].id, 'model' : brandmodels[i].model, 'image' : brandmodels[i].image, 'image_thumbnail' : brandmodels[i].image_thumbnail, 'type' : brandmodels[i].cam_type.camera_type, 'kind' : brandmodels[i].cam_type.brand.company});     
        }

        if (newmodels){
            
            this.setState({models: newmodels})
        }
    }

    filterModel(camCategory) {
        
        const { brandmodels } = this.state
        let newmodels = []
        
        for (let i = 0; i < brandmodels.length; i++){
            if ( brandmodels[i].cam_type.camera_type == camCategory ) newmodels.push({'id' : brandmodels[i].id, 'model' : brandmodels[i].model, 'image' : brandmodels[i].image, 'image_thumbnail' : brandmodels[i].image_thumbnail, 'type' : brandmodels[i].cam_type.camera_type, 'kind' : brandmodels[i].cam_type.brand.company});     
        }

        if (newmodels){
            
            this.setState({models: newmodels})
        }
    }

    searchModel(event) {
        const { brandmodels } = this.state
        let searchResult = brandmodels
        let newmodels = []

        searchResult = searchResult.filter(function(item){
            return item.model.toString().toLowerCase().search(
                event.target.value.toLowerCase()) !== -1;
        });

        for (let i = 0; i < searchResult.length; i++){
            newmodels.push({'id' : searchResult[i].id, 'model' : searchResult[i].model, 'image' : searchResult[i].image, 'image_thumbnail' : searchResult[i].image_thumbnail, 'type' : searchResult[i].cam_type.camera_type, 'kind' : searchResult[i].cam_type.brand.company});     
        }

        this.setState({models: newmodels})
    }

    renderModels() {
        
        const { models } = this.state
        
        
        if (models) {
            
            return (
                <div className="sk-hits-grid m-3">
                    { models.map((model) => {
                        return (
                            <div id="monolight-brand" className="sk-hits-grid-hit sk-hits-grid__item">
                                <a href="" key={model.id} onClick={(e)=>{e.preventDefault(); 
                                    history.push({'pathname':'/complete_form',     
                                        'state' : {
                                            'id' : model.id,
                                            'model' : model.model,
                                            'brand' : model.kind,
                                            'type' : model.type,
                                            'image' : model.image,
                                        }
                                    })
                                }}>
                                    <img src={ model.image_thumbnail } style={camStyle} />
                                    <div className="sk-hits-grid-hit__title text-center">{ model.model }</div>
                                </a>
                            </div>    
                        )    
                    })}
                </div>
            )
        }else {
            return <h4>Choose camera category</h4>
        }

    }

    render() {
        const { brandmodels, searchKeyword } = this.state
        if (brandmodels.length>0){
        return (
            <div>
                <ul className="breadcrumb">
                    <li><Link to="/">{strings.home}</Link></li>
                    <li><Link to="/sell">{strings.sell}</Link></li>
                    <li><Link to="/sell_brand">{strings.photography_cameras}</Link></li>
                    <li>{brandmodels[0].cam_type.brand.company}</li>
                </ul>
                <div className=" bg-company-orange">
                      <ul className="d-flex mx-auto justify-content-center align-self-center zeroleftpadding">
                        <li className="list-inline-item ml-4 mr-4 active">
                          <a className="awhite" href="" onClick={this.clickStillHandle}><span>{strings.photography_cameras}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                          <a className="awhite" href="" onClick={this.clickLensHandle}><span>{strings.lenses}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                          <a className="awhite" href="" onClick={this.clickVideoHandle}><span>{strings.video_cameras}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                          <a className="awhite" href="" onClick={this.clickLightingHandle}><span>{strings.lighting}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                          <a className="awhite" href="" onClick={this.clickAccessoriesHandle}><span>{strings.accessories}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickDroneHandle}><span>{strings.drone}</span></a>
                        </li>
                      </ul>
                </div>
                <div className='container'>
                    <div className="text-center">
                        <img className="brandimgheight" src={images_url + "/images/logo-"+brandmodels[0].cam_type.brand.company+".png"} style={{logoStyle}} />
                    </div>
                    <div>
                        { this.renderCategory() }
                    </div>
                    <hr/>
                    <div className={"filter-list" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                        <form onSubmit={e => { e.preventDefault(); }}>
                            <fieldset className="form-group">
                                <input type="text" className="form-control form-control-lg searchModel" placeholder={strings.search_model} onChange={(e) =>{ this.searchModel(e);}}/>
                                <span className="spannote spanhowto" onClick={(e)=>{e.preventDefault(); history.push(
                                    {'pathname':'/new_model_notification',     
                                    'state' : {
                                        'subject' : "New Model for Still Camera"
                                    }
                                })}}>
                                    ({strings.please_contact_us_camera})
                                </span>
                            </fieldset>
                        </form>
                    </div>
                    <div>
                        { this.renderModels() }
                    </div>  
                </div>
                <br />
                <br />
                <br />
                <br />
          </div>
            
        );
        }else{
            return (
                <div className='container'>
                    <div className="loading">{strings.loading_camera_models}</div>
                    <LoadingIndicator />
                </div>
            )
        }
    }
}

function mapStateToProps(state){
    
    return {
        camera_models: state.camera_models,
        rtlconv: state.rtlconv
    };
}


export default connect(mapStateToProps)(SelectCategory)