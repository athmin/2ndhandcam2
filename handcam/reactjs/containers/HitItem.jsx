import * as _ from "lodash/core";
import * as React from "react";
import { connect } from 'react-redux';
import { history } from '../store/configureStore'



const CameraGridItem = (props)=> {
  const {bemBlocks, result } = props
  const source:any = _.extend({}, result._source, result.highlight)

  return (
    <div className={bemBlocks.item().mix(bemBlocks.container("item"))} data-qa="hit">
      <a href="" onClick={(e)=>{e.preventDefault(); 
        history.push({'pathname':'/camera_detail', 
          'state':{'id':result._source.id,
          'model':result._source.model,
          'image':result._source.image,
          'company':result._source.company,
          'megapixels':result._source.megapixels,
          'sensor_size':result._source.sensor_size,
          'lcd_size':result._source.lcd_size,
          'lcd_resolution':result._source.lcd_resolution,
          'weight':result._source.weight,
          'iso_low':result._source.iso_low,
          'iso_high':result._source.iso_high,
          'video':result._source.video,
          'frames_per_sec':result._source.frames_per_sec,
          'af_points':result._source.af_points,
          'aperture':result._source.aperture,
          'max_aperture':result._source.max_aperture,
          'focal_length_from':result._source.focal_length_from,
          'focal_length_to':result._source.focal_length_to,
          'mirrorless':result._source.mirrorless,
          'wifi':result._source.wifi,
          'gps':result._source.gps,
          'optical_zoom':result._source.optical_zoom,
          'digital_zoom':result._source.digital_zoom,
          'waterproof':result._source.waterproof,
          'effective_resolution':result._source.effective_resolution,
          'feature':result._source.camera_feature,
          'specifications':result._source.specifications
        }})}}>
        <img data-qa="poster" className={bemBlocks.item("poster")} src={'/static/media_cdn/'+result._source.image} width="200" height="133"/>
        <div data-qa="title" className={bemBlocks.item("title")} dangerouslySetInnerHTML={{__html:source.company+' '+source.model}}>
        </div>
      </a>
    </div>
  )
}

const CameraListItem = (props)=> {
  const {bemBlocks, result} = props
  const source:any = _.extend({}, result._source, result.highlight)

  return (
    <div className={bemBlocks.item().mix(bemBlocks.container("item"))} data-qa="hit">
      <div className={bemBlocks.item("poster")}>
        <img data-qa="poster" src={'/static/media_cdn/'+result._source.image} width="300" height="200"/>
      </div>
      <div className={bemBlocks.item("details")}>
        <a href="" onClick={(e)=>{e.preventDefault(); 
        history.push({'pathname':'/camera_detail', 
          'state':{'id':result._source.id,
          'model':result._source.model,
          'image':result._source.image,
          'company':result._source.company,
          'megapixels':result._source.megapixels,
          'sensor_size':result._source.sensor_size,
          'lcd_size':result._source.lcd_size,
          'lcd_resolution':result._source.lcd_resolution,
          'weight':result._source.weight,
          'iso_low':result._source.iso_low,
          'iso_high':result._source.iso_high,
          'video':result._source.video,
          'frames_per_sec':result._source.frames_per_sec,
          'af_points':result._source.af_points,
          'aperture':result._source.aperture,
          'max_aperture':result._source.max_aperture,
          'focal_length_from':result._source.focal_length_from,
          'focal_length_to':result._source.focal_length_to,
          'mirrorless':result._source.mirrorless,
          'wifi':result._source.wifi,
          'gps':result._source.gps,
          'optical_zoom':result._source.optical_zoom,
          'digital_zoom':result._source.digital_zoom,
          'waterproof':result._source.waterproof,
          'effective_resolution':result._source.effective_resolution,
          'feature':result._source.camera_feature,
          'specifications':result._source.specifications
        }})}}><h2 className={bemBlocks.item("title")} dangerouslySetInnerHTML={{__html:source.company+' '+source.model}}></h2></a>
        {source.megapixels ? <div className={bemBlocks.item("text")} dangerouslySetInnerHTML={{__html:'- '+source.megapixels +' '+'Megapixel'}}></div> : null}
        {source.sensor_size ? <div className={bemBlocks.item("text")} dangerouslySetInnerHTML={{__html:'- '+source.sensor_size +' '+ 'Sensor size.'}}></div> :null}
        {source.sensor_type ? <div className={bemBlocks.item("text")} dangerouslySetInnerHTML={{__html:'- '+source.sensor_type +' '+ 'Sensor type.'}}></div> : null}
        {source.image_sensor_format ? <div className={bemBlocks.item("text")} dangerouslySetInnerHTML={{__html:'- '+source.image_sensor_format +' '+ 'Image sensor format.'}}></div> : null}
        {source.wifi ? <div className={bemBlocks.item("text")} dangerouslySetInnerHTML={{__html:'- Built in Wi-Fi Connectivity'}}></div> : null}
      </div>
      <hr />
    </div>
  )
}


export {CameraGridItem, CameraListItem}