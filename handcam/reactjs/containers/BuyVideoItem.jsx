import * as React from "react";
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'
import "searchkit/theming/theme.scss";
import { VideoGridItem, VideoListItem } from './BuyVideoItems'
import { RefinementOption } from './RefinementBrandFilter'
import { CustomRefinementListFilter } from '../components/CustomRefinementList'
import { history } from '../store/configureStore'
import { strings } from '../lib/strings';
import FaFilter from 'react-icons/lib/fa/filter'
let CONFIG = require('../lib/config.json')

import {
  SearchBox,
  Hits,
  HitsStats,
  SearchkitProvider,
  SearchkitManager,
  SortingSelector,
  RefinementListFilter,
  RangeFilter,
  NumericRefinementListFilter,
  NoHits,
  ViewSwitcherToggle,
  ViewSwitcherHits,
  Pagination,
  ResetFilters,
  Layout, LayoutBody, LayoutResults,
  SideBar, TopBar,
  ActionBar, ActionBarRow,
  TermQuery,
    FilteredQuery,
    BoolShould, BoolMust
} from "searchkit";

const camStyle = {
  width: 75,
    height: 54,
};
const logoStyle = {
  width: 75,
    height: 25,
};

class BuyVideoItem extends React.Component {

	searchkit:SearchkitManager

    constructor(props) {
        super(props)
        const host = CONFIG.server_url + "/es/forsalevideo/for_sale_video_index/"
        //const host = "http://173.249.7.103:9200/forsalelens/for_sale_lens_index/"
        this.searchkit = new SearchkitManager(host)
        this.searchkit.addDefaultQuery((query)=> {
          //return query.addQuery(BoolMust([TermQuery("description", "forsale")]));
          return query.addQuery(BoolMust([(TermQuery("description", "forsale")), (TermQuery("status", "active"))]));
        })
        this.state = {
          openbrand: true,
          opencategory: true,
          openmodel: true,
          opensensonsize: true,
          opensensortype: true,
          openvideoresolution: true,
          openpackage: true,
          openlcdsize: true,
        }

        this.searchkit.translateFunction = (key) => {
          let translations = {
            "pagination.previous":"Previous page",
            "pagination.next":"Next page",
            "id1":"Color",
            "id2": "Red",
            "searchbox.placeholder": strings.search,
            //"hitstats.results_found": {hitCount} + " " + strings.results_found_in + " " + {timeTaken}+ "ms"
          }
          return translations[key]
        }
    }

    componentWillMount(){
        /*let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }  */ 
    }

  togglebrand() {
    
    this.setState({
      openbrand: !this.state.openbrand
    });
    
  }

  togglecategory() {
    this.setState({
      opencategory: !this.state.opencategory
    });
  }

  togglemodel() {
    this.setState({
      openmodel: !this.state.openmodel
    });
  }

  togglesensorsize() {
    this.setState({
      opensensonsize: !this.state.opensensonsize
    });
  }

  togglesensortype() {
    this.setState({
      opensensortype: !this.state.opensensortype
    });
  }

  togglevideoresolution() {
    this.setState({
      openvideoresolution: !this.state.openvideoresolution
    });
  }

  togglepackage() {
    this.setState({
      openpackage: !this.state.openpackage
    });
  }

  togglelcdsize() {
    this.setState({
      openlcdsize: !this.state.openlcdsize
    });
  }

   clickStillHandle = (e) => {
    e.preventDefault();
    history.push('/buy_item')
  }

  clickVideoHandle = (e) => {
    e.preventDefault();
    history.push('/buy_video_cameras')
  }

  clickLensHandle = (e) => {
    e.preventDefault();
    history.push('/buy_lens')
  }

  clickLightingHandle = (e) => {
      e.preventDefault();
      history.push('/buy_lighting')
  }

  clickAccessoriesHandle = (e) => {
      e.preventDefault();
      history.push('/buy_accessories')
  }

  clickDroneHandle = (e) => {
      e.preventDefault();
      history.push('/buy_drones')
  }

  render() {
    return (
      <div>
          
          <ul className="breadcrumb">
              <li><Link to="/">Home</Link></li>
              <li><Link to="/buy">Buy</Link></li>
              <li>Video Camera</li>
          </ul>

          <div className=" bg-company-orange">
              <ul className="d-flex mx-auto justify-content-center align-self-center ulmage zeroleftpadding">
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickStillHandle}><span>{strings.photography_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickLensHandle}><span>{strings.lenses}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4 active">
                  <a className="awhite" href="" onClick={this.clickVideoHandle}><span>{strings.video_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickLightingHandle}><span>{strings.lighting}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickAccessoriesHandle}><span>{strings.accessories}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickDroneHandle}><span>{strings.drone}</span></a>
                </li>
              </ul>
          </div>
          


        <SearchkitProvider searchkit={this.searchkit}>
        <Layout size="l">

          <LayoutBody>

          <SideBar>
              <div className="serchtop">
                <div className="text-center">
                  <FaFilter /><span className="colorsearch">{strings.narrow_result}</span>
               </div>
              </div>

              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglepackage.bind(this)}>{strings.country}</h6>
                </div>
                <div className="clanheader">
                  <RefinementListFilter
                    id="country"
                    field="seller_country.keyword"
                    operator="OR"
                    size={10}/>
                </div>    
              </div>    

              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglepackage.bind(this)}>{strings.city}</h6>
                </div>
                <div className="clanheader">
                  <RefinementListFilter
                    id="city"
                    field="seller_city.keyword"
                    operator="OR"
                    size={10}/>
                </div>    
              </div>         
              
              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>  
                    <h6 onClick={this.togglebrand.bind(this)} >{strings.brand}</h6>
                </div>
                <div className="clanheader">
                  <CustomRefinementListFilter
                     id="brand"
                     field="brand.keyword"
                     operator="OR"
                     size={10}/>
                </div>
              </div>

              <div>
                  <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                      <h6>{strings.condition}</h6>
                  </div>
                  <div className="clanheader">
                    <RefinementListFilter
                       id="neworused"
                       field="neworused.keyword"
                       operator="OR"
                       size={10}/>
                  </div>
                </div>
                  
              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                    <h6 onClick={this.togglesensorsize.bind(this)} >{strings.sensor_size}</h6>
                </div>
                <div className="clanheader">
                  <CustomRefinementListFilter
                    id="sensor_size"
                    field="sensor_size.keyword"
                    operator="OR"
                    size={10}/>
                </div>
              </div>
             <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                    <h6 onClick={this.togglesensortype.bind(this)} >{strings.video_resolution}</h6>
                </div>
                <div className="clanheader">
                  <CustomRefinementListFilter
                    id="video"
                    field="video.keyword"
                    operator="OR"
                    size={10}/>
                </div>    
              </div>

              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                    <h6 onClick={this.togglevideoresolution.bind(this)} >{strings.frame_rate}</h6>
                </div>
                <div className="clanheader">
                  <CustomRefinementListFilter
                    id="frame_rate"
                    field="frame_rate.keyword"
                    operator="OR"
                    size={10}/>  
                </div>
              </div>

              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                    <h6 onClick={this.togglepackage.bind(this)} >Effective Resolution</h6>
                </div>
                <div className="clanheader">
                  <CustomRefinementListFilter
                    id="effective_resolution"
                    field="effective_resolution.keyword"
                    operator="OR"
                    size={10}/>  
                </div>
              </div>

              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                    <h6 onClick={this.togglepackage.bind(this)} >{strings.lens_mount}</h6>
                </div>
                <div className="clanheader">
                  <CustomRefinementListFilter
                    id="lens_mount"
                    field="lens_mount.keyword"
                    operator="OR"
                    size={10}/>
                </div>
              </div>

             <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                    <h6 onClick={this.togglepackage.bind(this)} >{strings.dynamic_range}</h6>
                </div>
                <div className="clanheader">
                  <CustomRefinementListFilter
                    id="dynamic_range"
                    field="dynamic_range.keyword"
                    operator="OR"
                    size={10}/>
                </div>
              </div>

              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                    <h6 onClick={this.togglepackage.bind(this)} >{strings.price}</h6>
                </div>
                <div className="clanheader">
                  <NumericRefinementListFilter id="price" field="price" options={[
                    {title:"All"},
                    {title:"Under SA500", from:0, to:500},
                    {title:"SA501 to SA750", from:501, to:750},
                    {title:"SA751 to SA1000", from:751, to:1000},
                    {title:"SA1001 to SA1500", from:1001, to:1500},
                    {title:"SA1501 to SA2000", from:1501, to:2000},
                    {title:"Above SA2000", from:2001, to:15000}
                  ]}/> 
                </div>
              </div> 

               <SortingSelector options={[
                  {label:"Date-desc", field:"date", order:"desc", defaultOption:true},
                  {label:"Date-asc", field:"date", order:"asc"},
                ]}/>   

          </SideBar>
          
          <LayoutResults>
                <div className="search_container">
                  <SearchBox
                    searchOnChange={true}
                    queryOptions={{analyzer:"standard"}}
                    queryFields={["model", "brand", "seller_location"]}/>
                </div>
                <ActionBar>
                  <ActionBarRow>
                      <HitsStats/>
                      <ViewSwitcherToggle/>
                  </ActionBarRow>
                </ActionBar>

                <ViewSwitcherHits
                          hitsPerPage={24} highlightFields={["brand","label"]}
                    sourceFilter={["brand",
                      "id",
                      "product_id",
                      "seller",
                      "seller_pk",
                      "seller_location",
                      "seller_country",
                      "seller_city",
                      "seller_location",
                      "seller_phonenumber",
                      "label",
                      "model",
                      "price",
                      "currency",
                      "condition",
                      "lens_description",
                      "lens_photos",
                      "lens_package",
                      "comments",
                      "actual_photos",
                      "status",
                      "warranty",
                      "package",
                      "image", 
                      "id",
                      "company",
                      "megapixels", 
                      "sensor_type", 
                      "sensor_size",
                      "image_sensor_format",
                      "lcd",
                      "lcd_size",
                      "lcd_resolution",
                      "weight",
                      "iso",
                      "iso_low",
                      "iso_high",
                      "video",
                      "frames_per_sec",
                      "af_points",
                      "optical_zoom",
                      "digital_zoom",
                      "aperture",
                      "max_aperture",
                      "mirrorless",
                      "wifi",
                      "gps",
                      "waterproof",
                      "focal_length_from",
                      "focal_length_to",
                      "effective_resolution",
                      "dynamic_range",
                      "view_finder",
                      "lens_mount",
                      "frame_rate",
                      "effective_pixels",
                      "cam_features",
                      "cam_specifications"
                    ]}
                    hitComponents = {[
                      {key:"grid", title:strings.grid, itemComponent:VideoGridItem},
                      {key:"list", title:strings.list, itemComponent:VideoListItem, defaultOption:true}
                    ]}
                    scrollTo="body"/>
                <NoHits suggestionsField="brand"/>
                <Pagination showNumbers={true}/>
              </LayoutResults>
          </LayoutBody>
            </Layout>
      </SearchkitProvider>
    
    <br />
    <br />
    <br />
    <br />
    <br />
    </div>
    )
  }
}

function mapStateToProps(state){
    return{
        rtlconv: state.rtlconv
    }
}

export default connect(mapStateToProps)(BuyVideoItem);