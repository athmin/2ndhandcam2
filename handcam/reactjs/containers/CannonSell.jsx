import * as React from "react";
import { connect } from 'react-redux';
import "searchkit/theming/theme.scss";
import '../scss/customization.scss'
import { CameraGridItem, CameraListItem } from './HitItem'
import { history } from '../store/configureStore'
let CONFIG = require('../lib/config.json')

import {
  SearchBox,
  Hits,
  HitsStats,
  SearchkitProvider,
  SearchkitManager,
  HierarchicalMenuFilter,
  RefinementListFilter,
  RangeFilter,
  NumericRefinementListFilter,
  NoHits,
  ViewSwitcherToggle,
  ViewSwitcherHits,
  Pagination,
  listComponent,
  ItemHistogramList,
  Layout, LayoutBody, LayoutResults,
  SideBar, TopBar,
  ActionBar, ActionBarRow,
  TermQuery,
  FilteredQuery,
  BoolShould
} from "searchkit";

/*const RefinementOption = (props) => (
  <div className={props.bemBlocks.item().state({selected:props.selected}).mix(this.bemBlocks.container("item"))} onClick={props.toggleFilter}>
    <div className={props.bemBlocks.item("label")}>{props.label}</div>
    <div className={props.bemBlocks.item("count")}>{props.docCount}</div>
  </div>
)*/

class CannonSell extends React.Component {

	searchkit:SearchkitManager

    constructor(props) {
        super(props)

        const host = CONFIG.server_url + "/es/cameras/camera"
        this.searchkit = new SearchkitManager(host)
        this.searchkit.addDefaultQuery((query)=> {
          return query.addQuery(TermQuery("kind_of_camera", "Still Camera"));
        })
    }

    

    doParentToggle(data){
      console.log('data')
    }

    componentWillMount(){
        let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }   
    }


  render() {
    return (
        <div>
          <div className=" bg-company-orange">
            
              <ul className="d-flex mx-auto justify-content-center align-self-center">
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="#"><span>Photography Camera</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="#"><span>Lenses</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="#"><span>Video Camera</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="#"><span>Lighthing</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="#"><span>accessories</span></a>
                </li>
              </ul>
            
          </div>
        <SearchkitProvider searchkit={this.searchkit}>
        <Layout size="l">
          <LayoutBody>
        <SideBar>

              <RefinementListFilter
                id="company"
                title="Brand"
                field="company.keyword"
                operator="OR"
                size={10}/>

            <RefinementListFilter
                id="camera_type"
                title="Type"
                field="camera_type.keyword"
                operator="OR"
                size={10}/>

            <RefinementListFilter
                id="lcd_resolution"
                title="LCD Resolution"
                field="lcd_resolution.keyword"
                operator="OR"
                size={10}/>


            <RefinementListFilter
                id="wifi"
                title="Wifi"
                field="wifi.keyword"
                operator="OR"
                size={10}/>

            <RefinementListFilter
                id="gps"
                title="GPS"
                field="gps.keyword"
                operator="OR"
                size={10}/>

        </SideBar>
          
                <LayoutResults>
              <ActionBar>
                <ActionBarRow>
                    <HitsStats/>
                    <ViewSwitcherToggle/>
                </ActionBarRow>
              </ActionBar>
              <ViewSwitcherHits
                        hitsPerPage={12} highlightFields={["company","model"]}
                  sourceFilter={["company", 
                    "model", 
                    "image", 
                    "id", 
                    "company", 
                    "megapixels", 
                    "sensor_type", 
                    "sensor_size",
                    "image_sensor_format",
                    "lcd_size",
                    "camera_type",
                    "lcd_resolution",
                    "weight",
                    "iso_low",
                    "iso_high",
                    "video",
                    "frames_per_sec",
                    "af_points",
                    "aperture",
                    "focal_length_from",
                    "focal_length_to",
                    "wifi",
                    "gps",
                    "specs",
                    "camera_feature",
                  ]}
                  hitComponents = {[
                    {key:"grid", title:"Grid", itemComponent:CameraGridItem},
                    {key:"list", title:"List", itemComponent:CameraListItem, defaultOption:true}
                  ]}
                  scrollTo="body"/>
              <NoHits suggestionsField="company"/>
              <Pagination showNumbers={true}/>
                </LayoutResults>
          </LayoutBody>
            </Layout>
      </SearchkitProvider>
      </div>
    	
    )
  }
}

export default connect()(CannonSell);