import React from 'react';
import { connect } from 'react-redux'
import { history } from '../store/configureStore'
import { GetSellerProfileAction } from '../actions'
import { Tabs, TabLink, TabContent } from 'react-tabs-redux'
import Time from 'react-time'
import StarRatingComponent from 'react-star-rating-component';
import TimeAgo from 'react-time-ago'
import { strings } from '../lib/strings';


const camStyle = {
    'max-width': '80%',
    height: 'auto'
};


class PublicProfile extends React.Component{

	constructor(props){
		super(props);
		this.state = {
			public_profile : [],
			fetching : false,
			profile : [],
			cam_forsale : [],
			lens_forsale : [],
			reviews : [],
			seller_username : this.props.location.state.seller
		}
	}

	componentWillMount() {
		const { dispatch } = this.props;
		const { seller_username } = this.state;
		let token = localStorage.getItem('token')
		if (!token){
          history.push('/login')
        }

        dispatch(GetSellerProfileAction.getSellerProfile(seller_username)).then( () => {
            this.setState({ public_profile: this.props.publicProfile })
            this.setState({ profile: this.props.publicProfile.profile })
            this.setState({ cam_forsale: this.props.publicProfile.cameras })
            this.setState({ reviews: this.props.publicProfile.reviews })
            this.setState({ lens_forsale: this.props.publicProfile.lenses })
        	this.setState({ fetching: true })
        })
	}

	renderProfile(){
		const { profile } = this.state

		return (
			<div className="row">
				<div className="col-4">
					<img className="card-img-left m-3" src={profile.image} style={camStyle}/>
				</div>
				<div className="col-6">
					<div className={this.props.rtlconv == "rtl" ? ' righttoleft' : ''}>
						{ profile.user.username ? <h2>{profile.user.username }</h2> : null}<br />
					</div>
                    <table className={"table table-striped" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                        <tbody>
                        <tr>
                            <td>{strings.first_name}:</td>
                            <td>{ profile.user.first_name ? profile.user.first_name : strings.not_available}</td>
                        </tr>
                        <tr>
                            <td>{strings.last_name}:</td>
                            <td>{ profile.user.last_name ? profile.user.last_name : strings.not_available}</td>
                        </tr>
                        <tr>
                            <td>{strings.city}:</td>
                            <td>{ profile.city ? profile.city : strings.not_available}</td>
                        </tr>
                        <tr>
                            <td>{strings.country}:</td>
                            <td>{ profile.country ? profile.country : strings.not_available}</td>
                        </tr>
                        <tr>
                            <td>{strings.contact_number}:</td>
                            <td>{ profile.phone_number ? profile.phone_number : strings.not_available}</td>
                        </tr>
                        <tr>
                            <td>{strings.member_since}:</td>
                            <td><Time value={profile.user.date_joined} format="YYYY/MM/DD" /></td>
                        </tr>
                        </tbody>
                    </table>
				</div>
			</div>
		)
	}

	renderCamForsale(){
		const { cam_forsale } = this.state
		
		if (cam_forsale.length>0){
			return (
				<table className="table">
	                <thead>
	                    <tr>
	                        <th>{strings.brand_c}</th>
	                        <th>{strings.model_c}</th>
	                        <th>{strings.price_c}</th>
	                        <th>{strings.status_tc}</th>
	                    </tr>
	                </thead>
					<tbody>	
						{ cam_forsale.map((cam) => {
							return(
							<tr key={cam.id}>
					            <td>{cam.item.cam_type.brand.company}</td>
					            <td>{cam.item.model}</td>
					            <td>{cam.price}</td>
					            <td>{cam.status}</td>
				          	</tr>
				          	)
			          	})}
			        </tbody> 
			    </table>   
			)
		}else{
			return (
				<div>
					<p>{strings.no_camera_for_sale_to_display}.</p>
				</div>
			)
		}

	}

	renderLensForsale(){
		const { lens_forsale } = this.state

		if (lens_forsale.length>0){
			return (
				<table className="table">
	                <thead>
	                    <tr>
	                        <th>{strings.brand_c}</th>
	                        <th>{strings.model_c}</th>
	                        <th>{strings.price_c}</th>
	                        <th>{strings.status_tc}</th>
	                    </tr>
	                </thead>
	                <tbody>
						{ lens_forsale.map((lens) => {
							return(
							<tr key={lens.id}>
					            <td>{lens.item.lens_type.brand.company}</td>
					            <td>{lens.item.model}</td>
					            <td>{lens.price}</td>
					            <td>{lens.status}</td>
				          	</tr>
				          	)
			          	})}
			        </tbody>
			    </table>   
			)
		}else{
			return (
				<div>
					<p>{strings.no_lens_for_sale_to_display}.</p>
				</div>
			)
		}
	}

	renderSellerReviews(){
		const { reviews } = this.state
		if (this.state.fetching && this.state.reviews && this.state.reviews.length>0){
			return (
	            <div className="div-reviews">
	                {this.state.fetching && this.state.reviews && this.state.reviews.map((review, i) => {
	                	
	                    return <div className="reviews" key={i}> 
	                        <div className="top-review">
	                            <StarRatingComponent 
	                              name="rate2" 
	                              editing={false}
	                              starCount={5}
	                              value={review.ratings}
	                            />
	                            <div className="date-review"><Time value={review.review_date} format="YYYY/MM/DD" /></div>
	                        </div>
	                        <div className="middle-review">
	                            <div className="reviewer-review">by: {review.reviewer.user.username}</div>
	                        </div>
	                        <div className="bottom-review">
	                            <div className="content-review">"{review.comment}"</div>
	                        </div>
	                         
	                    </div>
	                })}
	            </div>
            )
		}else{
			return (
				<div>
					<p>{strings.no_reviews_to_display}.</p>
				</div>
			)
		}	
	}

	render() {
		const { public_profile, profile, reviews, cam_forsale, lens_forsale, fetching } = this.state
		

		if (fetching){
			
			return (
				<div>
					<div  className={this.props.rtlconv == "rtl" ? ' righttoleft' : ''}>
						<h2>{strings.seller_profile}</h2>
					</div>	
					<div>
						{ this.renderProfile() }
					</div>
					<hr/>
					<div className="container">
						<Tabs
		                    name="tabs1"
		                    className="tabs tabs-1"
		                >
		                    <div className="tab-links">
		                        <TabLink to="reviews">{strings.seller_review}</TabLink>
		                    </div>

		                    <div className="content">
		                        <TabContent for="reviews">
		                            <h2>{strings.seller_review}</h2>
		                            { this.renderSellerReviews() }
		                        </TabContent>
		                    </div>
		                </Tabs>
		            </div>
	                <br />
	                <br />
	                <br />
	                <br />
	                <br />
				</div>					
			)
		}else{
			return(
				<div>{strings.loading_seller_profile}....</div>
			)
		}
	}
}

function mapStateToProps(state){
	return {
		publicProfile: state.publicProfile,
        rtlconv: state.rtlconv
	}
}

export default connect(mapStateToProps)(PublicProfile)