import React from 'react'
import { connect } from 'react-redux'
import {Link} from 'react-router-dom'
import { sellLensActions } from '../actions';
import ImageUploader from 'react-images-upload';
import SellForm from '../components/SellForm'
import { history } from '../store/configureStore'
import OverlayLoader from 'react-loading-indicator-overlay/lib/OverlayLoader'
import { strings } from '../lib/strings';



import Dropzone from 'react-dropzone';


/*const camStyle = {
      width: 400,
    height: 267
    };*/
const camStyle = {
    "max-width": "100%",
    height: "auto"
    };



class LensDetail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            item: this.props.location.state.id,
            category: this.props.location.state.type,
            price: "",
            condition: "",
            comments: "",
            status: 'Active',
            warranty: "",
            actual_photos: [],
            with_camera: 'No',
            submitted: false,
            priceError: false,
            warrantyError: false,
            conditionError: false,
            actualPhotoError: false,
            spinner: false,
            showSizeError: false,
        }

        this.onDrop = this.onDrop.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.handleSelectCondition = this.handleSelectCondition.bind(this);
        this.handleSelectWarranty = this.handleSelectWarranty.bind(this);
    }

    componentWillMount(){
        let token = localStorage.getItem('token')
        if (!token){
          history.push('/login_register')
        }   
    }

    componentDidMount(){
        window.scrollTo(0, 0)
    }

    handleSelect(e) {
        this.setState({with_camera: e.target.value});
        
    }

    handleSelectCondition(e) {
        this.setState({condition: e.target.value});
        
    }

    handleSelectWarranty(e) {
        this.setState({warranty: e.target.value});
        
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        
    }


    onDrop(picture) {
        
        this.setState({
            actual_photos: picture,
            actualPhotoError: false,
            showSizeError: false
        });
    }

    clickStillHandle = (e) => {
        e.preventDefault();
        history.push('/sell_brand')
    }

    clickVideoHandle = (e) => {
        e.preventDefault();
        history.push('/sell_video_brand')
    }

    clickLensHandle = (e) => {
        e.preventDefault();
        history.push('/sell_lens_brand')
    }

    clickLightingHandle = (e) => {
        e.preventDefault();
        history.push('/sell_lighting')
    }

    clickAccessoriesHandle = (e) => {
        e.preventDefault();
        history.push('/sell_accessories')
    }

    clickDroneHandle = (e) => {
        e.preventDefault();
        history.push('/sell_drone')
    }



    handleSubmit = (e) => {
        e.preventDefault()
        const { item, price, with_camera, condition, warranty, actual_photos, submitted, comments } = this.state
        const { dispatch } = this.props;
        
        
        let body = new FormData()
        body.append('item', item)
        body.append('price', price)
        body.append('condition', condition)
        body.append('comments', comments)
        body.append('warranty', warranty) 
        body.append('with_camera', with_camera)
        body.append('status', 'Active')
        Object.keys(actual_photos).forEach(( key, i ) => {
            body.append('actual_photos', actual_photos[key]);
        });

        if (price && actual_photos.length>0 && warranty && condition){ 
            for (var pair of body.entries()){
                
            }
            this.setState({ spinner : true })
            dispatch(sellLensActions.sellLens(body)).then(()=>{
                this.setState({ showSizeError: true })
                this.setState({ spinner: false })
            })
        }

        if (price == ""){
            this.setState({ priceError: true})
        }else{
            this.setState({ priceError: false})
        }

        if (condition == ""){
            this.setState({ conditionError: true})
        }else{
            this.setState({ conditionError: false})
        }

        if (warranty == ""){
            this.setState({ warrantyError: true})
        }else{
            this.setState({ warrantyError: false})
        }

        if (actual_photos.length == 0){
            this.setState({ actualPhotoError: true})
        }
    }

    renderPackage() {
        const { category, submitted, campackage } = this.state

        
        return (
            <div className={'form-group' + (submitted && !with_camera ? ' has-error' : '')}>
                <label htmlFor="campackage">{strings.with_camera}</label>
                <select className="form-control" 
                    name="with_camera" 
                    
                    onChange={(e) => this.handleSelect(e)}
                >
                    <option value="No">{strings.no}</option>
                    <option value="Yes">{strings.yes}</option>
                </select>
            </div>
        )
        

    }

    renderPhotoSizeError(){
        
        if (this.state.showSizeError){
            if (Object.keys(this.props.lens_sell).length > 0) {
                   
                return(
                    <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_decent_photo_size}</div>
                )
            }
        }
    }


    renderSellForm(){
        let token = localStorage.getItem('token')
        const { price, condition, warranty, actual_photos, submitted, file, with_camera, comments} = this.state;
        if(!token){
            return <p>{strings.please_login_to_sell}</p>
        }else{
           return  <form name="form" onSubmit={this.handleSubmit}>
                        <div className={'form-group' + (submitted && !price ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="price">{strings.price}</label><span className="spannote required">({strings.should_be_in_number})</span>
                            <input type="text" className="form-control form-price" name="price" value={price} onChange={this.handleChange} />
                            {this.state.priceError ?
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                                : null
                            }
                        </div>
                        
                        <div className={'form-group' + (submitted && !warranty ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="warranty" className="required">{strings.warranty}</label>
                            <select className="form-control form-warranty" 
                                name="warranty" 
                                
                                onChange={(e) => this.handleSelectWarranty(e)}
                            >
                                <option value="">{strings.sellect_warranty}</option>
                                <option value="No Warranty">{strings.no_warranty}</option>
                                <option value="One (1) Week">{strings.one_week}</option>
                                <option value="Two (2) Weeks">{strings.two_weeks}</option>
                                <option value="One (1) Month">{strings.one_month}</option>
                            </select>
                            {this.state.warrantyError ?
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                                : null
                            }
                        </div>

                        <div className={'form-group' + (submitted && !condition ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="condition" className="required">{strings.condition}</label>
                            <select className="form-control form-condition" 
                                name="condition" 
                                
                                onChange={(e) => this.handleSelectCondition(e)}
                            >
                                <option value="">{strings.select_lens_condition}</option>
                                <option value="Considered new - barely used">{strings.considered_new_barely_used}</option>
                                <option value="Brand new - not used">{strings.brand_new_not_used}</option>
                                <option value="Used for demo only">{strings.used_for_demo_only}</option>
                                <option value="Show signs of use but works perfectly">{strings.show_sign_of_use_but_works_perfectly}</option>
                                <option value="Shows some wear">{strings.shows_some_wear}</option>
                                <option value="Manufacturer Defect">{strings.manufacturer_defect}</option>
                                <option value="Broken but still works fine">{strings.broken_but_still_works_fine}</option>
                                <option value="Broken and need some maintenance">{strings.broken_and_need_some_maintenance}</option>
                                <option value="Other - please specify in the comment box below">{strings.others_please_specify}</option>
                            </select>
                            {this.state.conditionError ?
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                                : null
                            }
                        </div>

                        <div className={'form-group' + (submitted && !comments ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="comments">{strings.comments}</label>
                            <textarea className="form-control replyBox" name="comments" value={comments} onChange={this.handleChange} />
                        </div>

                        
                        <ImageUploader
                            withIcon={true}
                            buttonText={strings.choose_images}
                            onChange={this.onDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                            maxFileSize={5242880}
                            buttonType='button'
                            withPreview={true}/>

                        {this.state.actualPhotoError ? <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_lens_photos}</div> : null}   

                        { this.state.spinner ?
                        
                            <OverlayLoader 
                              color={'blue'} // default is white
                              loader="ScaleLoader" // check below for more loaders
                              text={strings.uploading_your_item_please_wait} 
                              active={this.state.spinner} 
                              backgroundColor={'white'} // default is black
                              opacity=".4" // default is .9  
                            >
                                <div className="spinner">
                                </div>
                            </OverlayLoader>
                        
                        : null}
                        
                        <div className="form-group text-center">
                            <button className="btn btn-lg btn-primary">{strings.submit}</button>
                        </div>
                    </form> 
        }
    }


    

  render() {
        const { state } = this.props.location
        
        return (
            <div>
                <ul className="breadcrumb">
                    <li><Link to="/">{strings.home}</Link></li>
                    <li><Link to="/sell">{strings.sell}</Link></li>
                    <li><Link to="/sell_lens_brand">{strings.lenses}</Link></li>
                    <li><a href="#" onClick={(e)=>{e.preventDefault(); history.push(
                        {'pathname':'/select_lens_category', 
                        'state':{'brand':state.brand}
                        })}}>{state.brand}</a></li>
                    <li>{state.model} - {strings.lens_sell_form}</li>
                </ul>
                <div className=" bg-company-orange">
                    <ul className="d-flex mx-auto justify-content-center nomarge zeroleftpadding">
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickStillHandle}><span>{strings.photography_cameras}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4 active">
                            <a className="awhite" href="" onClick={this.clickLensHandle}><span>{strings.lenses}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickVideoHandle}><span>{strings.video_cameras}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickLightingHandle}><span>{strings.lighting}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickAccessoriesHandle}><span>{strings.accessories}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickDroneHandle}><span>{strings.drone}</span></a>
                        </li>
                    </ul>
                </div>
                <div className="card m-2">
                    <div className={"card-header" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                        <h3 className="mb-0" >{state.brand} {state.model} {state.type}</h3>
                    </div>
                    <div className="card-body" dir="ltr">
                        <div className="row">
                            <div className="col-4">
                                <img className="card-img-left" src={state.image} style={camStyle}/><br />
                            </div>
                            <div className="col mr-5" dir={this.props.rtlconv}>
                                <p className="text-center">{strings.please_fill_up_the_form_and_the_system_will_autogenerate_other_details}..</p>
                                { this.renderSellForm() }
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <br />
                <br />
            </div>
        )
  }
}

function mapStateToProps(state){
    
    return {
        rtlconv: state.rtlconv,
        lens_sell: state.lenssell,
    };
}


export default connect(mapStateToProps)(LensDetail)