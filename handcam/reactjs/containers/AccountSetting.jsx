import React from 'react'
import { connect } from 'react-redux'
import {Link} from 'react-router-dom'
import { getUserProfileAction, editUserAction, editProfileAction } from '../actions'
import moment from 'moment'
import ImageUploader from 'react-images-upload';
import DatePicker from 'react-datepicker';
import { strings } from '../lib/strings';
import { authHeader } from '../lib';
 
import 'react-datepicker/dist/react-datepicker.css';

let CONFIG = require('../lib/config.json')


const divStyle = {
      marginTop: '10px'
    };

const camStyle = {
    "max-width": "80%",
    height: "auto"
};

const buttonStyle = {
    margin: "5px"
    };


class AccountSetting extends React.Component{

	constructor(props){
		super(props);
		this.state = {
            account: '',
            username: '',
            first_name: '',
            last_name: '',
            email: '',
            city: '',
            country: '',
            phone_number: '',
            image: '',
            user_pk: '',
            profile_pk: '',
            hide_phone: '',
            fetching: false,
            date_of_birth: '',
            to_refresh: false
        }

        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleCheckChange = this.handleCheckChange.bind(this);
        this.onDrop = this.onDrop.bind(this);
        this.deleteUserAccount = this.deleteUserAccount.bind(this);
        this.fetch_profile = this.fetch_profile.bind(this);
	}
	
	fetch_profile() {
	    const { dispatch } = this.props;
	    dispatch(getUserProfileAction.userCredentials()).then( () => {
            this.setState({fetching: true})
            this.setState({ account: this.props.account.user_profile})
            this.setState({ username: this.props.account.user_profile.user.username})
            this.setState({ first_name: this.props.account.user_profile.user.first_name})
            this.setState({ last_name: this.props.account.user_profile.user.last_name})
            this.setState({ email: this.props.account.user_profile.user.email})
            if (this.props.account.user_profile.date_of_birth) {
                this.setState({ date_of_birth: moment(this.props.account.user_profile.date_of_birth)})
            }
            this.setState({ city: this.props.account.user_profile.city})
            this.setState({ country: this.props.account.user_profile.country})
            this.setState({ image: this.props.account.user_profile.image})
            this.setState({ phone_number: this.props.account.user_profile.phone_number})
            this.setState({ user_pk: this.props.account.user_profile.user.pk})
            this.setState({ profile_pk: this.props.account.user_profile.pk})

            
            if (this.props.account.user_profile.hide_phone){
                
                this.setState({ hide_phone: true})
            }else{
                
                this.setState({ hide_phone: false})
            }
            
            
        })
	}

	componentWillMount(){
		//const { dispatch } = this.props;
        let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }

        this.fetch_profile();
	}

    handleDateChange(date) {
        this.setState({
          date_of_birth: date
        });
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        
    }

    handleCheckChange(e){
        const { name, value } = e.target;
        let hide = e.target.checked;
        
        this.setState({[name]: hide})
    }

    onDrop(picture) {
        this.setState({
            image: picture,
        });
        
    }

    exit(){
        history.back()
    }

    saveChanges(){
        const { username, first_name, last_name, email, city, country, image, phone_number, date_of_birth, user_pk, profile_pk, hide_phone } = this.state
        const { dispatch } = this.props;
        var fetch_profil = false;
        
        let body = new FormData()
        let profilebody = new FormData()
        
        body.append('pk', user_pk)
        body.append('username', username)
        body.append('first_name', first_name)
        body.append('last_name', last_name)
        body.append('email', email)

        profilebody.append('pk', profile_pk)
        profilebody.append('date_of_birth', date_of_birth)
        profilebody.append('city', city)
        profilebody.append('country', country)
        profilebody.append('phone_number', phone_number)
        profilebody.append('hide_phone', hide_phone)
        //profilebody.append('image', image);   

        if (image){
            Object.keys(image).forEach(( key, i ) => {
                profilebody.append('image', image[key]);
            });
        }
        
       


        if ( username != this.props.account.user_profile.user.username ||
             first_name != this.props.account.user_profile.user.first_name ||
             last_name != this.props.account.user_profile.user.last_name ||
             email != this.props.account.user_profile.user.email 
            ){
            
             dispatch(editUserAction.edituser(body)).then( () => {
                localStorage.setItem('profile', username);
                this.fetch_profile();
             })
        }else{
            //console.log("no changes made in user");
        }

        if ( city != this.props.account.user_profile.city ||
             country != this.props.account.user_profile.country ||
             phone_number != this.props.account.user_profile.phone_number ||
             date_of_birth != this.props.account.user_profile.date_of_birth ||
             image != this.props.account.user_profile.image
            ){
             dispatch(editProfileAction.editprofile(profilebody)).then( () => {
                this.fetch_profile();
             })
        }else{
            
        }

        //history.back()
    }
    
    deleteUserAccount() {
		var retVal = confirm(strings.delete_account_confirm_msg);
	   if( retVal == true ) {
			console.log("User wants to continue!");
			let pk = localStorage.getItem('profile_pk');
			let token = JSON.parse(localStorage.getItem('token'))
			if (token) {
				fetch(CONFIG.server_url + '/api/profile/delete-user', {
					method: "POST",
					headers: { 'Authorization': 'JWT ' + token, 'Content-Type': 'application/json'},
					body: JSON.stringify({"pk": pk}),
				})
				.then((response) => response.json() )
				.then((responseData) => {
				   if(responseData.status_code == 200) {
						localStorage.removeItem('token');
						localStorage.removeItem('profile');
						window.location.href = "/"
				   } else {
						console.log(responseData);
				   }
				}).catch( (error) => {
				  console.log(error);
				});
			}
			return true;
	   } else {
			return false;
	   }
   }

	render() {
	    console.log("In render of account settings", this.state.account.image, this.props.account.user_profile);
        const { username, first_name, last_name, email, city, country, date_of_birth, image, phone_number, hide_phone } = this.state
        return (
            <div>

                <ul className="breadcrumb">
                      <li><Link to="/">{strings.home}</Link></li>
                      <li>{strings.account_settings}</li>
                </ul>
                <div className="container-fluid" dir="ltr">
                    <div className="row">
                        <div className="col-4">
                            <div className="accountPhoto">
                                <img className="m-3" src={this.state.account.image} style={camStyle}/>
                            </div>    
                            <ImageUploader
                                withIcon={false}
                                buttonText={strings.change_account_photo}
                                onChange={this.onDrop}
                                imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                                maxFileSize={5242880}
                                withLabel={false}/>
                        </div>
                        <div className="col-6" dir={this.props.rtlconv}>

                            <div className={this.props.rtlconv == "rtl" ? ' righttoleft' : ''}>
                                { this.state.account ? <h2>{this.state.account.user.username}</h2> : null } <br />
                            </div>
                            <table className="table table-hover">
                                <tbody>
                                <tr>
                                    <td width="15%">{strings.username}:</td>
                                    <td width="85%">
                                        <input type="text" className="form-control form-username" name="username" value={username} onChange={this.handleChange} />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">{strings.first_name}:</td>
                                    <td width="85%">
                                        <input type="text" className="form-control form-first_name" name="first_name" value={first_name} onChange={this.handleChange} />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">{strings.last_name}:</td>
                                    <td width="85%">
                                        <input type="text" className="form-control form-last_name" name="last_name" value={last_name} onChange={this.handleChange} />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">{strings.email}:</td>
                                    <td width="85%">
                                        <input type="text" className="form-control form-email" name="email" value={email} onChange={this.handleChange} />
                                    </td>
                                </tr>
                                                     
                                <tr>
                                    <td width="15%">{strings.date_of_birth}:</td>
                                    <td width="85%" className={this.props.rtlconv == "rtl" ? ' righttoleft' : ''}>
                                        <DatePicker
                                            selected={this.state.date_of_birth}
                                            onChange={this.handleDateChange}
                                            peekNextMonth
                                            showMonthDropdown
                                            showYearDropdown
                                            dropdownMode="select"
                                            dateFormat="LLL"
                                        />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">{strings.city}:</td>
                                    <td width="85%">
                                        <input type="text" className="form-control form-city" name="city" value={city} onChange={this.handleChange} />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">{strings.country}:</td>
                                    <td width="85%">
                                        <input type="text" className="form-control form-country" name="country" value={country} onChange={this.handleChange} />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">{strings.phone_number}:</td>
                                    <td width="85%">
                                        <input type="text" className="form-control form-phone_number" name="phone_number" value={phone_number} onChange={this.handleChange} />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%"></td>
                                    <td width="85%">
                                        <div className="checkbox">
                                            <label><input type="checkbox" name="hide_phone" checked={hide_phone} ref="hide" onChange = {this.handleCheckChange}/> {strings.hide_phone} </label>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                            <div className="form-group text-center">
								<button className="btn btn-lg btn-primary" style={buttonStyle} onClick={(e)=>{e.preventDefault(); this.deleteUserAccount() }}>{strings.delete_account}</button>
                                <button className="btn btn-lg btn-primary" style={buttonStyle} onClick={(e)=>{e.preventDefault(); this.saveChanges() }}>{strings.save_changes}</button>
                                <button className="btn btn-lg btn-primary" style={buttonStyle} onClick={(e)=>{e.preventDefault(); this.exit() }}>{strings.exit}</button>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </div>
                </div>
                <br />
                <br />
                <br />
                <br />
                <br />
            </div>
        )
	}
}

function mapStatetoProps(state){
	return {
        account: state.credentialsReducer,
        rtlconv: state.rtlconv
    };
}

export default connect(mapStatetoProps)(AccountSetting)
