import React from 'react'
import { connect } from 'react-redux'
import {Link} from 'react-router-dom'
import { sellLightingActions } from '../actions';
import ImageUploader from 'react-images-upload';
import SellForm from '../components/SellForm'
import { history } from '../store/configureStore'
import OverlayLoader from 'react-loading-indicator-overlay/lib/OverlayLoader'
import { strings } from '../lib/strings';




import Dropzone from 'react-dropzone';


const camStyle = {
      width: 400,
    height: 267
    };



class LightingDetail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showBrands: this.props.location.state.showBrands,
            category: this.props.location.state.category, 
            brand: "",
            model: "",
            comment: "",
            price: "",
            condition: "",
            status: 'Active',
            warranty: "",
            actual_photos: [],
            submitted: false,
            priceError: false,
            brandError: false,
            modelError: false,
            conditionError: false,
            warrantyError: false,
            brandIsNanError: false,
            actualPhotoError: false,
            spinner: false,
            showSizeError: false,
        }

        this.onDrop = this.onDrop.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleChangeEnglishonly = this.handleChangeEnglishonly.bind(this);
        this.handleSelectWarranty = this.handleSelectWarranty.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this);
        this.clickStillHandle = this.clickStillHandle.bind(this);
        this.clickVideoHandle = this.clickVideoHandle.bind(this);
        this.clickLensHandle = this.clickLensHandle.bind(this);
        this.clickLightingHandle = this.clickLightingHandle.bind(this);
        this.clickAccessoriesHandle = this.clickAccessoriesHandle.bind(this);

        

    }

    componentWillMount(){
        let token = localStorage.getItem('token')
        if (!token){
          history.push('/login_register')
        }   
    }

    componentDidMount(){
        window.scrollTo(0, 0)
    }

    handleSelect(e) {
        
        this.setState({brand: e.target.value});
        
    }

    handleSelectWarranty(e) {
        this.setState({warranty: e.target.value});
       
    }


    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        
    }

    handleChangeEnglishonly(e) {
        const { name, value } = e.target;
        let regEx = /^[ A-Za-z_0-9@./#&+-]*$/;
       
        if (regEx.test(value)) {
            this.setState({ [name]: value });
            
        }
    }

    handleSelectCondition(e) {
        this.setState({condition: e.target.value});
        
    }


    onDrop(picture) {
        
        this.setState({
            actual_photos: picture,
            actualPhotoError: false,
            showSizeError: false
        });
    }


    clickStillHandle = (e) => {
        e.preventDefault();
        history.push('/sell_brand')
    }

    clickVideoHandle = (e) => {
        e.preventDefault();
        history.push('/sell_video_brand')
    }

    clickLensHandle = (e) => {
        e.preventDefault();
        history.push('/sell_lens_brand')
    }

    clickLightingHandle = (e) => {
        e.preventDefault();
        history.push('/sell_lighting')
    }

    clickAccessoriesHandle = (e) => {
        e.preventDefault();
        history.push('/sell_accessories')
    }

    clickDroneHandle = (e) => {
        e.preventDefault();
        history.push('/sell_drone')
    }



    handleSubmit = (e) => {
        e.preventDefault()
        const { item, price, brand, model, category, condition, warranty, comment, actual_photos, submitted } = this.state
        const { dispatch } = this.props;

        let body = new FormData()
        body.append('item', item)
        body.append('price', price)
        body.append('brand', brand)
        body.append('model', model)
        body.append('category', category)
        body.append('condition', condition)
        body.append('warranty', warranty) 
        body.append('comment', comment)
        body.append('status', 'Active')
        Object.keys(actual_photos).forEach(( key, i ) => {
            body.append('actual_photos', actual_photos[key]);
        });

        if (price && brand && condition && warranty && actual_photos.length>0){ 
            for (var pair of body.entries()){
                
            }
            
            if (isNaN(brand)){
                this.setState({ spinner: true })
                dispatch(sellLightingActions.sellLighting(body)).then(()=>{
                    this.setState({ showSizeError: true })
                })
            }else{
                this.setState({ brandIsNanError: true})
            }
        }

        if (price == ""){
            this.setState({ priceError: true})
        }else{
            this.setState({ priceError: false})
        }

        if (brand == ""){
            this.setState({ brandError: true})
        }else{
            this.setState({ brandError: false})
        }

        if (warranty == ""){
            this.setState({ warrantyError: true})
        }else{
            this.setState({ warrantyError: false})
        }

        if (condition == ""){
            this.setState({ conditionError: true})
        }else{
            this.setState({ conditionError: false})
        }

        if (actual_photos.length == 0){
            this.setState({ actualPhotoError: true})
        }

    }

    renderLightingCategory(cat) {

         if (cat=="Continuous Lighting"){
            return (strings.continuous_lighting)
         }else if(cat=="Strobe Lighting"){
            return (strings.strobe_lighting)
         }else if(cat=="Flashes On Camera"){
            return (strings.flashes_on_camera)
         }else if(cat=="Monolights"){
            return (strings.monolights)
         }else if(cat=="Battery Powered Stobes"){
            return (strings.battery_powered_strobes)
         }else if(cat=="Power Packs"){
            return (strings.power_packs)
         }else{
            return (strings.slaves)
         } 
      }

    renderBrands() {
        const { showBrands, submitted, brand } = this.state
            
        if (showBrands){    
            return (
                <div className={'form-group' + (submitted && !brand ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <label htmlFor="brand">{strings.brand}</label>
                    <select className="form-control form-brand" 
                        name="brand" 
                        onChange={(e) => this.handleSelect(e)}
                    >
                        <option value="">{strings.choose_brand}</option>
                        <option value="Angler">Angler</option>
                        <option value="Bowens">Bowens</option>
                        <option value="Broncolor">Broncolor</option>
                        <option value="Comet">Comet</option>
                        <option value="Dyanlite">Dyanlite</option>
                        <option value="Electra">Electra</option>
                        <option value="Elinchrom">Elinchrom</option>
                        <option value="Godex">Godex</option>
                        <option value="Hensel">Hensel</option>
                        <option value="Impact">Impact</option>
                        <option value="Interfit">Interfit</option>
                        <option value="Norman">Norman</option>
                        <option value="Novatron">Novatron</option>
                        <option value="Photoflex">Photoflex</option>
                        <option value="Photogenic">Photogenic</option>
                        <option value="Phottix">Phottix</option>
                        <option value="Prophoto">Prophoto</option>
                        <option value="Raya">Raya</option>
                        <option value="Smith-Vector">Smith-Vector</option>
                        <option value="Sp Studio System">Sp Studio System</option>
                        <option value="Visatec">Visatec</option>
                        <option value="Westcott">Westcott</option>
                    </select>
                </div>
            )
        }else{
            return (
                <div className={'form-group' + (submitted && !brand ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <label htmlFor="brand" className="required">{strings.brand}</label>{this.props.rtlconv == "rtl" ? <span className="spannote">({strings.english_only})</span> : null}
                    <input type="text" className="form-control form-brand" name="brand" value={brand} onChange={this.handleChangeEnglishonly} />
                    {this.state.brandError ?
                        <div className="errorMessage">{strings.this_field_is_required}</div>
                        : null
                    }
                    {this.state.IsNanError ?
                        <div className="errorMessage">{strings.should_not_be_a_number}</div>
                        : null
                    }
                </div>
            )
        }
        

    }


    renderPhotoSizeError(){
        
        if (this.state.showSizeError){
            if (Object.keys(this.props.lighting_sell).length > 0) {
                   
                return(
                    <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_decent_photo_size}</div>
                )
            }
        }
    }


    renderSellForm(){
        let token = localStorage.getItem('token')
        const { price, condition, warranty, actual_photos, submitted, file, brand, model, comment} = this.state;
        if(!token){
            return <p>{strings.please_login_to_sell}</p>
        }else{
           return  <form name="form" onSubmit={this.handleSubmit}>

                        { this.renderBrands() }

                        <div className={'form-group' + (submitted && !price ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="price">{strings.price}</label><span className="spannote required">({strings.should_be_in_number})</span>
                            <input type="number" inputMode="numeric" className="form-control form-price" name="price" value={price} onChange={this.handleChange} />
                            {this.state.priceError ?
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                                : null
                            }
                        </div>

                        <div className={'form-group' + (submitted && !model ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="brand" className="required">{strings.model_name}</label>{this.props.rtlconv == "rtl" ? <span className="spannote">({strings.english_only})</span> : null}
                            <input type="text" className="form-control form-model" name="model" value={model} onChange={this.handleChangeEnglishonly} />
                            {this.state.modelError ?
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                                : null
                            }
                            {this.state.IsNanError ?
                                <div className="errorMessage">{strings.should_not_be_a_number}</div>
                                : null
                            }
                        </div>
                        
                        <div className={'form-group' + (submitted && !warranty ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="warranty" className="required">{strings.warranty}</label>
                            <select className="form-control form-warranty" 
                                name="warranty" 
                                
                                onChange={(e) => this.handleSelectWarranty(e)}
                            >
                                <option value="">{strings.sellect_warranty}</option>
                                <option value="No Warranty">{strings.no_warranty}</option>
                                <option value="One (1) Week">{strings.one_week}</option>
                                <option value="Two (2) Weeks">{strings.two_weeks}</option>
                                <option value="One (1) Month">{strings.one_month}</option>
                            </select>
                            {this.state.warrantyError ?
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                                : null
                            }
                        </div>
                        <div className={'form-group' + (submitted && !condition ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="condition" className="required">{strings.condition}</label>
                            <select className="form-control form-condition" 
                                name="condition" 
                                
                                onChange={(e) => this.handleSelectCondition(e)}
                            >
                                <option value="">{strings.select_condition}</option>
                                <option value="Considered new - barely used">{strings.considered_new_barely_used}</option>
                                <option value="Brand new - not used">{strings.brand_new_not_used}</option>
                                <option value="Used for demo only">{strings.used_for_demo_only}</option>
                                <option value="Show signs of use but works perfectly">{strings.show_sign_of_use_but_works_perfectly}</option>
                                <option value="Shows some wear">{strings.shows_some_wear}</option>
                                <option value="Manufacturer Defect">{strings.manufacturer_defect}</option>
                                <option value="Broken but still works fine">{strings.broken_but_still_works_fine}</option>
                                <option value="Broken and need some maintenance">{strings.broken_and_need_some_maintenance}</option>
                                <option value="Other - please specify in the comment box below">{strings.others_please_specify}</option>
                            </select>
                            {this.state.conditionError ?
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                                : null
                            }
                        </div>

                        <div className={'form-group' + (submitted && !comment ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="comment">{strings.comments}</label>
                            <textarea className="form-control replyBox" name="comment" value={comment} onChange={this.handleChange} />
                            {submitted && !comment &&
                                <div className="help-block">{strings.this_field_is_required}</div>
                            }
                        </div>
                        
                        <ImageUploader
                            withIcon={true}
                            buttonText={strings.choose_images}
                            onChange={this.onDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                            maxFileSize={5242880}
                            buttonType='button'
                            withPreview={true}/>

                        {this.state.actualPhotoError ? <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_lighting_photos}</div> : null}   

                        { this.state.spinner ?
                        
                            <OverlayLoader 
                              color={'blue'} // default is white
                              loader="ScaleLoader" // check below for more loaders
                              text={strings.uploading_your_item_please_wait}
                              active={this.state.spinner} 
                              backgroundColor={'white'} // default is black
                              opacity=".4" // default is .9  
                            >
                                <div className="spinner">
                                </div>
                            </OverlayLoader>
                        
                        : null}

                        <div className="form-group text-center">
                            <button className="btn btn-lg btn-primary">{strings.submit}</button>
                        </div>
                    </form> 
        }
    }

    lightingCategory(category){
        
        if((category == "Battery Powered Stobes") || (category == "Power Packs") || (category == "Slaves") || (category == "Monolights")){
            return (
                <ul className="breadcrumb">
                    <li><Link to="/">{strings.home}</Link></li>
                    <li><Link to="/sell">{strings.sell}</Link></li>
                    <li><Link to="/sell_lighting">{strings.lighting}</Link></li>
                    <li><Link to="/sell_strobe_lighting">Strobe Lighting</Link></li>
                    <li>{this.renderLightingCategory(category)} - {strings.lighting_sell_form}</li>
                </ul>
            )
        }
        else {
            return (
                <ul className="breadcrumb">
                    <li><Link to="/">{strings.home}</Link></li>
                    <li><Link to="/sell">{strings.sell}</Link></li>
                    <li><Link to="/sell_lighting">{strings.lighting}</Link></li>
                    <li>{this.renderLightingCategory(category)} - {strings.lighting_sell_form}</li>
                </ul>
            )
        }
    }

  render() {
        const { state } = this.props.location
        
        return (
            <div>
                { this.lightingCategory(state.category) } 
                <div className=" bg-company-orange">
                    <ul className="d-flex mx-auto justify-content-center nomarge zeroleftpadding">
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickStillHandle}><span>{strings.photography_cameras}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickLensHandle}><span>{strings.lenses}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickVideoHandle}><span>{strings.video_cameras}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4 active">
                            <a className="awhite" href="" onClick={this.clickLightingHandle}><span>{strings.lighting}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickAccessoriesHandle}><span>{strings.accessories}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickDroneHandle}><span>{strings.drone}</span></a>
                        </li>
                    </ul>
                </div>
                <div className="card m-2">
                    <div className={"card-header" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                        <h3 className="mb-0" >{this.renderLightingCategory(state.category)}</h3>
                    </div>
                    <div className="card-body">
                        <div className="form-container">
                            <div className="form-width">
                                <p className="text-center">{strings.please_fill_up_the_form}</p>
                                { this.renderSellForm() }
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <br />
                <br />
                <br />
            </div>
        )
  }
}

function mapStateToProps(state){
    
    return {
        rtlconv: state.rtlconv,
        lighting_sell: state.lightingsell,
    };
}

export default connect(mapStateToProps)(LightingDetail)

