import React from 'react'
import { history } from '../store/configureStore'
import { connect } from 'react-redux'
import {Link} from 'react-router-dom'
import { getVideoBrandModelsAction } from '../actions'
import { strings } from '../lib/strings';
let CONFIG = require('../lib/config.json')

const images_url = CONFIG.server_url + "/static/media_cdn"


const camStyle = {
    width: 200,
    height: "auto"
    };
const logoStyle = {
	width: 150,
    height: 50,
};

class SellVideoBrand extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			videobrandmodels: []
		}
	}

	componentWillMount(){
        /*let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }*/   
    }


	clickHandle(brand){
		const { dispatch } = this.props;
		

		this.setState({ videobrandmodels: this.props.video_models })
		history.push('/select_category');
		/*if (this.state.brandmodels.length>0){
			history.push('/select_category');
		}else{
			dispatch(getBrandModelsAction.selectCategory(brand));
			history.push('/select_category');
		}*/
	}

	clickStillHandle = (e) => {
	    e.preventDefault();
	    history.push('/sell_brand')
	}

	clickVideoHandle = (e) => {
	    e.preventDefault();
	    history.push('/sell_video_brand')
	}

	clickLensHandle = (e) => {
	    e.preventDefault();
	    history.push('/sell_lens_brand')
	}

	clickLightingHandle = (e) => {
	    e.preventDefault();
		history.push('/sell_lighting')
	}

	clickAccessoriesHandle = (e) => {
        e.preventDefault();
        history.push('/sell_accessories')
    }

    clickDroneHandle = (e) => {
        e.preventDefault();
        history.push('/sell_drone')
    }

  render() {
    return (
    	<div>
        <ul className="breadcrumb">
            <li><Link to="/">{strings.home}</Link></li>
            <li><Link to="/sell">{strings.sell}</Link></li>
            <li>Video Camera</li>
        </ul>
		<div className="vidcam-back">
			<div className=" bg-company-orange">
              <ul className="d-flex mx-auto justify-content-center nomarge zeroleftpadding">
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickStillHandle}><span>{strings.photography_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickLensHandle}><span>{strings.lenses}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4 active">
                  <a className="awhite" href="" onClick={this.clickVideoHandle}><span>{strings.video_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickLightingHandle}><span>{strings.lighting}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickAccessoriesHandle}><span>{strings.accessories}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                    <a className="awhite" href="" onClick={this.clickDroneHandle}><span>{strings.drone}</span></a>
                </li>
              </ul>
            
          </div>
          <div className="brand-header text-center">
	            <span>{strings.video_cameras}</span>
	        </div>
	        

			<div className="category-body" dir="ltr">
			<h4 className="text-center card-title choose-cat">{strings.choose_video_camera_brand}</h4>
			<br />
			
				<div className="m-2" id="selectBrand">
					<div className="row lighting-category">
						<div className="col text-center" id="choose-brand">
							<a href="" onClick={(e)=>{e.preventDefault(); history.push({'pathname':'/select_video_category', 'state':{'brand':'BlackMagic'}})}}>
								<img src={images_url + "/images/vid-BlackMagic.png"} style={camStyle} /><br />
								<img src={images_url + "/images/logo-BlackMagic.png"} style={logoStyle} />
							</a>
						</div>
						<div className="col text-center" id="choose-brand">
							<a href="" onClick={(e)=>{e.preventDefault(); history.push({'pathname':'/select_video_category', 'state':{'brand':'Canon'}})}}>
								<img src={images_url + "/images/vid-Canon.png"} style={camStyle} /><br />
								<img src={images_url + "/images/logo-Canon.png"} style={logoStyle} />
							</a>
						</div>
						<div className="col text-center" id="choose-brand">
							<a href="" onClick={(e)=>{e.preventDefault(); history.push({'pathname':'/select_video_category', 'state':{'brand':'Panasonic'}})}}>
								<img src={images_url + "/images/vid-Panasonic.png"} style={camStyle} /><br />
								<img src={images_url + "/images/logo-Panasonic.png"} style={logoStyle} />
							</a>
						</div>
					</div>
					<br />
					<div className="row lighting-category">
						<div className="col text-center" id="choose-brand">
							<a href="" onClick={(e)=>{e.preventDefault(); history.push({'pathname':'/select_video_category', 'state':{'brand':'Red'}})}}>
								<img src={images_url + "/images/vid-Red.png"} style={camStyle} /><br />
								<img src={images_url + "/images/logo-Red.png"} style={logoStyle} />
							</a>
						</div>
						<div className="col text-center" id="choose-brand">
							<a href="" onClick={(e)=>{e.preventDefault(); history.push({'pathname':'/select_video_category', 'state':{'brand':'Sony'}})}}>
								<img src={images_url + "/images/vid-Sony.png"} style={camStyle} /><br />
								<img src={images_url + "/images/logo-Sony.png"} style={logoStyle} />
							</a>
						</div>
					</div>
				</div>
			</div>
			<br />
			<br />
			<br />
			<br />
		</div>
		</div>
    )
  }
}


function mapStateToProps(state){
    return {
        video_models: state.video_models
    };
}

export default connect(mapStateToProps)(SellVideoBrand);