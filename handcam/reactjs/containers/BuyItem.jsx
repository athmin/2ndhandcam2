import * as React from "react";
import { connect } from 'react-redux';
import "searchkit/theming/theme.scss";
import { SellGridItem, SellListItem } from './SellItems'
import { RefinementOption } from './RefinementBrandFilter'
import { CustomRefinementListFilter } from '../components/CustomRefinementList'
import { history } from '../store/configureStore'
import {Link} from 'react-router-dom'
import { strings } from '../lib/strings';
import FaFilter from 'react-icons/lib/fa/filter'
let CONFIG = require('../lib/config.json')

import {
  SearchBox,
  Hits,
  HitsStats,
  SearchkitProvider,
  SortingSelector,
  CheckboxFilter,
  SearchkitManager,
  RefinementListFilter,
  NumericRefinementListFilter,
  NoHits,
  ViewSwitcherToggle,
  ViewSwitcherHits,
  Pagination,
  ResetFilters,
  Layout, LayoutBody, LayoutResults,
  SideBar, TopBar,
  ActionBar, ActionBarRow,
  TermQuery,
    FilteredQuery,
    BoolShould, BoolMust
} from "searchkit";

const camStyle = {
  width: 75,
    height: 54,
};
const logoStyle = {
  width: 75,
    height: 25,
};

//,(TermQuery("seller_location", "Riyadh, Saudi Arabia"))

class BuyItem extends React.Component {

	searchkit:SearchkitManager

    constructor(props) {
        super(props)
        const host = CONFIG.server_url + "/es/forsale/for_sale_index/"
        this.searchkit = new SearchkitManager(host)
        this.searchkit.addDefaultQuery((query)=> {
          return query.addQuery(BoolMust([(TermQuery("description", "forsale")), (TermQuery("status", "active"))]));
        })
        this.state = {
          openbrand: true,
          opencategory: true,
          openmodel: true,
          opensensonsize: true,
          opensensortype: true,
          openvideoresolution: true,
          openpackage: true,
          openlcdsize: true,
          direction: this.props.rtlconv
        }

        this.searchkit.translateFunction = (key) => {
          let translations = {
            "pagination.previous":"Previous page",
            "pagination.next":"Next page",
            "id1":"Color",
            "id2": "Red",
            "searchbox.placeholder": strings.search,
            //"hitstats.results_found": {hitCount} + " " + strings.results_found_in + " " + {timeTaken}+ "ms"
          }
          return translations[key]
        }
    }

    componentWillMount(){
        /*let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }   */
    }

    componentDidMount(){
      
    }

  renderBrand(){
    return <div>
              
              {/*<div className="brandcss">
                <CheckboxFilter id="canon" label="Canon" filter={TermQuery("brand.keyword", 'Canon')} />
                <CheckboxFilter id="fujifilm" label="Fujifilm" filter={TermQuery("brand.keyword", 'Fujifilm')} />
                <CheckboxFilter id="hasselblad" label="Hasselblad" filter={TermQuery("brand.keyword", 'Hasselblad')} />
                <CheckboxFilter id="leica" label="Leica" filter={TermQuery("brand.keyword", 'Leica')} />
                <CheckboxFilter id="nikon" label="Nikon" filter={TermQuery("brand.keyword", 'Nikon')} />
                <CheckboxFilter id="olympus" label="Olympus" filter={TermQuery("brand.keyword", 'Olympus')} />
                <CheckboxFilter id="panasonic" label="Panasonic" filter={TermQuery("brand.keyword", 'Panasonic')} />
                <CheckboxFilter id="pentax" label="Pentax" filter={TermQuery("brand.keyword", 'Pentax')} />
                <CheckboxFilter id="samsung" label="Samsung" filter={TermQuery("brand.keyword", 'Samsung')} />
                <CheckboxFilter id="sony" label="Sony" filter={TermQuery("brand.keyword", 'Sony')} />
              </div>*/}
          </div>
            
  }

  renderCategory(){
    return <div>
              <div className="brandcss">
                  <CheckboxFilter id="actioncameras" label="Action Cameras" filter={TermQuery("category.keyword", 'Action Cameras')} />
                  <CheckboxFilter id="compactcameras" label="Compact Cameras" filter={TermQuery("category.keyword", 'Compact Cameras')} />
                  <CheckboxFilter id="compactpowershotscameras" label="Compact / Powershot Cameras" filter={TermQuery("category.keyword", 'Compact / Powershot Cameras')} />
                  <CheckboxFilter id="dslrcameras" label="DSLR Cameras" filter={TermQuery("category.keyword", 'DSLR Cameras')} />
                  <CheckboxFilter id="eosmseriescameras" label="EOS M Series Cameras" filter={TermQuery("category.keyword", 'EOS M Series Cameras')} />
                  <CheckboxFilter id="galaxycameras" label="Galaxy Cameras" filter={TermQuery("category.keyword", 'Galaxy Cameras')} />
                  <CheckboxFilter id="instax" label="Instax Cameras" filter={TermQuery("category.keyword", 'Instax Cameras')} />
                  <CheckboxFilter id="leicaprofessionalcameras" label="Leica Professional Cameras" filter={TermQuery("category.keyword", 'Leica Professional Camera')} />
                  <CheckboxFilter id="mediumformatcameras" label="Medium Format Cameras" filter={TermQuery("category.keyword", 'Medium Format Cameras')} />
                  <CheckboxFilter id="mirrorlesscameras" label="Mirrorless Cameras" filter={TermQuery("category.keyword", 'Mirrorless Cameras')} />
                  <CheckboxFilter id="nikon1everdaycameras" label="Nikon 1 Everyday Cameras" filter={TermQuery("category.keyword", 'Nikon 1 Everyday Cameras')} />
                  <CheckboxFilter id="nikon1performancecameras" label="Nikon 1 Performance Cameras" filter={TermQuery("category.keyword", 'Nikon 1 Performance Cameras')} />
                  <CheckboxFilter id="nxcameras" label="NX Cameras" filter={TermQuery("category.keyword", 'NX Cameras')} />
                  <CheckboxFilter id="omdcameras" label="OMD Cameras" filter={TermQuery("category.keyword", 'OMD Cameras')} />
                  <CheckboxFilter id="pointandshoot" label="Point and Shoot Cameras" filter={TermQuery("category.keyword", 'Point and Shoot Cameras')} />
                  <CheckboxFilter id="slrcameras" label="SLR Cameras" filter={TermQuery("category.keyword", 'SLR Cameras')} />
                  <CheckboxFilter id="xmirrorlesscameras" label="X Mirrorless Cameras" filter={TermQuery("category.keyword", 'X Mirrorless Cameras')} />
                  <CheckboxFilter id="xpremiumcompactcameras" label="X Premium Compact Cameras" filter={TermQuery("category.keyword", 'X Premium Compact Cameras')} />
                </div>
          </div>
            
  }

  renderModel(){
    return <div>
              <div className="brandcss">
                  <CheckboxFilter id="canoneos1dx" label="Canon EOS 1d X" filter={TermQuery("model.keyword", 'Canon EOS 1d X')} />
                  <CheckboxFilter id="canoneos1dmarkiv" label="Canon EOS 1d Mark IV" filter={TermQuery("model.keyword", 'Canon EOS 1d Mark IV')} />
                  <CheckboxFilter id="canoneos1dsmarkiii" label="Canon EOS 1Ds Mark III" filter={TermQuery("model.keyword", 'Canon EOS 1Ds Mark III')} />
                  <CheckboxFilter id="nokond40" label="Nikon D40" filter={TermQuery("model.keyword", 'Nikon D40')} />
                  <CheckboxFilter id="nikond3000" label="Nikon D3000" filter={TermQuery("model.keyword", 'Nikon D3000')} />
                  <CheckboxFilter id="nikond3100" label="Nikon D3100" filter={TermQuery("model.keyword", 'Nikon D3100')} />
                </div>
          </div>
            
  }

  renderSensorSize(){
    return <div>
              <div className="brandcss">
                  <CheckboxFilter id='1 "' label='1 "' filter={TermQuery("sensor_size.keyword", '1 "')} />
                  <CheckboxFilter id='1 / 1.7"' label='1 / 1.7"' filter={TermQuery("sensor_size.keyword", '1 / 1.7"')} />
                  <CheckboxFilter id='1 / 2.3 "' label='1 / 2.3 "' filter={TermQuery("sensor_size.keyword", '1 / 2.3 "')} />
                  <CheckboxFilter id='1 / 2.5  "' label='1 / 2.5  "' filter={TermQuery("sensor_size.keyword", '1 / 2.5  "')} />
                  <CheckboxFilter id='1 / 3.1 "' label='1 / 3.1 "' filter={TermQuery("sensor_size.keyword", '1 / 3.1 "')} />
                  <CheckboxFilter id='1 / 4 "' label='1 / 4 "' filter={TermQuery("sensor_size.keyword", '1 / 4 "')} />
                  <CheckboxFilter id='1.5 "' label='1.5 "' filter={TermQuery("sensor_size.keyword", '1.5 "')} />
                  <CheckboxFilter id='13.1 mm x 8.8 mm' label='13.1 mm x 8.8 mm' filter={TermQuery("sensor_size.keyword", '13.1 mm x 8.8 mm')} />
                  <CheckboxFilter id='13.2 mm x 8.8 mm' label='13.2 mm x 8.8 mm' filter={TermQuery("sensor_size.keyword", '13.2 mm x 8.8 mm')} />
                  <CheckboxFilter id='17.3 mm x 13 mm' label='17.3 mm x 13 mm' filter={TermQuery("sensor_size.keyword", '17.3 mm x 13 mm')} />
                  <CheckboxFilter id='2 / 3 "' label='2 / 3 "' filter={TermQuery("sensor_size.keyword", '2 / 3 "')} />
                  <CheckboxFilter id='2.2 "' label='2.2 "' filter={TermQuery("sensor_size.keyword", '2.2 "')} />
                  <CheckboxFilter id='22.2 mm x 14.8 mm' label='22.2 mm x 14.8 mm' filter={TermQuery("sensor_size.keyword", '22.2 mm x 14.8 mm')} />
                  <CheckboxFilter id='22.3 mm x 14.9 mm' label='22.3 mm x 14.9 mm' filter={TermQuery("sensor_size.keyword", '22.3 mm x 14.9 mm')} />
                  <CheckboxFilter id='22.4 mm x 15 mm' label='22.4 mm x 15 mm' filter={TermQuery("sensor_size.keyword", '22.4 mm x 15 mm')} />
                  <CheckboxFilter id='22.5 mm x 15 mm' label='22.5 mm x 15 mm' filter={TermQuery("sensor_size.keyword", '22.5 mm x 15 mm')} />
                  <CheckboxFilter id='22.7 mm x 15.1 mm' label='22.7 mm x 15.1 mm' filter={TermQuery("sensor_size.keyword", '22.7 mm x 15.1 mm')} />
                  <CheckboxFilter id='23.1 mm x 15.4 mm' label='23.1 mm x 15.4 mm' filter={TermQuery("sensor_size.keyword", '23.1 mm x 15.4 mm')} />
                  <CheckboxFilter id='23.2 mm x 15.4 mm' label='23.2 mm x 15.4 mm' filter={TermQuery("sensor_size.keyword", '23.2 mm x 15.4 mm')} />
                  <CheckboxFilter id='23.3 mm x 15.5 mm' label='23.3 mm x 15.5 mm' filter={TermQuery("sensor_size.keyword", '23.3 mm x 15.5 mm')} />
                  <CheckboxFilter id='23.4 mm x 15.5 mm' label='23.4 mm x 15.5 mm' filter={TermQuery("sensor_size.keyword", '23.4 mm x 15.5 mm')} />
                  <CheckboxFilter id='23.5 mm × 15.7 mm' label='23.5 mm × 15.7 mm' filter={TermQuery("sensor_size.keyword", '23.5 mm × 15.7 mm')} />
                  <CheckboxFilter id='23.5 mm x 15.6 mm' label='23.5 mm x 15.6 mm' filter={TermQuery("sensor_size.keyword", '23.5 mm x 15.6 mm')} />
                  <CheckboxFilter id='23.5 mm x 15.7 mm' label='23.5 mm x 15.7 mm' filter={TermQuery("sensor_size.keyword", '23.5 mm x 15.7 mm')} />
                  <CheckboxFilter id='23.6 mm × 15.8 mm' label='23.6 mm × 15.8 mm' filter={TermQuery("sensor_size.keyword", '23.6 mm × 15.8 mm')} />
                  <CheckboxFilter id='23.6 mm x 15.6 mm' label='23.6 mm x 15.6 mm' filter={TermQuery("sensor_size.keyword", '23.6 mm x 15.6 mm')} />
                  <CheckboxFilter id='23.6 mm x 15.7 mm' label='23.6 mm x 15.7 mm' filter={TermQuery("sensor_size.keyword", '23.6 mm x 15.7 mm')} />
                  <CheckboxFilter id='23.6 mm x 15.8 mm' label='23.6 mm x 15.8 mm' filter={TermQuery("sensor_size.keyword", '23.6 mm x 15.8 mm')} />
                  <CheckboxFilter id='23.7 mm x 15.6 mm' label='23.7 mm x 15.6 mm' filter={TermQuery("sensor_size.keyword", '23.7 mm x 15.6 mm')} />
                  <CheckboxFilter id='23.7 mm x 15.7 mm' label='23.7 mm x 15.7 mm' filter={TermQuery("sensor_size.keyword", '23.7 mm x 15.7 mm')} />
                  <CheckboxFilter id='23.9 mm x 35.8 mm' label='23.9 mm x 35.8 mm' filter={TermQuery("sensor_size.keyword", '23.9 mm x 35.8 mm')} />
                  <CheckboxFilter id='24 mm × 36 mm' label='24 mm × 36 mm' filter={TermQuery("sensor_size.keyword", '24 mm × 36 mm')} />
                  <CheckboxFilter id='26.6 mm x 17.9 mm' label='26.6 mm x 17.9 mm' filter={TermQuery("sensor_size.keyword", '26.6 mm x 17.9 mm')} />
                  <CheckboxFilter id='27.9 mm x 18.6 mm' label='27.9 mm x 18.6 mm' filter={TermQuery("sensor_size.keyword", '27.9 mm x 18.6 mm')} />
                  <CheckboxFilter id='28.1 mm x 18.7 mm' label='28.1 mm x 18.7 mm' filter={TermQuery("sensor_size.keyword", '28.1 mm x 18.7 mm')} />
                  <CheckboxFilter id='28.7 mm x 19.1 mm' label='28.7 mm x 19.1 mm' filter={TermQuery("sensor_size.keyword", '28.7 mm x 19.1 mm')} />
                  <CheckboxFilter id='30 mm x 45 mm' label='30 mm x 45 mm' filter={TermQuery("sensor_size.keyword", '30 mm x 45 mm')} />
                  <CheckboxFilter id='32.9 mm x 43.9 mm' label='32.9 mm x 43.9 mm' filter={TermQuery("sensor_size.keyword", '32.9 mm x 43.9 mm')} />
                  <CheckboxFilter id='33.1mm x 44.2mm' label='33.1mm x 44.2mm' filter={TermQuery("sensor_size.keyword", '33.1mm x 44.2mm')} />
                  <CheckboxFilter id='35.6 mm x 23.8 mm' label='35.6 mm x 23.8 mm' filter={TermQuery("sensor_size.keyword", '35.6 mm x 23.8 mm')} />
                  <CheckboxFilter id='35.8 mm x 23.9 mm' label='35.8 mm x 23.9 mm' filter={TermQuery("sensor_size.keyword", '35.8 mm x 23.9 mm')} />
                  <CheckboxFilter id='35.9 mm x 23.9 mm' label='35.9 mm x 23.9 mm' filter={TermQuery("sensor_size.keyword", '35.9 mm x 23.9 mm')} />
                  <CheckboxFilter id='35.9 mm x 24 mm' label='35.9 mm x 24 mm' filter={TermQuery("sensor_size.keyword", '35.9 mm x 24 mm')} />
                  <CheckboxFilter id='36 mm x 23.9 mm' label='36 mm x 23.9 mm' filter={TermQuery("sensor_size.keyword", '36 mm x 23.9 mm')} />
                  <CheckboxFilter id='36 mm x 24 mm' label='36 mm x 24 mm' filter={TermQuery("sensor_size.keyword", '36 mm x 24 mm')} />
                  <CheckboxFilter id='36.7 mm x 49.1 mm' label='36.7 mm x 49.1 mm' filter={TermQuery("sensor_size.keyword", '36.7 mm x 49.1 mm')} />
                  <CheckboxFilter id='36.8 mm x 49.1mm' label='36.8 mm x 49.1mm' filter={TermQuery("sensor_size.keyword", '36.8 mm x 49.1mm')} />
                  <CheckboxFilter id='4 / 3 "' label='4 / 3 "' filter={TermQuery("sensor_size.keyword", '4 / 3 "')} />
                  <CheckboxFilter id='40.2 mm x 53.7 mm' label='40.2 mm x 53.7 mm' filter={TermQuery("sensor_size.keyword", '40.2 mm x 53.7 mm')} />
                  <CheckboxFilter id='43.8 mm x 32.8 mm' label='43.8 mm x 32.8 mm' filter={TermQuery("sensor_size.keyword", '43.8 mm x 32.8 mm')} />
                  <CheckboxFilter id='43.8 mm x 32.9 mm' label='43.8 mm x 32.9 mm' filter={TermQuery("sensor_size.keyword", '43.8 mm x 32.9 mm')} />
                  <CheckboxFilter id='53.4 mm x 40 mm' label='53.4 mm x 40 mm' filter={TermQuery("sensor_size.keyword", '53.4 mm x 40 mm')} />
                  <CheckboxFilter id='53.7 mm x 40.4 mm' label='53.7 mm x 40.4 mm' filter={TermQuery("sensor_size.keyword", '53.7 mm x 40.4 mm')} />
                  <CheckboxFilter id='56 mm x 41.5 mm' label='56 mm x 41.5 mm' filter={TermQuery("sensor_size.keyword", '56 mm x 41.5 mm')} />
                </div>
          </div>
            
  }

  renderSensorType(){
    return <div>
              <div className="brandcss">
                  <CheckboxFilter id="aps-c" label="APS-C" filter={TermQuery("sensor_type.keyword", 'APS-C')} />
                  <CheckboxFilter id="apsc-ccd" label="APS-C CCD" filter={TermQuery("sensor_type.keyword", 'APS-C CCD')} />
                  <CheckboxFilter id="ccd" label="CCD" filter={TermQuery("sensor_type.keyword", 'CCD')} />
                  <CheckboxFilter id="cmos" label="CMOS" filter={TermQuery("sensor_type.keyword", 'CMOS')} />
                  <CheckboxFilter id="cmosx2" label="CMOS X2" filter={TermQuery("sensor_type.keyword", 'CMOS X2')} />
                  <CheckboxFilter id="exmorr" label="Exmor R" filter={TermQuery("sensor_type.keyword", 'Exmor R')} />
                  <CheckboxFilter id="exrcmos" label="EXR CMOS" filter={TermQuery("sensor_type.keyword", 'EXR CMOS')} />
                  <CheckboxFilter id="fullframe" label="Full Frame" filter={TermQuery("sensor_type.keyword", 'Full Frame')} />
                  <CheckboxFilter id="hadccd" label="HAD CCD" filter={TermQuery("sensor_type.keyword", 'HAD CCD')} />
                  <CheckboxFilter id="mediumformat" label="Medium Format" filter={TermQuery("sensor_type.keyword", 'Medium format')} />
                  <CheckboxFilter id="mos" label="MOS" filter={TermQuery("sensor_type.keyword", 'MOS')} />
                  <CheckboxFilter id="x-transcmosii" label="X-Trans CMOS II" filter={TermQuery("sensor_type.keyword", 'X-Trans CMOS II')} />
                </div>
          </div>
            
  }

  renderVideoResolution(){
    return <div>
              <div className="brandcss">
                  <CheckboxFilter id="aps-c" label="4K" filter={TermQuery("video.keyword", '4K')} />
                  <CheckboxFilter id="fullhd" label="Full HD" filter={TermQuery("video.keyword", 'Full HD')} />
                  <CheckboxFilter id="hd" label="HD" filter={TermQuery("video.keyword", 'HD')} />
                </div>
          </div>
            
  }

  renderPackage(){
    return <div>
              <div className="brandcss">
                  <CheckboxFilter id="bodyonly" label="Body Only" filter={TermQuery("video.keyword", 'Body Only')} />
                  <CheckboxFilter id="withlens" label="With Lens" filter={TermQuery("video.keyword", 'With Lens')} />
                  <CheckboxFilter id="withaccesories" label="With Accessories" filter={TermQuery("video.keyword", 'With Accessories')} />
                  <CheckboxFilter id="withlensandaccesories" label="With Lens and Accessories" filter={TermQuery("video.keyword", 'With Lens and Accessories')} />
                </div>
          </div>
            
  }

  renderLCDSize(){
    return <div>
              <div className="brandcss">
                  <CheckboxFilter id="1.5" label="1.5" filter={TermQuery("lcd_size.keyword", '1.5')} />
                  <CheckboxFilter id="1.75" label="1.75" filter={TermQuery("lcd_size.keyword", '1.75')} />
                  <CheckboxFilter id="1.8" label="1.8" filter={TermQuery("lcd_size.keyword", '1.8')} />
                  <CheckboxFilter id="2" label="2" filter={TermQuery("lcd_size.keyword", '2')} />
                  <CheckboxFilter id="2.2" label="2.2" filter={TermQuery("lcd_size.keyword", '2.2')} />
                  <CheckboxFilter id="2.5" label="2.5" filter={TermQuery("lcd_size.keyword", '2.5')} />
                  <CheckboxFilter id="2.6" label="2.6" filter={TermQuery("lcd_size.keyword", '2.6')} />
                  <CheckboxFilter id="2.7" label="2.7" filter={TermQuery("lcd_size.keyword", '2.7')} />
                  <CheckboxFilter id="2.8" label="2.8" filter={TermQuery("lcd_size.keyword", '2.8')} />
                  <CheckboxFilter id="2.95" label="2.95" filter={TermQuery("lcd_size.keyword", '2.95')} />
                  <CheckboxFilter id="3" label="3" filter={TermQuery("lcd_size.keyword", '3')} />
                  <CheckboxFilter id="3.2" label="3.2" filter={TermQuery("lcd_size.keyword", '3.2')} />
                  <CheckboxFilter id="3.3" label="3.3" filter={TermQuery("lcd_size.keyword", '3.3')} />
                  <CheckboxFilter id="3.31" label="3.31" filter={TermQuery("lcd_size.keyword", '3.31')} />
                  <CheckboxFilter id="3.7" label="3.7" filter={TermQuery("lcd_size.keyword", '3.7')} />
                  <CheckboxFilter id="4.8" label="4.8" filter={TermQuery("lcd_size.keyword", '4.8')} />
                </div>
          </div>
            
  }

  

  togglebrand() {
    
    this.setState({
      openbrand: !this.state.openbrand
    });
    
  }

  togglecategory() {
    this.setState({
      opencategory: !this.state.opencategory
    });
  }

  togglemodel() {
    this.setState({
      openmodel: !this.state.openmodel
    });
  }

  togglesensorsize() {
    this.setState({
      opensensonsize: !this.state.opensensonsize
    });
  }

  togglesensortype() {
    this.setState({
      opensensortype: !this.state.opensensortype
    });
  }

  togglevideoresolution() {
    this.setState({
      openvideoresolution: !this.state.openvideoresolution
    });
  }

  togglepackage() {
    this.setState({
      openpackage: !this.state.openpackage
    });
  }

  togglelcdsize() {
    this.setState({
      openlcdsize: !this.state.openlcdsize
    });
  }

  clickStillHandle = (e) => {
    e.preventDefault();
    history.push('/buy_item')
  }

  clickVideoHandle = (e) => {
    e.preventDefault();
    history.push('/buy_video_cameras')
  }

  clickLensHandle = (e) => {
    e.preventDefault();
    history.push('/buy_lens')
  }

  clickLightingHandle = (e) => {
      e.preventDefault();
      history.push('/buy_lighting')
  }

  clickAccessoriesHandle = (e) => {
      e.preventDefault();
      history.push('/buy_accessories')
  }

  clickDroneHandle = (e) => {
      e.preventDefault();
      history.push('/buy_drones')
  }


  render() {
    return (
      <div>
          <ul className="breadcrumb">
                <li><Link to="/">{strings.home}</Link></li>
                <li><Link to="/buy">{strings.buy}</Link></li>
                <li>{strings.photography_cameras}</li>
          </ul>
      <div>
          <div className=" bg-company-orange">
              <ul className="d-flex mx-auto justify-content-center align-self-center ulmage zeroleftpadding">
                <li className="list-inline-item ml-4 mr-4 active">
                  <a className="awhite" href="" onClick={this.clickStillHandle}><span>{strings.photography_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickLensHandle}><span>{strings.lenses}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickVideoHandle}><span>{strings.video_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickLightingHandle}><span>{strings.lighting}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickAccessoriesHandle}><span>{strings.accessories}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickDroneHandle}><span>{strings.drone}</span></a>
                </li>
              </ul>
          </div>
          


        <SearchkitProvider searchkit={this.searchkit}>
        <Layout size="l">
          {/*<ul className="list-inline remmarge text-center">
                          <RefinementListFilter
                            field="brand.keyword"
                            id="brand" itemComponent={RefinementOption}/>
                    </ul>*/}

          <LayoutBody>

          <SideBar>
              <div className="serchtop">
                <div className="text-center">
                  <FaFilter /><span className="colorsearch">{strings.narrow_result}</span>
                </div>
              </div>


              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglepackage.bind(this)}>{strings.country}</h6>
                </div>
                <div className="clanheader">
                  <RefinementListFilter
                    id="country"
                    field="seller_country.keyword"
                    operator="OR"
                    size={10}/>
                </div>    
              </div>    

              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglepackage.bind(this)}>{strings.city}</h6>
                </div>
                <div className="clanheader">
                  <RefinementListFilter
                    id="city"
                    field="seller_city.keyword"
                    operator="OR"
                    size={10}/>
                </div>    
              </div>       
              
              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>  
                    <h6 onClick={this.togglebrand.bind(this)} >{strings.brand}</h6>
                </div>
                <div className="clanheader">
                  <CustomRefinementListFilter
                     id="brand"
                     field="brand.keyword"
                     operator="OR"
                     size={10}/>
                </div>
              </div>

              <div>
                  <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                      <h6>{strings.condition}</h6>
                  </div>
                  <div className="clanheader">
                    <RefinementListFilter
                       id="neworused"
                       field="neworused.keyword"
                       operator="OR"
                       size={10}/>
                  </div>
                </div>
                
              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglecategory.bind(this)} >{strings.category}</h6>
                </div>
                <div className="clanheader">
                  <RefinementListFilter
                    id="category"
                    field="category.keyword"
                    operator="OR"
                    size={10}
                    orderDirection="asc"/> 
                </div>
              </div>
              
              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglemodel.bind(this)} >{strings.model}</h6>
                </div>
                <div className="clanheader">
                  <RefinementListFilter
                    id="model"
                    field="model.keyword"
                    operator="OR"
                    size={1000}/>
                </div>
              </div>
                  
              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglesensorsize.bind(this)} >{strings.sensor_size}</h6>
                </div>
                <div className="clanheader">
                  <RefinementListFilter
                      id="sensor_size"
                      field="sensor_size.keyword"
                      operator="OR"
                      size={10}/>
                </div>
              </div>
              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglesensortype.bind(this)} >{strings.sensor_type}</h6>
                </div>
                <div className="clanheader">
                  <RefinementListFilter
                    id="sensor_type"
                    field="sensor_type.keyword"
                    operator="OR"
                    size={10}/>
                </div>
              </div>
              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                  <h6 onClick={this.togglesensorsize.bind(this)} >{strings.still_image_resolution} [MP]</h6>
                </div>
                <div className="clanheader">
                  <NumericRefinementListFilter id="megapixels" field="megapixels" options={[
                    {title:"All"},
                    {title:"up to 10 MP", from:0, to:10},
                    {title:"10.1 MP to 20 MP", from:10.1, to:20},
                    {title:"20.1 MP to 30 MP", from:20.1, to:30},
                    {title:"30.1 MP to 40 MP", from:30.1, to:40},
                    {title:"40.1 MP to 50 MP", from:40.1, to:50},
                    {title:"50.1 MP or more", from:50.1, to:200}
                  ]}/>
                </div> 
              </div>

              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglevideoresolution.bind(this)} >{strings.video_resolution}</h6>
                </div>
                <div className="clanheader">
                  <RefinementListFilter
                    id="video"
                    field="video.keyword"
                    operator="OR"
                    size={10}/>  
                </div>    
              </div>
              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                  <h6 onClick={this.togglesensorsize.bind(this)} >{strings.frames_per_second}</h6>
                </div>
                <div className="clanheader">
                  <NumericRefinementListFilter id="fps" field="frames_per_sec" options={[
                    {title:"All"},
                    {title:"up to 10 fps", from:0, to:10},
                    {title:"10.1 fps to 20 fps", from:10.1, to:20},
                    {title:"20.1 fps to 30 fps", from:20.1, to:30},
                    {title:"30.1 fps to 40 fps", from:30.1, to:40},
                    {title:"40.1 fps to 50 fps", from:40.1, to:50},
                    {title:"50.1 fps or more", from:50.1, to:200}
                  ]}/>
                </div>
              </div>

              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglepackage.bind(this)} >{strings.package_q}</h6>

                </div>
                <div className="clanheader">
                  <RefinementListFilter
                    id="package"
                    field="package.keyword"
                    operator="OR"
                    size={10}/> 
                </div>     
              </div>

              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                  <h6 onClick={this.togglesensorsize.bind(this)} >{strings.price}</h6>
                </div>
                <div className="clanheader">
                  <NumericRefinementListFilter id="price" field="price" options={[
                    {title:"All"},
                    {title:"Under SA500", from:0, to:500},
                    {title:"SA501 to SA750", from:501, to:750},
                    {title:"SA751 to SA1000", from:751, to:1000},
                    {title:"SA1001 to SA1500", from:1001, to:1500},
                    {title:"SA1501 to SA2000", from:1501, to:2000},
                    {title:"Above SA2000", from:2001, to:15000}
                  ]}/> 
                </div>
              </div>

              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglepackage.bind(this)} >{strings.wifi}</h6>
                </div>
                <div className="clanheader">
                  <RefinementListFilter
                    id="wifi"
                    field="wifi.keyword"
                    operator="OR"
                    size={10}/>
                </div>
              </div>

              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglepackage.bind(this)} >GPS</h6>
                </div>
                <div className="clanheader">
                  <RefinementListFilter
                    id="gps"
                    field="gps.keyword"
                    operator="OR"
                    size={10}/>
                </div>
              </div>

              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglepackage.bind(this)} >{strings.mirrorless}</h6>
                </div>
                <div className="clanheader">
                  <RefinementListFilter
                    id="mirrorless"
                    field="mirrorless.keyword"
                    operator="OR"
                    size={10}/>
                </div>    
              </div>  
              <SortingSelector options={[
                  {label:"Date-desc", field:"date", order:"desc", defaultOption:true},
                  {label:"Date-asc", field:"date", order:"asc"},
                ]}/> 

          </SideBar>
          
          <LayoutResults>
                <div className="search_container">
                  <SearchBox
                    searchOnChange={true}
                    queryOptions={{analyzer:"standard"}}
                    queryFields={["model", "brand", "seller_location"]}
                    />
                </div>

                <ActionBar>
                  <ActionBarRow>
                      <HitsStats/>
                      <ViewSwitcherToggle/>
                  </ActionBarRow>
                </ActionBar>
               
                <ViewSwitcherHits
                          hitsPerPage={24} highlightFields={["brand","label"]}
                    sourceFilter={[
                      "id",
                      "product_id",
                      "brand",
                      "seller",
                      "seller_pk",
                      "seller_location",
                      "seller_country",
                      "seller_city",
                      "seller_phonenumber",
                      "label",
                      "model",
                      "price",
                      "currency",
                      "condition",
                      "lens_description",
                      "shutter_count",
                      "actual_photos",
                      "lens_photos",
                      "lens_package",
                      "comments",
                      "status",
                      "warranty",
                      "package",
                      "image", 
                      "id",
                      "company",
                      "megapixels", 
                      "sensor_type", 
                      "sensor_size",
                      "image_sensor_format",
                      "lcd",
                      "lcd_size",
                      "lcd_resolution",
                      "weight",
                      "iso",
                      "iso_low",
                      "iso_high",
                      "video",
                      "frames_per_sec",
                      "af_points",
                      "optical_zoom",
                      "digital_zoom",
                      "aperture",
                      "max_aperture",
                      "mirrorless",
                      "wifi",
                      "gps",
                      "waterproof",
                      "focal_length_from",
                      "focal_length_to",
                      "cam_features",
                      "cam_specifications",
                      "date"
                    ]}
                    hitComponents = {[
                      {key:"grid", title:strings.grid, itemComponent:SellGridItem},
                      {key:"list", title:strings.list, itemComponent:SellListItem, defaultOption:true}
                    ]}
                    scrollTo="body"/>
                <NoHits suggestionsField="brand"/>
                <Pagination showNumbers={true}/>
              </LayoutResults>
          </LayoutBody>
            </Layout>
      </SearchkitProvider>
   
      <br />
      <br />
      <br />
      <br />
      <br />
    </div>
    </div>	
    )
  }
}

function mapStateToProps(state){
    return{
        rtlconv: state.rtlconv
    }
}

export default connect(mapStateToProps)(BuyItem);