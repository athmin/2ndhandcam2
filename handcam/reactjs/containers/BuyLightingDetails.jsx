import React from 'react'
import { connect } from 'react-redux'
import {Link} from 'react-router-dom'
import { camDetails } from '../actions'
import Markdown from 'react-remarkable'
import { sendMessageAction, createSellerReviewAction, getReviewsAction, createSellerReportAction, getSellerProfileAction  } from '../actions'
import ReactImageZoom from 'react-image-zoom'
import { Tabs, TabLink, TabContent } from 'react-tabs-redux'
import { history } from '../store/configureStore'
import Viewer from 'react-viewer';
import 'react-viewer/dist/index.css';
import Modal from 'react-modal';
import StarRatingComponent from 'react-star-rating-component';
import TimeAgo from 'react-time-ago'
import { strings } from '../lib/strings';


const camStyle = {
    "max-width": "100%",
    height: "auto"
    };


const smallStyle = {
    width: 70,
    height: 35
}

const customStyles = {
    content: {
        'border-left': '5px solid #f4a72e',
        'border-top': '5px solid #f4a72e',
        'border-right': '5px solid #3d4041',
        'border-bottom': '5px solid #3d4041',
        borderRadius: '4px',
        bottom: 'auto',
        minHeight: '10rem',
        left: '50%',
        padding: '2rem',
        position: 'fixed',
        right: 'auto',
        top: '50%',
        transform: 'translate(-50%,-50%)',
        minWidth: '20rem',
        width: '80%',
        maxWidth: '60rem',
        padding: '0'
      },
    overlay: {
        backgroundColor: "rgba(255, 255, 255, 0.25)"
    }  
};

const customStylesForAcc = {
    content: {
        'border-left': '5px solid #f4a72e',
        'border-top': '5px solid #f4a72e',
        'border-right': '5px solid #3d4041',
        'border-bottom': '5px solid #3d4041',
        borderRadius: '4px',
        bottom: 'auto',
        minHeight: '10rem',
        left: '50%',
        padding: '2rem',
        position: 'fixed',
        right: 'auto',
        top: '50%',
        transform: 'translate(-50%,-50%)',
        minWidth: '20rem',
        width: '60%',
        maxWidth: '60rem',
        padding: '0'
      },
    overlay: {
        backgroundColor: "rgba(255, 255, 255, 0.25)"
    }  
};

class LightingCameraDetail extends React.Component {

	constructor(props) {
        super(props);
        this.state = {
            actual_photos: [],
            submitted: false,
            showText: false,
            messageValue: strings.hi_im_interested_in_this_item_is_it_available,
            recipient: this.props.location.state.seller_pk,
            messageSent: '',  
            full_comment: this.props.location.state.comment,
            slice_comment: "",
            show_read_more: false,
            long_comment: false,
            visible: false,
            activeIndex: 0,
            messageGroupId: "",

            reviewModal: false,
            reviewModalIsOpen: false, 
            reviewSent: false,

            seller_reviews: [],

            hideContact: '',

            reportModal: false,
            reportModalIsOpen: false, 
            reportSent: false,
            issue: '',

            rating: 5,
            comment: "",  
            isOwner: false,   
        }

        this.handleMessageChange = this.handleMessageChange.bind(this);
        this.handleMessageSubmit = this.handleMessageSubmit.bind(this);

        this.openModalReview = this.openModalReview.bind(this);
        this.afterOpenModalReview = this.afterOpenModalReview.bind(this);
        this.closeModalReview = this.closeModalReview.bind(this);

        this.openModalReport = this.openModalReport.bind(this);
        this.afterOpenModalReport = this.afterOpenModalReport.bind(this);
        this.closeModalReport = this.closeModalReport.bind(this);
    }

    componentWillMount(){
        let token = localStorage.getItem('token')
        const { full_comment } = this.state.full_comment
        const { dispatch } = this.props;

        if (!token){
          history.push('/login')
        } 

        let profile_pk = localStorage.getItem('profile_pk')
        
        if (profile_pk == this.props.location.state.seller_pk){
            this.setState({ isOwner:true })
        }

        dispatch(getReviewsAction.getSellerReviews(this.props.location.state.seller_pk)).then( () => {
            this.setState({fetchingReviews: true})
            this.setState({ sellerReview: this.props.sellerReview })
            
        }) 

        dispatch(getSellerProfileAction.sellerProfile(this.props.location.state.seller_pk)).then( () => {
            this.setState({fetchingReviews: true})
            this.setState({ hideContact: this.props.hideContact.hide_phone })
            
            
        }) 

        if (this.state.full_comment){
            if (this.state.full_comment.length > 150) {
                this.setState({ slice_comment: this.state.full_comment.substr(0, 150)})
                this.setState({ show_read_more: true })
                this.setState({ long_comment: true })
            }
        }
    }

    componentDidMount(){
        window.scrollTo(0, 0)
    }

    makeid() {
        let text = "";
        const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 20; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text
        
    }


    handleMessageChange(e) {
        this.setState({messageValue: e.target.value});
    }

    handleMessageSubmit(e) {
        this.makeid()
        e.preventDefault();
        const { messageValue, recipient } = this.state
        const { dispatch } = this.props;

        let body = new FormData()
        body.append('subject', "Your " + this.props.location.state.model)
        body.append('body', messageValue)
        body.append('recipient', recipient)
        body.append('parent', this.makeid())
        if (messageValue && recipient){
            dispatch(sendMessageAction.sendMessageToSeller(body)).then( () => {
                
                this.setState({ messageSent: this.props.messageSent})
        })
        }
    }

    clickStillHandle = (e) => {
        e.preventDefault();
        history.push('/buy_item')
    }

    clickVideoHandle = (e) => {
        e.preventDefault();
        history.push('/buy_video_cameras')
    }

    clickLensHandle = (e) => {
        e.preventDefault();
        history.push('/buy_lens')
    }

    clickLightingHandle = (e) => {
        e.preventDefault();
        history.push('/buy_lighting')
    }

    clickAccessoriesHandle = (e) => {
        e.preventDefault();
        history.push('/buy_accessories')
    }

    clickDroneHandle = (e) => {
        e.preventDefault();
        history.push('/buy_drones')
    }

    showFullComment() {
        this.setState({ show_read_more: false})
    }

    showSliceComment() {
        this.setState({ show_read_more: true})
    }

    renderFlashMessages(){

        if (this.state.messageSent){
            if(this.state.messageSent.detail == "Cannot send message to yourself."){
                return(
                    <div className="alert alert-danger">
                        { strings.cannot_message_yourself }
                    </div>
                )
            }else{
                return(
                    <div className="alert alert-success">
                        { this.state.messageSent.detail }
                    </div>
                )
            }
        }
    }

    lightingCategory(category, model){
        
        if(category == "Monolights"){
            return (
                <ul className="breadcrumb">
                    <li><Link to="/">{strings.home}</Link></li>
                    <li><Link to="/buy">{strings.buy}</Link></li>
                    <li><Link to="/buy_lighting">{strings.lighting}</Link></li>
                    <li><Link to="/buy_strobe_lighting">{strings.stobe_lighting}</Link></li>
                    <li><a href="#" onClick={(e)=>{e.preventDefault(); history.push(
                            {'pathname':'/buy_lighting_monolight_item',     
                            'state' : {
                                'showFilter' : false,
                                'category' : category
                            }})}}>Monolights</a></li>
                    <li>{model}</li>
                </ul>
            )
        }
        else if((category == "Battery Powered Stobes") || (category == "Power Packs") || (category == "Slaves")){
            return (
                <ul className="breadcrumb">
                    <li><Link to="/">{strings.home}</Link></li>
                    <li><Link to="/buy">{strings.buy}</Link></li>
                    <li><Link to="/buy_lighting">{strings.lighting}</Link></li>
                    <li><Link to="/buy_strobe_lighting">{strings.stobe_lighting}</Link></li>
                    <li><a href="#" onClick={(e)=>{e.preventDefault(); history.push(
                            {'pathname':'/buy_lighting_item',     
                            'state' : {
                                'showFilter' : false,
                                'category' : category
                            }})}}>{category}</a></li>
                    <li>{model}</li>
                </ul>
            )
        }
        else {
            return (
                <ul className="breadcrumb">
                    <li><Link to="/">{strings.home}</Link></li>
                    <li><Link to="/buy">{strings.buy}</Link></li>
                    <li><Link to="/buy_lighting">{strings.lighting}</Link></li>
                    <li><a href="#" onClick={(e)=>{e.preventDefault(); history.push(
                            {'pathname':'/buy_lighting_item',     
                            'state' : {
                                'showFilter' : false,
                                'category' : category
                            }})}}>{category}</a></li>
                    <li>{model}</li>
                </ul>
            )
        }
    }

    addSellerReview(){
        const { rating, recipient, comment } = this.state
        const { dispatch } = this.props;

        let body = new FormData()
        body.append('seller', recipient)
        body.append('ratings', rating)
        body.append('comment', comment)
        
        if (comment){
            dispatch(createSellerReviewAction.createSellerReview(body)).then( () => {
                this.setState({ reviewSent: true})
                this.closeModalReview()
        })
        }
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        
    }


    openModalReview() {
        this.setState({reviewModalIsOpen: true});
    }

    afterOpenModalReview() {
        // references are now sync'd and can be accessed.
        //this.subtitle.style.color = '#f00';
    }

    closeModalReview() {
        this.setState({reviewModalIsOpen: false});
    }

    onStarClick(nextValue, prevValue, name) {
        this.setState({rating: nextValue});
    }


    renderSellerReviewModal() {

        const { seller, rating, comment, submitted } = this.state

        return (

            <div className={"topspace" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                <h2>{strings.seller_review}</h2>

                <p>{strings.rate_the_seller}</p>
                <StarRatingComponent 
                  name="sellerRating" 
                  starCount={5}
                  value={rating}
                  onStarClick={this.onStarClick.bind(this)}
                />

                <div className={'form-group' + (submitted && !description ? ' has-error' : '')}>
                    <label htmlFor="description">{strings.comments}</label>
                    <textarea className="form-control replyBox" name="comment" value={comment} onChange={(e)=>{this.handleChange(e)}} />
                </div>

                <button className="btn btn-lg btn-primary addButtonMargin" onClick={(e)=>{e.preventDefault(); this.addSellerReview()}}>
                   {strings.add_review}
                </button>

            </div>

        )
    }

    renderSellerReviews() {
        
        return (
            <div className="div-reviews">
                {this.state.fetchingReviews && Array.isArray(this.state.sellerReview) && this.state.sellerReview.map((review, i) => {
                    return <div className="reviews" key={i}> 
                        <div className="top-review">
                            <StarRatingComponent 
                              name="rate2" 
                              editing={false}
                              starCount={5}
                              value={review.ratings}
                            />
                            <div className="date-review"><TimeAgo>{new Date(review.review_date)}</TimeAgo></div>
                        </div>
                        <div className="middle-review">
                            <div className="reviewer-review">by: {review.reviewer.user.username}</div>
                        </div>
                        <div className="bottom-review">
                            <div className="content-review">"{review.comment}"</div>
                        </div>
                         
                    </div>
                })}
            </div>

            )
    }


    addSellerReport(){
        const { recipient, issue } = this.state
        const { dispatch } = this.props;

        let body = new FormData()
        body.append('seller', recipient)
        body.append('issue', issue)
        
        if (issue){
            dispatch(createSellerReportAction.createSellerReport(body)).then( () => {
                this.setState({ reportSent: true})
                this.closeModalReport()
        })
        }
    }

    openModalReport() {
        this.setState({reportModalIsOpen: true});
    }

    afterOpenModalReport() {
        // references are now sync'd and can be accessed.
        //this.subtitle.style.color = '#f00';
    }

    closeModalReport() {
        this.setState({reportModalIsOpen: false});
    }


    renderSellerReportModal() {

        const { seller, issue, submitted } = this.state

        return (

            <div className={"topspace" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                <h2>{strings.report_seller}</h2>

                <div className={'form-group' + (submitted && !issue ? ' has-error' : '')}>
                    <label htmlFor="issue">{strings.please_provide_as_much_detail_as_possible}.</label>
                    <textarea className="form-control replyBox" name="issue" value={issue} onChange={(e)=>{this.handleChange(e)}} />
                </div>

                <button className="btn btn-lg btn-primary addButtonMargin" onClick={(e)=>{e.preventDefault(); this.addSellerReport()}}>
                    {strings.send_report}
                </button>

            </div>

        )
    }

    renderContact(seller_phonenumber){
        
        if (this.state.hideContact == false){
            
            return(
                <div>
                    <div>{strings.contact_number} <span className="text-right">{seller_phonenumber}</span></div>  
                </div>
            )
        }else{
            
        }
    }


    renderCameraWarranty(warranty){
        if (warranty=="No Warranty"){
            return <span className="blue-font">{strings.no_warranty}</span>
        }else if (warranty=="One (1) Week"){
            return <span className="blue-font">{strings.one_week}</span>
        }else if (warranty=="Two (2) Weeks"){
            return <span className="blue-font">{strings.two_weeks}</span>
        }else {
            return <span className="blue-font">{strings.one_month}</span>
        }
    }

    renderCameraCondition(condition){
        if (condition=="Considered new - barely used"){
            return <span>{strings.considered_new_barely_used}</span>
        }else if (condition=="Brand new - not used"){
            return <span>{strings.brand_new_not_used}</span>
        }else if (condition=="Used for demo only"){
            return <span>{strings.used_for_demo_only}</span>
        }else if (condition=="show_sign_of_use_but_works_perfectly"){
            return <span>{strings.show_sign_of_use_but_works_perfectly}</span>
        }else if (condition=="Shows some wear"){
            return <span>{strings.shows_some_wear}</span>
        }else if (condition=="Manufacturer Defect"){
            return <span>{strings.manufacturer_defect}</span>
        }else if (condition=="Broken but still works fine"){
            return <span>{strings.broken_but_still_works_fine}</span>
        }else if (condition=="Broken and need some maintenance"){
            return <span>{strings.broken_and_need_some_maintenance}</span>
        }else {
            return <span>{strings.others_please_specify}</span>
        }
    }

    renderCurrency(currency){
        if (currency=="SAR"){
            return (strings.sar)
        }else if (currency=="AED"){
            return (strings.aed)
        }else if (currency=="KWD"){
            return (strings.kwd)
        }else if (currency=="BHD"){
            return (strings.bhd)
        }else{
            return (strings.omr)
        }
    }
    

    renderReviewReportButton(){
        if (this.state.isOwner){
            return(
                <div className="form-group">
                    <button className="btn btn_default btn-block" disabled>{strings.review_seller}</button>
                    <button className="btn btn-danger btn-block" disabled>{strings.report_seller}</button>
                </div> 
            )
        }else{
            return(
                <div className="form-group">
                    <button className="btn btn_default btn-block" onClick={(e)=>{e.preventDefault(); this.openModalReview()}}>{strings.review_seller}</button>
                    <button className="btn btn-danger btn-block" onClick={(e)=>{e.preventDefault(); this.openModalReport()}}>{strings.report_seller}</button>
                </div> 
            ) 
        }
    }


  render() {
        const { state } = this.props.location
        
        const { actual_photos, submitted, messageSent } = this.state;
        let loose = this.props.location.state.actual_photos.replace(/[\[\]']+/g,'');
        let explode = loose.split(',')

        let actualImage=[]
        if (explode.length > 0) {
            {explode.map((item, i) => {
                actualImage.push({"src": item})
            })}
        }

        return (
            
            <div>
                
                { this.lightingCategory(state.category, state.model) } 
               
                <div className=" bg-company-orange">
                  <ul className="d-flex mx-auto justify-content-center nomarge zeroleftpadding">
                    <li className="list-inline-item ml-4 mr-4">
                      <a className="awhite" href="" onClick={this.clickStillHandle}><span>{strings.photography_cameras}</span></a>
                    </li>
                    <li className="list-inline-item ml-4 mr-4">
                      <a className="awhite" href="" onClick={this.clickLensHandle}><span>{strings.lenses}</span></a>
                    </li>
                    <li className="list-inline-item ml-4 mr-4">
                      <a className="awhite" href="" onClick={this.clickVideoHandle}><span>{strings.video_cameras}</span></a>
                    </li>
                    <li className="list-inline-item ml-4 mr-4 active">
                      <a className="awhite" href="" onClick={this.clickLightingHandle}><span>{strings.lighting}</span></a>
                    </li>
                    <li className="list-inline-item ml-4 mr-4">
                      <a className="awhite" href="" onClick={this.clickAccessoriesHandle}><span>{strings.accessories}</span></a>
                    </li>
                    <li className="list-inline-item ml-4 mr-4">
                      <a className="awhite" href="" onClick={this.clickDroneHandle}><span>{strings.drone}</span></a>
                    </li>
                  </ul>
                </div>
                
                <div className="container details">
                    <div className={this.props.rtlconv == "rtl" ? ' righttoleft' : ''}>
                        <h3 className="labeltext" >{state.company} - {state.model}</h3>
                        <div><span className="pstyle">{strings.product_id}:</span><span> {state.product_id}</span></div>
                    </div>
                    
                        <div className="details">
                            <div className="row">
                                <div className="col-8">
                                    <div className="row">
                                        <div className="col">
                                            <img className="card-img-left m-3" src={state.image} style={camStyle}/>
                                            <h5 className="text-center act-style">{strings.actual_photo}</h5>
                                            { explode != '' ? 
                                            <div className="row m-2">

                                                {explode.map((item, i) => {
                                                    const props = {"width":100,"img":item,"zoomStyle":"z-index: 2;position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);"};
                                                    return ( <div key={item} className="actualPhoto" onClick={() => { this.setState({ visible: !this.state.visible, activeIndex: i });}}>
                                                                <ReactImageZoom {...props} />
                                                            </div>)
                                                    }
                                                )}

                                                <Viewer
                                                visible={this.state.visible}
                                                onClose={() => { this.setState({ visible: false }); } }
                                                images={actualImage}
                                                activeIndex={this.state.activeIndex}
                                                />
                                            </div>
                                            : null }  
                                        </div>
                                        <div className={"col" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                                            <div className="m-3">
                                                
                                                <div className="seller-top">
                                                    <table width="100%">

                                                        {state.warranty ?
                                                        <tr>
                                                            <td width="40%" className="top-align"><span className="phigh">{strings.warranty}:</span></td>
                                                            <td>{this.renderCameraWarranty(state.warranty)}</td>
                                                        </tr>
                                                        : null}

                                                        {state.condition ?
                                                        <tr>
                                                            <td width="40%" className="top-align"><span className="phigh">{strings.condition}:</span></td>
                                                            <td>{this.renderCameraCondition(state.condition)}</td>
                                                        </tr>
                                                        : null}

                                                    </table>
                                                </div>
                                                
                                                {this.state.show_read_more ?
                                                <div>
                                                    <p className="phigh pdtop">{strings.comments}:</p>
                                                    <div className="border commentsDiv">
                                                        <p className="boxstyle padtop">{this.state.slice_comment}</p> 
                                                        <p className="blue-font" onClick={(e)=>{e.preventDefault(); this.showFullComment()}}>{strings.read_more}..</p>
                                                    </div>    
                                                </div>
                                                : 
                                                <div>
                                                    <p className="phigh pdtop">{strings.comments}:</p>
                                                    <div className="border commentsDiv">
                                                        <p className="boxstyle padtop">{this.state.full_comment}</p> 
                                                        {this.state.long_comment ? <p className="blue-font" onClick={(e)=>{e.preventDefault(); this.showSliceComment()}}>{strings.show_less}..</p> : null }
                                                    </div>    
                                                </div>
                                                }  

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div className={"col" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                                        <div className="leftprice">

                                            <div className="seller-top">
                                                <a href="" onClick={(e)=>{e.preventDefault(); history.push({'pathname':'/public_profile', 'state':{'seller':state.seller_pk}})}}>
                                                    <p className="seller-style padtop"><span className="black-font">{strings.seller}:</span> <span className={"blue-font" + (this.props.rtlconv == "rtl" ? ' ' : ' span-right')}>{state.seller}</span></p>
                                                </a>    
                                                <p className="seller-style"><span className="black-font">{strings.location}:</span> <span className={"blue-font" + (this.props.rtlconv == "rtl" ? ' ' : ' span-right')}>{state.seller_location}</span></p>
                                            </div>
                                            
                                            <div className="row">
                                                
                                                <form className="form-control" onSubmit={this.handleMessageSubmit}>
                                                    <div className="row">
                                                        <div className={"priceleft col" + (this.props.rtlconv == "rtl" ? '-md-2 zeroleftpadding' : '')}>{strings.price}</div>
                                                        <div className={"col" + (this.props.rtlconv == "rtl" ? '-md-8 zeropadleftright' : '')}>
                                                            <span className={"priceright"  + (this.props.rtlconv == "rtl" ? ' span-left' : ' text-right')}>{this.renderCurrency(state.currency)}</span>
                                                            <span className={"priceright"  + (this.props.rtlconv == "rtl" ? ' span-left' : ' text-right')}> {state.price}</span>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                    <div className="form-group">
                                                    <label>
                                                        {strings.send_seller_a_message}
                                                    </label>
                                                    </div>
                                                    <div className="form-group">
                                                        <textarea className="textwidth" value={this.state.messageValue} onChange={this.handleMessageChange} />
                                                    </div>
                                                    <div className="form-group">
                                                        <button className="btn btn-primary btn-block">{strings.send}</button>
                                                    </div> 

                                                    { this.renderFlashMessages() }
                                                    <hr/>
                                                    { this.renderContact(state.seller_phonenumber) }

                                                    <hr/>
                                                    {this.renderReviewReportButton()}
                                                    
                                                    {this.state.reviewSent ?
                                                        <div className="alert alert-success">{strings.seller_review_posted}</div>
                                                        : null
                                                    }
                                                    {this.state.reportSent ?
                                                        <div className="alert alert-success">{strings.report_has_been_sent}.</div>
                                                        : null
                                                    }
                                                </form>
                                                
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <Tabs
                            name="tabs1"
                            className={"tabs tabs-1" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}
                        >
                            <div className="tab-links">
                                <TabLink to="reviews">{strings.seller_review}</TabLink>
                            </div>

                            <div className="tabContent"  dir="ltr">
                                <TabContent for="reviews">
                                    <h2>{strings.seller_review}</h2>
                                    {this.renderSellerReviews()}
                                </TabContent>
                            </div>
                        </Tabs>
            	   </div>

                   <Modal
                        isOpen={this.state.reviewModalIsOpen}
                        onAfterOpen={this.afterOpenModalReview}
                        onRequestClose={this.closeModalReview}
                        style={customStyles}
                        contentLabel={strings.seller_review}
                        ariaHideApp={false}
                    >
                        
                        <div className="frame arabicfont"> 
                            <button className="btn btn-danger btn-sm float-right fixedclosed modalButton" onClick={this.closeModalReview}> X </button>
                            <div className="scroll"> 
                                  {this.renderSellerReviewModal()}
                            </div>
                        </div>
                        
                    </Modal>

                    <Modal
                        isOpen={this.state.reportModalIsOpen}
                        onAfterOpen={this.afterOpenModalReport}
                        onRequestClose={this.closeModalReport}
                        style={customStyles}
                        contentLabel={strings.report_seller}
                        ariaHideApp={false}
                    >
                        
                        <div className="acc-frame arabicfont"> 
                            <button className="btn btn-danger btn-sm float-right fixedclosed modalButton" onClick={this.closeModalReport}> X </button>
                            <div className="scroll"> 
                                  {this.renderSellerReportModal()}
                            </div>
                        </div>
                        
                    </Modal>
            
                <br />
                <br />
                <br />
                <br />   
            </div>
        )
  }
}


function mapStateToProps(state){
    return{
        messageSent: state.messageSent,
        sellerReview: state.sellerReview,
        rtlconv: state.rtlconv,
        hideContact: state.hideContact
    }
}

export default connect(mapStateToProps)(LightingCameraDetail)