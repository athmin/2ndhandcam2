import React from 'react'
import { history } from '../store/configureStore'
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'
let CONFIG = require('../lib/config.json')
import { strings } from '../lib/strings';

const images_url = CONFIG.server_url + "/static/media_cdn"


const camStyle = {
      width: 150,
    height: 115
};

class SellAccessories extends React.Component {

    componentWillMount(){
        /*let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }*/   
    }

    clickStillHandle = (e) => {
        e.preventDefault();
        history.push('/sell_brand')
    }

    clickVideoHandle = (e) => {
        e.preventDefault();
        history.push('/sell_video_brand')
    }

    clickLensHandle = (e) => {
        e.preventDefault();
        history.push('/sell_lens_brand')
    }

    clickLightingHandle = (e) => {
        e.preventDefault();
        history.push('/sell_lighting')
    }

    clickCompleteFormHandle = (e) => {
        e.preventDefault();
        history.push('/complete_accessories_form')   
    }

    clickAccessoriesHandle = (e) => {
        e.preventDefault();
        history.push('/sell_accessories')
    }

    clickDroneHandle = (e) => {
        e.preventDefault();
        history.push('/sell_drone')
    }

  render() {
    return (
        <div>
        <ul className="breadcrumb">
            <li><Link to="/">{strings.home}</Link></li>
            <li><Link to="/sell">{strings.sell}</Link></li>
            <li>{strings.accessories}</li>
        </ul>
        <div className="acc-back">
            <div className=" bg-company-orange">
              <ul className="d-flex mx-auto justify-content-center nomarge zeroleftpadding">
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickStillHandle}><span>{strings.photography_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickLensHandle}><span>{strings.lenses}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickVideoHandle}><span>{strings.video_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickLightingHandle}><span>{strings.lighting}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4 active">
                  <a className="awhite" href="" onClick={this.clickAccessoriesHandle}><span>{strings.accessories}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                    <a className="awhite" href="" onClick={this.clickDroneHandle}><span>{strings.drone}</span></a>
                </li>
              </ul>
            
          </div>

        <div className="brand-header text-center">
            <span>{strings.accessories}</span>
        </div>
        
            <div className="category-body m-2" dir="ltr">
                <h4 className="text-center card-title choose-cat">{strings.choose_category}</h4>
                <br />
                <br />
                <br />
                
                    <div className="row lighting-category">
                        <div id="choose-category" className="col text-center">
                            <img src={images_url + "/images/light_modifier.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_accessories_form',     
                                'state' : {
                                    'showBrand' : false,
                                    'category' : "Light Modifiers"
                                }
                            })}}/><br />
                            <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_accessories_form',     
                                'state' : {
                                    'showBrand' : false,
                                    'category' : "Light Modifiers"
                                }
                            })}}>{strings.light_modifiers}</button>
                        </div>
                        <div id="choose-category" className="col text-center">
                            <img src={images_url + "/images/light_stand.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_accessories_form',     
                                'state' : {
                                    'showBrand' : false,
                                    'category' : "Light Stand and Mounting"
                                }
                            })}}/><br />
                            <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_accessories_form',     
                                'state' : {
                                    'showBrand' : false,
                                    'category' : "Light Stand and Mounting"
                                }
                            })}}>{strings.light_and_stand_mounting}</button>
                        </div>
                        <div id="choose-category" className="col text-center">
                            <img src={images_url + "/images/power_and_cables.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_accessories_form',     
                                'state' : {
                                    'showBrand' : false,
                                    'category' : "Powers & Cables"
                                }
                            })}}/><br />
                            <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_accessories_form',     
                                'state' : {
                                    'showBrand' : false,
                                    'category' : "Powers & Cables"
                                }
                            })}}>{strings.power_cables}</button>
                        </div>
                        
                    </div>
                    <br />
                    <br />
                    <br />
                    <div className="row lighting-category">
                        <div id="choose-category" className="col text-center">
                            <img src={images_url + "/images/radio_and_optical_slaves.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_accessories_form',     
                                'state' : {
                                    'showBrand' : false,
                                    'category' : "Radio & Optical Slave"
                                }
                            })}}/><br />
                            <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_accessories_form',     
                                'state' : {
                                    'showBrand' : false,
                                    'category' : "Radio & Optical Slave"
                                }
                            })}}>{strings.radio_optical_slave} </button>
                        </div>
                        <div id="choose-category" className="col text-center">
                            <img src={images_url + "/images/light_meters.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_accessories_form',     
                                'state' : {
                                    'showBrand' : false,
                                    'category' : "Light Meters"
                                }
                            })}}/><br />
                            <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_accessories_form',     
                                'state' : {
                                    'showBrand' : false,
                                    'category' : "Light Meters"
                                }
                            })}}>{strings.light_meters}</button>
                        </div>
                        <div id="choose-category" className="col text-center">
                            <img src={images_url + "/images/light_cases.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_accessories_form',     
                                'state' : {
                                    'showBrand' : false,
                                    'category' : "Light Cases"
                                }
                            })}}/><br />
                            <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_accessories_form',     
                                'state' : {
                                    'showBrand' : false,
                                    'category' : "Light Cases"
                                }
                            })}}>{strings.light_cases}</button>
                        </div>
                    </div>
                    <br />
                    <br />
                    <br />
                    <div className="row lighting-category">
                        <div id="choose-category" className="col text-center">
                            <img src={images_url + "/images/camera_bags.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_accessories_form',     
                                'state' : {
                                    'showBrand' : false,
                                    'category' : "Camera Bags"
                                }
                            })}}/><br />
                            <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_accessories_form',     
                                'state' : {
                                    'showBrand' : false,
                                    'category' : "Camera Bags"
                                }
                            })}}>{strings.camera_bags}</button>
                        </div>
                        <div id="choose-category" className="col text-center">
                            <img src={images_url + "/images/tripods.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_accessories_form',     
                                'state' : {
                                    'showBrand' : false,
                                    'category' : "Tripods"
                                }
                            })}}/><br />
                            <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_accessories_form',     
                                'state' : {
                                    'showBrand' : false,
                                    'category' : "Tripods"
                                }
                            })}}>{strings.tripods}</button>
                        </div>
                        <div id="choose-category" className="col text-center">
                            <img src={images_url + "/images/others.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_accessories_form',     
                                'state' : {
                                    'showBrand' : false,
                                    'category' : "Others"
                                }
                            })}}/><br />
                            <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_accessories_form',     
                                'state' : {
                                    'showBrand' : false,
                                    'category' : "Others"
                                }
                            })}}>{strings.others}</button>
                        </div>
                    </div>
                
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
        </div>
        </div>
    )
  }
}

export default connect()(SellAccessories);