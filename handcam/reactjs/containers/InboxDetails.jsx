import React from 'react'
import { connect } from 'react-redux'
import {Link} from 'react-router-dom'
import { changeMessageStatusAction, sendMessageAction } from '../actions'
import moment from 'moment'
import { strings } from '../lib/strings';

const divStyle = {
	  marginTop: '10px'
	};

const cord = {
	  border: '1px solid gray'
	};

const camStyle = {
	  width: 200,
    height: 150
	};


class InboxDetails extends React.Component{

	constructor(props){
		super(props);
		this.state = {
			messageid : this.props.location.state.id,
            messageParent : this.props.location.state.parent,
			messageBody : this.props.location.state.body,
            messageSubject : "",
			messageSender : this.props.location.state.sender,
			messageSent_at : this.props.location.state.sent_at,
            messages : [],
            inboxMessages: [],
            sentMessages: [],
            conversation: [],
            messageValue: '',
            messageSent: '',
            fetching: false   
        }
        this.handleMessageChange = this.handleMessageChange.bind(this);
        this.handleMessageSubmit = this.handleMessageSubmit.bind(this);
	}

	componentDidMount(){
		const { dispatch } = this.props;
        let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }

        

        dispatch(changeMessageStatusAction.changeStatus(this.state.messageid)).then( () => {
            this.setState({fetching: true})
            this.setState({ messages: this.props.inboxReducer.inbox_messages})
            this.setState({ inboxMessages: this.props.inboxReducer.inbox_messages})
            this.setState({ sentMessages: this.props.inboxReducer.sent_messages})
            this.setState({ conversation: this.props.inboxReducer.conversation})
            this.setState({ messageSubject: this.props.inboxReducer.subject})
            this.setState({ unread_count: this.props.inboxReducer.unread_count})
            
        })
	}

	handleMessageChange(e) {
        this.setState({messageValue: e.target.value});
    }

    handleMessageSubmit(e) {
        e.preventDefault();
        const { messageValue, messageSender, messageSubject, messageParent } = this.state
        const { dispatch } = this.props;

        let body = new FormData()
        body.append('parent', messageParent)
        body.append('subject', messageSubject)
        body.append('body', messageValue)
        body.append('recipient', messageSender)
        if (messageValue && messageSender){
            dispatch(sendMessageAction.sendMessageToSeller(body)).then( () => {
                
                if (this.props.messageSent.status_code==201){
                	
                	this.setState({ messageSent: this.props.messageSent.detail})
                    dispatch(changeMessageStatusAction.changeStatus(this.state.messageid)).then( () => {
                        this.setState({fetching: true})
                        this.setState({ messages: this.props.inboxReducer.inbox_messages})
                        this.setState({ inboxMessages: this.props.inboxReducer.inbox_messages})
                        this.setState({ sentMessages: this.props.inboxReducer.sent_messages})
                        this.setState({ conversation: this.props.inboxReducer.conversation})
                        this.setState({ messageSubject: this.props.inboxReducer.subject})
                        this.setState({ unread_count: this.props.inboxReducer.unread_count})
                        this.setState({messageValue: ""});
                    })

                }else{
                	
                	this.setState({ messageSent: this.props.messageSent})
                }
        })
        }
    }


    renderFlashMessages(){

        if(this.state.messageSent){
            return(
                <div className="alert alert-success">
                    { this.state.messageSent }
                </div>
            )
        }
    }

    renderMessage(){
    	const { messages } = this.state
    	let message
    }

    renderConversation(){
        return (
            <div>
            {this.state.fetching && this.state.conversation.map((inbox, i) => {
                return(
                    <div className="inbox-container">
                        <hr />
                        <div>
                            <table width="100%">
                                <tbody>
                                <tr>
                                    <td width="15%" className="blue-font">{strings.sender}:</td>
                                    <td width="85%">{ inbox.sender.user.username}</td> 
                                </tr>
                                <tr>
                                    <td width="15%" className="blue-font">{strings.sent_at}:</td>
                                    <td width="85%" style={{fontSize: "small"}}>{moment(inbox.sent_at).format('MMMM Do YYYY, h:mm:ss a')}</td> 
                                </tr>
                                <tr>
                                    <td width="15%" className="blue-font">{strings.message}:</td>
                                    <td width="85%" style={{fontSize: "large"}}><i>{ inbox.body }</i></td> 
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                )
            })}
            </div>
        )
    }

	render() {
		return (
            <div>
                
                <ul className="breadcrumb">
                      <li><Link to="/">{strings.home}</Link></li>
                      <li><Link to="/message">{strings.inbox}</Link></li>
                      <li>{strings.inbox_details}</li>
                </ul>
                
    			<div className='container'>
                    <div className={this.props.rtlconv == "rtl" ? ' righttoleft' : ''}>
                        <h2 style={divStyle}>{strings.inbox_details}</h2>
                        <h3 className="box-title">{ this.state.messageSubject }</h3>
                    </div>
                    <div className="inbox-container">
                        {this.state.fetching && this.state.conversation.map((inbox, i) => {
                            return(
                                <div key={inbox.id}>
                                    <hr />   
                                    <table width="100%">
                                        <tbody>
                                        <tr>
                                            <td width="15%" className="blue-font">{strings.sender}:</td>
                                            <td width="85%" className="message-unread">{ inbox.sender.user.username}</td> 
                                        </tr>
                                        <tr>
                                            <td width="15%" className="blue-font">{strings.sent_at}:</td>
                                            <td width="85%"style={{fontSize: "small"}} >{moment(inbox.sent_at).format('MMMM Do YYYY, h:mm:ss a')}</td> 
                                        </tr>
                                        <tr>
                                            <td width="15%" className="blue-font">{strings.message}:</td>
                                            <td width="85%" style={{fontSize: "large"}}>{ inbox.body }</td> 
                                        </tr>
                                        </tbody>
                                    </table> 
                                </div>
                            )   
                        })}

                    </div>

                    

                    <br />
                    <br />
                    <br />
    				<form className={"form-control" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')} onSubmit={this.handleMessageSubmit}>
                        <div className="form-group">
                        <label>
                            {strings.reply}
                        </label>
                        </div>
                        <div className="form-group">
                            <textarea className="form-control replyBox" value={this.state.messageValue} onChange={this.handleMessageChange} />
                        </div>
                        <div className="form-group">
                            <button className="btn btn-primary">{strings.send}</button>
                        </div> 

                        { this.renderFlashMessages() }
                    </form>
    			</div>
                <br />
                <br />
                <br />
                <br />
                <br />
            </div>
		)
	}
}

function mapStatetoProps(state){
	return {
        messageSent: state.messageSent,
        inboxReducer: state.inboxReducer,
        rtlconv: state.rtlconv
    };
}

export default connect(mapStatetoProps)(InboxDetails)
