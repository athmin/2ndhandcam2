import React from 'react'
import { connect } from 'react-redux'
import {Link} from 'react-router-dom'
import { editLensForsaleAction, removeLensPhotoAction } from '../actions';
import ImageUploader from 'react-images-upload';
import SellForm from '../components/SellForm'
import { history } from '../store/configureStore'
import OverlayLoader from 'react-loading-indicator-overlay/lib/OverlayLoader'
import { strings } from '../lib/strings';


import Dropzone from 'react-dropzone';


/*const camStyle = {
      width: 400,
    height: 267
    };*/
const camStyle = {
    "max-width": "100%",
    height: "auto"
    };

const uploadStyle = {
    "position": "relative", 
    "display": "flex", 
    "alignItems": "center", 
    "justifyContent": "center", 
    "flexWrap": "wrap", 
    width: "100%",
};



class EditLensDetail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          
            actual_photos: [],
            id: this.props.location.state.id,
            item: this.props.location.state.item,
            brand: this.props.location.state.brand,
            price: this.props.location.state.price,
            model: this.props.location.state.model,
            type: this.props.location.state.type,
            status: this.props.location.state.status,
            condition: this.props.location.state.condition,
            comments: this.props.location.state.comments,
            warranty: this.props.location.state.warranty,
            //actual_photos: this.props.location.state.actual_photos,
            uploaded_photos: this.props.location.state.actual_photos ? this.props.location.state.actual_photos : [],
            actual_photos:[],
            flashMessage: null,

            with_camera: "No",

            submitted: false,
            priceError: false,
            warrantyError: false,
            conditionError: false,
            actualPhotoError: false,
            spinner: false,
            showSizeError: false,
        }

        this.onDrop = this.onDrop.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.handleSelectCondition = this.handleSelectCondition.bind(this);
        this.handleSelectWarranty = this.handleSelectWarranty.bind(this);
    }

    componentWillMount(){
        let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }   

        let viewportmeta = document.querySelector('meta[name="viewport"]');
        if(viewportmeta===null){
            viewportmeta = document.createElement("meta");
            viewportmeta.setAttribute("name","viewport");
            document.head.appendChild(viewportmeta);
              
            viewportmeta = document.querySelector('meta[name="viewport"]');
        }
        viewportmeta.setAttribute('content', 'width=1470, initial-scale=0.3, maximum-scale=1.0');
    }

    componentDidMount(){
        window.scrollTo(0, 0)
    }

    handleSelect(e) {
        this.setState({with_camera: e.target.value});
        
    }

    handleSelectCondition(e) {
        this.setState({condition: e.target.value});
        
    }

    handleSelectWarranty(e) {
        this.setState({warranty: e.target.value});
        
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        
    }


    onDrop(picture) {
        
        this.setState({
            actual_photos: picture,
            actualPhotoError: false,
            showSizeError: false
        });
    }

    clickStillHandle = (e) => {
        e.preventDefault();
        history.push('/sell_brand')
    }

    clickVideoHandle = (e) => {
        e.preventDefault();
        history.push('/sell_video_brand')
    }

    clickLensHandle = (e) => {
        e.preventDefault();
        history.push('/sell_lens_brand')
    }

    clickLightingHandle = (e) => {
        e.preventDefault();
        history.push('/sell_lighting')
    }

    clickAccessoriesHandle = (e) => {
        e.preventDefault();
        history.push('/sell_accessories')
    }



    handleSubmit = (e) => {
        e.preventDefault()
        const { id, item, brand, model, type, price, with_camera, condition, warranty, actual_photos, submitted, comments } = this.state
        const { dispatch } = this.props;
        
        
        let body = new FormData()
        body.append('pk', id)
        body.append('price', price)
        body.append('condition', condition)
        body.append('comments', comments)
        body.append('warranty', warranty) 
        body.append('with_camera', with_camera)
        body.append('status', 'Active')
        body.append('item', item.id)
        Object.keys(actual_photos).forEach(( key, i ) => {
            body.append('actual_photos', actual_photos[key]);
        });

        if (price && warranty && condition){ 
            for (var pair of body.entries()){
                
            }
            this.setState({ spinner : true })
            dispatch(editLensForsaleAction.editlensforsale(body)).then( () => {
                this.setState({ showSizeError: true })
                this.setState({ spinner: false })
            })
        }

        if (price == ""){
            this.setState({ priceError: true})
        }else{
            this.setState({ priceError: false})
        }

        if (condition == ""){
            this.setState({ conditionError: true})
        }else{
            this.setState({ conditionError: false})
        }

        if (warranty == ""){
            this.setState({ warrantyError: true})
        }else{
            this.setState({ warrantyError: false})
        }

        /*if (actual_photos.length == 0){
            this.setState({ actualPhotoError: true})
        }*/
    }

    renderPackage() {
        const { category, submitted, campackage } = this.state

        
        return (
            <div className={'form-group' + (submitted && !with_camera ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                <label htmlFor="campackage">With Camera</label>
                <select className="form-control" 
                    name="with_camera" 
                    value={this.state.with_camera}
                    onChange={(e) => this.handleSelect(e)}
                >
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                </select>
            </div>
        )
        

    }

    removeUploadedPhoto(photo){
        const { dispatch } = this.props;
      
        dispatch(removeLensPhotoAction.removelensphoto(this.state.id, photo.id)).then( () => {
            this.setState({ flashMessage: this.props.removephoto.status}) 
            var array = this.state.uploaded_photos.filter(function(s) { return s != photo });
            this.setState({uploaded_photos: array });
        });
    }

    renderUploadedPhotos() {
      
        return (
            <div>
                {this.renderFlashMessages()}

                <h4>Uploaded Photos</h4>
                <div className="fileContainer">
                    <div className="uploadPicturesWrapper">
                        <div style={uploadStyle}>
                            { this.state.uploaded_photos.map((photo, index) => {
                                return (
                                    <div className="uploadPictureContainer" key={index}>
                                        <div className="deleteUploadedImage" onClick={() => { this.removeUploadedPhoto(photo) }}>X</div>
                                        <img src={photo.watermarked} className="uploadPicture" alt="preview"/>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderFlashMessages(){

        if(this.state.flashMessage){
            return(
                <div className="alert alert-success">
                    { this.state.flashMessage }
                </div>
            )
        }
    }


    renderPhotoSizeError(){
        
        if (this.state.showSizeError){
            if (Object.keys(this.props.lens_sell).length > 0) {
                   
                return(
                    <div className={"alert alert-danger" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>{strings.please_upload_decent_photo_size}</div>
                )
            }
        }
    }


    renderSellForm(){
        let token = localStorage.getItem('token')
        const { price, condition, warranty, actual_photos, submitted, file, with_camera, comments} = this.state;
        if(!token){
            return <p>{strings.please_login_to_sell}</p>
        }else{
           return  <form name="form" onSubmit={this.handleSubmit}>
                        <div className={'form-group' + (submitted && !price ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="price">{strings.price} ({strings.should_be_in_number})</label>
                            <input type="text" className="form-control form-price" name="price" value={price} onChange={this.handleChange} />
                            {this.state.priceError ?
                                <div className="errorMessage">{strings.price_is_required}</div>
                                : null
                            }
                        </div>
                        
                        <div className={'form-group' + (submitted && !warranty ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="warranty">{strings.warranty}</label>
                            <select className="form-control form-warranty" 
                                name="warranty" 
                                value={warranty}
                                onChange={(e) => this.handleSelectWarranty(e)}
                            >
                                <option value="">{strings.sellect_warranty}</option>
                                <option value="No Warranty">{strings.no_warranty}</option>
                                <option value="One (1) Week">{strings.one_week}</option>
                                <option value="Two (2) Weeks">{strings.two_weeks}</option>
                                <option value="One (1) Month">{strings.one_month}</option>
                            </select>
                            {this.state.warrantyError ?
                                <div className="errorMessage">{strings.warranty_is_required}</div>
                                : null
                            }
                        </div>

                        <div className={'form-group' + (submitted && !condition ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="condition">{strings.condition}</label>
                            <select className="form-control form-condition" 
                                name="condition" 
                                value={condition}
                                onChange={(e) => this.handleSelectCondition(e)}
                            >
                                <option value="">{strings.select_conditions}</option>
                                <option value="Considered new - barely used">{strings.considered_new_barely_used}</option>
                                <option value="Brand new - not used">{strings.brand_new_not_used}</option>
                                <option value="Used for demo only">{strings.used_for_demo_only}</option>
                                <option value="Show signs of use but works perfectly">{strings.show_sign_of_use_but_works_perfectly}</option>
                                <option value="Shows some wear">{strings.shows_some_wear}</option>
                                <option value="Manufacturer Defect">{strings.manufacturer_defect}</option>
                                <option value="Broken but still works fine">{strings.broken_but_still_works_fine}</option>
                                <option value="Broken and need some maintenance">{strings.broken_and_need_some_maintenance}</option>
                                <option value="Other - please specify in the comment box below">{strings.others_please_specify}</option>
                            </select>
                            {this.state.conditionError ?
                                <div className="errorMessage">{strings.condition_is_required}</div>
                                : null
                            }
                        </div>

                        <div className={'form-group' + (submitted && !comments ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                           <label htmlFor="comment">{strings.comments}</label>
                            <textarea className="form-control replyBox" name="comments" value={comments} onChange={this.handleChange} />
                            {submitted && !comments &&
                                <div className="errorMessage">{strings.comment_is_required}</div>
                            }
                        </div>

                        {this.renderUploadedPhotos()}
                        
                        <ImageUploader
                            withIcon={true}
                            buttonText={strings.choose_images}
                            onChange={this.onDrop}
                            imgExtension={['.jpg', '.gif', '.png', '.jpeg','.JPG', '.GIF', '.PNG', '.JPEG']}
                            maxFileSize={5242880}
                            buttonType='button'
                            withPreview={true}/>

                        {this.state.actualPhotoError ? <div className="alert alert-danger">{strings.please_upload_accessories_photo}</div> : null}    
                        { this.state.spinner ?
                        
                            <OverlayLoader 
                              color={'blue'} // default is white
                              loader="ScaleLoader" // check below for more loaders
                              text={strings.uploading_your_item_please_wait}
                              active={this.state.spinner} 
                              backgroundColor={'white'} // default is black
                              opacity=".4" // default is .9  
                            >
                                <div className="spinner">
                                </div>
                            </OverlayLoader>
                        
                        : null}
                        
                        <div className="form-group text-center">
                            <button className="btn btn-lg btn-primary">{strings.submit}</button>
                        </div>
                    </form> 
        }
    }


    

  render() {
        const { state } = this.props.location
        
        return (
            <div>
                <ul className="breadcrumb">
                    <li><Link to="/">{strings.home}</Link></li>
                    <li><Link to="/dashboard">{strings.dashboard}</Link></li>
                    <li>{state.model} - Edit Lens</li>
                </ul>
                <div className=" bg-company-orange">
                    <ul className="d-flex mx-auto justify-content-center nomarge">
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickStillHandle}><span>{strings.photography_cameras}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4 active">
                            <span>{strings.lenses}</span>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickVideoHandle}><span>{strings.video_cameras}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickLightingHandle}><span>{strings.lighting}</span></a>
                        </li>
                        <li className="list-inline-item ml-4 mr-4">
                            <a className="awhite" href="" onClick={this.clickAccessoriesHandle}><span>{strings.accessories}</span></a>
                        </li>
                    </ul>
                </div>
                <div className="card m-2">
                    <div className={"card-header" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                        <h3 className="mb-0" >{state.brand} {state.model} {state.type}</h3>
                    </div>
                    <div className="card-body">
                        <div className="row">
                            <div className="col-4">
                                <img className="card-img-left" src={state.image} style={camStyle}/><br />
                            </div>
                            <div className="col mr-5">
                                <p className="text-center">{strings.please_fill_up_the_form_and_the_system_will_autogenerate_other_details}..</p>
                                { this.renderSellForm() }
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <br />
                <br />
            </div>
        )
  }
}

function mapStateToProps(state){
    
    return {
        rtlconv: state.rtlconv,
        removephoto: state.removephoto,
        lens_sell: state.lenssell
    };
}

export default connect(mapStateToProps)(EditLensDetail)