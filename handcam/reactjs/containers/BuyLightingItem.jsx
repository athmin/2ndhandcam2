import * as React from "react";
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'
import "searchkit/theming/theme.scss";
import { LightingGridItem, LightingListItem } from './BuyLightingItems'
import { history } from '../store/configureStore'
import { CustomRefinementListFilter } from '../components/CustomRefinementList'
import { strings } from '../lib/strings';
import FaFilter from 'react-icons/lib/fa/filter'
let CONFIG = require('../lib/config.json')
import {
  SearchBox,
  Hits,
  HitsStats,
  SearchkitProvider,
  CheckboxFilter,
  SearchkitManager,
  SortingSelector,
  HierarchicalMenuFilter,
  RefinementListFilter,
  RangeFilter,
  NumericRefinementListFilter,
  NoHits,
  ViewSwitcherToggle,
  ViewSwitcherHits,
  Pagination,
  SelectedFilters,
  ResetFilters,
  listComponent,
  ItemHistogramList,
  Layout, LayoutBody, LayoutResults,
  SideBar, TopBar,
  ActionBar, ActionBarRow,
  TermQuery,
    FilteredQuery,
    BoolShould, BoolMust
} from "searchkit";

const camStyle = {
  width: 75,
    height: 54,
};
const logoStyle = {
  width: 75,
    height: 25,
};

class BuyLightingItem extends React.Component {

	searchkit:SearchkitManager

    constructor(props) {
        super(props)

        this.state = {
          category: this.props.location.state.category,
          openbrand: true,
          opencategory: true,
          openmodel: true,
          opensensonsize: true,
          opensensortype: true,
          openvideoresolution: true,
          openpackage: true,
          openlcdsize: true,
        }

        const host = CONFIG.server_url + "/es/forsalelighting/for_sale_lighting_index/"
        let cat = this.state.category
        let query_cat = cat.split(" ");
        query_cat = query_cat[0].toLowerCase()
        this.searchkit = new SearchkitManager(host)
        this.searchkit.addDefaultQuery((query)=> {
          //return query.addQuery(BoolMust([TermQuery("category", query_cat)]));
          return query.addQuery(BoolMust([(TermQuery("category", query_cat)), (TermQuery("status", "active"))]));
        })
        
        this.searchkit.translateFunction = (key) => {
          let translations = {
            "pagination.previous":"Previous page",
            "pagination.next":"Next page",
            "id1":"Color",
            "id2": "Red",
            "searchbox.placeholder": strings.search,
            //"hitstats.results_found": {hitCount} + " " + strings.results_found_in + " " + {timeTaken}+ "ms"
          }
          return translations[key]
        }
    }

    componentWillMount(){
        let token = localStorage.getItem('token')
        if (!token){
          history.push('/login_register')
        }   
    }

  clickStillHandle = (e) => {
      e.preventDefault();
      history.push('/buy_item')
  }

  clickVideoHandle = (e) => {
      e.preventDefault();
      history.push('/buy_video_cameras')
  }

  clickLensHandle = (e) => {
      e.preventDefault();
      history.push('/buy_lens')
  }

  clickLightingHandle = (e) => {
      e.preventDefault();
      history.push('/buy_lighting')
  }

  clickAccessoriesHandle = (e) => {
      e.preventDefault();
      history.push('/buy_accessories')
  }

  clickDroneHandle = (e) => {
      e.preventDefault();
      history.push('/buy_drones')
  }

  togglepackage() {
    this.setState({
      openpackage: !this.state.openpackage
    });
  }

  lightingCategory(category){
      
      if((category == "Battery Powered Stobes") || (category == "Power Packs") || (category == "Slaves") || (category == "Monolights")){
          return (
              <ul className="breadcrumb">
                  <li><Link to="/">{strings.home}</Link></li>
                  <li><Link to="/buy">{strings.buy}</Link></li>
                  <li><Link to="/buy_lighting">{strings.lighting}</Link></li>
                  <li><Link to="/buy_strobe_lighting">{strings.strobe_lighting}</Link></li>
                  <li>{category}</li>
                  
              </ul>
          )
      }
      else {
          return (
              <ul className="breadcrumb">
                  <li><Link to="/">{strings.home}</Link></li>
                  <li><Link to="/buy">{strings.buy}</Link></li>
                  <li><Link to="/buy_lighting">{strings.lighting}</Link></li>
                  <li>{category}</li>
              </ul>
          )
      }
  }

  renderLightingCategory(cat) {

     if (cat=="Continuous Lighting"){
        return (strings.continuous_lighting)
     }else if(cat=="Strobe Lighting"){
        return (strings.strobe_lighting)
     }else if(cat=="Flashes On Camera"){
        return (strings.flashes_on_camera)
     }else if(cat=="Monolights"){
        return (strings.monolights)
     }else if(cat=="Battery Powered Stobes"){
        return (strings.battery_powered_strobes)
     }else if(cat=="Power Packs"){
        return (strings.power_packs)
     }else{
        return (strings.slaves)
     } 
  }

  render() {
    return (
      <div>
          <ul className="breadcrumb">
              <li><Link to="/">{strings.home}</Link></li>
              <li><Link to="/buy">{strings.buy}</Link></li>
              <li><Link to="/buy_lighting">{strings.lighting}</Link></li>
              <li>{this.renderLightingCategory(this.state.category)}</li>
          </ul>

          <div className=" bg-company-orange">
            <ul className="d-flex mx-auto justify-content-center nomarge zeroleftpadding">
              <li className="list-inline-item ml-4 mr-4">
                <a className="awhite" href="" onClick={this.clickStillHandle}><span>{strings.photography_cameras}</span></a>
              </li>
              <li className="list-inline-item ml-4 mr-4">
                <a className="awhite" href="" onClick={this.clickLensHandle}><span>{strings.lenses}</span></a>
              </li>
              <li className="list-inline-item ml-4 mr-4">
                <a className="awhite" href="" onClick={this.clickVideoHandle}><span>{strings.video_cameras}</span></a>
              </li>
              <li className="list-inline-item ml-4 mr-4 active">
                <a className="awhite" href="" onClick={this.clickLightingHandle}><span>{strings.lighting}</span></a>
              </li>
              <li className="list-inline-item ml-4 mr-4">
                <a className="awhite" href="" onClick={this.clickAccessoriesHandle}><span>{strings.accessories}</span></a>
              </li>
              <li className="list-inline-item ml-4 mr-4">
                <a className="awhite" href="" onClick={this.clickDroneHandle}><span>{strings.drone}</span></a>
              </li>
            </ul>
          </div>
          
        <div className="brand-header text-center">
            <h1 className="text-center p-5">{strings.lighting} - {this.renderLightingCategory(this.state.category)}</h1>
        </div>

        <SearchkitProvider searchkit={this.searchkit}>
        <Layout size="l">

          <LayoutBody>
          <SideBar>
              <div className="serchtop">
                <div className="text-center">
                  <FaFilter /><span className="colorsearch">{strings.narrow_result}</span>
               </div>
              </div>


              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglepackage.bind(this)}>{strings.country}</h6>
                </div>
                <div className="clanheader">
                  <RefinementListFilter
                    id="country"
                    field="seller_country.keyword"
                    operator="OR"
                    size={10}/>
                </div>    
              </div>    

              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglepackage.bind(this)}>{strings.city}</h6>
                </div>
                <div className="clanheader">
                  <RefinementListFilter
                    id="city"
                    field="seller_city.keyword"
                    operator="OR"
                    size={10}/>
                </div>    
              </div>         
              
              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>  
                    <h6>{strings.brand}</h6>
                </div>
                <div className="clanheader">
                   <RefinementListFilter
                    id="brand"
                    field="brand.keyword"
                    operator="OR"
                    size={20}/>
                </div>
              </div>

              <div>
                  <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                      <h6>{strings.condition}</h6>
                  </div>
                  <div className="clanheader">
                    <RefinementListFilter
                       id="neworused"
                       field="neworused.keyword"
                       operator="OR"
                       size={10}/>
                  </div>
                </div>

              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>  
                    <h6>{strings.price}</h6>
                </div>
                <div className="clanheader">
                  <NumericRefinementListFilter id="price" field="price" options={[
                    {title:"All"},
                    {title:"Under SAR 500", from:0, to:500},
                    {title:"SAR 501 to SAR 750", from:501, to:750},
                    {title:"SAR 751 to SAR 1000", from:751, to:1000},
                    {title:"SAR 1001 to SAR 1500", from:1001, to:1500},
                    {title:"SAR 1501 to SAR 2000", from:1501, to:2000},
                    {title:"Above SAR 2000", from:2001, to:15000}
                  ]}/> 
                </div>
              </div>

              <SortingSelector options={[
                  {label:"Date-desc", field:"date", order:"desc", defaultOption:true},
                  {label:"Date-asc", field:"date", order:"asc"},
                ]}/> 
  
          </SideBar>

          <LayoutResults>
                <div className="search_container">
                  <SearchBox
                    searchOnChange={true}
                    queryOptions={{analyzer:"standard"}}
                    queryFields={["brand", "model"]}/>
                </div>
                <ActionBar>
                  <ActionBarRow>
                      <HitsStats/>
                      <ViewSwitcherToggle/>
                  </ActionBarRow>
                </ActionBar>
          
                <ViewSwitcherHits
                          hitsPerPage={24} highlightFields={["brand","label"]}
                    sourceFilter={["brand",
                      "id",
                      "product_id",
                      "seller",
                      "seller_pk",
                      "seller_location",
                      "seller_country",
                      "seller_city",
                      "seller_location",
                      "seller_phonenumber",
                      "label",
                      "model",
                      "price",
                      "currency",
                      "condition",
                      "actual_photos",
                      "image",
                      "status",
                      "warranty",
                      "category",
                      "comment"
                    ]}
                    hitComponents = {[
                      {key:"grid", title:strings.grid, itemComponent:LightingGridItem},
                      {key:"list", title:strings.list, itemComponent:LightingListItem, defaultOption:true}
                    ]}
                    scrollTo="body"/>
                <NoHits suggestionsField="brand"/>
                <Pagination showNumbers={true}/>
              </LayoutResults>
          </LayoutBody>
            </Layout>
      </SearchkitProvider>
      <br />
      <br />
      <br />
      <br />
      <br />
    </div>
    	
    )
  }
}

function mapStateToProps(state){
    return{
        rtlconv: state.rtlconv
    }
}

export default connect(mapStateToProps)(BuyLightingItem);