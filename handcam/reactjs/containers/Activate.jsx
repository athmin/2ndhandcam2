import React from 'react'
import { connect } from 'react-redux';
import { getUserActivation } from '../actions';


class About extends React.Component {

	constructor(props) {
        super(props);
    }

    componentDidMount(){
    	const { dispatch, location } = this.props
    	if (location.pathname){
    		dispatch(getUserActivation.activateUser(location.pathname))
    	}
    }



	render() {
		return (
			<div className="card m-5 mx-auto col-md-6 col-md-offset-3">
                <div className="card-body">
                    <p className="text-center p-5">Account already confirmed. Please login to continue.</p>
                </div>
            </div>
		)
	}
}

function mapStateToProps(state){
	return{
        activated: state.activated
    }
}



export default connect()(About)