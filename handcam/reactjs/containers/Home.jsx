import React from 'react'
import Slider from 'react-slick'
import {Link} from 'react-router-dom'
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import { history } from '../store/configureStore'
import ImageMapper from 'react-image-mapper';
import { connect } from 'react-redux';
import { strings } from '../lib/strings';
import { userActions } from '../actions';
import { getMessagesAction, getUserProfileAction, getMaintenanceAction } from '../actions';

let CONFIG = require('../lib/config.json')

const images_url = CONFIG.server_url + "/static/media_cdn"

const camStyle = {
    "max-width": "84%",
    height: "auto"
    };

const buyStyle = {
    "background-color": "#3d4041"
    };

const buy = {
  position: "absolute", 
  left: "0%",
  top: "2.86%", 
  width: "38.02%", 
  height: "82.38%", 
  "z-index": "2",
}

const sell = {
  position: "absolute", 
  left: "59.34%",
  top: "0%",
  width: "39.11%",
  height: "86.56%",
  "z-index": "2",
}

const logoHeight = {
  "min-height": "40vw"
}

//var buyArea = [31,13,571,13,573,375,438,378,440,442,36,440]
//var sellArea = [1024,0,1027,89,937,89,948,380,1550,381,1550,0,1548,1]

var buyArea = [414,0,414,69,479,70,483,316,342,316,343,345,0,348,2,0]
var sellArea = [818,3,820,84,771,85,779,307,942,309,945,345,1282,343,1284,0,1281,0,1280,0]


class Home extends React.Component {
  
  constructor(props) {
      super(props);
      this.state = {
          messages: [],
          unread_count : 0,
          expired_items : 0,
          fetching: false,
          profile: '',
          direction: "ltr"
      };


      const { dispatch } = this.props;
      history.listen((location, action) => {
          dispatch(userActions.clearAlert());
      });
  }

  componentWillMount(){
    const { dispatch } = this.props;

    /*var viewportmeta = document.querySelector('meta[name="viewport"]');
    if (viewportmeta) {
        viewportmeta.setAttribute('content', 'width=1470, initial-scale=0.25, maximum-scale=1.0, target-densitydpi=high-dpi');
    }*/

    let viewportmeta = document.querySelector('meta[name="viewport"]');
    if(viewportmeta===null){
        viewportmeta = document.createElement("meta");
        viewportmeta.setAttribute("name","viewport");
        document.head.appendChild(viewportmeta);
        
        viewportmeta = document.querySelector('meta[name="viewport"]');
    }
    viewportmeta.setAttribute('content', 'width=1470, initial-scale=0.25, maximum-scale=1.0');

    //var scale = 'scale(1)'
    //document.body.style.zoom = "100%"
    document.body.style.zoom = screen.logicalXDPI / screen.deviceXDPI;
    //document.body.style.webkitTransform = scale;      // Chrome, Opera, Safari
    //document.body.style.msTransform =  scale;       // IE 9
    //document.body.style.transform = scale;     // General

    if (!this.props.maintenanceMode.hasOwnProperty('mode')){
      dispatch(getMaintenanceAction.getMode()).then( () => {
            if (this.props.maintenanceMode){
                if (this.props.maintenanceMode.mode.status){
                    history.push('/maintenance')
                }
            } 
        })
    }else{
      if (this.props.maintenanceMode.mode.status){
              history.push('/maintenance')
          }
    }
    
    
    this.setState({});
      let token = localStorage.getItem('token')
      if (token){
          dispatch(getMessagesAction.getUserInbox()).then( () => {
          this.setState({fetching: true})
            this.setState({ messages: this.props.inboxReducer.messages})
            this.setState({ unread_count: this.props.inboxReducer.unread_count})
            
        })

        dispatch(getUserProfileAction.userCredentials()).then( () => {
          this.setState({fetching: true})
            this.setState({ profile: this.props.credentials})
        })
    
      }
  }

  clickedBuyArea() {
    history.push("/buy")
  }

  clickedSellArea() {
    history.push("/sell")
  }

  render() {
  	let settings = {
      dots: true,
      infinite: true,
      speed: 500,
      autoplay: true,
      adaptiveHeight: true,
      slidesToShow: 1,
      slidesToScroll: 1
    }
  
    return (
    	<div className={'container-fluid' + (this.props.rtlconv=='rtl' ? ' mainpagertl':' mainpageltr')}>
    		
        <div style={logoHeight}>
          <div className="main-logo" >
          { this.props.rtlconv == "rtl" ?
            <img src={images_url + '/images/Arabic-2.png'} style={camStyle} useMap="#image-map"/>
            :
            <img src={images_url + '/images/English-2.png'} style={camStyle} useMap="#image-map"/>
          }  
            
            <map name="image-map">
                <area target="" style={buyStyle} onClick={()=> this.clickedBuyArea()} alt="Buy" title="buy"  coords={buyArea} shape="poly" />
                <area target="" onClick={()=> this.clickedSellArea()} alt="Sell" title="sell" coords={sellArea} shape="poly" />
            </map>
            
            
          </div>
          
        </div>
        <br />
        <br />
        <div className="siteinfo">
          <hr className="infoline"/>
          <div className={"row" + (this.props.rtlconv=='rtl' ? ' fivepxrightpad':' fivepxleftpad')}>
            <div className={"col-md-1" + (this.props.rtlconv=='rtl' ? ' righttoleft':' col7percent')}>
              <p className="footer-title">{strings.about_us}</p>
              <ul className={"footer-list" + (this.props.rtlconv=='rtl' ? ' zerorightpadding':'')}>
                <li className="footer-li"><Link to="/about_us" className="infolink">{strings.who_are_we}</Link></li>
                <li className="footer-li"><Link to="/website_policy" className="infolink">{strings.website_policies}</Link></li>
                <li className="footer-li"><Link to="/terms_of_use" className="infolink">{strings.terms_of_use}</Link></li>
              </ul>
            </div>
            <div className={"col-md-1" + (this.props.rtlconv=='rtl' ? ' col7percent righttoleft':'')}>
              <p className="footer-title">{strings.contact_us}</p>
              <ul className={"footer-list" + (this.props.rtlconv=='rtl' ? ' zerorightpadding':'')}>
                <li className="footer-li"><Link to="/customer_services" className="infolink">{strings.customer_services}</Link></li>
                <li className="footer-li"><Link to="/suggestions" className="infolink">{strings.add_your_suggestion}</Link></li>
              </ul>
            </div>
            <div className={"col-md-2" + (this.props.rtlconv=='rtl' ? ' righttoleft':'')}>
              <p className="footer-title">{strings.important_information}</p>
              <ul className={"footer-list" + (this.props.rtlconv=='rtl' ? ' zerorightpadding':'')}>
                <li className="footer-li"><Link to="/website_commission" className="infolink">{strings.website_commission}</Link></li>
              </ul>
            </div>
          </div>
        </div>
       
    	</div>
    )
  }
}

function mapStatetoProps(state){
    return {
        loginToken: state.loginToken,
        loginTokenError: state.loginTokenError,
        inboxReducer: state.inboxReducer,
        dash: state.dash,
        credentials: state.credentialsReducer,
        rtlconv: state.rtlconv,
        maintenanceMode: state.maintenanceMode
    }
}

export default connect(mapStatetoProps)(Home)
