import React from 'react'
import { history } from '../store/configureStore'

const divStyle = {
	  marginTop: '150px'
	};

export default class Logout extends React.Component {

	componentWillMount(){
		localStorage.removeItem('token');
    localStorage.removeItem('profile');
    localStorage.removeItem('profile_pk');
    localStorage.removeItem('direction');
		history.push('/login')
	}

  render() {
    return (
    	<div className='container'>
    		<h2 style={divStyle}>Logout</h2>
    	</div>
    )
  }
}