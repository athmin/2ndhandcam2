import React from 'react'
import { history } from '../store/configureStore'
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'
import { strings } from '../lib/strings';
let CONFIG = require('../lib/config.json')

const images_url = CONFIG.server_url + "/static/media_cdn"


const camStyle = {
      width: 200,
    height: 150
};

class SellLighting extends React.Component {

    componentWillMount(){
        /*let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }*/   
    }

    clickContinuosHandle(){
        history.push('/sell_brand')
    }

    clickStrobeHandle(){
        history.push('/sell_strobe_lighting')
    }

    clickFlashHandle(){
        history.push('/sell_video_brand')
    }

    clickStillHandle = (e) => {
        e.preventDefault();
        history.push('/sell_brand')
    }

    clickVideoHandle = (e) => {
        e.preventDefault();
        history.push('/sell_video_brand')
    }

    clickLensHandle = (e) => {
        e.preventDefault();
        history.push('/sell_lens_brand')
    }

    clickLightingHandle = (e) => {
        e.preventDefault();
        history.push('/sell_lighting')
    }

    clickAccessoriesHandle = (e) => {
        e.preventDefault();
        history.push('/sell_accessories')
    }

    clickDroneHandle = (e) => {
        e.preventDefault();
        history.push('/sell_drone')
    }

    clickLightingHideBrandHandle = (e) => {
        e.preventDefault();
        history.push(
            {'pathname':'/complete_lighting_form',     
            'state' : {
                'showBrand' : false,
            }
        })   
    }

  render() {
    return (
        <div>
        <ul className="breadcrumb">
            <li><Link to="/">{strings.home}</Link></li>
            <li><Link to="/sell">{strings.sell}</Link></li>
            <li>{strings.lighting}</li>
        </ul>
        <div className="lighting-back">
            <div className=" bg-company-orange">
              <ul className="d-flex mx-auto justify-content-center nomarge zeroleftpadding">
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickStillHandle}><span>{strings.photography_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickLensHandle}><span>{strings.lenses}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickVideoHandle}><span>{strings.video_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4 active">
                  <a className="awhite" href="" onClick={this.clickLightingHandle}><span>{strings.lighting}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickAccessoriesHandle}><span>{strings.accessories}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                    <a className="awhite" href="" onClick={this.clickDroneHandle}><span>{strings.drone}</span></a>
                </li>
              </ul>
            
          </div>

        <div className="brand-header text-center">
            <span>{strings.lighting}</span>
        </div>
        
            <div className="category-body m-2" dir="ltr">
                <h4 className="text-center card-title choose-cat">{strings.continuous_and_strobe_lighting}</h4>
                <div className="row lighting-category">
                    <div className="col text-center">
                        <img src={images_url + "/images/continuous.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                            {'pathname':'/complete_lighting_form',     
                            'state' : {
                                'showBrands' : false,
                                'category' : "Continuous Lighting"
                            }
                        })}}/><br />
                        <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                            {'pathname':'/complete_lighting_form',     
                            'state' : {
                                'showBrands' : false,
                                'category' : "Continuous Lighting"
                            }
                        })}}>{strings.continuous_lighting}</button>
                    </div>
                    <div className="col text-center">
                        <img src={images_url + "/images/strobe.png"} style={camStyle} className="imageBuySell" onClick={this.clickStrobeHandle}/><br />
                        <button className="btn btn-warning" onClick={this.clickStrobeHandle}>{strings.strobe_lighting}</button>
                    </div>
                    <div className="col text-center">
                        <img src={images_url + "/images/flash.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                            {'pathname':'/complete_lighting_form',     
                            'state' : {
                                'showBrands' : false,
                                'category' : "Flashes and On Camera Lighting"
                            }
                        })}}/><br />
                        <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                            {'pathname':'/complete_lighting_form',     
                            'state' : {
                                'showBrands' : false,
                                'category' : "Flashes On Camera"
                            }
                        })}}>{strings.flashes_on_camera}</button>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <br />
            <br />
        </div>
        </div>
    )
  }
}

export default connect()(SellLighting);