import * as _ from "lodash";
import * as React from "react";
import { connect } from 'react-redux';
import { history } from '../store/configureStore'
import { strings } from '../lib/strings';



const AccessoriesGridItem = (props)=> {
  const {bemBlocks, result} = props
  const source:any = _.extend({}, result._source, result.highlight)
  var _a = props, bemBlock = _a.bemBlock, hasFilters = _a.hasFilters;
  return (
    <div className={bemBlocks.item().mix(bemBlocks.container("item"))} data-qa="hit">
      <a href="" onClick={(e)=>{e.preventDefault(); 
        history.push({'pathname':'/buy_accessories_details', 
          'state':{'id':source.id,
          'product_id':result._source.product_id,
          'label':result._source.label,
          'seller':result._source.seller,
          'seller_pk':result._source.seller_pk,
          'seller_country':result._source.seller_country,
          'seller_city':result._source.seller_city,
          'seller_location':result._source.seller_location,
          'seller_phonenumber':result._source.seller_phonenumber,
          'condition':result._source.condition,
          'price':result._source.price,
          'currency':result._source.currency,
          'actual_photos':result._source.actual_photos,
          'image':result._source.image,
          'status':result._source.status,
          'warranty':result._source.warranty,
          'company':result._source.brand,
          'category':result._source.category,
          'model':result._source.model,
          'comment':result._source.comment
        }})}}>
        <img data-qa="poster" className={bemBlocks.item("poster")} src={result._source.image} width="200" height="133"/>
        <div data-qa="title" className={bemBlocks.item("title")} dangerouslySetInnerHTML={{__html:source.label}}>
        </div>
      </a>
    </div>
  )
}

const AccessoriesListItem = (props)=> {
  const direction = localStorage.getItem('direction')
  const {bemBlocks, result} = props
  const source:any = _.extend({}, result._source, result.highlight)
  var _a = props, bemBlock = _a.bemBlock, hasFilters = _a.hasFilters;

  var transCondition = ""

  if (result._source.condition=="Considered new - barely used"){
      transCondition = strings.considered_new_barely_used
  }else if (result._source.condition=="Brand new - not used"){
      transCondition = strings.brand_new
  }else if (result._source.condition=="Used for demo only"){
      transCondition = strings.used_for_demo_only
  }else if (result._source.condition=="Show signs of use but works perfectly"){
      transCondition = strings.show_sign_of_use_but_works_perfectly
  }else if (result._source.condition=="Shows some wear"){
      transCondition = strings.shows_some_wear
  }else if (result._source.condition=="Manufacturer defect"){
      transCondition = strings.manufacturer_defect
  }else if (result._source.condition=="Broken but still works fine"){
      transCondition = strings.broken_but_still_works_fine
  }else if (result._source.condition=="Broken and need some maintenance"){
      transCondition = strings.broken_and_need_some_maintenance
  }else {
      transCondition = strings.others_please_specify
  }
  
  return ( 
    <div className={bemBlocks.item().mix(bemBlocks.container("item"))} data-qa="hit">
      <div className={bemBlocks.item("poster")}>
        <a href="" onClick={(e)=>{e.preventDefault(); 
        history.push({'pathname':'/buy_accessories_details', 
          'state':{'id':source.id,
          'product_id':result._source.product_id,
          'label':result._source.label,
          'seller':result._source.seller,
          'seller_pk':result._source.seller_pk,
          'seller_country':result._source.seller_country,
          'seller_city':result._source.seller_city,
          'seller_location':result._source.seller_location,
          'seller_phonenumber':result._source.seller_phonenumber,
          'condition':result._source.condition,
          'price':result._source.price,
          'currency':result._source.currency,
          'actual_photos':result._source.actual_photos,
          'image':result._source.image,
          'status':result._source.status,
          'warranty':result._source.warranty,
          'company':result._source.brand,
          'category':result._source.category,
          'model':result._source.model,
          'comment':result._source.comment
        }})}}><img data-qa="poster" src={result._source.image} width="200" height="auto"/></a>
      </div>
      <div className={bemBlocks.item("details") + (direction == '"rtl"' ? ' righttoleft' : '')}>
        <a href="" onClick={(e)=>{e.preventDefault(); 
        history.push({'pathname':'/buy_accessories_details', 
          'state':{'id':source.id,
          'product_id':result._source.product_id,
          'label':result._source.label,
          'seller':result._source.seller,
          'seller_pk':result._source.seller_pk,
          'seller_country':result._source.seller_country,
          'seller_city':result._source.seller_city,
          'seller_location':result._source.seller_location,
          'seller_phonenumber':result._source.seller_phonenumber,
          'condition':result._source.condition,
          'price':result._source.price,
          'currency':result._source.currency,
          'actual_photos':result._source.actual_photos,
          'image':result._source.image,
          'status':result._source.status,
          'warranty':result._source.warranty,
          'company':result._source.brand,
          'category':result._source.category,
          'model':result._source.model,
          'comment':result._source.comment
        }})}}><h2 className={bemBlocks.item("title")} dangerouslySetInnerHTML={{__html:source.brand +" - "+ source.model}}></h2></a>
        <hr className="bigHr" />
        <div>
          <span className={bemBlocks.item("condition")} dangerouslySetInnerHTML={{__html:strings.item_condition+': '}}></span>
          <span className={bemBlocks.item("conditiontext")} dangerouslySetInnerHTML={{__html:transCondition}}></span>
        </div>
      </div>
      <div className={bemBlocks.item("details") + (direction == '"rtl"' ? ' righttoleft' : '')}>
        <div className={bemBlocks.item("detailprice price-border")}>
          <div className="row">
            <div className="col-md-4">
              <span className={bemBlocks.item("sem")} dangerouslySetInnerHTML={{__html:strings.price +':'}}></span>
            </div>  
            <div className={direction == '"rtl"' ? 'zeropadleftright' : ''}>
              <span className={bemBlocks.item("seme") + (direction == '"rtl"' ? ' span-left' : '')} dangerouslySetInnerHTML={{__html:source.currency + " " }}></span>
              <span className={bemBlocks.item("seme") + (direction == '"rtl"' ? ' span-left' : '')} dangerouslySetInnerHTML={{__html:source.price + " " }}></span>
            </div>
          </div>
          <hr />
          <div>
              <table width="100%">
                <tr>
                  <td width="20%">
                      <span className="black-font14px" dangerouslySetInnerHTML={{__html:strings.seller +':'}}></span>
                  </td>
                  <td>
                      <span className={bemBlocks.item("condition")} dangerouslySetInnerHTML={{__html:source.seller}}></span>
                  </td>
                </tr>
                <tr>
                  <td width="20%">
                      <span className="black-font14px" dangerouslySetInnerHTML={{__html:strings.location +':'}}></span>
                  </td>
                  <td>
                      <span className={bemBlocks.item("condition")} dangerouslySetInnerHTML={{__html:source.seller_location}}></span>
                  </td>
                </tr>
              </table>
          </div>
        </div>
      </div>
    </div>
  )
}


export {AccessoriesGridItem, AccessoriesListItem}