import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import { changePasswordActions } from '../actions';
import { strings } from '../lib/strings';

const divStyle = {
	  marginTop: '150px'
	};



class ChangePassword extends React.Component {
    constructor(props) {
        super(props);


        this.state = {
            old_password: '',
            new_password: '',
            confirm_password: '',
            submitted: false,
            confirm_pass_error: false,
            error_message: '',
            cp_status: this.props.cp_status
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        
    }


    componentWillMount(){
        this.setState({submitted: false})       
    }


    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { old_password, new_password, confirm_password, submitted } = this.state;
        const { dispatch } = this.props;
        const data = {
        	'old_password': old_password,
        	'new_password': new_password,
        }
        if (new_password == confirm_password) {
            

            if(new_password.length < 6) {
                
                this.setState({ confirm_pass_error: true });
                this.setState({ error_message: "This password is too short. It must contain at least 6 characters." });
            }/* else if (!isNaN(new_password)){
                
                this.setState({ confirm_pass_error: true });
                this.setState({ error_message: "This password is entirely numeric." });
            } */else{
                
                dispatch(changePasswordActions.changePassword(data));
            }
            
        }else{
            this.setState({ confirm_pass_error: true });
            this.setState({ error_message: "Password does not match." });
        }
    }


    render() {
        
        const { old_password, new_password, confirm_password, submitted } = this.state;
        return (
            <div className="card mt-4 mx-auto col-md-6 col-md-offset-3">
                <div className={"card-header" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h3 className="mb-0">{strings.change_password}</h3>
                </div>
                <div className="card-body">
                    <form name="form" onSubmit={this.handleSubmit}>
                        
                        <div className={'form-group' + (submitted && this.state.old_password ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="old_password">{strings.old_password}</label>
                            <input type="password" className="form-control" name="old_password" value={old_password} onChange={this.handleChange} />
                             {submitted && !old_password &&
                                <div className="help-block text-danger">{strings.this_field_is_required}</div>
                            }
                        </div>
                        <div className={'form-group' + (submitted && this.state.new_password ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="password">{strings.new_password}</label>
                            <input type="password" className="form-control" name="new_password" value={new_password} onChange={this.handleChange} />
                             {submitted && !new_password &&
                                <div className="help-block text-danger">{strings.this_field_is_required}</div>
                            }
                        </div>
                        <div className={'form-group' + (submitted && this.state.confirm_pass_error ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="password">{strings.confirm_password}</label>
                            <input type="password" className="form-control" name="confirm_password" value={confirm_password} onChange={this.handleChange} />
                            {submitted && this.state.confirm_pass_error &&
                                <div className="help-block text-danger">{this.state.error_message}</div>
                            }
                            {submitted && !confirm_password &&
                                <div className="help-block text-danger">{strings.this_field_is_required}</div>
                            }
                        </div>
                        <div className={"form-group"+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <button className="btn btn-primary">{strings.submit}</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state){
    return{
        cp_status: state.changePassReducer,
        rtlconv: state.rtlconv
    }
}


export default connect(mapStateToProps)(ChangePassword)