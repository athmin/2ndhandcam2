import React from 'react'
import { history } from '../store/configureStore'
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'
import { strings } from '../lib/strings';


class TermsOfUse extends React.Component {

    componentWillMount(){
         
    }

    clickStillHandle(){
        history.push('/buy_item')
    }

    clickVideoHandle(){
        history.push('/buy_video_cameras')
    }

    clickLensHandle(){
        history.push('/buy_lens')
    }

    clickLightingHandle(){
        history.push('/buy_lighting')
    }

    clickAccessoriesHandle(){
        history.push('/buy_accessories')
    }

    clickDroneHandle(){
        history.push('/buy_drones')
    }

    goBack() {
        history.goBack();
    }

    closeCustomerServices = (e) => {
        e.preventDefault();
        this.goBack();
    }

  render() {
    return (
        <div className="row homelogo" dir="ltr">
            <div className="mt-4 mx-auto col-md-3">
                
            </div>
            <div className={"card mt-4 col-md-5 infoForms secondhandlogo" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')} dir={this.props.rtlconv}>
                <button className={"btn btn-danger btn-sm margintop10 modalButton" + (this.props.rtlconv == "rtl" ? ' float-left' : ' float-right')} onClick={this.closeCustomerServices}> X </button>
                <div className="card-header registration-header">
                    <h6 className="mb-0">{strings.terms_of_use}</h6>
                </div>
                
                <div className="card-body">

                    <p>{strings.terms_of_use_1}</p>
                    <ul className={this.props.rtlconv == "rtl" ? 'ta-right' : ''} dir={this.props.rtlconv}>    
                        <li>{strings.terms_of_use_2}</li>
                        <li>{strings.terms_of_use_3}</li>
                        <li>{strings.terms_of_use_4}</li>
                        <li>{strings.terms_of_use_5}</li>
                        <li>{strings.terms_of_use_6}</li>
                        <li>{strings.terms_of_use_7}</li>
                        <li>{strings.terms_of_use_8}</li>
                    </ul>

                </div>
                <br />
                <br />
            </div> 
            <div className="mt-4 mx-auto col-md-3">
                
            </div>
        </div>        
    )
  }
}

function mapStatetoProps(state){
    return {
        rtlconv: state.rtlconv
    }
}

export default connect(mapStatetoProps)(TermsOfUse);
