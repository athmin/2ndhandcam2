import React from 'react'
import Slider from 'react-slick'
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
let CONFIG = require('../lib/config.json')

const images_url = CONFIG.server_url + "/static/media_cdn"

const camStyle = {
    "max-width": "84%",
    height: "auto"
    };

export default class Home extends React.Component {

  render() {
  	
    return (
    	<div className='container-fluid'>
        <div>
          <div className="main-logo">
            <img src={images_url + '/images/main-big-arrow.png'}/>
          </div> 
        </div>
    	</div>
    )
  }
}
