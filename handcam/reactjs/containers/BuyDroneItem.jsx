import * as React from "react";
import { connect } from 'react-redux';
import "searchkit/theming/theme.scss";
import { DroneGridItem, DroneListItem } from './BuyDroneItems'
import { history } from '../store/configureStore'
import {Link} from 'react-router-dom'
import { strings } from '../lib/strings';
import FaFilter from 'react-icons/lib/fa/filter'
let CONFIG = require('../lib/config.json')

import {
  SearchBox,
  Hits,
  HitsStats,
  SearchkitProvider,
  CheckboxFilter,
  SearchkitManager,
  SortingSelector,
  HierarchicalMenuFilter,
  RefinementListFilter,
  RangeFilter,
  NumericRefinementListFilter,
  NoHits,
  ViewSwitcherToggle,
  ViewSwitcherHits,
  Pagination,
  SelectedFilters,
  ResetFilters,
  listComponent,
  ItemHistogramList,
  Layout, LayoutBody, LayoutResults,
  SideBar, TopBar,
  ActionBar, ActionBarRow,
  TermQuery,
    FilteredQuery,
    BoolShould, BoolMust
} from "searchkit";

const camStyle = {
  width: 75,
    height: 54,
};
const logoStyle = {
  width: 75,
    height: 25,
};

class BuyDronesItem extends React.Component {

  searchkit:SearchkitManager

    constructor(props) {
        super(props)

        this.state = {
          drone_type: this.props.location.state.drone_type,
          openbrand: true,
          opencategory: true,
          openmodel: true,
          opensensonsize: true,
          opensensortype: true,
          openvideoresolution: true,
          openpackage: true,
          openlcdsize: true,
        }

        const host = CONFIG.server_url + "/es/forsaledrones/for_sale_drones_index/"

        let cat = this.state.drone_type
        
        this.searchkit = new SearchkitManager(host)
        this.searchkit.addDefaultQuery((query)=> {
          return query.addQuery(BoolMust([(TermQuery("status", "active"))]));
        })
        
    }

    componentWillMount(){
        /*let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }  */ 
    }

    componentDidMount(){
        //console.log(this.props.location.state.package)
        window.scrollTo(0, 0)

    }

  clickStillHandle = (e) => {
      e.preventDefault();
      history.push('/buy_item')
  }

  clickVideoHandle = (e) => {
      e.preventDefault();
      history.push('/buy_video_cameras')
  }

  clickLensHandle = (e) => {
      e.preventDefault();
      history.push('/buy_lens')
  }

  clickLightingHandle = (e) => {
      e.preventDefault();
      history.push('/buy_lighting')
  }

  clickAccessoriesHandle = (e) => {
      e.preventDefault();
      history.push('/buy_accessories')
  }

  clickDroneHandle = (e) => {
      e.preventDefault();
      history.push('/buy_drones')
  }

  togglepackage() {
    this.setState({
      openpackage: !this.state.openpackage
    });
  }


  renderDroneType(cat) {

     if (cat=="Quadcopter"){
        return (strings.quadcopter)
     }else if(cat=="Hexacopter"){
        return (strings.hexacopter)
     }else if(cat=="Octacopter"){
        return (strings.octacopter)
     }else if(cat=="Helicopter"){
        return (strings.helicopter)
     }else{
        return (strings.airplane)
     } 
  }


  render() {
    return (
      <div>
        <ul className="breadcrumb">
              <li><Link to="/">{strings.home}</Link></li>
              <li><Link to="/buy">{strings.buy}</Link></li>
              <li><Link to="/buy_drones">{strings.drone}</Link></li>
              <li>{this.renderDroneType(this.state.drone_type)}</li>
        </ul>
        <div>
            <div className=" bg-company-orange">
              <ul className="d-flex mx-auto justify-content-center nomarge zeroleftpadding">
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickStillHandle}><span>{strings.photography_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickLensHandle}><span>{strings.lenses}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickVideoHandle}><span>{strings.video_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickLightingHandle}><span>{strings.lighting}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickAccessoriesHandle}><span>{strings.accessories}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4 active">
                  <a className="awhite" href="" onClick={this.clickDroneHandle}><span>{strings.drone}</span></a>
                </li>
              </ul>
            </div>

          <SearchkitProvider searchkit={this.searchkit}>
          <Layout size="l">

            <LayoutBody>
            <SideBar>
                <div className="serchtop">
                  <div className="text-center">
                    <FaFilter /><span className="colorsearch">{strings.narrow_result}</span>
                 </div>
                </div>


               <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglepackage.bind(this)}>{strings.country}</h6>
                </div>
                <div className="clanheader">
                  <RefinementListFilter
                    id="country"
                    field="seller_country.keyword"
                    operator="OR"
                    size={10}/>
                </div>    
              </div>    

              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglepackage.bind(this)}>{strings.city}</h6>
                </div>
                <div className="clanheader">
                  <RefinementListFilter
                    id="city"
                    field="seller_city.keyword"
                    operator="OR"
                    size={10}/>
                </div>    
              </div>      
               
              <div>
		<div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
		  <h6>{strings.drone_type}</h6>
		</div>
		<div className="clanheader">
		  <RefinementListFilter
	            id="drone_type"
	            field="drone_type.keyword"
	            operator="OR"
	            size={10}/>
		</div>
              </div>


                <div>
                  <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                      <h6>{strings.brand}</h6>
                  </div>
                  <div className="clanheader">
                    <RefinementListFilter
                       id="brand"
                       field="brand.keyword"
                       operator="OR"
                       size={10}/>
                  </div>
                </div>

                <div>
                  <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                      <h6>{strings.condition}</h6>
                  </div>
                  <div className="clanheader">
                    <RefinementListFilter
                       id="neworused"
                       field="neworused.keyword"
                       operator="OR"
                       size={10}/>
                  </div>
                </div>

                <div>
                  <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                    <h6>{strings.price}</h6>
                  </div>
                  <div className="clanheader">
                    <NumericRefinementListFilter id="price" field="price" options={[
                      {title:"All"},
                      {title:"Under SA300", from:0, to:300},
                      {title:"SA301 to SA500", from:301, to:500},
                      {title:"SA501 to SA750", from:501, to:750},
                      {title:"SA751 to SA1000", from:751, to:1000},
                      {title:"SA1001 to SA1500", from:1001, to:1500},
                      {title:"SA1501 to SA2000", from:1501, to:2000},
                      {title:"Above SA2000", from:2001, to:15000}
                    ]}/> 
                  </div>
                </div> 

                <div>
                  <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                      <h6>{strings.camera}</h6>
                  </div>
                  <div className="clanheader">
                    <RefinementListFilter
                       id="camera"
                       field="camera.keyword"
                       operator="OR"
                       size={10}/>
                  </div>
                </div>   

                <div>
                  <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                      <h6>{strings.camera_video_resolution}</h6>
                  </div>
                  <div className="clanheader">
                    <RefinementListFilter
                       id="video_resolution"
                       field="video_resolution.keyword"
                       operator="OR"
                       size={10}/>
                  </div>
                </div>

                <div>
                  <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6>{strings.camera_still_resolution}</h6>
                  </div>
                  <div className="clanheader">
                    <NumericRefinementListFilter id="megapixels" field="megapixels" options={[
                      {title:"All"},
                      {title:"up to 10 MP", from:0, to:10},
                      {title:"10.1 MP to 20 MP", from:10.1, to:20},
                      {title:"20.1 MP to 30 MP", from:20.1, to:30},
                      {title:"30.1 MP to 40 MP", from:30.1, to:40},
                      {title:"40.1 MP to 50 MP", from:40.1, to:50},
                      {title:"50.1 MP to 60 MP", from:50.1, to:60},
                      {title:"60.1 MP to 70 MP", from:60.1, to:70},
                      {title:"70.1 MP to 80 MP", from:70.1, to:80}
                    ]}/>
                  </div> 
                </div>

                <div>
                  <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                      <h6>{strings.operating_time}</h6>
                  </div>
                  <div className="clanheader">
                    <RefinementListFilter
                       id="operating_time"
                       field="operating_time.keyword"
                       operator="OR"
                       size={10}/>
                  </div>
                </div>

                <div>
                  <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                      <h6>{strings.control_range}</h6>
                  </div>
                  <div className="clanheader">
                    <RefinementListFilter
                       id="control_range"
                       field="control_range.keyword"
                       operator="OR"
                       size={10}/>
                  </div>
                </div>

                 <SortingSelector options={[
                  {label:"Date-desc", field:"date", order:"desc", defaultOption:true},
                  {label:"Date-asc", field:"date", order:"asc"},
                ]}/> 
    
            </SideBar>
            <LayoutResults>
                  <div className="search_container">
                    <SearchBox
                      searchOnChange={true}
                      queryOptions={{analyzer:"standard"}}
                      queryFields={["brand", "model"]}/>
                  </div>
                  <ActionBar>
                    <ActionBarRow>
                        <HitsStats/>
                        <ViewSwitcherToggle/>
                    </ActionBarRow>
                  </ActionBar>

                  <ViewSwitcherHits
                            hitsPerPage={24} highlightFields={["brand","label"]}
                      sourceFilter={["brand",
                        "id",
                        "product_id",
                        "seller",
                        "seller_pk",
                        "seller_location",
                        "seller_country",
                        "seller_city",
                        "seller_location",
                        "seller_phonenumber",
                        "label",
                        "model",
                        "price",
                        "currency",
                        "condition",
                        "actual_photos",
                        "image",
                        "status",
                        "warranty",
                        "comment",
                        "megapixels",
                        "video_resolution",
                        "camera",
                        "control_range",
                        "operating_time",
                        "drone_type"
                      ]}
                      hitComponents = {[
                        {key:"grid", title:"Grid", itemComponent:DroneGridItem},
                        {key:"list", title:"List", itemComponent:DroneListItem, defaultOption:true}
                      ]}
                      scrollTo="body"/>
                  <NoHits suggestionsField="brand"/>
                  <Pagination showNumbers={true}/>
                </LayoutResults>
            </LayoutBody>
              </Layout>
        </SearchkitProvider>
        <br />
        <br />
        <br />
        <br />
      </div>
    </div>     
    )
  }
}

function mapStateToProps(state){
    return{
        rtlconv: state.rtlconv
    }
}

export default connect(mapStateToProps)(BuyDronesItem);
