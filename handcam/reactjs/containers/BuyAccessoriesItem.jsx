import * as React from "react";
import { connect } from 'react-redux';
import "searchkit/theming/theme.scss";
import { AccessoriesGridItem, AccessoriesListItem } from './BuyAccessoriesItems'
import { history } from '../store/configureStore'
import {Link} from 'react-router-dom'
import { strings } from '../lib/strings';
import FaFilter from 'react-icons/lib/fa/filter'
let CONFIG = require('../lib/config.json')

import {
  SearchBox,
  Hits,
  HitsStats,
  SearchkitProvider,
  CheckboxFilter,
  SearchkitManager,
  SortingSelector,
  HierarchicalMenuFilter,
  RefinementListFilter,
  RangeFilter,
  NumericRefinementListFilter,
  NoHits,
  ViewSwitcherToggle,
  ViewSwitcherHits,
  Pagination,
  SelectedFilters,
  ResetFilters,
  listComponent,
  ItemHistogramList,
  Layout, LayoutBody, LayoutResults,
  SideBar, TopBar,
  ActionBar, ActionBarRow,
  TermQuery,
    FilteredQuery,
    BoolShould, BoolMust
} from "searchkit";

const camStyle = {
  width: 75,
    height: 54,
};
const logoStyle = {
  width: 75,
    height: 25,
};

class BuyAccessoriesItem extends React.Component {

  searchkit:SearchkitManager

    constructor(props) {
        super(props)

        this.state = {
          category: this.props.location.state.category,
          openbrand: true,
          opencategory: true,
          openmodel: true,
          opensensonsize: true,
          opensensortype: true,
          openvideoresolution: true,
          openpackage: true,
          openlcdsize: true,
        }

        this.renderAccessoryCategory = this.renderAccessoryCategory.bind(this);

        const host = CONFIG.server_url + "/es/forsaleaccessories/for_sale_accessories_index/"
        let cat = this.state.category
        let query_cat = cat.split(" ");

        if (query_cat.length == 2 || cat.toLowerCase() == "light stand and mounting"){
          query_cat = query_cat[1].toLowerCase()
        }else{
          query_cat = query_cat[0].toLowerCase()
        }
        
        this.searchkit = new SearchkitManager(host)
        this.searchkit.addDefaultQuery((query)=> {
          //return query.addQuery(BoolMust([TermQuery("category", query_cat)]));
          return query.addQuery(BoolMust([(TermQuery("category", query_cat)), (TermQuery("status", "active"))]));
        })
    }

    componentWillMount(){
        let token = localStorage.getItem('token')
        if (!token){
          history.push('/login_register')
        }   
    }

    componentDidMount(){
        
        window.scrollTo(0, 0)

    }

  clickStillHandle = (e) => {
      e.preventDefault();
      history.push('/buy_item')
  }

  clickVideoHandle = (e) => {
      e.preventDefault();
      history.push('/buy_video_cameras')
  }

  clickLensHandle = (e) => {
      e.preventDefault();
      history.push('/buy_lens')
  }

  clickLightingHandle = (e) => {
      e.preventDefault();
      history.push('/buy_lighting')
  }

  clickAccessoriesHandle = (e) => {
      e.preventDefault();
      history.push('/buy_accessories')
  }

  clickDroneHandle = (e) => {
      e.preventDefault();
      history.push('/buy_drones')
  }

  togglepackage() {
    this.setState({
      openpackage: !this.state.openpackage
    });
  }

  renderAccessoryCategory(cat) {

     if (cat=="Light Modifiers"){
        return (strings.light_modifiers)
     }else if(cat=="Light Stand and Mounting"){
        return (strings.light_and_stand_mounting)
     }else if(cat=="Powers & Cables"){
        return (strings.power_cables)
     }else if(cat=="Radio & Optical Slave"){
        return (strings.radio_optical_slave)
     }else if(cat=="Light Meters"){
        return (strings.light_meters)
     }else if(cat=="Light Cases"){
        return (strings.light_cases)
     }else if(cat=="Camera Bags"){
        return (strings.camera_bags)
     }else if(cat=="Tripods"){
        return (strings.tripods)
     }else{
        return (strings.others)
     } 
  }


  render() {
    return (
      <div>
        <ul className="breadcrumb">
              <li><Link to="/">{strings.home}</Link></li>
              <li><Link to="/buy">{strings.buy}</Link></li>
              <li><Link to="/buy_accessories">{strings.accessories}</Link></li>
              <li>{this.renderAccessoryCategory(this.state.category)}</li>
        </ul>
        <div>
            <div className=" bg-company-orange">
              <ul className="d-flex mx-auto justify-content-center nomarge zeroleftpadding">
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickStillHandle}><span>{strings.photography_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickLensHandle}><span>{strings.lenses}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickVideoHandle}><span>{strings.video_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickLightingHandle}><span>{strings.lighting}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4 active">
                  <a className="awhite" href="" onClick={this.clickAccessoriesHandle}><span>{strings.accessories}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickDroneHandle}><span>{strings.drone}</span></a>
                </li>
              </ul>
            </div>
            
          <div className="brand-header text-center">
              <h1 className="text-center p-5">{strings.accessories} - {this.renderAccessoryCategory(this.state.category)}</h1>
          </div>

          <SearchkitProvider searchkit={this.searchkit}>
          <Layout size="l">

            <LayoutBody>
            <SideBar>
                <div className="serchtop">
                  <div className="text-center">
                    <FaFilter /><span className="colorsearch">{strings.narrow_result}</span>
                 </div>
                </div>


               <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglepackage.bind(this)}>{strings.country}</h6>
                </div>
                <div className="clanheader">
                  <RefinementListFilter
                    id="country"
                    field="seller_country.keyword"
                    operator="OR"
                    size={10}/>
                </div>    
              </div>    

              <div>
                <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 onClick={this.togglepackage.bind(this)}>{strings.city}</h6>
                </div>
                <div className="clanheader">
                  <RefinementListFilter
                    id="city"
                    field="seller_city.keyword"
                    operator="OR"
                    size={10}/>
                </div>    
              </div>      
                
                <div>
                  <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                      <h6>{strings.brand}</h6>
                  </div>
                  <div className="clanheader">
                    <RefinementListFilter
                       id="brand"
                       field="brand.keyword"
                       operator="OR"
                       size={10}/>
                  </div>
                </div>

                <div>
                  <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                      <h6>{strings.condition}</h6>
                  </div>
                  <div className="clanheader">
                    <RefinementListFilter
                       id="neworused"
                       field="neworused.keyword"
                       operator="OR"
                       size={10}/>
                  </div>
                </div>

                <div>
                  <div className={'filter-title' + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}> 
                    <h6>{strings.price}</h6>
                  </div>
                  <div className="clanheader">
                    <NumericRefinementListFilter id="price" field="price" options={[
                      {title:"All"},
                      {title:"Under SA300", from:0, to:300},
                      {title:"SA301 to SA500", from:301, to:500},
                      {title:"SA501 to SA750", from:501, to:750},
                      {title:"SA751 to SA1000", from:751, to:1000},
                      {title:"SA1001 to SA1500", from:1001, to:1500},
                      {title:"SA1501 to SA2000", from:1501, to:2000},
                      {title:"Above SA2000", from:2001, to:15000}
                    ]}/> 
                  </div>
                </div> 

                 <SortingSelector options={[
                  {label:"Date-desc", field:"date", order:"desc", defaultOption:true},
                  {label:"Date-asc", field:"date", order:"asc"},
                ]}/>    
    
            </SideBar>
            <LayoutResults>
                  <div className="search_container">
                    <SearchBox
                      searchOnChange={true}
                      queryOptions={{analyzer:"standard"}}
                      queryFields={["brand", "model"]}/>
                  </div>
                  <ActionBar>
                    <ActionBarRow>
                        <HitsStats/>
                        <ViewSwitcherToggle/>
                    </ActionBarRow>
                  </ActionBar>

                  <ViewSwitcherHits
                            hitsPerPage={24} highlightFields={["brand","label"]}
                      sourceFilter={["brand",
                        "id",
                        "product_id",
                        "seller",
                        "seller_pk",
                        "seller_location",
                        "seller_country",
                        "seller_city",
                        "seller_location",
                        "seller_phonenumber",
                        "label",
                        "model",
                        "price",
                        "currency",
                        "condition",
                        "actual_photos",
                        "image",
                        "status",
                        "warranty",
                        "category",
                        "comment"
                      ]}
                      hitComponents = {[
                        {key:"grid", title:"Grid", itemComponent:AccessoriesGridItem},
                        {key:"list", title:"List", itemComponent:AccessoriesListItem, defaultOption:true}
                      ]}
                      scrollTo="body"/>
                  <NoHits suggestionsField="brand"/>
                  <Pagination showNumbers={true}/>
                </LayoutResults>
            </LayoutBody>
              </Layout>
        </SearchkitProvider>
        <br />
        <br />
        <br />
        <br />
      </div>
    </div>     
    )
  }
}

function mapStateToProps(state){
    return{
        rtlconv: state.rtlconv
    }
}

export default connect(mapStateToProps)(BuyAccessoriesItem);