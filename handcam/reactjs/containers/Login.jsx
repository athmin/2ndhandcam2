import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import { userActions, getMaintenanceAction } from '../actions';
import { history } from '../store/configureStore'
import { strings } from '../lib/strings';
import Modal from 'react-modal';

const divStyle = {
	  marginTop: '150px'
	};


const customStyles = {
    content: {
        'border-left': '5px solid #f4a72e',
        'border-top': '5px solid #f4a72e',
        'border-right': '5px solid #3d4041',
        'border-bottom': '5px solid #3d4041',
        borderRadius: '4px',
        bottom: 'auto',
        minHeight: '10rem',
        left: '50%',
        padding: '2rem',
        position: 'fixed',
        right: 'auto',
        top: '50%',
        transform: 'translate(-50%,-50%)',
        minWidth: '20rem',
        width: '60%',
        maxWidth: '60rem',
        padding: '0'
      },
    overlay: {
        backgroundColor: "rgba(255, 255, 255, 0.25)"
    }  
};



class Login extends React.Component {
    constructor(props) {
        super(props);


        this.state = {
            username: '',
            password: '',
            submitted: false,
            modalIsOpen: false,
            forget_pass_email: '',
            submitted_forget: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.forgotPassword = this.forgotPassword.bind(this);
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.closeModalCancel = this.closeModalCancel.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        
    }


    componentWillMount(){
        const { dispatch } = this.props;

        let viewportmeta = document.querySelector('meta[name="viewport"]');
        if(viewportmeta===null){
            viewportmeta = document.createElement("meta");
            viewportmeta.setAttribute("name","viewport");
            document.head.appendChild(viewportmeta);
              
            viewportmeta = document.querySelector('meta[name="viewport"]');
        }
        viewportmeta.setAttribute('content', 'width=1470, initial-scale=0.25, maximum-scale=1.0');

        //var scale = 'scale(1)'
        //document.body.style.zoom = "100%"
        document.body.style.zoom = screen.logicalXDPI / screen.deviceXDPI;
        //document.body.style.webkitTransform = scale;      // Chrome, Opera, Safari
        //document.body.style.msTransform =  scale;       // IE 9
        //document.body.style.transform = scale;     // General

        if (!this.props.maintenanceMode.hasOwnProperty('mode')){
            dispatch(getMaintenanceAction.getMode()).then( () => {
                if (this.props.maintenanceMode){
                    if (this.props.maintenanceMode.mode.status){

                        history.push('/maintenance')
                    }
                } 
            })
        }else{
            if (this.props.maintenanceMode.mode.status){
                history.push('/maintenance')
            }
        }
        this.setState({submitted: false})
        dispatch(userActions.clearForgetPass())
    }


    handleSubmit(e) {
        e.preventDefault();
        this.setState({ submitted_forget: false });
        this.setState({ submitted: true });
        const { username, password } = this.state;
        const { dispatch } = this.props;
        const data = {
        	'username': username,
        	'password': password,
        }
        if (username && password) {
            dispatch(userActions.fetchUserToken(data));
            
            /*var viewportmeta = document.querySelector('meta[name="viewport"]');
            if (viewportmeta) {
                viewportmeta.setAttribute('content', 'width=1470, initial-scale=0.25, maximum-scale=1.0, target-densitydpi=high-dpi');
            }*/

            let viewportmeta = document.querySelector('meta[name="viewport"]');
            if(viewportmeta===null){
              viewportmeta = document.createElement("meta");
              viewportmeta.setAttribute("name","viewport");
              document.head.appendChild(viewportmeta);
              
              viewportmeta = document.querySelector('meta[name="viewport"]');
            }
            viewportmeta.setAttribute('content', 'width=1470, initial-scale=0.3, maximum-scale=1.0, target-densitydpi=high-dpi');

        }
    }

    forgotPassword(){
      
        const { forget_pass_email } = this.state;
        const { dispatch } = this.props;
        const data = {
            'email': forget_pass_email,
        }

        if (forget_pass_email) {
            this.setState({ submitted: false });
            this.setState({ submitted_forget: true });
            dispatch(userActions.forgotPassword(data));
        }
    }

    openModal() {
        this.setState({modalIsOpen: true});
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        //this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({modalIsOpen: false});
        
        this.forgotPassword();
    }

    closeModalCancel() {
        this.setState({modalIsOpen: false});
    }

    renderForgetPassMessage(){
        
        const { username, password, submitted, forget_pass_email, submitted_forget} = this.state;

        

        
        if (typeof(this.props.forget_pass.message) != "undefined"){
            
            if (this.props.forget_pass.message.status_code == 200){
                return (
                    <div className="alert alert-success text-center" role="alert">
                        <strong>{this.props.forget_pass.message.detail}</strong>
                    </div> 
                )
            }else{

                if(submitted_forget)
                {

                    return(
                        <div className="alert alert-danger text-center" role="alert">
                            <strong>{this.props.forget_pass.message.detail}</strong>
                        </div> 
                    )

                }
              
            }
        }
    }

    render() {
        const { loginTokenError, login } = this.props
        const { username, password, submitted, forget_pass_email, submitted_forget} = this.state;
        return (
            <div>

            {this.renderForgetPassMessage()}

            <div className={"card mt-4 mx-auto col-md-6 col-md-offset-3" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                <div className="card-header">
                    <h3 className="mb-0">{strings.login}</h3>
                </div>
                <div className="card-body">
                    <form name="form" onSubmit={this.handleSubmit}>
                        {loginTokenError.message && submitted &&
                            <div className="alert alert-danger text-center" role="alert">
                                <strong>{loginTokenError.message}</strong>
                            </div> 
                        }
                        <div className={'form-group' + (submitted && !username ? ' has-error' : '')}>
                            <label htmlFor="username">{strings.email} / {strings.username}</label>
                            <input type="text" className="form-control" name="username" value={username} onChange={this.handleChange} />
                            {submitted && !username &&
                                <div className="help-block">{strings.username_is_required}</div>
                            }
                        </div>
                        <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
                            <label htmlFor="password">{strings.password}</label>
                            <input type="password" className="form-control" name="password" value={password} onChange={this.handleChange} />
                            <span className="spannote spanhowto" onClick={(e)=>{e.preventDefault(); this.openModal()}}>{strings.forgot_password}</span>
                            {submitted && !password &&
                                <div className="help-block">{strings.password_is_required}</div>
                            }
                        </div>
                        <div className="form-group">
                            <button className="btn btn-primary">{strings.login}</button>
                            <Link to="/register" className="btn btn-link">{strings.register}</Link>
                        </div>
                    </form>
                </div>


                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    style={customStyles}
                    contentLabel={strings.add_lens}
                    ariaHideApp={false}
                >
                    
                    <div className="forget-frame arabicfont"> 
                        <div className="scroll"> 
                            <div className={this.props.rtlconv == "rtl" ? ' righttoleft' : ''} dir={this.props.rtlconv}>
                                <div className="card-header registration-header">
                                    <h6 className="mb-0">{strings.forgot_password}</h6>
                                </div>
                                
                                <div className="card-body">

                                    <p>{strings.forgot_label_1}</p>

                                    <p>{strings.forgot_label_2}</p>
                                    <form name="forget-form">
                                        
                                        <div className={'form-group' + (submitted_forget && !forget_pass_email ? ' has-error' : '')}>
                                            <label htmlFor="username">{strings.email}</label>
                                            <input type="text" className={"form-control" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')} name="forget_pass_email" value={forget_pass_email} onChange={this.handleChange} />
                                            {submitted_forget && !forget_pass_email &&
                                                <div className="help-block">{strings.this_field_is_required}</div>
                                            }
                                        </div>

                                        <button className="btn btn-sm btn-primary float-center margin-right" onClick={(e)=>{e.preventDefault(); this.closeModal()}}>{strings.submit}</button>
                                        <button className="btn btn-sm float-center" onClick={(e)=>{e.preventDefault(); this.closeModalCancel()}}>{strings.cancel}</button>
                                        
                                    </form>
                                    
                                </div>
                            </div>
                            <br />
                            <br />
                        </div>
                    </div>
                    
                </Modal>
            </div>

            </div>
        );
    }
}

function mapStateToProps(state){
    return{
        loginToken: state.loginToken,
        loginTokenError: state.loginTokenError,
        rtlconv: state.rtlconv,
        maintenanceMode: state.maintenanceMode,
        forget_pass: state.forget_pass
    }
}


export default connect(mapStateToProps)(Login)