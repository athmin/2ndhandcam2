import React from 'react';
import Time from 'react-time'
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'
import { history } from '../store/configureStore'
import { getMessagesAction } from '../actions';
import { getUserDashboardAction, getUserProfileAction } from '../actions';
import { removeForsaleCameraAction } from '../actions';
import { changeStatusToSoldAction } from '../actions';
import { activateForSaleAction } from '../actions';
import { disableForSaleAction } from '../actions';
import { enableForSaleAction } from '../actions';

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
import { strings } from '../lib/strings';
import Modal from 'react-modal';

const divStyle = {
    marginTop: '150px'
  };


const customStylesForAcc = {
    content: {
        'border-left': '5px solid #f4a72e',
        'border-top': '5px solid #f4a72e',
        'border-right': '5px solid #3d4041',
        'border-bottom': '5px solid #3d4041',
        borderRadius: '4px',
        bottom: 'auto',
        minHeight: '10rem',
        left: '50%',
        padding: '2rem',
        position: 'fixed',
        right: 'auto',
        top: '50%',
        transform: 'translate(-50%,-50%)',
        minWidth: '20rem',
        width: '35%',
        maxWidth: '60rem',
        padding: '0'
      },
    overlay: {
        backgroundColor: "rgba(255, 255, 255, 0.25)"
    }  
};



class Dashboard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          dashboard: [],
          video_cameras: [],
          lenses: [],
          lightings: [],
          accessories: [],
          drones: [],
          profile: '',
          fetching: false,
          flashMessage: null,
          forsale_error: null,
          hasSoldItems: false,
          hasExpiredDisabledItems: false,

          removeModal: false,
          removeModalIsOpen: false, 
          removeID: null,
          removeItem: null,
        }

        this.openModalRemove = this.openModalRemove.bind(this);
        this.afterOpenModalRemove = this.afterOpenModalRemove.bind(this);
        this.closeModalRemove = this.closeModalRemove.bind(this);
    }

    zoomOutMobile() {
      var viewport = document.querySelector('meta[name="viewport"]');

      if ( viewport ) {
        viewport.content = "initial-scale=0.1";
        viewport.content = "width=1200";
      }
    }

    componentWillMount(){
        const { dispatch } = this.props;
        let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }

        /*var viewportmeta = document.querySelector('meta[name="viewport"]');
        if (viewportmeta) {
            viewportmeta.setAttribute('content', 'width=1470, initial-scale=0.25, maximum-scale=1.0, target-densitydpi=high-dpi');
        }*/

        let viewportmeta = document.querySelector('meta[name="viewport"]');
        if(viewportmeta===null){
            viewportmeta = document.createElement("meta");
            viewportmeta.setAttribute("name","viewport");
            document.head.appendChild(viewportmeta);
              
            viewportmeta = document.querySelector('meta[name="viewport"]');
        }
        viewportmeta.setAttribute('content', 'width=1470, initial-scale=0.3, maximum-scale=1.0');

        //var scale = 'scale(1)'
        //document.body.style.zoom = "25%"
        document.body.style.zoom = screen.logicalXDPI / screen.deviceXDPI;
        //document.body.style.webkitTransform = scale;      // Chrome, Opera, Safari
        //document.body.style.msTransform =  scale;       // IE 9
        //document.body.style.transform = scale;     // General
        
        dispatch(getUserDashboardAction.userDashboard()).then( () => {
            this.setState({fetching: true})
            this.setState({ dashboard: this.props.dash.cameras})
            this.setState({ video_cameras: this.props.dash.video_cameras})
            this.setState({ lenses: this.props.dash.lenses})
            this.setState({ lightings: this.props.dash.lightings})
            this.setState({ accessories: this.props.dash.accessories})
            this.setState({ drones: this.props.dash.drones})
            this.setState({ profile: this.props.dash.user_profile})
            

            this.checkIfHasSold(this.props.dash)
            this.checkIfHasExpiredDisabled(this.props.dash)
        })

        dispatch(getUserProfileAction.userCredentials()).then( () => {
            
            //console.log("logged in as");
            
        })

        dispatch(getMessagesAction.getUserInbox()).then( () => {
            this.setState({fetching: true})
            this.setState({ messages: this.props.inboxReducer.messages})
            this.setState({ unread_count: this.props.inboxReducer.unread_count})
              
        })
       
        
        if (Object.keys(this.props.camera_sell).length > 0) {

            this.setState({ flashMessage: this.props.camera_sell.sell.status.status})
            
        }

        if (Object.keys(this.props.video_sell).length > 0) {
            
            this.setState({ flashMessage: this.props.video_sell.sell.status.status})
            
        }

        if (Object.keys(this.props.lens_sell).length > 0) {
            
            this.setState({ flashMessage: this.props.lens_sell.sell.status.status})
            
        }

        if (Object.keys(this.props.lighting_sell).length > 0) {
            
            this.setState({ flashMessage: this.props.lighting_sell.sell.status.status})
            
        }

        if (Object.keys(this.props.accessories_sell).length > 0) {
            
            this.setState({ flashMessage: this.props.accessories_sell.sell.status.status})
            
        }

        if (Object.keys(this.props.drone_sell).length > 0) {
            
            this.setState({ flashMessage: this.props.drone_sell.sell.status.status})
            
        }

        /*dispatch(getUserDashboardAction.userDashboard)*/   
    }

    checkIfHasSold(items){
        items.cameras.map((cam, i) => {
            if (cam.status == "Sold"){
                this.setState({ hasSoldItems: true});
                return;
            }
        })    
        items.video_cameras.map((cam, i) => {
            if (cam.status == "Sold"){
                this.setState({ hasSoldItems: true});
                return;
            }
        })
        items.lenses.map((cam, i) => {
            if (cam.status == "Sold"){
                this.setState({ hasSoldItems: true});
                return;
            }
        })
        items.lightings.map((cam, i) => {
            if (cam.status == "Sold"){
                this.setState({ hasSoldItems: true});
                return;
            }
        })
        items.accessories.map((cam, i) => {
            if (cam.status == "Sold"){
                this.setState({ hasSoldItems: true});
                return;
            }
        })
        items.drones.map((cam, i) => {
            if (cam.status == "Sold"){
                this.setState({ hasSoldItems: true});
                return;
            }
        })

        
    }

    checkIfHasExpiredDisabled(items){
        items.cameras.map((cam, i) => {
            if (cam.status == "Expired" || cam.status == "Disabled") {
                this.setState({ hasExpiredDisabledItems: true});
                return;
            }
        })    
        items.video_cameras.map((cam, i) => {
            if (cam.status == "Expired" || cam.status == "Disabled"){
                this.setState({ hasExpiredDisabledItems: true});
                return;
            }
        })
        items.lenses.map((cam, i) => {
            if (cam.status == "Expired" || cam.status == "Disabled"){
                this.setState({ hasExpiredDisabledItems: true});
                return;
            }
        })
        items.lightings.map((cam, i) => {
            if (cam.status == "Expired" || cam.status == "Disabled"){
                this.setState({ hasExpiredDisabledItems: true});
                return;
            }
        })
        items.accessories.map((cam, i) => {
            if (cam.status == "Expired" || cam.status == "Disabled"){
                this.setState({ hasExpiredDisabledItems: true});
                return;
            }
        })
        items.drones.map((cam, i) => {
            if (cam.status == "Expired" || cam.status == "Disabled"){
                this.setState({ hasExpiredDisabledItems: true});
                return;
            }
        })

        
    }

    removeButtonClicked(id, type){
        const { dispatch } = this.props;

        this.setState({ removeModalIsOpen: false })

        dispatch(removeForsaleCameraAction.removeForsaleCamera(id, type)).then( () => {
            if (!this.props.dash.error){
                
                this.setState({ flashMessage: this.props.dash.status.status });
                

                this.setState({ dashboard: this.props.dash.cameras})
                this.setState({ video_cameras: this.props.dash.video_cameras})
                this.setState({ lenses: this.props.dash.lenses})
                this.setState({ lightings: this.props.dash.lightings})
                this.setState({ accessories: this.props.dash.accessories})
                this.setState({ drones: this.props.dash.drones})
                this.setState({ profile: this.props.dash.user_profile});
                

                this.checkIfHasSold(this.props.dash)
                this.checkIfHasExpiredDisabled(this.props.dash)
            }else{
                
                this.setState({ flashMessage: this.props.dash.error })
            }
        })

    }

    reactivateButtonClicked(model, pk){
        const { dispatch } = this.props;

        dispatch(activateForSaleAction.activateItems(model, pk)).then( () => {
            if (!this.props.dash.error){
                
                this.setState({ flashMessage: this.props.dash.status.status });
                

                this.setState({ dashboard: this.props.dash.cameras})
                this.setState({ video_cameras: this.props.dash.video_cameras})
                this.setState({ lenses: this.props.dash.lenses})
                this.setState({ lightings: this.props.dash.lightings})
                this.setState({ accessories: this.props.dash.accessories})
                this.setState({ drones: this.props.dash.drones})
                this.setState({ profile: this.props.dash.user_profile});
                

                this.checkIfHasSold(this.props.dash)
                this.checkIfHasExpiredDisabled(this.props.dash)
            }else{
               
                this.setState({ flashMessage: this.props.dash.error })
            }
        })
    }

    soldButtonClicked(model, pk){
        const { dispatch } = this.props;

        this.setState({ removeModalIsOpen: false })

        dispatch(changeStatusToSoldAction.soldItems(model, pk)).then( () => {
            if (!this.props.dash.error){
               
                this.setState({ flashMessage: this.props.dash.status.status });
                

                this.setState({ dashboard: this.props.dash.cameras})
                this.setState({ video_cameras: this.props.dash.video_cameras})
                this.setState({ lenses: this.props.dash.lenses})
                this.setState({ lightings: this.props.dash.lightings})
                this.setState({ accessories: this.props.dash.accessories})
                this.setState({ drones: this.props.dash.drones})
                this.setState({ profile: this.props.dash.user_profile});
                

                this.checkIfHasSold(this.props.dash)
                this.checkIfHasExpiredDisabled(this.props.dash)
            }else{
                
                this.setState({ flashMessage: this.props.dash.error })
            }
        })  
    }

    disableButtonClicked(model, pk){
        const { dispatch } = this.props;

        dispatch(disableForSaleAction.disableItems(model, pk)).then( () => {
            
            if (!this.props.dash.error){
            
                this.setState({ flashMessage: this.props.dash.status.status });
               

                this.setState({ dashboard: this.props.dash.cameras})
                this.setState({ video_cameras: this.props.dash.video_cameras})
                this.setState({ lenses: this.props.dash.lenses})
                this.setState({ lightings: this.props.dash.lightings})
                this.setState({ accessories: this.props.dash.accessories})
                this.setState({ drones: this.props.dash.drones})
                this.setState({ profile: this.props.dash.user_profile});
                

                this.checkIfHasSold(this.props.dash)
                this.checkIfHasExpiredDisabled(this.props.dash)
            }else{
                this.setState({ flashMessage: this.props.dash.error })
            }
        })
    }


    enableButtonClicked(model, pk){
        const { dispatch } = this.props;

        dispatch(enableForSaleAction.enableItems(model, pk)).then( () => {
            
            if (!this.props.dash.error){
                
                this.setState({ flashMessage: this.props.dash.status.status });

                this.setState({ dashboard: this.props.dash.cameras})
                this.setState({ video_cameras: this.props.dash.video_cameras})
                this.setState({ lenses: this.props.dash.lenses})
                this.setState({ lightings: this.props.dash.lightings})
                this.setState({ accessories: this.props.dash.accessories})
                this.setState({ drones: this.props.dash.drones})
                this.setState({ profile: this.props.dash.user_profile});

                this.checkIfHasSold(this.props.dash)
                this.checkIfHasExpiredDisabled(this.props.dash)
               
            }else{
               
                this.setState({ flashMessage: this.props.dash.error })
            }
        })
    }


    renderWelcome(){
      const { dashboard, profile } = this.state
      if(this.state.profile==''){
        return <h2>Profile is null</h2>
      }else{
        return(
          <h2>Welcome back { profile.user.username }</h2>
        )
      }
    }

    renderFlashMessages(){

        if(this.state.flashMessage=="Item successfully uploaded."){
            return(
                <div className={"alert alert-success " + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    { strings.item_uploaded_successfully }
                </div>
            )
        }else{
            if(this.state.flasMessage){
                return(
                    <div className={"alert alert-success " + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                        { this.state.flashMessage }
                    </div>
                )
            }
        }
    }

    onModalConfirm(type, cam, item){

        if (type=="Sold"){
            this.soldButtonClicked(item, cam.id)
        }else if (type=="Activate"){
            this.reactivateButtonClicked(item, cam.id)
        }else if (type=="Disable"){
            this.disableButtonClicked(item, cam.id)
        }else {
            this.enableButtonClicked(item, cam.id)
        }

    }

    renderConfirmationModal(text, type, cam, item){
        
        confirmAlert({
          title: strings.confirm,                        
          message: text,               
          confirmLabel: strings.yes,                           
          cancelLabel: strings.no,                             
          onConfirm: () => {
            if (type=="Sold"){
                this.soldButtonClicked(item, cam.id)
            }else if (type=="Activate"){
                this.reactivateButtonClicked(item, cam.id)
            }else if (type=="Disable"){
                this.disableButtonClicked(item, cam.id)
            }else {
                this.enableButtonClicked(item, cam.id)
            }
          },
          onCancel: () => {} ,
        })
        
    }

    renderRemoveConfirmationModal(id, item){

        confirmAlert({
            title: strings.confirm,
            message: 'Are you sure you want to delete this item?',
            confirmLabel: strings.remove,                           
            cancelLabel: strings.sold,
            onConfirm: () => {
                this.removeButtonClicked(id, item)
            },
            onCancel: () => {this.soldButtonClicked(item, id)} ,
        })

    }

    openModalRemove(id, item) {
        this.setState({removeModalIsOpen: true});
        this.setState({removeID: id});
        this.setState({removeItem: item});
    }

    afterOpenModalRemove() {
        // references are now sync'd and can be accessed.
        //this.subtitle.style.color = '#f00';
    }

    closeModalRemove() {
        this.setState({removeModalIsOpen: false});
    }

    renderRemoveModal() {

        return (

            <div className="topspace ta-center">
                <h4>{strings.confirm}</h4>
                <hr />
                <h6>{strings.are_you_sure_you_want_to_remove}</h6>
                <br />
                <button className="btn btn-sm btn-secondary" onClick={(e)=>{e.preventDefault(); this.soldButtonClicked(this.state.removeItem, this.state.removeID); this.closeModalRemove}}>
                    {strings.sold}
                </button>

                <button className="btn btn-sm btn-secondary addButtonMargin" onClick={(e)=>{e.preventDefault(); this.removeButtonClicked(this.state.removeID, this.state.removeItem); this.closeModalRemove}}>
                    {strings.remove}
                </button>
            

            </div>

        )
    }

    renderActivateButton(cam, item){
        if (cam.status == "Active"){ 
            return(
              <p className="greenFont">
                  {strings.activated}
              </p>
            )
        }

        if (cam.status == "Sold" || cam.status == "Disabled") {
            return (  
              <p className="greenFont">
                  {strings.activated}
              </p>
            )
        }

        if (cam.status == "Expired"){ 
            return(
              <button className="btn btn-success" onClick={(e)=>{e.preventDefault(); this.renderConfirmationModal(strings.activate_item, "Activate", cam, item) }}>
                  {strings.reactivated}
              </button>
            )
        }

    }

    renderSoldButton(cam, item){

        if (cam.status == "Active"){ 
            return(
              <button className="btn btn-secondary" onClick={(e)=>{e.preventDefault(); this.renderConfirmationModal(strings.change_to_sold, "Sold", cam, item)} }>
                  {strings.sold}
              </button>
            )
        }

        if (cam.status == "Sold" || cam.status == "Disabled" || cam.status == "Expired") {
            return (  
              <button className="btn btn-secondary" disabled="true">
                {strings.sold}
              </button>
            )
        }
    }

    renderDisableButton(cam, item){
        //<button className={cam.status == "Sold" ? "btn-default" : "btn-default"} disabled={cam.status == "Sold" ? true : false} onClick={(e)=>{e.preventDefault(); this.disableButtonClicked("Camera", cam.id) }}>
        //                              Disable
        //                            </button>
        if (cam.status == 'Active'){
            return(
                <button className="btn btn-secondary" onClick={(e)=>{e.preventDefault(); this.renderConfirmationModal(strings.are_you_sure_you_want_to_disable, "Disable", cam, item) }}>
                    {strings.disable}
                </button>
            )
        }

        if (cam.status == 'Sold' || cam.status == "Expired" ){
            return(
                <button className="btn btn-secondary" disabled="true">
                    {strings.disable}
                </button>
            )
        }

        if (cam.status == 'Disabled'){
            return(
                <button className="btn btn-secondary" onClick={(e)=>{e.preventDefault(); this.renderConfirmationModal(strings.enable_this_item, "Enable", cam, item) }}>
                    {strings.enable}
                </button>
            )
        }
    }


    render() {
        const { dashboard, profile, video_cameras } = this.state
        return (
            <div>
            <div>
                <ul className="breadcrumb">
                      <li><Link to="/">{strings.home}</Link></li>
                      <li>{strings.dashboard}</li>
                </ul>
            </div>
            <div className='container'>
                <div className={this.props.rtlconv == "rtl" ? ' righttoleft' : ''}>
                    {this.state.fetching ? null : <p>Loading...</p> }
                    <h4>{strings.your_second_hand_account}</h4>
                    { this.state.profile != '' ? <h2>{strings.welcome_back} { this.state.profile.user.username }</h2> : null }
                    <h4>{strings.my_items_forsale}</h4>
                </div>

                { this.renderFlashMessages() }
                
                <div className="dash-container">
                    <div className={this.props.rtlconv == "rtl" ? ' righttoleft' : ''}>
                        <span className={this.props.rtlconv == "rtl" ? "side-arrow-rtl orange-font" : 'side-arrow orange-font'}>{strings.active}</span>
                    </div>
                    <table className="table table-striped">
                        <thead className="table-header">
                            <tr>
                                <th>{strings.product_type_c}</th>
                                <th>{strings.brand_c}</th>
                                <th>{strings.model_c}</th>
                                <th>{strings.price_c}</th>
                                <th>{strings.status_tc}</th>
                                <th>{strings.expiry_c}</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        {this.state.fetching && this.state.dashboard && this.state.dashboard.map((cam, i) => {
                            
                            
                            return cam.status == "Active" ?
                              <tr key={cam.id} className={cam.status == "Sold" || cam.status == "Disabled" ? 'item-sold' : null}>
                                  <td>Still Camera</td>  
                                  <td>{cam.item.cam_type.brand.company}</td>
                                  <td>{cam.item.model}</td>
                                  <td>{cam.price}</td>
                                  <td>{cam.status}</td>
                                  <td><Time value={cam.expiry} format="YYYY/MM/DD" /></td>
                                  <td className="ta-center">
                                    {this.renderActivateButton(cam, "Camera")}
                                  </td>
                                  <td>
                                    {this.renderSoldButton(cam, "Camera")}
                                  </td>
                                  <td className="ta-center">
                                    {this.renderDisableButton(cam, "Camera")}
                                  </td>
                                  <td>
                                    <button className={cam.status == "Sold" || cam.status == "Disabled" || cam.status == "Expired" ? "btn btn-secondary" : "btn btn-secondary"}
                                        disabled={cam.status == "Sold" || cam.status == "Disabled" || cam.status == "Expired" ? true : false} 
                                        onClick={(e)=>{e.preventDefault(); 
                                        
                                        history.push({
                                          'pathname':'/edit_cam_for_sale',     
                                          'state' : {
                                              'id' : cam.id,
                                              'item' : cam.item.id,
                                              'brand' : cam.item.cam_type.brand.company,
                                              'type' : cam.item.cam_type.camera_type,
                                              'model' : cam.item.model,
                                              'price' : cam.price,
                                              'status' : cam.status,
                                              'condition' : cam.condition,
                                              'shutter_count' : cam.shutter_count,
                                              'warranty' : cam.warranty,
                                              'cam_package' : cam.package,
                                              'image' : cam.item.image,
                                              'actual_photos' : cam.actual_photos,
                                              'comments' : cam.comments,
                                              'description' : cam.description,
                                              'lens_photos' : cam.lens_photos,
                                              'lens_package' : cam.lens_package,
                                          }
                                        })
                                      }}>
                                      {strings.edit}
                                    </button>
                                  </td>
                                  <td>
                                    <button className="btn btn-danger" onClick={(e)=>{e.preventDefault(); this.openModalRemove(cam.id, "Camera") }}>
                                      {strings.remove}
                                    </button>
                                  </td>
                              </tr>  
  
                            :null
                        })}

                        {this.state.fetching && this.state.video_cameras && this.state.video_cameras.map((cam, i) => {
                              
                            return cam.status == "Active" ?
                              <tr key={cam.id} className={cam.status == "Sold" || cam.status == "Disabled" ? 'item-sold' : null}>
                                  <td>Video Camera</td>   
                                  <td>{cam.item.cam_type.brand.company}</td>
                                  <td>{cam.item.model}</td>
                                  <td>{cam.price}</td>
                                  <td>{cam.status}</td>
                                  <td><Time value={cam.expiry} format="YYYY/MM/DD" /></td>
                                  <td className="ta-center">
                                    {this.renderActivateButton(cam, "Video")}
                                  </td>
                                   <td>
                                    {this.renderSoldButton(cam, "Video")}
                                  </td>
                                  <td className="ta-center">
                                    {this.renderDisableButton(cam, "Video")}
                                  </td>
                                  <td>
                                    <button className={cam.status == "Sold" || cam.status == "Disabled" || cam.status == "Expired" ? "btn btn-secondary" : "btn btn-secondary"}
                                        disabled={cam.status == "Sold" || cam.status == "Disabled" || cam.status == "Expired" ? true : false} 
                                        onClick={(e)=>{e.preventDefault(); 
                                         
                                        history.push({
                                          'pathname':'/edit_video_for_sale',     
                                          'state' : {
                                              'id' : cam.id,
                                              'item' : cam.item.id,
                                              'brand' : cam.item.cam_type.brand.company,
                                              'type' : cam.item.cam_type.camera_type,
                                              'model' : cam.item.model,
                                              'price' : cam.price,
                                              'status' : cam.status,
                                              'condition' : cam.condition,
                                              'shutter_count' : cam.shutter_count,
                                              'warranty' : cam.warranty,
                                              'cam_package' : cam.package,
                                              'image' : cam.item.image,
                                              'actual_photos' : cam.actual_photos,
                                              'comments' : cam.comments,
                                              'description' : cam.description,
                                              'lens_photos' : cam.lens_photos,
                                              'lens_package' : cam.lens_package,
                                          }
                                        })
                                      }}>
                                      {strings.edit}
                                    </button>
                                  </td>
                                  <td>
                                    <button className="btn btn-danger" onClick={(e)=>{e.preventDefault(); this.openModalRemove(cam.id, "Video") }}>
                                      {strings.remove}
                                    </button>
                                  </td>                                  
                              </tr>    

                            :null
                        })}

                        {this.state.fetching && this.state.lenses && this.state.lenses.map((lens, i) => {
                            
                            return lens.status == "Active" ?
                                
                              <tr key={lens.id} className={lens.status == "Sold" || lens.status == "Disabled" ? 'item-sold' : null}>
                                  <td>Lens</td> 
                                  <td>{lens.item.type_of_lens.brand.company}</td>
                                  <td>{lens.item.model}</td>
                                  <td>{lens.price}</td>
                                  <td>{lens.status}</td>
                                  <td><Time value={lens.expiry} format="YYYY/MM/DD" /></td>
                                  <td className="ta-center">
                                    {this.renderActivateButton(lens, "Lens")}
                                  </td>
                                   <td>
                                    {this.renderSoldButton(lens, "Lens")}
                                  </td>
                                  <td className="ta-center">
                                    {this.renderDisableButton(lens, "Lens")}
                                  </td>
                                  <td>
                                    <button className={lens.status == "Sold" || lens.status == "Disabled" || lens.status == "Expired" ? "btn btn-secondary" : "btn btn-secondary"}
                                        disabled={lens.status == "Sold" || lens.status == "Disabled" || lens.status == "Expired" ? true : false} 
                                        onClick={(e)=>{e.preventDefault(); 
                                         
                                        history.push({
                                          'pathname':'/edit_lens_for_sale',     
                                          'state' : {
                                              'id' : lens.id,
                                              'item': lens.item,
                                              'brand' : lens.item.type_of_lens.brand.company,
                                              'type' : lens.item.type_of_lens.lens_type,
                                              'model' : lens.item.model,
                                              'price' : lens.price,
                                              'status' : lens.status,
                                              'condition' : lens.condition,
                                              'comments' : lens.comments,
                                              'description' : lens.description,
                                              'warranty' : lens.warranty,
                                              'lens_package' : lens.package,
                                              'image' : lens.item.image,
                                              'actual_photos' : lens.actual_photos,
                                          }
                                        })
                                      }}>
                                      {strings.edit}
                                    </button>
                                  </td> 
                                  <td>
                                    <button className="btn btn-danger" onClick={(e)=>{e.preventDefault(); this.openModalRemove(lens.id, "Lens") }}>
                                      {strings.remove}
                                    </button>
                                  </td>                                 
                              </tr> 

                              :null
                              
                        })}

                        {this.state.fetching && this.state.lightings && this.state.lightings.map((light, i) => {
                            
                            return light.status == "Active" ?
  
                              <tr key={light.id} className={light.status == "Sold" || light.status == "Disabled" ? 'item-sold' : null}>
                                  <td>Lighting</td> 
                                  <td>{light.brand}</td>
                                  <td>{light.model}</td>
                                  <td>{light.price}</td>
                                  <td>{light.status}</td>
                                  <td><Time value={light.expiry} format="YYYY/MM/DD" /></td>
                                  <td className="ta-center">
                                    {this.renderActivateButton(light, "Lighting")}
                                  </td>
                                   <td>
                                    {this.renderSoldButton(light, "Lighting")}
                                  </td>
                                  <td className="ta-center">
                                    {this.renderDisableButton(light, "Lighting")}
                                  </td>
                                  <td>
                                    <button className={light.status == "Sold" || light.status == "Disabled" || light.status == "Expired" ? "btn btn-secondary" : "btn btn-secondary"}
                                        disabled={light.status == "Sold" || light.status == "Disabled" || light.status == "Expired" ? true : false} 
                                        onClick={(e)=>{e.preventDefault(); 
                                          
                                        history.push({
                                          'pathname':'/edit_lighting_for_sale',     
                                          'state' : {
                                              'id' : light.id,
                                              'brand' : light.brand,
                                              'model' : light.model,
                                              'category' : light.category,
                                              'price' : light.price,
                                              'status' : light.status,
                                              'condition' : light.condition,
                                              'comment' : light.comment,
                                              'description' : light.description,
                                              'warranty' : light.warranty,
                                              'actual_photos' : light.actual_photos
                                          }
                                        })
                                      }}>
                                      {strings.edit}
                                    </button>
                                  </td>  
                                  <td>
                                    <button className="btn btn-danger" onClick={(e)=>{e.preventDefault(); this.openModalRemove(light.id, "Lighting") }}>
                                      {strings.remove}
                                    </button>
                                  </td>                                
                              </tr>   

                              :null
                              
                        })}

                        {this.state.fetching && this.state.accessories && this.state.accessories.map((acc, i) => {
                            
                            return acc.status == "Active" ?
  
                              <tr key={acc.id} className={acc.status == "Sold" || acc.status == "Disabled" ? 'item-sold' : null}>
                                  <td>Accessories</td> 
                                  <td>{acc.brand}</td>
                                  <td>{acc.model}</td>
                                  <td>{acc.price}</td>
                                  <td>{acc.status}</td>
                                  <td><Time value={acc.expiry} format="YYYY/MM/DD" /></td>
                                  <td className="ta-center">
                                    {this.renderActivateButton(acc, "Accessories")}
                                  </td>
                                   <td>
                                    {this.renderSoldButton(acc, "Accessories")}
                                  </td>
                                  <td className="ta-center">
                                    {this.renderDisableButton(acc, "Accessories")}
                                  </td>
                                  <td>
                                    <button className={acc.status == "Sold" || acc.status == "Disabled" || acc.status == "Expired" ? "btn btn-secondary" : "btn btn-secondary"}
                                        disabled={acc.status == "Sold" || acc.status == "Disabled" || acc.status == "Expired" ? true : false} 
                                        onClick={(e)=>{e.preventDefault(); 
                                         
                                        history.push({
                                          'pathname':'/edit_accessories_for_sale',     
                                          'state' : {
                                              'id' : acc.id,
                                              'brand' : acc.brand,
                                              'model' : acc.model,
                                              'category' : acc.category,
                                              'price' : acc.price,
                                              'status' : acc.status,
                                              'condition' : acc.condition,
                                              'comment' : acc.comment,
                                              'description' : acc.description,
                                              'warranty' : acc.warranty,
                                              'actual_photos' : acc.actual_photos
                                          }
                                        })
                                      }}>
                                      {strings.edit}
                                    </button>
                                  </td>  
                                  <td>
                                    <button className="btn btn-danger" onClick={(e)=>{e.preventDefault(); this.openModalRemove(acc.id, "Accessories") }}>
                                      {strings.remove}
                                    </button>
                                  </td>                                
                              </tr>    

                              :null
                               
                        })}


                        {this.state.fetching && this.state.drones && this.state.drones.map((drone, i) => {
                            
                            return drone.status == "Active" ?
  
                              <tr key={drone.id} className={drone.status == "Sold" || drone.status == "Disabled" ? 'item-sold' : null}>
                                  <td>Drones</td> 
                                  <td>{drone.brand}</td>
                                  <td>{drone.model}</td>
                                  <td>{drone.price}</td>
                                  <td>{drone.status}</td>
                                  <td><Time value={drone.expiry} format="YYYY/MM/DD" /></td>
                                  <td className="ta-center">
                                    {this.renderActivateButton(drone, "Drone")}
                                  </td>
                                   <td>
                                    {this.renderSoldButton(drone, "Drone")}
                                  </td>
                                  <td className="ta-center">
                                    {this.renderDisableButton(drone, "Drone")}
                                  </td>
                                  <td>
                                    <button className={drone.status == "Sold" || drone.status == "Disabled" || drone.status == "Expired" ? "btn btn-secondary" : "btn btn-secondary"}
                                        disabled={drone.status == "Sold" || drone.status == "Disabled" || drone.status == "Expired" ? true : false} 
                                        onClick={(e)=>{e.preventDefault(); 

                                        history.push({
                                          'pathname':'/edit_drones_for_sale',     
                                          'state' : {
                                              'id' : drone.id,
                                              'brand' : drone.brand,
                                              'model' : drone.model,
                                              'category' : drone.drone_type,
                                              'price' : drone.price,
                                              'status' : drone.status,
                                              'condition' : drone.condition,
                                              'comment' : drone.comment,
                                              'warranty' : drone.warranty,
                                              'camera' : drone.camera,
                                              'video_resolution' : drone.video_resolution,
                                              'megapixels' : drone.megapixels,
                                              'operating_time' : drone.operating_time,
                                              'control_range' : drone.control_range,
                                              'control_range_unit' : drone.control_range_unit,
                                              'actual_photos' : drone.actual_photos
                                          }
                                        })
                                      }}>
                                      {strings.edit}
                                    </button>
                                  </td>  
                                  <td>
                                    <button className="btn btn-danger" onClick={(e)=>{e.preventDefault(); this.openModalRemove(drone.id, "Drone") }}>
                                      {strings.remove}
                                    </button>
                                  </td>                                
                              </tr>    

                              :null
                               
                        })}
                        </tbody>
                    </table>


                    {this.state.hasExpiredDisabledItems ? 
                    <div>
                    <div className={this.props.rtlconv == "rtl" ? ' righttoleft' : ''}>
                        <span className={this.props.rtlconv == "rtl" ? "side-arrow-rtl orange-font" : 'side-arrow orange-font'}>{strings.expired}</span>
                    </div>  
                    <table className="table table-striped">
                        <thead className="table-header">
                            <tr>
                                <th>{strings.product_type_c}</th>
                                <th>{strings.brand_c}</th>
                                <th>{strings.model_c}</th>
                                <th>{strings.price_c}</th>
                                <th>{strings.status_tc}</th>
                                <th>{strings.expiry_c}</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        {this.state.fetching && this.state.dashboard && this.state.dashboard.map((cam, i) => {
                            
                            return cam.status == "Expired" || cam.status == "Disabled" ?
                                
                              <tr key={cam.id} className={cam.status == "Sold" || cam.status == "Disabled" ? 'item-sold' : null}>
                                  <td>Still Camera</td>  
                                  <td>{cam.item.cam_type.brand.company}</td>
                                  <td>{cam.item.model}</td>
                                  <td>{cam.price}</td>
                                  <td>{cam.status}</td>
                                  <td><Time value={cam.expiry} format="YYYY/MM/DD" /></td>
                                  <td className="ta-center">
                                    {this.renderActivateButton(cam, "Camera")}
                                  </td>
                                  <td>
                                    {this.renderSoldButton(cam, "Camera")}
                                  </td>
                                  <td className="ta-center">
                                    {this.renderDisableButton(cam, "Camera")}
                                  </td>
                                  <td>
                                    <button className={cam.status == "Sold" || cam.status == "Disabled" || cam.status == "Expired" ? "btn btn-secondary" : "btn btn-secondary"}
                                        disabled={cam.status == "Sold" || cam.status == "Disabled" || cam.status == "Expired" ? true : false} 
                                        onClick={(e)=>{e.preventDefault(); 
                                        
                                        history.push({
                                          'pathname':'/edit_cam_for_sale',     
                                          'state' : {
                                              'id' : cam.id,
                                              'brand' : cam.item.cam_type.brand.company,
                                              'type' : cam.item.cam_type.camera_type,
                                              'model' : cam.item.model,
                                              'price' : cam.price,
                                              'status' : cam.status,
                                              'condition' : cam.condition,
                                              'shutter_count' : cam.shutter_count,
                                              'warranty' : cam.warranty,
                                              'cam_package' : cam.package,
                                              'image' : cam.item.image,
                                              'actual_photos' : cam.actual_photos,
                                              'lens_photos' : cam.lens_photos,
                                              'lens_package' : cam.lens_package.all(),
                                              'acc_package' : cam.description
                                          }
                                        })
                                      }}>
                                      {strings.edit}
                                    </button>
                                  </td>
                                  <td>
                                    <button className="btn btn-danger" onClick={(e)=>{e.preventDefault(); this.openModalRemove(cam.id, "Camera") }}>
                                      {strings.remove}
                                    </button>
                                  </td>
                              </tr>

                              :null
                              
                        })}

                        {this.state.fetching && this.state.video_cameras && this.state.video_cameras.map((cam, i) => {
                            
                            return cam.status == "Expired" || cam.status == "Disabled" ?
                                
                              <tr key={cam.id} className={cam.status == "Sold" || cam.status == "Disabled" ? 'item-sold' : null}>
                                  <td>Video Camera</td>   
                                  <td>{cam.item.cam_type.brand.company}</td>
                                  <td>{cam.item.model}</td>
                                  <td>{cam.price}</td>
                                  <td>{cam.status}</td>
                                  <td><Time value={cam.expiry} format="YYYY/MM/DD" /></td>
                                  <td className="ta-center">
                                    {this.renderActivateButton(cam, "Video")}
                                  </td>
                                   <td>
                                    {this.renderSoldButton(cam, "Video")}
                                  </td>
                                  <td className="ta-center">
                                    {this.renderDisableButton(cam, "Video")}
                                  </td>
                                  <td>
                                    <button className={cam.status == "Sold" || cam.status == "Disabled" || cam.status == "Expired" ? "btn btn-secondary" : "btn btn-secondary"}
                                        disabled={cam.status == "Sold" || cam.status == "Disabled" || cam.status == "Expired" ? true : false} 
                                        onClick={(e)=>{e.preventDefault(); 
                                          
                                        history.push({
                                          'pathname':'/edit_cam_for_sale',     
                                          'state' : {
                                              'id' : cam.id,
                                              'brand' : cam.item.cam_type.brand.company,
                                              'type' : cam.item.cam_type.camera_type,
                                              'model' : cam.item.model,
                                              'price' : cam.price,
                                              'status' : cam.status,
                                              'condition' : cam.condition,
                                              'description' : cam.description,
                                              'warranty' : cam.warranty,
                                              'cam_package' : cam.package,
                                              'image' : cam.item.image
                                          }
                                        })
                                      }}>
                                      {strings.edit}
                                    </button>
                                  </td>
                                  <td>
                                    <button className="btn btn-danger" onClick={(e)=>{e.preventDefault(); this.openModalRemove(cam.id, "Video") }}>
                                      {strings.remove}
                                    </button>
                                  </td>                                  
                              </tr>  

                              :null
                              
                        })}

                        {this.state.fetching && this.state.lenses && this.state.lenses.map((lens, i) => {
                            
                            return lens.status == "Expired" || lens.status == "Disabled" ?
  
                              <tr key={lens.id} className={lens.status == "Sold" || lens.status == "Disabled" ? 'item-sold' : null}>
                                  <td>Lens</td> 
                                  <td>{lens.item.type_of_lens.brand.company}</td>
                                  <td>{lens.item.model}</td>
                                  <td>{lens.price}</td>
                                  <td>{lens.status}</td>
                                  <td><Time value={lens.expiry} format="YYYY/MM/DD" /></td>
                                  <td className="ta-center">
                                    {this.renderActivateButton(lens, "Lens")}
                                  </td>
                                   <td>
                                    {this.renderSoldButton(lens, "Lens")}
                                  </td>
                                  <td className="ta-center">
                                    {this.renderDisableButton(lens, "Lens")}
                                  </td>
                                  <td>
                                    <button className={lens.status == "Sold" || lens.status == "Disabled" || lens.status == "Expired" ? "btn btn-secondary" : "btn btn-secondary"}
                                        disabled={lens.status == "Sold" || lens.status == "Disabled" || lens.status == "Expired" ? true : false} 
                                        onClick={(e)=>{e.preventDefault(); 
                                       
                                        history.push({
                                          'pathname':'/edit_lens_for_sale',     
                                          'state' : {
                                              'id' : lens.id,
                                              'brand' : lens.item.type_of_lens.brand.company,
                                              'type' : lens.item.type_of_lens.lens_type,
                                              'model' : lens.item.model,
                                              'price' : lens.price,
                                              'status' : lens.status,
                                              'condition' : lens.condition,
                                              'description' : lens.description,
                                              'warranty' : lens.warranty,
                                              'lens_package' : lens.package,
                                              'image' : lens.item.image
                                          }
                                        })
                                      }}>
                                      {strings.edit}
                                    </button>
                                  </td> 
                                  <td>
                                    <button className="btn btn-danger" onClick={(e)=>{e.preventDefault(); this.openModalRemove(lens.id, "Lens") }}>
                                      {strings.remove}
                                    </button>
                                  </td>                                 
                              </tr> 
                              :null
                              
                        })}

                        {this.state.fetching && this.state.lightings && this.state.lightings.map((light, i) => {
                            
                            return light.status == "Expired" || light.status == "Disabled" ?
                                
                              <tr key={light.id} className={light.status == "Sold" || light.status == "Disabled" ? 'item-sold' : null}>
                                  <td>Lighting</td> 
                                  <td>{light.brand}</td>
                                  <td>{light.model}</td>
                                  <td>{light.price}</td>
                                  <td>{light.status}</td>
                                  <td><Time value={light.expiry} format="YYYY/MM/DD" /></td>
                                  <td className="ta-center">
                                    {this.renderActivateButton(light, "Lighting")}
                                  </td>
                                   <td>
                                    {this.renderSoldButton(light, "Lighting")}
                                  </td>
                                  <td className="ta-center">
                                    {this.renderDisableButton(light, "Lighting")}
                                  </td>
                                  <td>
                                    <button className={light.status == "Sold" || light.status == "Disabled" || light.status == "Expired" ? "btn btn-secondary" : "btn btn-secondary"}
                                        disabled={light.status == "Sold" || light.status == "Disabled" || light.status == "Expired" ? true : false} 
                                        onClick={(e)=>{e.preventDefault(); 
                                          
                                        history.push({
                                          'pathname':'/edit_light_for_sale',     
                                          'state' : {
                                              'id' : light.id,
                                              'brand' : light.brand,
                                              'model' : light.model,
                                              'price' : light.price,
                                              'status' : light.status,
                                              'condition' : light.condition,
                                              'description' : light.description,
                                              'warranty' : light.warranty,
                                              'image' : light.item.image
                                          }
                                        })
                                      }}>
                                      {strings.edit}
                                    </button>
                                  </td>  
                                  <td>
                                    <button className="btn btn-danger" onClick={(e)=>{e.preventDefault(); this.openModalRemove(light.id, "Lighting") }}>
                                      {strings.remove}
                                    </button>
                                  </td>                                
                              </tr>   

                              :null
                              
                        })}

                        {this.state.fetching && this.state.accessories && this.state.accessories.map((acc, i) => {
                            return acc.status == "Expired" || acc.status == "Disabled" ?
                                
                              <tr key={acc.id} className={acc.status == "Sold" || acc.status == "Disabled" ? 'item-sold' : null}>
                                  <td>Accessories</td> 
                                  <td>{acc.brand}</td>
                                  <td>{acc.model}</td>
                                  <td>{acc.price}</td>
                                  <td>{acc.status}</td>
                                  <td><Time value={acc.expiry} format="YYYY/MM/DD" /></td>
                                  <td className="ta-center">
                                    {this.renderActivateButton(acc, "Accessories")}
                                  </td>
                                   <td>
                                    {this.renderSoldButton(acc, "Accessories")}
                                  </td>
                                  <td className="ta-center">
                                    {this.renderDisableButton(acc, "Accessories")}
                                  </td>
                                  <td>
                                    <button className={acc.status == "Sold" || acc.status == "Disabled" || acc.status == "Expired" ? "btn btn-secondary" : "btn btn-secondary"}
                                        disabled={acc.status == "Sold" || acc.status == "Disabled" || acc.status == "Expired" ? true : false} 
                                        onClick={(e)=>{e.preventDefault(); 
                                         
                                        history.push({
                                          'pathname':'/edit_accessories_for_sale',     
                                          'state' : {
                                              'id' : acc.id,
                                              'brand' : acc.brand,
                                              'model' : acc.model,
                                              'price' : acc.price,
                                              'status' : acc.status,
                                              'condition' : acc.condition,
                                              'description' : acc.description,
                                              'warranty' : acc.warranty,
                                              'image' : acc.item.image
                                          }
                                        })
                                      }}>
                                      {strings.edit}
                                    </button>
                                  </td>  
                                  <td>
                                    <button className="btn btn-danger" onClick={(e)=>{e.preventDefault(); this.openModalRemove(acc.id, "Accessories") }}>
                                      {strings.remove}
                                    </button>
                                  </td>                                
                              </tr>  

                              :null
                              
                        })}

                        {this.state.fetching && this.state.drones && this.state.drones.map((dron, i) => {
                            return dron.status == "Expired" || dron.status == "Disabled" ?
                                
                              <tr key={dron.id} className={dron.status == "Sold" || dron.status == "Disabled" ? 'item-sold' : null}>
                                  <td>Drones</td> 
                                  <td>{dron.brand}</td>
                                  <td>{dron.model}</td>
                                  <td>{dron.price}</td>
                                  <td>{dron.status}</td>
                                  <td><Time value={dron.expiry} format="YYYY/MM/DD" /></td>
                                  <td className="ta-center">
                                    {this.renderActivateButton(dron, "Drones")}
                                  </td>
                                   <td>
                                    {this.renderSoldButton(dron, "Drones")}
                                  </td>
                                  <td className="ta-center">
                                    {this.renderDisableButton(dron, "Drones")}
                                  </td>
                                  <td>
                                    <button className={dron.status == "Sold" || dron.status == "Disabled" || dron.status == "Expired" ? "btn btn-secondary" : "btn btn-secondary"}
                                        disabled={dron.status == "Sold" || dron.status == "Disabled" || dron.status == "Expired" ? true : false} 
                                        onClick={(e)=>{e.preventDefault(); 
                                         
                                        history.push({
                                          'pathname':'/edit_drones_for_sale',     
                                          'state' : {
                                              'id' : dron.id,
                                              'brand' : dron.brand,
                                              'model' : dron.model,
                                              'price' : dron.price,
                                              'status' : dron.status,
                                              'condition' : dron.condition,
                                              'description' : dron.description,
                                              'warranty' : dron.warranty,
                                              'image' : dron.item.image
                                          }
                                        })
                                      }}>
                                      {strings.edit}
                                    </button>
                                  </td>  
                                  <td>
                                    <button className="btn btn-danger" onClick={(e)=>{e.preventDefault(); this.openModalRemove(acc.id, "Drones") }}>
                                      {strings.remove}
                                    </button>
                                  </td>                                
                              </tr>  

                              :null
                              
                        })}
                        </tbody>
                    </table>
                    </div>
                    :null}

                    {this.state.hasSoldItems ?
                    <div>
                    <div className={this.props.rtlconv == "rtl" ? ' righttoleft' : ''}>
                        <span className={this.props.rtlconv == "rtl" ? "side-arrow-rtl orange-font" : 'side-arrow orange-font'}>{strings.sold}</span>
                    </div>  
                    <table className="table">
                        <thead className="table-header">
                            <tr>
                                <th>{strings.product_type_c}</th>
                                <th>{strings.brand_c}</th>
                                <th>{strings.model_c}</th>
                                <th>{strings.price_c}</th>
                                <th>{strings.status_tc}</th>
                                <th>{strings.soldon_c}</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        {this.state.fetching && this.state.dashboard && this.state.dashboard.map((cam, i) => {

                            return cam.status == "Sold" ?

                              <tr key={cam.id} className={cam.status == "Sold" || cam.status == "Disabled" ? 'item-sold' : null}>
                                  <td>Still Camera</td>  
                                  <td>{cam.item.cam_type.brand.company}</td>
                                  <td>{cam.item.model}</td>
                                  <td>{cam.price}</td>
                                  <td>{cam.status}</td>
                                  <td><Time value={cam.sold_on} format="YYYY/MM/DD" /></td>
                                  <td></td>
                                    
                                  <td></td>
                                    
                                  <td></td>
                                    
                                  <td></td>
                                    
                                  <td></td>
                              </tr>   

                              : null 
                              
                        })}

                        {this.state.fetching && this.state.video_cameras && this.state.video_cameras.map((cam, i) => {
                            
                            return cam.status == "Sold" ?
  
                              <tr key={cam.id} className={cam.status == "Sold" || cam.status == "Disabled" ? 'item-sold' : null}>
                                  <td>Video Camera</td>   
                                  <td>{cam.item.cam_type.brand.company}</td>
                                  <td>{cam.item.model}</td>
                                  <td>{cam.price}</td>
                                  <td>{cam.status}</td>
                                  <td><Time value={cam.sold_on} format="YYYY/MM/DD" /></td>
                                  <td></td>
                                    
                                  <td></td>
                                    
                                  <td></td>
                                    
                                  <td></td>
                                    
                                  <td></td>                                 
                              </tr>
                              
                              : null
                              
                        })}

                        {this.state.fetching && this.state.lenses && this.state.lenses.map((lens, i) => {
                            
                            return lens.status == "Sold" ?
                                
                              <tr key={lens.id} className={lens.status == "Sold" || lens.status == "Disabled" ? 'item-sold' : null}>
                                  <td>Lens</td> 
                                  <td>{lens.item.type_of_lens.brand.company}</td>
                                  <td>{lens.item.model}</td>
                                  <td>{lens.price}</td>
                                  <td>{lens.status}</td>
                                  <td><Time value={lens.sold_on} format="YYYY/MM/DD" /></td>
                                  <td></td>
                                    
                                  <td></td>
                                    
                                  <td></td>
                                    
                                  <td></td>
                                    
                                  <td></td>                             
                              </tr>

                              :null
                              
                        })}

                        {this.state.fetching && this.state.lightings && this.state.lightings.map((light, i) => {
                            
                            return light.status == "Sold" ?
                              
                              <tr key={light.id} className={light.status == "Sold" || light.status == "Disabled" ? 'item-sold' : null}>
                                  <td>Lighting</td> 
                                  <td>{light.brand}</td>
                                  <td>{light.model}</td>
                                  <td>{light.price}</td>
                                  <td>{light.status}</td>
                                  <td><Time value={light.sold_on} format="YYYY/MM/DD" /></td>
                                  <td></td>
                                    
                                  <td></td>
                                    
                                  <td></td>
                                    
                                  <td></td>
                                    
                                  <td></td>                               
                              </tr> 

                              :null
                              
                        })}

                        {this.state.fetching && this.state.accessories && this.state.accessories.map((acc, i) => {
                            
                            return acc.status == "Sold" ?  
                                
                              <tr key={acc.id} className={acc.status == "Sold" || acc.status == "Disabled" ? 'item-sold' : null}>
                                  <td>Accessories</td> 
                                  <td>{acc.brand}</td>
                                  <td>{acc.model}</td>
                                  <td>{acc.price}</td>
                                  <td>{acc.status}</td>
                                  <td><Time value={acc.sold_on} format="YYYY/MM/DD" /></td>
                                  <td></td>
                                    
                                  <td></td>
                                    
                                  <td></td>
                                    
                                  <td></td>
                                    
                                  <td></td>                               
                              </tr>

                              :null
                              
                        })}

                        {this.state.fetching && this.state.drones && this.state.drones.map((dron, i) => {
                            
                            return dron.status == "Sold" ?  
                                
                              <tr key={dron.id} className={dron.status == "Sold" || dron.status == "Disabled" ? 'item-sold' : null}>
                                  <td>Drones</td> 
                                  <td>{dron.brand}</td>
                                  <td>{dron.model}</td>
                                  <td>{dron.price}</td>
                                  <td>{dron.status}</td>
                                  <td><Time value={dron.sold_on} format="YYYY/MM/DD" /></td>
                                  <td></td>
                                    
                                  <td></td>
                                    
                                  <td></td>
                                    
                                  <td></td>
                                    
                                  <td></td>                               
                              </tr>

                              :null
                              
                        })}
                        </tbody>
                    </table>
                    </div>
                    :null}
                </div>  

                
                { this.renderFlashMessages() }

                <Modal
                    isOpen={this.state.removeModalIsOpen}
                    onAfterOpen={this.afterOpenModalRemove}
                    onRequestClose={this.closeModalRemove}
                    style={customStylesForAcc}
                    contentLabel={strings.confirm}
                    ariaHideApp={false}
                >
                    
                    <div className="remove-frame arabicfont"> 
                        <button className="btn btn-danger btn-sm float-right fixedclosed modalButton" onClick={this.closeModalRemove}> X </button>
                        <div className="scroll"> 
                              {this.renderRemoveModal()}
                        </div>
                    </div>
                    
                </Modal>
                <br />
                <br />
                <br />
                <br />
                <br />  
            </div>
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        dash: state.dash,
        inboxReducer: state.inboxReducer,
        camera_sell: state.camera_sell,
        video_sell: state.camerasell,
        lens_sell: state.lenssell,
        lighting_sell: state.lightingsell,
        accessories_sell: state.accessoriessell,
        drone_sell: state.dronesell,
        credentials: state.credentialsReducer,
        rtlconv: state.rtlconv
    };
}


export default connect(mapStateToProps)(Dashboard)