import React from 'react'
import { history } from '../store/configureStore'
import { connect } from 'react-redux'
import {Link} from 'react-router-dom'
import { getVideoBrandModelsAction } from '../actions'
import { strings } from '../lib/strings';
let CONFIG = require('../lib/config.json')

const images_url = CONFIG.server_url + "/static/media_cdn"


const camStyle = {
    width: 200,
    height: "auto"
    };
const logoStyle = {
	width: 150,
    height: 50,
};

class SellDrones extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			videobrandmodels: []
		}
	}

	componentWillMount(){
        /*let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }*/   
    }


	clickStillHandle = (e) => {
	    e.preventDefault();
	    history.push('/sell_brand')
	}

	clickVideoHandle = (e) => {
	    e.preventDefault();
	    history.push('/sell_video_brand')
	}

	clickLensHandle = (e) => {
	    e.preventDefault();
	    history.push('/sell_lens_brand')
	}

	clickLightingHandle = (e) => {
	    e.preventDefault();
		history.push('/sell_lighting')
	}

	clickAccessoriesHandle = (e) => {
        e.preventDefault();
        history.push('/sell_accessories')
    }

    clickDroneHandle = (e) => {
        e.preventDefault();
        history.push('/sell_drone')
    }

  render() {
    return (
    	<div>
        <ul className="breadcrumb">
            <li><Link to="/">{strings.home}</Link></li>
            <li><Link to="/sell">{strings.sell}</Link></li>
            <li>{strings.drone}</li>
        </ul>
		<div className="drone-back">
			<div className=" bg-company-orange">
              <ul className="d-flex mx-auto justify-content-center nomarge zeroleftpadding">
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickStillHandle}><span>{strings.photography_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickLensHandle}><span>{strings.lenses}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickVideoHandle}><span>{strings.video_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickLightingHandle}><span>{strings.lighting}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickAccessoriesHandle}><span>{strings.accessories}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4 active">
                    <a className="awhite" href="" onClick={this.clickDroneHandle}><span>{strings.drone}</span></a>
                </li>
              </ul>
            
          </div>
          <div className="brand-header text-center">
	            <span>{strings.drone}</span>
	        </div>
	        

			<div className="category-body m-2" dir="ltr">
                <h4 className="text-center card-title choose-cat">{strings.select_drone_type}</h4>
                <br />
                
                    <div className="row lighting-category">
                        <div id="choose-category" className="col text-center">
                            <img src={images_url + "/images/quadcopter.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_drone_form',     
                                'state' : {
                                    'drone_type' : "Quadcopter"
                                }
                            })}}/><br />
                            <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_drone_form',     
                                'state' : {
                                    'drone_type' : "Quadcopter"
                                }
                            })}}>{strings.quadcopter}</button>
                        </div>
                        <div id="choose-category" className="col text-center">
                            <img src={images_url + "/images/hexacopter.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_drone_form',     
                                'state' : {
                                    'drone_type' : "Hexacopter"
                                }
                            })}}/><br />
                            <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_drone_form',     
                                'state' : {
                                    'drone_type' : "Hexacopter"
                                }
                            })}}>{strings.hexacopter}</button>
                        </div>
                        <div id="choose-category" className="col text-center">
                            <img src={images_url + "/images/octacopter.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_drone_form',     
                                'state' : {
                                    'drone_type' : "Octacopter"
                                }
                            })}}/><br />
                            <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_drone_form',     
                                'state' : {
                                    'drone_type' : "Octacopter"
                                }
                            })}}>{strings.octacopter}</button>
                        </div>
                        
                    </div>
                    <br />
                    <br />
                    <br />
                    <div className="row lighting-category">
                        <div id="choose-category" className="col text-center">
                            <img src={images_url + "/images/helicopter.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_drone_form',     
                                'state' : {
                                    'drone_type' : "Helicopter"
                                }
                            })}}/><br />
                            <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_drone_form',     
                                'state' : {
                                    'drone_type' : "Helicopter"
                                }
                            })}}>{strings.helicopter} </button>
                        </div>
                        <div id="choose-category" className="col text-center">
                            <img src={images_url + "/images/airplane.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_drone_form',     
                                'state' : {
                                    'drone_type' : "Airplane"
                                }
                            })}}/><br />
                            <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                                {'pathname':'/complete_drone_form',     
                                'state' : {
                                    'drone_type' : "Airplane"
                                }
                            })}}>{strings.airplane}</button>
                        </div>
                        
                    </div>
                    
            </div>
			<br />
			<br />
			<br />
			<br />
		</div>
		</div>
    )
  }
}


export default connect()(SellDrones);