
import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import { history } from '../store/configureStore'
import { userActions, getUserLocation } from '../actions';
import { CountryDropdown, RegionDropdown } from 'react-country-region-selector';

import Modal from 'react-modal';
import { strings } from '../lib/strings';

let CONFIG = require('../lib/config.json')
const images_url = CONFIG.server_url + "/static/media_cdn"

const divStyle = {
	  marginTop: '150px'
	};

const customStyles = {
    content: {
        'border-left': '5px solid #f4a72e',
        'border-top': '5px solid #f4a72e',
        'border-right': '5px solid #3d4041',
        'border-bottom': '5px solid #3d4041',
        borderRadius: '4px',
        bottom: 'auto',
        minHeight: '10rem',
        left: '50%',
        padding: '2rem',
        position: 'fixed',
        right: 'auto',
        top: '50%',
        transform: 'translate(-50%,-50%)',
        minWidth: '20rem',
        width: '80%',
        maxWidth: '60rem',
        padding: '0'
      },
    overlay: {
        backgroundColor: "rgba(255, 255, 255, 0.25)"
    }  
};


class Registration extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            firstname: '',
            lastname: '',
            email: '',
            confirm_email: '',
            password: '',
            confirmpassword: '',
            passwordcheck: false,
            emailcheck: false,
            submitted: false,
            city: '',
            country: '',
            country2: '',
            telephone:'',
            fetching: false,
            userlocation: '',
            country_code: '',
            telephone: '',
            language: 'english',
            countries_old:[
                { name: 'Saudi Arabia',  cities: ['Select City','Al-Abwa', 'Al Artaweeiyah','Badr','Baljurashi','Bisha','Bareg','Buraydah','Al Bahah','Buq a','Dammam','Dhahran','Dhurma','Dahaban','Diriyah','Duba','Dumat Al-Jandal','Dawadmi','Farasan city','Gatgat','Gerrha','Gurayat','Al-Gwei iyyah','Hautat Sudair','Habala','Hajrah','Haql','Al-Hareeq','Harmah','Ha il','Hotat Bani Tamim','Hofuf','Huraymila','Hafr Al-Batin','Jabal Umm al Ru us','Jalajil','Al Jawf','Jeddah','Jizan','Jizan Economic City','Jubail','Al Jafer','Khafji','Khaybar','King Abdullah Economic City','Khamis Mushayt','Al Kharj','Knowledge Economic City , Medina','Khobar','Al-Khutt','Layla','Lihyan','Al Lith','Al Majma ah','Mastoorah','Al Mikhwah','Al-Mubarraz','Al Mawain','Mecca','Medina','Muzahmiyya','Najran','Al-Namas','Omloj','Al-Omran','Al-Oyoon','Qadeimah','Qatif','Qaisumah','Al Qunfudhah','Rabigh','Rafha','Ar Rass','Ras Tanura','Riyadh','Riyadh Al-Khabra','Rumailah','Sabt Al Alaya','Saihat','Safwa city','Sakakah','Sharurah','Shaqraa','Shaybah','As Sulayyil','Taif','Tabuk','Tanomah','Tarout','Tayma','Thadiq','Thuwal','Thuqbah','Turaif','Tabarjal','Udhailiyah','Al-Ula','Um Al-Sahek','Unaizah','Uqair','Uyayna','Uyun AlJiwa','Wadi Al-Dawasir','Al Wajh','Yanbu','Az Zaimah','Zulfi']},
                { name: 'United Arab Emirates', cities: ['Al Awir First','Abu al Abyad','Abu Dhabi','Aj man','Al Ad la','Al Ain','Al ain','Al Awir Second','Al Bada','Al Baraha','Al Barsha First','Al Barsha Second','Al Barsha South First','Al Barsha South Second','Al Barsha South Third','Al Barsha Third','Al Buteen','Al Dhagaya','Al Garhoud','Al Guoz Fourth','Al Hamriya Port','Al Hamriya, Dubai','Al Hudaiba','Al Jaddaf','Al Jafiliya','Al Jazirah Al Hamra','Al Karama','Al Khabisi','Al Khwaneej First','Al Khwaneej Second','Al Kifaf','Al Mamzar','Al Manara','Al Mankhool','Al Merkad','Al Mina','Al Mizhar First','Al Mizhar Second','Al Mu azaz','Al Muraqqabat','Al Murar','Al Mushrif','Al Muteena','Al Muteena First','Al Muteena Second','Al Nahda First','Al Nahda Second','Al Nasr, Dubai','Al Ozaib','Al Quoz First','Al Quoz Industrial First','Al Quoz Industrial Fourth','Al Quoz Industrial Second','Al Quoz Industrial Third','Al Quoz Second','Al Quoz Third','Al Qusais First','Al Qusais Industrial Fifth','Al Qusais Industrial First','Al Qusais Industrial Fourth','Al Qusais Industrial Second','Al Qusais Industrial Third','Al Qusais Second','Al Qusais Third','Al Raffa','Al Ras','Al Rashidiya','Al Rigga','Al Sabkha','Al Safa First','Al Safa Second','Al Safouh First','Al Safouh Second','Al Salama','Al Satwa','Al Shahama','Al Shalelah','Al Shamkha','Al Shindagha','Al Souq Al Kabeer','Al Tawelah','Al Twar First','Al Twar Second','Al Twar Third','Al Waheda','Al Warqa a Fifth','Al Warqa a First','Al Warqa a Fourth','Al Warqa a Second','Al Warqa a Third','Al Wasl','Al Wathba','Al-Aryam Island','Aleyas','Ar-Rams','Arabian Ranches','Ayal Nasir','Bani Yas City','Bu Kadra','Business Bay','Diqdaqah','Dubai Investment park First','Dubai Investment Park Second','Emirates Hill First','Emirates Hill Second','Emirates Hill Third','Fujairah City','Ghantoot','Ghayathi','Habshan','Halat Al Bahrani','Hatta','Hor Al Anz','Hor Al Anz East','Huwaylat','Jebel Ali 1','Jebel Ali 2','Jebel Ali Industrial','Jebel Ali Palm','Jumeira First','Jumeira Second','Jumeira Third','Khalifa Port','Khatt','Khawr Khuwayr','Liwa Oasis','Madinat Zayed','Manama','Marabe al Dhafra','Marawah','Marsa Dubai','Masafi','Masfout','Mirdif','Muhaisanah Fourth','Muhaisanah Second','Muhaisanah Third','Muhaisnah First','Nad Al Hammar','Nad Shamma','Nadd Al Shiba Fourth','Nadd Al Shiba Second','Nadd Al Shiba Third','Naif','New Salama','Palm Jumeira','Port Saeed','Port Zayed','Ras Al Khor','Ras Al Khor Industrial First','Ras Al Khor Industrial Second','Ras Al Khor Industrial Third','Rigga Al Buteen','Ruwais','Sharjah','Sheikh Khalifa City','Sila','Sir Bani Yas','Swehan','Tarif','Trade Centre 1','Trade Centre 2','Umm Al Sheif','Umm Hurair First','Umm Hurair Second','Umm Ramool','Umm Suqeim First','Umm Suqeim Second','Umm Suqeim Third','Wadi Alamardi','Warsan First','Warsan Second','Za abeel First','Za abeel Second']},
                { name: 'Bahrain', cities: ['Manama', 'A ali ','Adliya','Al Dur','Al Seef','Al-Malikiyah','Awali','Bani Jamrah','Budaiya ','Diraz','Hamad Town','Hidd','Hoora','Isa Town','Jid Ali','Jidhafs','Mahooz','Muharraq','Riffa','Sanabis','Sar','Sitra','Tubli']},
                { name: 'Kuwait', cities: ['Kuwait City','Abdullah as-Salim suburb','Abdullah Port','Abu Fteira','Abu Hulaifa','Adān','Adiliya','Agricultural Wafra','Ahmadi','Ali as-Salim suburb','Aqila','Bayān','Bi di','Bneid il-Gār','Bneidar','Da iya','Dasma','Dasmān','Doha','Fahaheel','Fahd al-Ahmad Suburb','Faiha','Fintās','Funaitīs','Ghirnata','Hadiya','Hawally','Hittin','Jabir al-Ahmad City','Jabir al-Ali Suburb','Jabriya','Jibla','Jilei a','Kaifan','Khairan','Khairan City','Khaldiya','Mahbula','Maidan Hawalli','Mangaf','Mansūriya','Miqwa','Mirgāb','Mishrif','Misīla','Mubarak aj-Jabir suburb','Mubarak al-Kabeer','Nahdha','Nigra','Nuwaiseeb','Nuzha','Qadsiya','Qairawān','Qurain','Qurtuba','Qusūr','Rai','Rawda','Rigga','Rumaithiya','Sabah al-Ahmad City','Sabah al-Ahmad Nautical City','Sabah as-Salim suburb','Sabahiya','Sabhān','Salhiya','Salmiya','Salwa','Sawābir','Sha ab','Shamiya','Sharq','Shu aiba','Shuwaikh','South Surra','Sulaibikhat','Surra','Wafra','Yarmūk','Zoor','Zuhar']},
                { name: 'Oman', cities: ['Adam','As Sib','Al Ashkharah','Al Buraimi','Al Hamra','Al Jazer','Al Madina A Zarqa','Al Suwaiq','Bahla','Barka','Bidbid','Bidiya','Dibba Al-Baya','Duqm','Haima','Ibra','Ibri','Izki','Jabrin','Jalan Bani Bu Hassan','Khasab','Madha','Mahooth','Manah','Masirah','Matrah','Mudhaybi','Mudhaireb','Muscat','Nizwa','Quriyat','Raysut','Rustaq','Ruwi','Saham','Shinas','Saiq','Salalah','Samail','Sohar','Sur','Tan am','Thumrait']}
              ],
            countries: [
                {'name':{'arabic': 'إختر الدولة', 'english': 'Select Country'}},
                {'name':{'arabic':'المملكة العربية السعودية', 'english':'Saudi Arabia'}, 'cities' : [{'arabic': 'Select City', 'english': 'Select City'}, {'arabic': 'الباحة', 'english': 'Al Bahah'},{'arabic': 'الجبيل', 'english': 'Al Jubail'},{'arabic': 'الجفر', 'english': 'Al Jafer'},{'arabic': 'الجوف', 'english': 'Al Jawf'},{'arabic': 'الحجرة', 'english': 'AL Hajrah'},{'arabic': 'الحريق', 'english': 'Al Hareeq'},{'arabic': 'الخبر', 'english': 'Al Khobar'},{'arabic': 'الخرج', 'english': 'Al Kharj'},{'arabic': 'الدرعية', 'english': 'Al Diriyah'},{'arabic': 'الدمام', 'english': 'Al Dammam'},{'arabic': 'الدوادمي', 'english': 'Al Dawadmi'},{'arabic': 'الرس', 'english': 'Ar Rass'},{'arabic': 'عالي', 'english': 'A ali'},{'arabic': 'الرميلة', 'english': 'Rumailah'},{'arabic': 'الرياض', 'english': 'Riyadh'},{'arabic': 'الزيمة', 'english': 'Az Zaimah'},{'arabic': 'السليل', 'english': 'As Sulayyil'},{'arabic': 'الطائف', 'english': 'Al Taif'},{'arabic': 'الطريف', 'english': 'Al Turaif'},{'arabic': 'الظهران', 'english': 'Al Dhahran'},{'arabic': 'العضيلية', 'english': 'Al Udhailiyah'},{'arabic': 'العقير', 'english': 'Al Uqair'},{'arabic': 'العلا', 'english': 'Al Ula'},{'arabic': 'العمران', 'english': 'Al Omran'},{'arabic': 'العيون', 'english': 'Al Oyoon'},{'arabic': 'العيينة', 'english': 'Al Uyayna'},{'arabic': 'فيفاء', 'english': 'Faifa'},{'arabic': 'القريات', 'english': 'Al Gurayat'},{'arabic': 'القضيمة', 'english': 'Al Qadeimah'},{'arabic': 'القطيف', 'english': 'Al Qatif'},{'arabic': 'القنفذة', 'english': 'Al Qunfudhah'},{'arabic': 'القويعية', 'english': 'Al Gweiiyyah'},{'arabic': 'القيصومة', 'english': 'Al Qaisumah'},{'arabic': 'الليث', 'english': 'Al Lith'},{'arabic': 'العدلية', 'english': 'Adliya'},{'arabic': 'المجمعة', 'english': 'Al Majma ah'},{'arabic': 'الدور‎', 'english': 'Al Dur'},{'arabic': 'المدينة المنورة', 'english': 'Al Medina'},{'arabic': 'المزاحمية', 'english': 'Al Muzahmiyya'},{'arabic': 'السويف', 'english': 'Al Seef'},{'arabic': 'النماص', 'english': 'Al Namas'},{'arabic': 'الهفوف', 'english': 'Al Hofuf'},{'arabic': 'الوجه', 'english': 'Al Wajh'},{'arabic': 'بدر حنين', 'english': 'Badr Haneen'},{'arabic': 'بريدة', 'english': 'Buraydah'},{'arabic': 'بقيق', 'english':'Abqaiq'},{'arabic': 'بلجرشي', 'english': 'Baljurashi'},{'arabic': 'بيشة', 'english': 'Bisha'},{'arabic': 'تبوك', 'english': 'Tabuk'},{'arabic': 'تاروت', 'english': 'Tarout'},{'arabic': 'تربة البقوم', 'english': 'Turbah AL Buqom'},{'arabic': 'تنومة', 'english': 'Tanomah'},{'arabic': 'تيماء', 'english': 'Tayma'},{'arabic': 'ثادق', 'english': 'Thadiq'},{'arabic': 'ثقبة', 'english': 'Thuqbah'},{'arabic': 'ثول', 'english': 'Thuwal'},{'arabic': 'جازان', 'english': 'Jizan'},{'arabic': 'المالكية', 'english': 'Al-Malikiyah'},{'arabic': 'جدة', 'english': 'Jeddah'},{'arabic': 'جلاجل', 'english': 'Jalajil'},{'arabic': 'حائل', 'english': 'Ha il'},{'arabic': 'حفر الباطن', 'english': 'Hafr Al-Batin'},{'arabic': 'حقل', 'english': 'Haql'},{'arabic': 'حوطة بني تميم', 'english':'Hotat Bani Tamim'},{'arabic': 'خفجي', 'english': 'Khafji'},{'arabic': 'خميس مشيط', 'english': 'Khamis Mushayt'},{'arabic': 'دهبان', 'english': 'Dahaban'},{'arabic': 'دومة الجندل', 'english': 'Dumat Al-Jandal'},{'arabic': 'رابغ', 'english': 'Rabigh'},{'arabic': 'رأس تنورة', 'english': 'Ras Tanura'},{'arabic': 'رفحا', 'english': 'Rafha'},{'arabic': 'زلفي', 'english': 'Zulfi'},{'arabic': 'عوالي', 'english': 'Awali'},{'arabic': 'سكاكا', 'english': 'Sakakah'},{'arabic': 'سيهات', 'english': 'Saihat'},{'arabic': 'شرورة', 'english': 'Sharurah'},{'arabic': 'شقراء', 'english': 'Shaqraa'},{'arabic': 'شيبه', 'english': 'Shaybah'},{'arabic': 'صفوى', 'english': 'Safwa'},{'arabic': 'ضبا', 'english': 'Duba'},{'arabic': 'ضرما', 'english': 'Dhurma'},{'arabic': 'بني جمرة', 'english':'Bani Jamrah'},{'arabic': 'عرعر', 'english': 'Arar'},{'arabic': 'عفيف', 'english': 'Afif'},{'arabic': 'عنيزة', 'english': 'Unaizah'},{'arabic': 'فرسان', 'english':'Farasan'},{'arabic': 'البديع', 'english': 'Budaiya'},{'arabic': 'ليلى', 'english': 'Layla'},{'arabic': 'مدينة الملك عبد الله الاقتصادية', 'english': 'King Abdullah Economic City'},{'arabic': 'الدراز', 'english': 'Diraz'},{'arabic': 'مدينة حمد', 'english': 'Hamad Town'},{'arabic': 'مكة المكرمة', 'english': 'Mecca'},{'arabic': 'نجران', 'english': 'Najran'},{'arabic': 'وادي الدواسر', 'english': 'Wadi Al-Dawasir'},{'arabic': 'ينبع', 'english': 'Yanbu'},{'arabic': 'سبت العلايا', 'english': 'Sabt Al Alaya'},{'arabic': 'عنيزة', 'english': 'Unaizah'}]},
                {'name':{'arabic': 'البحرين', 'english': 'Bahrain'},'cities':[{'arabic': 'Select City', 'english': 'Select City'},{'arabic': 'عالي', 'english': 'A ali '}, {'arabic': 'العدلية', 'english': 'Adliya'},{'arabic': 'الدور‎', 'english': 'Al Dur'},{'arabic': 'السويف', 'english': 'Al Seef'},{'arabic': 'المالكية', 'english': 'Al-Malikiyah'},{'arabic': 'عوالي', 'english': 'Awali'},{'arabic': 'بني جمرة', 'english': 'Bani Jamrah'},{'arabic': 'البديع', 'english': 'Budaiya'},{'arabic': 'الدراز', 'english': 'Diraz'},{'arabic': 'مدينة حمد', 'english': 'Hamad Town'},{'arabic': 'الحد‎', 'english': 'Hidd'},{'arabic': 'الحورة', 'english': 'Hoora'},{'arabic': 'مدينة عيسى', 'english': 'Isa Town'},{'arabic': 'جد علي', 'english': 'Jid Ali'},{'arabic': 'جد حفص', 'english': 'Jidhafs'},{'arabic': 'الماحوز', 'english': 'Mahooz'},{'arabic': 'عالي', 'english': 'Manama '},{'arabic': 'المحرق‎', 'english': 'Muharraq'},{'arabic': 'الرفاع', 'english': 'Riffa'},{'arabic': 'سنابس', 'english': 'Sanabis'},{'arabic': 'سار‎', 'english': 'Sar'},{'arabic': 'سترة', 'english': 'Sitra'},{'arabic': 'توبلي', 'english': 'Tubli'}]},
                {'name':{'arabic':'الكويت', 'english':'Kuwait'},'cities':[{'arabic': 'Select City', 'english': 'Select City'}, {'arabic': 'مدينة الكويت', 'english': 'Kuwait City'},{'arabic': 'ضاحية عبد الله السالم', 'english': 'Abdullah as-Salim suburb'},{'arabic': 'ميناء عبد الله', 'english': 'Abdullah Port'},{'arabic': 'أبو فطيرة', 'english': 'Abu Fteira'},{'arabic': 'أبو حليفة', 'english': 'Abu Hulaifa'},{'arabic': 'العدان', 'english': 'Adān'},{'arabic': 'العديلية', 'english': 'Adiliya'},{'arabic': 'الوفرة الزراعية', 'english': 'Agricultural Wafra'},{'arabic': 'الأحمدي', 'english': 'Ahmadi'},{'arabic': 'ضاحية علي صباح السالم', 'english': 'Ali as-Salim suburb'},{'arabic': 'العقيلة', 'english': 'Aqila'},{'arabic': 'بيان', 'english': 'Bayān'},{'arabic': 'البدع', 'english': 'Bi di'},{'arabic': 'بنيد القار', 'english': 'Bneid il-Gār'},{'arabic': 'بنيدر', 'english': 'Bneidar'},{'arabic': 'الدعية', 'english': 'Da iya'},{'arabic': 'الدسمة', 'english': 'Dasma'},{'arabic': 'دسمان', 'english': 'Dasmān'},{'arabic': 'الدوحة', 'english': 'Doha'},{'arabic': 'الفحيحيل', 'english': 'Fahaheel'},{'arabic': 'ضاحية فهد الأحمد', 'english': 'Fahd al-Ahmad Suburb'},{'arabic': 'الفيحاء', 'english': 'Faiha'},{'arabic': 'الفنطاس', 'english': 'Fintās'},{'arabic': 'الفنيطيس', 'english': 'Funaitīs'},{'arabic': 'غرناطة', 'english': 'Ghirnata'},{'arabic': 'هدية', 'english': 'Hadiya'},{'arabic': 'Hadiya', 'english': 'Hawally'},{'arabic': 'حطين', 'english': 'Hittin'},{'arabic': 'مدينة جابر الأحمد', 'english': 'Jabir al-Ahmad City'},{'arabic': 'ضاحية جابر العلي', 'english': 'Jabir al-Ali Suburb'},{'arabic': 'الجابرية', 'english': 'Jabriya'},{'arabic': 'جبلة', 'english': 'Jibla'},{'arabic': 'الجليعة', 'english': 'Jilei a'},{'arabic': 'كيفان', 'english': 'Kaifan'},{'arabic': 'الخيران', 'english': 'Khairan'},{'arabic': 'مدينة الخيران', 'english': 'Khairan City'},{'arabic': 'الخالدية', 'english': 'Khaldiya'},{'arabic': 'المهبولة', 'english': 'Mahbula'},{'arabic': 'ميدان حولي', 'english': 'Maidan Hawalli'},{'arabic': 'المنقف', 'english': 'Mangaf'},{'arabic': 'المنصورية', 'english': 'Mansūriya'},{'arabic': 'المقوع', 'english': 'Miqwa'},{'arabic': 'المرقاب', 'english': 'Mirgāb'},{'arabic': 'مشرف', 'english': 'Mishrif'},{'arabic': 'المسيلة', 'english': 'Misīla'},{'arabic': 'ضاحية مبارك الجابر', 'english': 'Mubarak aj-Jabir suburb'},{'arabic': 'مبارك الكبير', 'english': 'Mubarak al-Kabeer'},{'arabic': 'النهضة', 'english': 'Nahdha'},{'arabic': 'النقرة', 'english': 'Nigra'},{'arabic': 'النويصيب', 'english': 'Nuwaiseeb'},{'arabic': 'النزهة', 'english': 'Nuzha'},{'arabic': 'القادسية', 'english': 'Qadsiya'},{'arabic': 'القيروان', 'english': 'Qairawān'},{'arabic': 'القرين', 'english': 'Qurain'},{'arabic': 'قرطبة', 'english': 'Qurtuba'},{'arabic': 'القصور', 'english': 'Qusūr'},{'arabic': 'الري', 'english': 'Rai'},{'arabic': 'الروضة', 'english': 'Rawda'},{'arabic': 'الرقة', 'english': 'Rigga'},{'arabic': 'الرميثية', 'english': 'Rumaithiya'},{'arabic': 'مدينة صباح الأحمد', 'english': 'Sabah al-Ahmad City'},{'arabic': 'مدينة صباح الأحمد البحرية', 'english': 'Sabah al-Ahmad Nautical City'},{'arabic': 'ضاحية صباح السالم', 'english': 'Sabah as-Salim suburb'},{'arabic': 'الصباحية', 'english': 'Sabahiya'},{'arabic': 'صبحان', 'english': 'Sabhān'},{'arabic': 'الصالحية', 'english': 'Salhiya'},{'arabic': 'السالمية', 'english': 'Salmiya'},{'arabic': 'سلوى', 'english': 'Salwa'},{'arabic': 'الصوابر', 'english': 'Sawābir'},{'arabic': 'الشعب', 'english': 'Sha ab'},{'arabic': 'الشامية', 'english': 'Shamiya'},{'arabic': 'شرق', 'english': 'Sharq'},{'arabic': 'الشعيبة', 'english': 'Shu aiba'},{'arabic': 'الشويخ', 'english': 'Shuwaikh'},{'arabic': 'جنوب السرة', 'english': 'South Surra'},{'arabic': 'الصليبخات', 'english': 'Sulaibikhat'},{'arabic': 'السرة', 'english': 'Surra'},{'arabic': 'الوفرة', 'english': 'Wafra'},{'arabic': 'اليرموك', 'english': 'Yarmūk'},{'arabic': 'الزور', 'english': 'Zoor'},{'arabic': 'الظهر', 'english': 'Zuhar'}]},
                {'name':{'arabic': 'عمان', 'english': 'Oman'},'cities': [{'arabic': 'Select City', 'english': 'Select City'}, {'arabic': 'آدم', 'english': 'Adam'},{'arabic': 'كما سيب', 'english': 'As Sib'},{'arabic': 'السيب', 'english': 'Al Ashkharah'},{'arabic': 'البريمي (محافظة)', 'english': 'Al Buraimi'},{'arabic': 'الحمراء ', 'english': 'Al Hamra'},{'arabic': 'الجازر', 'english': 'Al Jazer'},{'arabic': 'المدينة الزرقاء ', 'english': 'Al Madina A Zarqa'},{'arabic': 'السويق', 'english': 'Al Suwaiq'},{'arabic': 'بهلا', 'english': 'Bahla'},{'arabic': 'ولاية بركاء', 'english': 'Barka'},{'arabic': 'ولاية بدبد', 'english': 'Bidbid'},{'arabic': 'ولاية بدية', 'english': 'Bidiya'},{'arabic': 'ولاية دباء', 'english': 'Dibba Al-Baya'},{'arabic': 'الدقم', 'english': 'Duqm'},{'arabic': 'ولاية هيما', 'english': 'Haima'},{'arabic': 'ولاية إبراء', 'english': 'Ibra'},{'arabic': 'عبري ', 'english': 'Ibri'},{'arabic': 'ولاية إزكي', 'english': 'Izki'},{'arabic': 'جبرين (بهلا)', 'english': 'Jabrin'},{'arabic': 'ولاية جعلان بني بو حسن', 'english': 'Jalan Bani Bu Hassan'},{'arabic': 'ولاية خصب', 'english': 'Khasab'},{'arabic': 'ولاية مدحاء', 'english': 'Mad ha'},{'arabic': 'Mahooth', 'english': 'Mahooth'},{'arabic': 'ولاية منح', 'english': 'Manah'},{'arabic': 'جزيرة مصيرة', 'english': 'Masirah'},{'arabic': 'ولاية مطرح', 'english': 'Matrah'},{'arabic': 'Mudhaybi', 'english': 'Mudhaybi'},{'arabic': 'Mudhaireb', 'english': 'Mudhaireb'},{'arabic': 'مسقط', 'english': 'Muscat'},{'arabic': 'ولاية نزوى', 'english': 'Nizwa'},{'arabic': 'ولاية قريات', 'english': 'Quriyat'},{'arabic': 'Raysut', 'english': 'Raysut'},{'arabic': 'ولاية الرستاق', 'english': 'Rustaq'},{'arabic': 'روي ', 'english': 'Ruwi'},{'arabic': 'ولاية صحم', 'english': 'Saham'},{'arabic': 'ولاية شناص', 'english': 'Shinas'},{'arabic': 'Saiq', 'english': 'Saiq'},{'arabic': 'صلالة', 'english': 'Salalah'},{'arabic': 'ولاية سمائل', 'english': 'Samail'},{'arabic': 'ولاية صحار', 'english': 'Sohar'},{'arabic': 'ولاية صور', 'english': 'Sur'},{'arabic': 'Tan`am', 'english': 'Tan`am'},{'arabic': 'ثمريت', 'english': 'Thumrait'},]},
                {'name':{'arabic': 'الإمارات العربية المتحدة', 'english': 'United Arab Emirates'},'cities': [{'arabic': 'Select City', 'english': 'Select City'}, {'arabic': 'عجمان', 'english': 'Ajman'},{'arabic': 'أبو ظبي', 'english': 'Abu Dhabi'},{'arabic': 'دبي', 'english': 'Dubai'},{'arabic': 'الشارقة', 'english': 'Sharjah'},{'arabic': 'أم القيوين', 'english': 'Umm Al Quwain'},{'arabic': 'رأس الخيمة', 'english': 'Ras Al Khaimah'},{'arabic': 'الفجيرة', 'english': 'Fujairah'}]}
            ],

            agree: false,
            
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validate = this.validate.bind(this);

        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.closeModalNotAgree = this.closeModalNotAgree.bind(this);
        this.resetViewport = this.resetViewport.bind(this)
    }

    selectCountry (val) {
        this.setState({ country: val });
    }

    selectCity (val) {
        this.setState({ city: val });
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    validate(e){
        const { password } = this.state
        if (password != e.keycode){
            setTimeout( () => {
                this.setState({ passwordcheck: true })
            },3000)
        }
    }

    validate_email(e){
        const { email } = this.state
        if (email != e.keycode){
            setTimeout( () => {
                this.setState({ emailcheck: true })
            },3000)
        }
    }

    resetViewport(){
        let viewportmeta = document.querySelector('meta[name="viewport"]');
        if(viewportmeta===null){
            viewportmeta = document.createElement("meta");
            viewportmeta.setAttribute("name","viewport");
            document.head.appendChild(viewportmeta);
              
            viewportmeta = document.querySelector('meta[name="viewport"]');
        }
        viewportmeta.setAttribute('content', 'width=1470, initial-scale=0.3, maximum-scale=1.0');

        document.body.style.zoom = screen.logicalXDPI / screen.deviceXDPI;
    }



    componentWillMount(){
        const { dispatch } = this.props;

        this.resetViewport()

        if (!this.props.maintenanceMode.hasOwnProperty('mode')){
          dispatch(getMaintenanceAction.getMode()).then( () => {
                if (this.props.maintenanceMode){
                    if (this.props.maintenanceMode.mode.status){
                       
                        history.push('/maintenance')
                    }
                } 
            })
        }else{
          if (this.props.maintenanceMode.mode.status){
                  history.push('/maintenance')
              }
        }

        let country_english = this.state.countries.filter(country_english => {
            return country_english.name.english === this.state.country
           })
        

        let country_arabic = this.state.countries.filter(country_arabic => {
            return country_arabic.name.arabic === this.state.country
           }) 
              
        
        dispatch(getUserLocation.getLocation()).then( () => {
            
            this.setState({ fetching : true})
            this.setState({ city : this.props.userlocation.city })
            this.setState({ country : this.props.userlocation.country })

            if (this.props.userlocation.country == 'Saudi Arabia'){
                this.setState({ country_code : 966 })
            }else if (this.props.userlocation.country == 'United Arab Emirates'){
                this.setState({ country_code : 971 })
            }else if (this.props.userlocation.country == 'Bahrain'){
                this.setState({ country_code : 973  })
            }else if (this.props.userlocation.country == 'Oman'){
                this.setState({ country_code : 968 })
            }else if (this.props.userlocation.country == 'Kuwait'){
                this.setState({ country_code : 965 })
            }else{
                console.log(this.props.userlocation.country);
            }
        })

        if(this.state.language == "english"){
            this.setState({country2 : country_english})
        }else{
            this.setState({country2 : country_arabic})
        }

        
        
        this.setState({submitted: false})

    }


    handleSubmit() {
        //e.preventDefault();
        //console.log("submitting registration");
        this.setState({ submitted: true });
        const { username, firstname, lastname, email, confirm_email, password, confirmpassword, city, country, telephone, country_code, agree } = this.state;
        const { dispatch } = this.props;
        const data = {
            'username': username,
            'firstname': firstname,
            'lastname': lastname,
            'email': email,
            'password': password,
            'city': city,
            'country': country,
            'phone_number': '('+ country_code + ') ' + telephone,
            'email_template': this.props.rtlconv
        }
        
        
        if ( password === confirmpassword && username && firstname && lastname && email && city && country && this.state.agree && email==confirm_email) {
            //console.log("dispatching");
            dispatch(userActions.registerUser(data));
            
        }else{
            console.log("incorrect data");
        }
    }


    handleSelectChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        
    }

    checkUsernameEmail() {
        const { dispatch } = this.props;
        const { username, email } = this.state;
        const udata = {
            'username': username,
            'email': email,
        }
        const edata = {
            'email': email,
        }

        dispatch(userActions.cleanUsername(udata)).then( () => {
            if (this.props.usernameError.message=="" && this.props.emailError.message=="" && this.props.blockError.message=="") {
                this.openModal();
            }
            window.scrollTo(0, 0)
        })
        //dispatch(userActions.cleanEmail(edata))

    }

    openModal() {
        this.resetViewport()
        this.setState({modalIsOpen: true});
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        //this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({modalIsOpen: false});
        this.setState({agree: true}, () => {
            this.handleSubmit();
        });
        
    }

    closeModalNotAgree() {
        this.setState({modalIsOpen: false});
        this.setState({agree: false});
        this.handleSubmit();
    }


  handleKeypress (e) {

    const characterCode = e.key
    if (characterCode === 'Backspace') return

    const characterNumber = Number(characterCode)
    if (characterNumber >= 0 && characterNumber <= 9) {
      if (e.currentTarget.value && e.currentTarget.value.length) {
        return
      } 
    } else {
      e.preventDefault()
    }
  }


    render() {
        const { username, firstname, lastname, email, confirm_email, password, confirmpassword, submitted, passwordcheck, city, country, telephone, country_code, agree} = this.state;
        const { registrationError, usernameError, emailError, blockError } = this.props
        let country2 = this.state.countries.filter(country2 => {
            return country2.name.english === this.state.country
           })        
            
        return (
            <div className="row registration-container" dir="ltr">
            <div className="mt-4 mx-auto col-md-3 buycontainer">
                <img src={images_url + "/images/buy.png"} className="buysellimage"/><br />
            </div>
            <div className="card mt-4 col-md-5" dir={this.props.rtlconv}>
                <div className={"card-header registration-header"  + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h3 className="mb-0">{strings.register}</h3>
                </div>
                
                <div className="card-body">
                    <form name="form" onSubmit={this.handleSubmit}>
                        <div className={'form-group form-control-sm' + (submitted && !username ? ' has-error' : '') + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="username" className="required">{strings.username}</label>
                            <input type="text" className="form-control form-control-sm input40" name="username" value={username} onChange={this.handleChange} />
                            {submitted && !username &&
                                <div className="errorMessage">{strings.username_is_required}</div>
                            }
                            {usernameError.message &&
                                <div className="errorMessage">{strings.username_error}</div>
                            }
                        </div>
                        <div className={'form-group form-control-sm' + (submitted && !firstname ? ' has-error' : '') + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="firstname" className="required">{strings.first_name}</label>
                            <input type="text" className="form-control form-control-sm input40" name="firstname" value={firstname} onChange={this.handleChange} />
                            {submitted && !firstname &&
                                <div className="errorMessage">{strings.first_name_is_required}</div>
                            }
                        </div>
                        <div className={'form-group form-control-sm' + (submitted && !lastname ? ' has-error' : '') + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="lastname" className="required">{strings.last_name}</label>
                            <input type="text" className="form-control form-control-sm input40" name="lastname" value={lastname} onChange={this.handleChange} />
                            {submitted && !lastname &&
                                <div className="errorMessage">{strings.last_name_is_required}</div>
                            }
                        </div>
                        <div className={'form-group form-control-sm' + (submitted && !email ? ' has-error' : '') + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="email" className="required">{strings.email}</label>
                            <input type="email" className="form-control form-control-sm input60" name="email" value={email} onChange={this.handleChange} />
                            {submitted && !email &&
                                <div className="errorMessage">{strings.email_is_required}</div>
                            }
                            {emailError.message &&
                                <div className="errorMessage">{strings.email_error}</div>
                            }
                            {blockError.message &&
                                <div className="errorMessage">{blockError.message}</div>
                            }
                        </div>
                        <div className={'form-group form-control-sm' + (submitted && !confirm_email ? ' has-error' : '') + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="confirm_email" className="required">{strings.confirm_email}</label>
                            <input type="email" className="form-control form-control-sm input60" name="confirm_email" value={confirm_email} onChange={this.handleChange} />
                            {submitted && !confirm_email &&
                                <div className="errorMessage">{strings.confirm_email_is_required}</div>
                            }
                            {submitted && confirm_email != email &&
                                <div className="errorMessage">{strings.email_does_not_match}</div>
                            }
                        </div>
                        
                        <div className={'form-group form-control-sm input60' + (submitted && !country ? ' has-error' : '') + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="country" className="required">{strings.country}</label>
                            <select className="form-control form-control-sm" value={this.state.country} name="country" value={country} onChange={this.handleSelectChange.bind(this)}>
                              {
                                this.state.countries.map((country, i) => {
                                    return <option>{country.name.english}</option>
                                })
                              }
                            </select>
                        </div>
                         <div className={'form-group form-control-sm input60' + (submitted && !city ? ' has-error' : '') + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="city" className="required">{strings.city}</label>
                            { this.state.country ? 
                                
                                <select className="form-control form-control-sm" value={this.state.city} name="city" value={city} onChange={this.handleSelectChange.bind(this)}>
                                    { this.state.country &&
                                        country2[0].cities.map((city, i) => {
                                          return <option>{city.english}</option>
                                        })
                                    }
                                  
                                </select>

                            :
                                <select className="form-control form-control-sm" value={this.state.city} name="city" onChange={this.handleSelectChange.bind(this)}>
                                    <option>{strings.select_city}</option>
                                </select>
                            }
                            {submitted && !city &&
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                            }
                        </div>
                        <div className={'form-group form-control-sm' + (submitted && !email ? ' has-error' : '') + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="email">{strings.telephone_number}</label>
                            <div className="row">
                                <div className="col-md-1">
                                    <input type="input" className="form-control form-control-sm" name="country_code" value={country_code} onChange={this.handleChange} />
                                </div>
                                <div className="col">
                                    <input type="number"   onKeyDown={this.handleKeypress}  min={'0'}   className="form-control form-control-sm input50" name="telephone" value={telephone} onChange={this.handleChange} />
                                </div>
                            </div>
                        </div>
                        <div className={'form-group form-control-sm' + (submitted && !password ? ' has-error' : '') + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="password" className="required">{strings.password}</label>
                            <input type="password" className="form-control form-control-sm input40" name="password" value={password} onChange={this.handleChange} />
                            {submitted && !password &&
                                <div className="errorMessage">{strings.password_is_required}</div>
                            }
                        </div>
                        <div className={'form-group form-control-sm' + (submitted && !confirmpassword ? ' has-error' : '') + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="confimpassword" className="required">{strings.confirm_password}</label>
                            <input type="password" className="form-control form-control-sm input40" name="confirmpassword" value={confirmpassword} onChange={this.handleChange} onKeyDown={this.validate} tabIndex="0" />
                            {submitted && !confirmpassword &&
                                <div className="errorMessage">{strings.confirm_password_is_required}</div>
                            }
                            {submitted && confirmpassword != password &&
                                <div className="errorMessage">{strings.password_does_not_match}</div>
                            }
                            {submitted && !agree &&
                                <div className="errorMessage">{strings.please_agree}</div>
                            }
                        </div>

                        {registrationError.message && submitted &&
                            <div className="alert alert-danger text-center" role="alert">
                                <strong>{registrationError.message}</strong>
                            </div> 
                        }


                        <div className={"form-group" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <button className="btn btn-primary" onClick={(e)=>{e.preventDefault(); this.checkUsernameEmail()}}>{strings.register}</button>
                        </div>
                        {/*
                        <div className={"form-group" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <Link to="/commercialAccount" className="spanhowto">
                                ({strings.to_create_commercial_account})
                            </Link>
                        </div>
                        */}
                    </form>
                </div>
                <br />
                <br />
                <br />
                <br />
            </div>
            <div className="mt-4 mx-auto col-md-3 sellcontainer">
                <img src={images_url + "/images/sell.png"} className="buysellimage"/><br />
            </div>
            
            <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    style={customStyles}
                    contentLabel={strings.add_lens}
                    ariaHideApp={false}
                >
                    
                    <div className="frame arabicfont"> 
                        <div className="scroll"> 
                            <div className={"secondhandlogo" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')} dir={this.props.rtlconv}>
                                <div className="card-header registration-header">
                                    <h6 className="mb-0">{strings.terms_of_use}</h6>
                                </div>
                                
                                <div className="card-body">

                                    <p>{strings.terms_of_use_1}</p>
                                    <ul className={this.props.rtlconv == "rtl" ? 'ta-right' : ''} dir={this.props.rtlconv}>    
                                        <li>{strings.terms_of_use_2}</li>
                                        <li>{strings.terms_of_use_3}</li>
                                        <li>{strings.terms_of_use_4}</li>
                                        <li>{strings.terms_of_use_5}</li>
                                        <li>{strings.terms_of_use_6}</li>
                                        <li>{strings.terms_of_use_7}</li>
                                        <li>{strings.terms_of_use_8}</li>
                                    </ul>
                                    <br />
                                    <button className="btn btn-sm btn-primary float-center margin-right" onClick={this.closeModal}>{strings.i_agree}</button>
                                    <button className="btn btn-sm float-center" onClick={this.closeModalNotAgree}>{strings.cancel}</button>
                                </div>
                            </div>
                            <br />
                            <br />
                        </div>
                    </div>
                    
                </Modal>

            </div>
        );
    }
}

function mapStatetoProps(state){
    return {
        userlocation: state.userLocationReducer,
        registrationError: state.registrationError,
        usernameError: state.usernameError,
        emailError: state.emailError,
        blockError: state.blockError,
        rtlconv: state.rtlconv,
        maintenanceMode: state.maintenanceMode
    }
}

export default connect(mapStatetoProps)(Registration)
