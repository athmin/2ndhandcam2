import React from 'react'
import { connect } from 'react-redux';
import { history } from '../store/configureStore'
let CONFIG = require('../lib/config.json')

const images_url = CONFIG.server_url + "/static/media_cdn"

const camStyle = {
	  width: 200,
    height: 68
};



class BuyPhotography extends React.Component {

	clickHandle(){
		history.push('/login')
	}

    componentWillMount(){
        let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }   
    }

  render() {
    return (
    	<div className='container'>
    		<div className="card mt-1">
    			<h2 className="text-center card-title mt-2">Choose from any brand you like.</h2>
    			<div className="row mt-4">
    				<div className="col-md-3 text-center">
    					<img src={images_url + "/images/cannon.png"} style={camStyle} value="canon" onClick={this.clickHandle}/>
    				</div>
    				<div className="col-md-3 text-center">
    					<img src={images_url + "/images/nikon.jpg"} style={camStyle} />
    				</div>
    				<div className="col-md-3 text-center">
    					<img src={images_url + "/images/sony.png"} style={camStyle} />
    				</div>
    				<div className="col-md-3 text-center">
    					<img src={images_url + "/images/pentax.png"} style={camStyle} />
    				</div>
    			</div>
                <div className="row">
                    <div className="col-md-3 text-center">
                        <img src={images_url + "/images/hasselblad.png"} style={camStyle} value="canon" onClick={this.clickHandle}/>
                    </div>
                    <div className="col-md-3 text-center">
                        <img src={images_url + "/images/olympus.png"} style={camStyle} />
                    </div>
                    <div className="col-md-3 text-center">
                        <img src={images_url + "/images/panasonic.png"} style={camStyle} />
                    </div>
                    <div className="col-md-3 text-center">
                        <img src={images_url + "/images/leica.png"} style={camStyle} />
                    </div>
                    
                </div>
                <div className="row">
                    <div className="col-md-4 text-center">
                        <img src={images_url + "/images/sigma.png"} style={camStyle} />
                    </div>
                    <div className="col-md-4 text-center">
                        <img src={images_url + "/images/fujifilm.png"} style={camStyle} />
                    </div>
                    <div className="col-md-4 text-center">
                        <img src={images_url + "/images/samsung.png"} style={camStyle} />
                    </div>
                </div>
    		</div>
    	</div>
    )
  }
}

export default connect()(BuyPhotography);