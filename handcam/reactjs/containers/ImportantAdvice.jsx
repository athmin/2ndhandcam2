import React from 'react'
import { history } from '../store/configureStore'
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'
import { strings } from '../lib/strings';


class ImportantAdvice extends React.Component {

    componentWillMount(){
         
    }

    clickStillHandle(){
        history.push('/buy_item')
    }

    clickVideoHandle(){
        history.push('/buy_video_cameras')
    }

    clickLensHandle(){
        history.push('/buy_lens')
    }

    clickLightingHandle(){
        history.push('/buy_lighting')
    }

    clickAccessoriesHandle(){
        history.push('/buy_accessories')
    }

    clickDroneHandle(){
        history.push('/buy_drones')
    }

    goBack() {
        history.goBack();
    }

    closeCustomerServices = (e) => {
        e.preventDefault();
        this.goBack();
    }

  render() {
    return (
        <div className="row homelogo" dir="ltr">
            <div className="mt-4 mx-auto col-md-3">
                
            </div>
            <div className={"card mt-4 col-md-5 infoForms secondhandlogo" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')} dir={this.props.rtlconv}>
                <button className={"btn btn-danger btn-sm margintop10 modalButton" + (this.props.rtlconv == "rtl" ? ' float-left' : ' float-right')} onClick={this.closeCustomerServices}> X </button>
                <div className="card-header registration-header">
                    <h6 className="mb-0">{strings.important_advice_for_you}</h6>
                </div>
                
                <div className="card-body">

                    

                </div>
                <br />
                <br />
            </div> 
            <div className="mt-4 mx-auto col-md-3">
                
            </div>
        </div>      
    )
  }
}

function mapStatetoProps(state){
    return {
        rtlconv: state.rtlconv
    }
}

export default connect(mapStatetoProps)(ImportantAdvice);
