import React from 'react'
import { history } from '../store/configureStore'
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'
import { strings } from '../lib/strings';
let CONFIG = require('../lib/config.json')

const images_url = CONFIG.server_url + "/static/media_cdn"


const camStyle = {
      width: 200,
    height: 150
};

class SellStrobeLighting extends React.Component {

    componentWillMount(){
        /*let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }*/   
    }
    
    constructor(props) {
        super(props);
        this.state = {
            showMonolightBrands: false
        }
        this.clickShowMonolightBrands = this.clickShowMonolightBrands.bind(this);
    }

    clickShowSellForm(){
        alert('sell LIGHTING');
    }

    clickShowMonolightBrands(){
        this.setState({showMonolightBrands : true});
        
    }

    componetDidMount(){
        
    }


     clickStillHandle = (e) => {
        e.preventDefault();
        history.push('/sell_brand')
    }

    clickVideoHandle = (e) => {
        e.preventDefault();
        history.push('/sell_video_brand')
    }

    clickLensHandle = (e) => {
        e.preventDefault();
        history.push('/sell_lens_brand')
    }

    clickLightingHandle = (e) => {
        e.preventDefault();
        history.push('/sell_lighting')
    }

    clickAccessoriesHandle = (e) => {
        e.preventDefault();
        history.push('/sell_accessories')
    }

    clickDroneHandle = (e) => {
        e.preventDefault();
        history.push('/sell_drone')
    }

    clickLightingHideBrandHandle = (e) => {
        e.preventDefault();
        history.push(
            {'pathname':'/complete_lighting_form',     
            'state' : {
                'showBrands' : false,
            }
        })   
    }

    clickLightingShowBrandHandle = (e) => {
        e.preventDefault();
        history.push(
            {'pathname':'/complete_lighting_form',     
            'state' : {
                'showBrands' : true,
            }
        })   
    }


    renderBrands() {
        
        const { showMonolightBrands } = this.state;

        var brands = ['Angler', 'Bowens', 'Broncolor', 'Comet', 'Dyanlite', 'Elinchrom', 'Godex', 'Hensel',
                'Impact', 'Interfit', 'Norman', 'Novatron', 'Photoflex', 'Photogenic', 'Phottix', 'Prophoto',
                'Raya', 'Smith-Vector', 'Sp Studio System', 'Westcott', 'Visatec', 'Electra'
            ]

        if (showMonolightBrands){  
            
            return (
                <div className="sk-hits-grid m-3">
                    { brands.map((brand) => {
                        return (
                            <div id="monolight-brand" className="sk-hits-grid-hit sk-hits-grid__item">
                                <a href="" key={brand} onClick={(e)=>{e.preventDefault(); }}>
                                    <div className="sk-hits-grid-hit__title text-center">{ brand }</div>
                                </a>
                            </div>    
                        )    
                    })}
                </div>
            )
        }else {
            return <div></div>
        }
        

    }

   

  render() {

    return (
        <div>
        <ul className="breadcrumb">
            <li><Link to="/">{strings.home}</Link></li>
            <li><Link to="/sell">{strings.sell}</Link></li>
            <li><Link to="/sell_lighting">{strings.lighting}</Link></li>
            <li>Strobe Lighting</li>
        </ul>
        <div className="lighting-back">

            <div className=" bg-company-orange">
              <ul className="d-flex mx-auto justify-content-center nomarge zeroleftpadding">
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickStillHandle}><span>{strings.photography_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickLensHandle}><span>{strings.lenses}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickVideoHandle}><span>{strings.video_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4 active">
                  <a className="awhite" href="" onClick={this.clickLightingHandle}><span>{strings.lighting}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickAccessoriesHandle}><span>{strings.accessories}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                    <a className="awhite" href="" onClick={this.clickDroneHandle}><span>{strings.drone}</span></a>
                </li>
              </ul>
            </div>

            <div className="brand-header text-center">
                <span>{strings.lighting}</span>
            </div>
            <hr />

            <div className="category-body m-2" dir="ltr">
                <h4 className="text-center card-title choose-cat">{strings.strobe_lighting}</h4>
                <div className="row category">
                    <div className="col text-center">
                        <img src={images_url + "/images/monolight.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                            {'pathname':'/complete_lighting_form',     
                            'state' : {
                                'showBrands' : true,
                                'category' : "Monolights"
                            }
                        })}}/><br />
                        <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                            {'pathname':'/complete_lighting_form',     
                            'state' : {
                                'showBrands' : true,
                                'category' : "Monolights"
                            }
                        })}}>{strings.monolights}</button>
                    </div>
                    <div className="col text-center">
                        <img src={images_url + "/images/batterypowered.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                            {'pathname':'/complete_lighting_form',     
                            'state' : {
                                'showBrands' : false,
                                'category' : "Battery Powered Stobes"
                            }
                        })}}/><br />
                        <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                            {'pathname':'/complete_lighting_form',     
                            'state' : {
                                'showBrands' : false,
                                'category' : "Battery Powered Stobes"
                            }
                        })}}>{strings.battery_powered_strobes}</button>
                    </div>
                    <div className="col text-center">
                        <img src={images_url + "/images/powerpack.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                            {'pathname':'/complete_lighting_form',     
                            'state' : {
                                'showBrands' : false,
                                'category' : "Power Packs"
                            }
                        })}}/><br />
                        <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                            {'pathname':'/complete_lighting_form',     
                            'state' : {
                                'showBrands' : false,
                                'category' : "Power Packs"
                            }
                        })}}>{strings.power_packs}</button>
                    </div>
                     <div className="col text-center">
                        <img src={images_url + "/images/slave.png"} style={camStyle} className="imageBuySell" onClick={(e)=>{e.preventDefault(); history.push(
                            {'pathname':'/complete_lighting_form',     
                            'state' : {
                                'showBrands' : false,
                                'category' : "Slaves"
                            }
                        })}}/><br />
                        <button className="btn btn-warning" onClick={(e)=>{e.preventDefault(); history.push(
                            {'pathname':'/complete_lighting_form',     
                            'state' : {
                                'showBrands' : false,
                                'category' : "Slaves"
                            }
                        })}}>{strings.slaves}</button>
                    </div>
                </div>
            </div>

            <div>
                {this.renderBrands()}
            </div>
            <br />
            <br />
            <br />
            <br />
        </div>
        </div>
    )
  }
}

export default connect()(SellStrobeLighting);