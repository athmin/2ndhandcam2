const camStyle = {
  width: 100,
    height: 75,
};

const logoStyle = {
  width: 75,
    height: 25,
    marginBottom: 5
};


const RefinementOption = (props) => {

  return(
      <div className={props.bemBlocks.item().state({selected:props.selected}).mix(this.bemBlocks.container("item"))} onClick={props.onClick}>
          <div className={props.bemBlocks.item("label")}>{props.label}</div>
          <div className={props.bemBlocks.item("count")}>{props.docCount}</div>
      </div>
  )
}

export { RefinementOption }