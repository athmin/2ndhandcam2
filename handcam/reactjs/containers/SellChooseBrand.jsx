import React from 'react'
import { history } from '../store/configureStore'
import { connect } from 'react-redux'
import {Link} from 'react-router-dom'
import { getBrandModelsAction } from '../actions'
let CONFIG = require('../lib/config.json')
import { strings } from '../lib/strings';

const images_url = CONFIG.server_url + "/static/media_cdn"


const camStyle = {
    width: 200,
    height: "auto"
    };
const logoStyle = {
	width: 150,
    height: 50,
};

class SellBrand extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			brandmodels: []
		}
	}

	componentWillMount(){
        /*let token = localStorage.getItem('token')
        if (!token){
          history.push('/login')
        }*/   
    }


	clickHandle(brand){
		const { dispatch } = this.props;
		

		this.setState({ brandmodels: this.props.camera_models })
		history.push('/select_category');
		/*if (this.state.brandmodels.length>0){
			history.push('/select_category');
		}else{
			dispatch(getBrandModelsAction.selectCategory(brand));
			history.push('/select_category');
		}*/
	}

	clickStillHandle = (e) => {
	    e.preventDefault();
	    history.push('/sell_brand')
	  }

	clickVideoHandle = (e) => {
	    e.preventDefault();
	    history.push('/sell_video_brand')
	}

	clickLensHandle = (e) => {
	    e.preventDefault();
	    history.push('/sell_lens_brand')
	}

	clickLightingHandle(){
		history.push('/sell_lighting')
	}

    clickAccessoriesHandle = (e) => {
        e.preventDefault();
        history.push('/sell_accessories')
    }

    clickDroneHandle = (e) => {
        e.preventDefault();
        history.push('/sell_drone')
    }

  render() {
    return (
    	<div>
        <ul className="breadcrumb">
            <li><Link to="/">{strings.home}</Link></li>
            <li><Link to="/sell">{strings.sell}</Link></li>
            <li>{strings.photography_cameras}</li>
        </ul>
		<div className="camera-back">
			<div className=" bg-company-orange">
              <ul className="d-flex mx-auto justify-content-center nomarge zeroleftpadding">
                <li className="list-inline-item ml-4 mr-4 active">
                  <a className="awhite" href="" onClick={this.clickStillHandle}><span>{strings.photography_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickLensHandle}><span>{strings.lenses}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickVideoHandle}><span>{strings.video_cameras}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickLightingHandle}><span>{strings.lighting}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                  <a className="awhite" href="" onClick={this.clickAccessoriesHandle}><span>{strings.accessories}</span></a>
                </li>
                <li className="list-inline-item ml-4 mr-4">
                    <a className="awhite" href="" onClick={this.clickDroneHandle}><span>{strings.drone}</span></a>
                </li>
              </ul>
            
          </div>

          
			<div className="brand-header text-center">
	            <span>{strings.photography_cameras}</span>
	        </div>
	        

			<div className="category-body" dir="ltr">
			<h4 className="text-center card-title choose-cat">{strings.choose_camera_brand}</h4>
			
				<div className="m-2" id="selectBrand">
					<div className="row lighting-category">
						<div className="col-xs-3 col colitems text-center" id="choose-brand">
							<a href="" onClick={(e)=>{e.preventDefault(); history.push({'pathname':'/select_category', 'state':{'brand':'Canon'}})}}>
								<img src={images_url + "/images/cam-Canon.png"} style={camStyle} /><br />
								<img src={images_url + "/images/logo-Canon.png"} style={logoStyle} />
							</a>
						</div>
						<div className="col-xs-3 col colitems text-center" id="choose-brand">
							<a href="" onClick={(e)=>{e.preventDefault(); history.push({'pathname':'/select_category', 'state':{'brand':'Nikon'}})}}>
								<img src={images_url + "/images/cam-Nikon.png"} style={camStyle} /><br />
								<img src={images_url + "/images/nikon.png"} style={logoStyle} />
							</a>
						</div>
						<div className="col-xs-4 col colitems text-center" id="choose-brand">
							<a href="" onClick={(e)=>{e.preventDefault(); history.push({'pathname':'/select_category', 'state':{'brand':'Sony'}})}}>
								<img src={images_url + "/images/cam-Sony.png"} style={camStyle} /><br />
								<img src={images_url + "/images/logo-Sony.png"} style={logoStyle} />
							</a>
						</div>
						<div className="col-xs-3 col colitems text-center" id="choose-brand">
							<a href="" onClick={(e)=>{e.preventDefault(); history.push({'pathname':'/select_category', 'state':{'brand':'Fujifilm'}})}}>
								<img src={images_url + "/images/cam-Fujifilm.png"} style={camStyle} /><br />
								<img src={images_url + "/images/logo-Fujifilm.png"} style={logoStyle} />
							</a>
						</div>
					
						<div className="col-xs-3 col colitems text-center" id="choose-brand">
							<a href="" onClick={(e)=>{e.preventDefault(); history.push({'pathname':'/select_category', 'state':{'brand':'Leica'}})}}>
								<img src={images_url + "/images/cam-Leica.png"} style={camStyle} /><br />
								<img src={images_url + "/images/logo-Leica.png"} style={logoStyle} />
							</a>
						</div>
						<div className="col-xs-3 col colitems text-center" id="choose-brand">
							<a href="" onClick={(e)=>{e.preventDefault(); history.push({'pathname':'/select_category', 'state':{'brand':'Olympus'}})}}>
								<img src={images_url + "/images/cam-Olympus.png"} style={camStyle} /><br />
								<img src={images_url + "/images/logo-Olympus.png"} style={logoStyle} />
							</a>
						</div>
						<div className="col-xs-3 col colitems text-center" id="choose-brand">
							<a href="" onClick={(e)=>{e.preventDefault(); history.push({'pathname':'/select_category', 'state':{'brand':'Panasonic'}})}}>
								<img src={images_url + "/images/cam-Panasonic.png"} style={camStyle} /><br />
								<img src={images_url + "/images/logo-Panasonic.png"} style={logoStyle} />
							</a>
						</div>
						<div className="col-xs-3 col colitems text-center" id="choose-brand">
							<a href="" onClick={(e)=>{e.preventDefault(); history.push({'pathname':'/select_category', 'state':{'brand':'Hasselblad'}})}}>
								<img src={images_url + "/images/cam-Hasselblad.png"} style={camStyle} /><br />
								<img src={images_url + "/images/logo-Hasselblad.png"} style={logoStyle} />
							</a>
						</div>
						
						<div className="col-xs-4 col colitems text-center" id="choose-brand">
							<a href="" onClick={(e)=>{e.preventDefault(); history.push({'pathname':'/select_category', 'state':{'brand':'Samsung'}})}}>
								<img src={images_url + "/images/cam-Samsung.png"} style={camStyle} /><br />
								<img src={images_url + "/images/logo-Samsung.png"} style={logoStyle} />
							</a>
						</div>
						<div className="col-xs-4 col colitems text-center" id="choose-brand">
							<a href="" onClick={(e)=>{e.preventDefault(); history.push({'pathname':'/select_category', 'state':{'brand':'Sigma'}})}}>
								<img src={images_url + "/images/cam-Sigma.png"} style={camStyle} /><br />
								<img src={images_url + "/images/logo-Sigma.png"} style={logoStyle} />
							</a>
						</div>
						<div className="col-xs-3 col colitems text-center" id="choose-brand">
							<a href="" onClick={(e)=>{e.preventDefault(); history.push({'pathname':'/select_category', 'state':{'brand':'Pentax'}})}}>
								<img src={images_url + "/images/cam-Pentax.png"} style={camStyle} /><br />
								<img src={images_url + "/images/logo-Pentax.png"} style={logoStyle} />
							</a>
						</div>
						
					</div>
				</div>
			</div>
			<br />
			<br />
			<br />
			<br />
		</div>
		</div>
    )
  }
}


function mapStateToProps(state){
    return {
        camera_models: state.camera_models
    };
}

export default connect(mapStateToProps)(SellBrand);