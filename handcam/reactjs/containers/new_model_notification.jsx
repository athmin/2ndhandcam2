import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import { strings } from '../lib/strings';
import { history } from '../store/configureStore'
import { contactUsActions } from '../actions'
import Modal from 'react-modal';

let CONFIG = require('../lib/config.json')
const images_url = CONFIG.server_url + "/static/media_cdn"

const divStyle = {
	  marginTop: '150px'
	};

const customStyles = {
    content: {
        'border-left': '5px solid #f4a72e',
        'border-top': '5px solid #f4a72e',
        'border-right': '5px solid #3d4041',
        'border-bottom': '5px solid #3d4041',
        borderRadius: '4px',
        bottom: 'auto',
        minHeight: '10rem',
        left: '50%',
        padding: '2rem',
        position: 'fixed',
        right: 'auto',
        top: '50%',
        transform: 'translate(-50%,-50%)',
        minWidth: '20rem',
        width: '40%',
        maxWidth: '60rem',
        padding: '0'
      },
    overlay: {
        backgroundColor: "rgba(255, 255, 255, 0.25)"
    }  
};



class NewModelNotification extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            brand_name: '',
            email: '',
            model: '',
            text: '',
            subject: this.props.location.state.subject,
        
            modalIsOpen: false,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.goBack = this.goBack.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        
    }


    closeCustomerServices = (e) => {
        e.preventDefault();
        this.goBack()
    }


    componentWillMount(){
        const { dispatch } = this.props;     

    }

    openModal() {
        this.setState({modalIsOpen: true});
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        //this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({modalIsOpen: false});
        this.goBack();
    }

    goBack() {
        
        history.goBack();
    }


    handleSubmit(e) {
        e.preventDefault();
        
        this.setState({ submitted: true });
        const { brand_name, model, email, text, subject } = this.state;
        const { dispatch } = this.props;
        
        
        const data = {
            'email': email,
            'brand_name': brand_name,
            'model': model,
            'text': text,
            'subject': subject
        }

        if ( email && brand_name && model ) {
            
            dispatch(contactUsActions.new_model(data)).then( () => {
                this.setState({modalIsOpen: true});
            })
        }
    }


    render() {
        const { email, brand_name, model, text, submitted } = this.state; 
            
        return (
            <div className="row registration-container" dir="ltr">
            <div className="mt-4 mx-auto col-md-3 buycontainer">
                <img src={images_url + "/images/buy.png"} className="buysellimage"/><br />
            </div>
            <div className="card mt-4 col-md-5 infoForms secondhandlogo" dir={this.props.rtlconv}>
                <button className={"btn btn-danger btn-sm margintop10 modalButton" + (this.props.rtlconv == "rtl" ? ' float-left' : ' float-right')} onClick={this.closeCustomerServices}> X </button>
                <div className={"card-header registration-header"  + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                    <h6 className="mb-0">{strings.please_contact_us_using_the_following_form}</h6>
                </div>
                
                <div className="card-body">
                    <form name="form" onSubmit={this.handleSubmit}>
                        <div className={'form-group form-control-sm' + (submitted && !email ? ' has-error' : '') + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="email">{strings.email}</label>
                            <input type="email" className="form-control form-control-sm input40" name="email" value={email} onChange={this.handleChange} />
                            {submitted && !email &&
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                            }
                        </div>

                        <div className={'form-group form-control-sm' + (submitted && !brand_name ? ' has-error' : '') + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="name">{strings.brand}</label>
                            <input type="text" className="form-control form-control-sm input40" name="brand_name" value={brand_name} onChange={this.handleChange} />
                            {submitted && !brand_name &&
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                            }
                        </div>

                        <div className={'form-group form-control-sm' + (submitted && !model ? ' has-error' : '') + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="name">{strings.model}</label>
                            <input type="text" className="form-control form-control-sm input40" name="model" value={model} onChange={this.handleChange} />
                            {submitted && !model &&
                                <div className="errorMessage">{strings.this_field_is_required}</div>
                            }
                        </div>

                        <div className={'form-group form-control-sm' + (this.state.submitted && !this.state.text ? ' has-error' : '')+ (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <label htmlFor="text">{strings.text}</label>
                            <textarea className="form-control replyBox" name="text" value={text} onChange={this.handleChange} />
                            
                        </div>

                        <br />
                        
                        <div className={"form-group" + (this.props.rtlconv == "rtl" ? ' righttoleft' : '')}>
                            <button className="btn btn-info">{strings.submit_customer_services}</button>
                        </div>
                    </form>
                </div>
                <br />
                <br />
                <br />
                <br />
            </div>
            <div className="mt-4 mx-auto col-md-3 sellcontainer">
                <img src={images_url + "/images/sell.png"} className="buysellimage"/><br />
            </div>

            <Modal
                isOpen={this.state.modalIsOpen}
                onAfterOpen={this.afterOpenModal}
                onRequestClose={this.closeModal}
                style={customStyles}
                contentLabel={strings.suggestion}
                ariaHideApp={false}
            >
                
                <div className="sentnotice-frame arabicfont"> 
                    <button className="btn btn-danger btn-sm float-right fixedclosed modalButton" onClick={this.closeModal}> X </button>
                    <div className="sentnotice"> 
                          <p>Your message was successfully sent to the 2ndhandcam support</p>
                    </div>
                </div>
                
            </Modal>
        </div>

            
        );
    }
}

function mapStatetoProps(state){
    return {
        rtlconv: state.rtlconv
    }
}

export default connect(mapStatetoProps)(NewModelNotification)