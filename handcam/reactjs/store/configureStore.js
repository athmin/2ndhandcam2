import createHistory from 'history/createBrowserHistory';
import { applyMiddleware, createStore, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import allReducer from '../reducers';
import DevTools from '../containers/DevTools';

export const history = createHistory();
const middleware = routerMiddleware(history);


export function configureStore(initialState) {
    return createStore(
        allReducer,
        initialState,
        compose(
            applyMiddleware(middleware, thunk, promise),
            DevTools.instrument()
        )
    );
}