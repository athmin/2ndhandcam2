import React, { Component } from 'react'

class VideoListItem extends Component {
    constructor(props) {
        super(props)
        this.video = props.video
        this.key = props.key
    }

    render() {
        const imgUrl = this.video.snippet.thumbnails.default.url
        const title = this.video.snippet.title
        const description = this.video.snippet.description
        const vidUrl = "https://www.youtube.com/watch?v=" + this.video.id.videoId
        var h = screen.height * .8;
        var w = screen.width * .8;

        return (
            <li className="list-group-item">
                <a onClick={() => window.open(vidUrl, "Popup", "toolbar=no, location=no, statusbar=no, menubar=no, scrollbars=1, resizable=0, width="+w+", height="+h+", top=30")} target="_blank">
                    <div className="video-list media">
                        <div className="media-left">
                            <img className="media-object" src={imgUrl} />
                        </div>
                        <div className="media-body gray-font padding10">
                            <div className="media-heading" >
                                {title}
                            </div>
                            <p className="modalButton">{description}</p> 
                        </div>
                    </div>
                </a>
            </li>  
        )
    }
}

export default VideoListItem