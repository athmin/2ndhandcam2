import React from 'react'
import { connect } from 'react-redux'
import { breadcrumbify } from 'redux-breadcrumb-trail'
 
export function LocationBreadcrumb ({location}) {
  if (!location.current) return <div><i className='fa fa-refresh fa-spin' /></div>
  return <i>{location.current.name}</i>
}
 
export function mapStateToProps(state) {
  return { location: state.location }
}
 
export default connect(mapStateToProps)(breadcrumbify(LocationBreadcrumb))