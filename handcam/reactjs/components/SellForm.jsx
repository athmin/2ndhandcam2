import React from 'react'
import { Field, reduxForm } from 'redux-form'
import Dropzone from 'react-dropzone';

const FILE_FIELD_NAME = 'files';

const renderDropzoneInput = (field) => {
  const files = field.input.value;
  
  return (
    <div>
      <Dropzone
        name={field.name}
        onDrop={( filesToUpload, e ) => field.input.onChange(filesToUpload)}
      >
        <div>Drag and drop images of the camera or click to select images to upload.</div>
      </Dropzone>
      {field.meta.touched &&
        field.meta.error &&
        <span className="error">{field.meta.error}</span>}
      {files && Array.isArray(files) && (
        <div>
          { files.map((file, i) => <img key={i} src={file.preview} style={{ width:75, hieght:35}}/>) }
        </div>
      )}
    </div>
  );
}

const renderTextArea = ({textarea, meta: { touched, error, warning }}) => (
    <div>
        <div>
            <span>{textarea}</span>
            <textarea {...textarea} placeholder="Condition" rows="10" cols="40" className="form-control"></textarea>
            {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    </div>
);

let SellForm = props => {
  const { handleSubmit } = props
  return (
    <form onSubmit={ handleSubmit }>
        <div className="container">
            <label>Price</label>
            <div className="form-group">
                <Field
                name="price"
                component="input"
                type="text"
                placeholder="Price"
                className="form-control"/>
            </div>
        
            <label>Shutter count</label>
            <div className="form-group">
                <Field
                name="shutter_count"
                component="input"
                type="text"
                placeholder="Shutter count"
                className="form-control"/>
            </div>
       
            <label>Warranty</label>
            <div className="form-group">
                <Field
                name="warranty"
                component="input"
                type="text"
                placeholder="Warranty"
                className="form-control"/>
            </div>
        
            <label>Condition</label>
            <div className="form-group">
                <Field
                name="condition"
                component={renderTextArea}
                type="textarea"
                placeholder="Condition"
                className="form-control"/> 
            </div>
        
            <label>With kit</label>
            <div className="form-group">
                <Field name="with_kit" component="input" type="checkbox" className="checkbox"/>
            </div>
            
            <div className="form-group">
                <label htmlFor={FILE_FIELD_NAME}>Actual Photos</label>
                <Field
                    name={FILE_FIELD_NAME}
                    component={renderDropzoneInput}/>
            </div>

            <button type="submit" className="btn btn-primary">
            Submit
            </button>
        </div>
    </form>
  )
}

SellForm = reduxForm({
  // a unique name for the form
  form: 'sell'
})(SellForm)

export default SellForm;