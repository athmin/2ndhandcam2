import React from "react"
import {Link} from 'react-router-dom'
import Routes from '../routes';
import { bindActionCreators } from 'redux'
import {authHeader} from '../lib'
import { connect } from 'react-redux';
import { userActions } from '../actions';
import { getMessagesAction, getUserProfileAction, socketIOReduxMiddleware } from '../actions';
import { history } from '../store/configureStore';
import LocationBreadcrumb from '../components/LocationBreadcrumb'

import ReactFlagsSelect from 'react-flags-select';
import 'react-flags-select/css/react-flags-select.css';
import { strings } from '../lib/strings';
import { rtlconvert, getMaintenanceAction } from '../actions';
import io from 'socket.io-client';

let CONFIG = require('../lib/config.json')

const divStyle = {
	  backgroundColor: '#ededed'
	};


class Headline extends React.Component {

	constructor(props) {
        super(props);
        this.state = {
            messages: [],
            unread_count : 0,
            expired_items : 0,
            fetching: false,
            profile: '',
            direction: "ltr",
            activeCountry: "US"
        };


        const { dispatch } = this.props;
        history.listen((location, action) => {
            dispatch(userActions.clearAlert());
        });
    }

     componentDidMount() {
	 let token = localStorage.getItem('token')
	 if (token) {
	      fetch(CONFIG.server_url + '/api/profile/user-credentials', {
	          method: 'get',
                  headers: authHeader(),
	      })
              .then((response) => response.json() )
              .then((responseData) => {
	      if(typeof responseData.detail != "undefined") {
		//console.log("Profile request failed");
	        localStorage.removeItem('token')
	        localStorage.removeItem('profile')
	        this.setState({fetching: false})
              } else {
		//console.log("Profile request passed");
	      }
	      }).catch( (error) => {
		    //console.log(error);
	      });                                                                                                                                       }
    }

    componentWillMount(){
    	const { dispatch } = this.props;
    	this.setState({});

    	if (!this.props.maintenanceMode.hasOwnProperty('mode')){
    		dispatch(getMaintenanceAction.getMode()).then( () => {
	          	if (this.props.maintenanceMode){
	              	if (this.props.maintenanceMode.mode.status){
	                  	history.push('/maintenance')
	              	}
	          	} 
	        })
    	}else{
    		if (this.props.maintenanceMode.mode.status){
                history.push('/maintenance')
            }
    	}

        let token = localStorage.getItem('token')
        let currentDIR = localStorage.getItem('direction')
		if (currentDIR) {
			if (currentDIR=='"rtl"'){
				this.setState({activeCountry: "SA"})
					const data = {
					"direct": "rtl"
				}
				dispatch(rtlconvert.convertrtlltr(data))
				strings.setLanguage('ar');
			}else{
				this.setState({activeCountry: "US"})
					const data = {
					"direct": "ltr"
				}
				dispatch(rtlconvert.convertrtlltr(data))
				strings.setLanguage('en');
			}
        }
        
        dispatch(socketIOReduxMiddleware.listener())

        if (token){
        	//let socket = io('http://localhost:3100');

          	dispatch(getMessagesAction.getUserInbox()).then( () => {
	        	this.setState({fetching: true})
	            this.setState({ messages: this.props.inboxReducer.messages})
	            this.setState({ unread_count: this.props.inboxReducer.unread_count})  
        	})

        	dispatch(getUserProfileAction.userCredentials()).then( () => {
	        	this.setState({fetching: true})
	            this.setState({ profile: this.props.credentials})
        	})
      
        }


    }


	renderLoginOrLogout(){
		let token = localStorage.getItem('token')

		if (!token){
			/*if (this.props.rtlconv == "rtl"){
				return <ul className="navbar-nav ml-auto" dir="rtl">
				<li className={"nav-item nav-link" + (this.props.rtlconv == "rtl" ? ' nav-right-pad' : '')}><Link to="/">{strings.home}</Link></li>
				<li className="nav-item nav-link width3rem"></li>
			    <li className={"nav-item nav-link twopxleftpad" + (this.props.rtlconv == "rtl" ? ' nav-right-pad' : '')}><Link to="/sell" className={"sell-nav"+ (this.props.rtlconv == "rtl" ? ' arabicbuysell' : '')}>{strings.sell}</Link></li>
			    <li className="nav-item nav-link"><Link to="/buy" className={"buy-nav"+ (this.props.rtlconv == "rtl" ? ' arabicbuysell' : '')}>{strings.buy}</Link></li>
				{/*<li className={"nav-item nav-link" + (this.props.rtlconv == "rtl" ? ' nav-right-pad' : '')}><Link to="/login">{strings.login}</Link></li>
				<li className={"nav-item nav-link" + (this.props.rtlconv == "rtl" ? ' nav-right-pad' : '')}><Link to="/register">{strings.register}</Link></li>
				</ul>
			}else{*/
				return <ul className="navbar-nav ml-auto" dir="ltr">
					<li className="nav-item nav-link"><Link to="/">{strings.home}</Link></li>
					<li className="nav-item nav-link width3rem"></li>
				    <li className="nav-item nav-link"><Link to="/buy" className={"buy-nav"+ (this.props.rtlconv == "rtl" ? ' arabicbuysell' : '')}>{strings.buy}</Link></li>
				    <li className="nav-item nav-link twopxleftpad"><Link to="/sell" className={"sell-nav"+ (this.props.rtlconv == "rtl" ? ' arabicbuysell' : '')}>{strings.sell}</Link></li>
					{/*<li className="nav-item nav-link"><Link to="/login">{strings.login}</Link></li>
					<li className="nav-item nav-link"><Link to="/register">{strings.register}</Link></li>*/}
				</ul>
			/*}*/
			
		}else{
			
				return <ul className="navbar-nav ml-auto" dir="ltr">
				<li className={"nav-item nav-link" + (this.props.rtlconv == "rtl" ? ' nav-right-pad' : '')}><Link to="/">{strings.home}</Link></li>
				<li className={"nav-item nav-link" + (this.props.rtlconv == "rtl" ? ' nav-right-pad' : '')}><Link to="/dashboard">{this.props.dash.expired_items != 0 ? <span className="badge badge-pill badge-primary menuNotify">{this.props.dash.expired_items}</span> : null } {strings.dashboard} </Link></li>
			    <li className={"nav-item nav-link" + (this.props.rtlconv == "rtl" ? ' nav-right-pad' : '')}><Link to="/message">{this.props.inboxReducer.unread_count != 0 ? <span className="badge badge-pill badge-primary menuNotify">{this.props.inboxReducer.unread_count}</span> : null} {strings.inbox}</Link></li>
			    <li className="nav-item nav-link"><Link to="/buy" className={"buy-nav"+ (this.props.rtlconv == "rtl" ? ' arabicbuysell' : '')}>{strings.buy}</Link></li>
			    <li className={"nav-item nav-link twopxleftpad" + (this.props.rtlconv == "rtl" ? ' nav-right-pad' : '')}><Link to="/sell" className={"sell-nav"+ (this.props.rtlconv == "rtl" ? ' arabicbuysell' : '')}>{strings.sell}</Link></li>
			    </ul>
			
	
		}
	}

	renderMyAccount(){
		let token = localStorage.getItem('token')
		let profile = localStorage.getItem('profile')
		
		if (token) {
			return <div className="column myaccount" dir={this.props.rtlconv}>
	  					<div className="dropdown">
							<span className="dropbtn">{strings.hello} <strong>{profile ? profile : null }</strong></span>
							<div className="dropdown-content">
							    <p className={this.props.rtlconv == "rtl" ? 'righttoleft' : ''}>{strings.loggedin_as} <strong>{profile ? profile : null }</strong></p>
							    <hr />
							    <Link to="/account_setting" className={this.props.rtlconv == "rtl" ? 'righttoleft' : ''}>{strings.account_settings}</Link>
							    <Link to="/change_password" className={this.props.rtlconv == "rtl" ? 'righttoleft' : ''}>{strings.change_password}</Link>
							    <Link to="/logout" className={this.props.rtlconv == "rtl" ? 'righttoleft' : ''}>{strings.logout}</Link>
							</div>
	  					</div>
  					</div>
		}else {
			return  <div className="navbar navbar-expand-sm">
					    <div className="collapse navbar-collapse navbar-gray" id="navbarSupportedContent">
				        		<ul className="navbar-nav ml-auto">
									<li className="nav-item nav-link login-nav zerotopbottompad"><Link to="/login">{strings.login}</Link></li>
									<li className="nav-item nav-link zerotopbottompad"><Link to="/register">{strings.register}</Link></li>
								</ul>
						</div>
					</div>
		}
	}

	clickHomeHandle = (e) => {
	    e.preventDefault();
	    history.push('/')
	}

	onSelectFlag = (countryCode) => {
		const {dispatch} = this.props
		if(countryCode == "SA") {
			const data = {
				"direct": "rtl"
			}
			dispatch(rtlconvert.convertrtlltr(data))
			strings.setLanguage('ar');
			this.setState({});
			window.location.reload()

		}else{
			const data = {
				"direct": "ltr"
			}
			dispatch(rtlconvert.convertrtlltr(data))
			strings.setLanguage('en');
			this.setState({});
			window.location.reload()
			
		}
	}

	render() {
		let token = localStorage.getItem('token')
		/*let currentDIR = localStorage.getItem('direction')
		var activeCountry = "US"
		if (currentDIR) {
			if (currentDIR=='"rtl"'){
				activeCountry = "SA"
			}
        }*/

	    return (
		    <div className={this.props.rtlconv == "rtl" ? 'arabicfont' : ''}>
		    	<div className="languageMenu">
		    		{/*<div className="column selectCountry">
			    		<span>
			    			Select Country
			    		</span>
			    		<ReactFlagsSelect 
			    			defaultCountry="SA"
	 						/>
 					</div>*/}
 					<div className="column selectLanguage">	
			    		<ReactFlagsSelect 
			    			countries={["US", "SA"]}
			    			defaultCountry={this.state.activeCountry}
			    			customLabels={{"US": strings.english,"SA": strings.arabic}}
			    			onSelect={this.onSelectFlag}
	 						/>
 					</div>

 					{this.renderMyAccount()}

		    	</div>
		    	
			    	<nav id="navigation" className="navbar navbar-expand-sm navbar-light" dir="ltr" >
					 
						      	<a className="navbar-brand" href="/"><img src={require('./logo.png')}/></a>
							    <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
								    <span className="navbar-toggler-icon"></span>
								</button>
						  
					</nav>
					<div className="navbar navbar-expand-sm navbar-custom navbar-pad">
						<div className="line-for-arrow arrow-right navbar-collapse collapse"></div> 
					    <div className={"collapse navbar-collapse" + (token ? '' : ' margin-right1')} id="navbarSupportedContent">
						    
				        		{this.renderLoginOrLogout()}
						    
						</div>

					</div>
				<div dir={this.props.rtlconv}>
			      		{ Routes }
			    </div>

		      	<footer className="footer-basic-centered">
					<div className="wrapper">

						<div className="col">
							<p className="footer-company-motto">"Your Past, Somebody else's Present."</p>
							<p className="footer-company-name">2ndHandCam &copy; 2018</p>
						</div>
	
					</div>
				</footer>
		    </div>
		    
	    )
	  
	}
}

function mapStateToProps(state){
    return{
        loginToken: state.loginToken,
        loginTokenError: state.loginTokenError,
        inboxReducer: state.inboxReducer,
        dash: state.dash,
        credentials: state.credentialsReducer,
        rtlconv: state.rtlconv,
        maintenanceMode: state.maintenanceMode
    }
}



export default connect(mapStateToProps)(Headline)
