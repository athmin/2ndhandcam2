import { RefinementListFilter } from "searchkit"

export class CustomRefinementListFilter extends RefinementListFilter {
    getAccessorOptions(){
        return Object.assign({
            min_doc_count:0
        }, super.getAccessorOptions())
    }
}