import React from 'react'
import { Field, reduxForm } from 'redux-form'
import ValidateRegForm from './ValidateRegForm'
import asyncValidateRegForm from './asyncValidateRegForm'


const renderField = ({
  input,
  label,
  type,
  meta: { asyncValidating, touched, error }
}) => (
  <div>
    <label>{label}</label>
    <div className={asyncValidating ? 'async-validating' : ''}>
      <input {...input} type={type} placeholder={label} />
      {touched && error && <span>{error}</span>}
    </div>
  </div>
)

let RegistrationForm = props => {
  const { handleSubmit, submitting } = props
  return (
    <form onSubmit={ handleSubmit }>
        <div className="form-group">
            <label>Username</label>
            <div>
                <Field
                name="userName"
                className="form-control"
                component={renderField}
                type="text"
                placeholder="Username"/>
            </div>
        </div>
        <div className="form-group">
            <label>First Name</label>
            <div>
                <Field
                name="firstName"
                className="form-control"
                component={renderField}
                type="text"
                placeholder="First Name"/>
            </div>
        </div>
        <div className="form-group">
            <label>Last Name</label>
            <div>
                <Field
                name="lastName"
                className="form-control"
                component={renderField}
                type="text"
                placeholder="Last Name"/>
            </div>
        </div>
        <div className="form-group">
            <label>Email</label>
            <div>
                <Field
                name="email"
                className="form-control"
                component={renderField}
                type="email"
                placeholder="Email"/>
            </div>
        </div>
        <div className="form-group">
            <label>Password</label>
            <div>
                <Field
                name="password"
                className="form-control"
                component={renderField}
                type="text"
                placeholder="Password"/>
            </div>
        </div>
        <div className="form-group"> 
            <label>Confirm Password</label>
            <div>
                <Field
                name="confirmPassword"
                className="form-control"
                component={renderField}
                type="text"
                placeholder="Confirm Password"/>
            </div>
        </div>
        <div>
            <button type="submit" disabled={submitting} className="btn btn-primary">
            Submit
            </button>
        </div>
    </form>
  )
}

RegistrationForm = reduxForm({
  // a unique name for the form
  form: 'register',
  ValidateRegForm,
  asyncValidateRegForm,
  asyncBlurFields: ['username']
})(RegistrationForm)

export default RegistrationForm;