const validate = values => {
  const errors = {}
  if (!values.username) {
    errors.username = 'Required'
  }
  if (!values.password) {
    errors.password = 'Required'
  }
  if (values.password != values.confirmPassword) {
    errors.confirmPassword = 'Password did not matched.'
  }
  return errors
}

export default validate