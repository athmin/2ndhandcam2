import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './containers/Home';
import Home2 from './containers/Home2';
import Inbox from './containers/Inbox';
import InboxDetails from './containers/InboxDetails';
import Login from './containers/Login';
import Logout from './containers/Logout';
import About from './containers/About';
import Buy from './containers/Buy'
import Sell from './containers/Sell'
import SellChooseBrand from './containers/SellChooseBrand'
import SellSelectModels from './containers/SellSelectModels'
import SellVideoCameraDetails from './containers/SellVideoCameraDetails'
import SellCameraDetails from './containers/SellCameraDetails'
import SellChooseVideoBrand from './containers/SellChooseVideoBrand'
import SellSelectVideoModels from './containers/SellSelectVideoModels'
import SellChooseLensBrand from './containers/SellChooseLensBrand'
import SellSelectLensModels from './containers/SellSelectLensModels'
import SellLensDetails from './containers/SellLensDetails'
import Dashboard from './containers/Dashboard'
import BuyPhotography from './containers/BuyPhotography'
import SellPhotography from './containers/SellPhotography'
import CannonSell from './containers/CannonSell'
import CameraDetail from './containers/CameraDetail'
import BuyItem from './containers/BuyItem'
import BuyCameraDetail from './containers/BuyCameraDetail'
import BuyVideoItem from './containers/BuyVideoItem'
import BuyVideoCameraDetail from './containers/BuyVideoCameraDetails'
import BuyLensItem from './containers/BuyLensItem'
import BuyLensDetail from './containers/BuyLensDetails'
import BuyLighting from './containers/BuyLighting'
import BuyStrobeLighting from './containers/BuyStrobeLighting'
import BuyLightingItem from './containers/BuyLightingItem'
import BuyLightingFilterItem from './containers/BuyLightingFilterItem'
import BuyLightingDetails from './containers/BuyLightingDetails'
import BuyAccessories from './containers/BuyAccessories'
import BuyAccessoriesItem from './containers/BuyAccessoriesItem'
import BuyAccessoriesDetails from './containers/BuyAccessoriesDetails'
import Registration from './containers/Registration'
import ConfirmEmail from './containers/ConfirmEmail'
import Activate from './containers/Activate'
import PublicProfile from './containers/Public_profile'
import EditCamForSale from './containers/EditCamForSale'
import EditVideoForSale from './containers/EditVideoForSale'
import EditLensForSale from './containers/EditLensForSale'
import EditLightingForSale from './containers/EditLightingForSale'
import EditAccessoriesForSale from './containers/EditAccessoriesForSale'
import EditDronesForSale from './containers/EditDronesForSale'
import SellLighting from './containers/SellLighting'
import SellStrobeLighting from './containers/SellStrobeLighting'
import SellLightingDetails from './containers/SellLightingDetails'
import SellAccessories from './containers/SellAccessories'
import SellAccessoriesDetails from './containers/SellAccessoriesDetails'
import SellDrones from './containers/SellDrones'
import SellDroneDetails from './containers/SellDroneDetails'
import BuyDrones from './containers/BuyDrones'
import BuyDroneItem from './containers/BuyDroneItem'
import BuyDroneDetails from './containers/BuyDroneDetails'
import ChangePassword from './containers/ChangePassword'
import AccountSetting from './containers/AccountSetting'
import CustomerServices from './containers/CustomerServices'
import Suggestions from './containers/Suggestions'
import NewModelNotification from './containers/new_model_notification'
import WebsitePolicy from './containers/WebsitePolicy'
import TermsOfUse from './containers/TermsOfUse'
import WebsiteCommission from './containers/WebsiteCommission'
import ImportantAdvice from './containers/ImportantAdvice'
import CommercialAccount from './containers/CommercialAccount'
import LoginRegister from './containers/LoginRegister'
import PrivateRoute  from './components'
import MaintenanceMode from './containers/MaintenanceMode'
import ChangePasswordForget from './containers/ChangePasswordForget'



export default (
	<Switch>
		<Route path="/" exact render={(props) => <Home {...props}/>} />
		<Route path="/maintenance" exact render={(props) => <MaintenanceMode {...props}/>} />
		<Route path="/home2" exact render={(props) => <Home2 {...props}/>} />
		<Route path="/message" render={(props) => <Inbox {...props}/>} />
		<Route path="/view_message" render={(props) => <InboxDetails {...props}/>} />
		<Route path="/about" render={(props) => <About {...props}/>} />
		<Route path="/login" render={(props) => <Login {...props}/>} />
		<Route path="/login_register" render={(props) => <LoginRegister {...props}/>} />
		<Route path="/dashboard" render={(props) => <Dashboard {...props}/>} />
		<Route path="/buy" render={(props) => <Buy {...props}/>} />
		<Route path="/sell" render={(props) => <Sell {...props}/>} />
		<Route path="/buy_photography" render={(props) => <BuyPhotography {...props}/>} />
		
		<Route path="/sell_photography" render={(props) => <SellPhotography {...props}/>} />
		<Route path="/sell_brand" render={(props) => <SellChooseBrand {...props}/>} />
		<Route path="/select_category" render={(props) => <SellSelectModels {...props}/>} />
		<Route path="/complete_form" render={(props) => <SellCameraDetails {...props}/>} />

		<Route path="/sell_video_brand" render={(props) => <SellChooseVideoBrand {...props}/>} />
		<Route path="/select_video_category" render={(props) => <SellSelectVideoModels {...props}/>} />
		<Route path="/complete_video_form" render={(props) => <SellVideoCameraDetails {...props}/>} />

		<Route path="/sell_lens_brand" render={(props) => <SellChooseLensBrand {...props}/>} />
		<Route path="/select_lens_category" render={(props) => <SellSelectLensModels {...props}/>} />
		<Route path="/complete_lens_form" render={(props) => <SellLensDetails {...props}/>} />

		<Route path="/sell_lighting" render={(props) => <SellLighting {...props}/>} />
		<Route path="/sell_strobe_lighting" render={(props) => <SellStrobeLighting {...props}/>} />
		<Route path="/complete_lighting_form" render={(props) => <SellLightingDetails {...props}/>} />

		<Route path="/sell_accessories" render={(props) => <SellAccessories {...props}/>} />
		<Route path="/complete_accessories_form" render={(props) => <SellAccessoriesDetails {...props}/>} />

		<Route path="/sell_drone" render={(props) => <SellDrones {...props}/>} />
		<Route path="/complete_drone_form" render={(props) => <SellDroneDetails {...props}/>} />
		<Route path="/buy_drones" render={(props) => <BuyDrones {...props}/>} />
		<Route path="/buy_drones_item" render={(props) => <BuyDroneItem {...props}/>} />
		<Route path="/buy_drone_details" render={(props) => <BuyDroneDetails {...props}/>} />

		<Route path="/cannon_sell" render={(props) => <CannonSell {...props}/>} />
		<Route path="/camera_detail" render={(props) => <CameraDetail {...props}/>} />
		<Route path="/buy_item" render={(props) => <BuyItem {...props}/>} />
		<Route path="/buy_camera_detail" render={(props) => <BuyCameraDetail {...props}/>} />

		<Route path="/buy_video_cameras" render={(props) => <BuyVideoItem {...props}/>} />
		<Route path="/buy_video-camera_detail" render={(props) => <BuyVideoCameraDetail {...props}/>} />

		<Route path="/buy_lens" render={(props) => <BuyLensItem {...props}/>} />
		<Route path="/buy_lens_detail" render={(props) => <BuyLensDetail {...props}/>} />

		<Route path="/buy_lighting" render={(props) => <BuyLighting {...props}/>} />
		<Route path="/buy_strobe_lighting" render={(props) => <BuyStrobeLighting {...props}/>} />
		<Route path="/buy_lighting_item" render={(props) => <BuyLightingItem {...props}/>} />
		<Route path="/buy_lighting_monolight_item" render={(props) => <BuyLightingFilterItem {...props}/>} />
		<Route path="/buy_lighting_details" render={(props) => <BuyLightingDetails {...props}/>} />

		<Route path="/buy_accessories" render={(props) => <BuyAccessories {...props}/>} />
		<Route path="/buy_accessories_item" render={(props) => <BuyAccessoriesItem {...props}/>} />
		<Route path="/buy_accessories_details" render={(props) => <BuyAccessoriesDetails {...props}/>} />

		<Route path="/logout" render={(props) => <Logout {...props}/>} />
		<Route path="/register" render={(props) => <Registration {...props}/>} />
		<Route path="/confirm" render={(props) => <ConfirmEmail {...props}/>} />
		<Route path="/activate" render={(props) => <Activate {...props}/>} />
		<Route path="/change_password" render={(props) => <ChangePassword {...props}/>} />
		<Route path="/change_password_reset/:token" render={(props) => <ChangePasswordForget {...props}/>} />
		<Route path="/public_profile" render={(props) => <PublicProfile {...props}/>} />
		<Route path="/account_setting" render={(props) => <AccountSetting {...props}/>} />
		<Route path="/customer_services" render={(props) => <CustomerServices {...props}/>} />
		<Route path="/suggestions" render={(props) => <Suggestions {...props}/>} />
		<Route path="/about_us" render={(props) => <About {...props}/>} />
		<Route path="/website_policy" render={(props) => <WebsitePolicy {...props}/>} />
		<Route path="/terms_of_use" render={(props) => <TermsOfUse {...props}/>} />
		<Route path="/website_commission" render={(props) => <WebsiteCommission {...props}/>} />
		<Route path="/important_advice" render={(props) => <ImportantAdvice {...props}/>} />
		<Route path="/commercialAccount" render={(props) => <CommercialAccount {...props}/>} />
		<Route path="/new_model_notification" render={(props) => <NewModelNotification {...props}/>} />
		<Route path="/edit_cam_for_sale" render={(props) => <EditCamForSale {...props}/>} />
		<Route path="/edit_video_for_sale" render={(props) => <EditVideoForSale {...props}/>} />
		<Route path="/edit_lens_for_sale" render={(props) => <EditLensForSale {...props}/>} />
		<Route path="/edit_lighting_for_sale" render={(props) => <EditLightingForSale {...props}/>} />
		<Route path="/edit_accessories_for_sale" render={(props) => <EditAccessoriesForSale {...props}/>} />
		<Route path="/edit_drones_for_sale" render={(props) => <EditDronesForSale {...props}/>} />
	</Switch>
);