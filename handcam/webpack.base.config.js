var path = require("path")
var webpack = require('webpack')

module.exports = {
  context: __dirname,

  entry: {
    // Add as many entry points as you have container-react-components here
    App: './reactjs/App',
    vendors: ['react', 'react-dom', 'react-router'],
  },

  output: {
      path: path.resolve('./handcam/static/bundles/prod/'),
      filename: "[name].[hash].js"
  },

  externals: [
  ], 

  plugins: [
    //new webpack.optimize.CommonsChunkPlugin(
    //  { 
    //    name: 'vendors', 
    //    filename: 'vendors.bundle.js',
    //    minChunks: Infinity 
        //async: true, 
        //children: true, 
        //minChunks: 3
    //  }),
   
  ], 

  module: {
    loaders: [],
  },

  resolve: {
    //modules: ['node_modules', 'bower_components'],
    extensions: ['.js', '.jsx']
  },
}