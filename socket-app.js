var app = require('express')();
var fs = require('fs');

var options = {
    key:fs.readFileSync('/etc/nginx/ssl/2ndhandcam_com/2ndhandcam_com.key'),
    cert:fs.readFileSync('/etc/nginx/ssl/2ndhandcam_com/ssl-bundle.crt'),
}

var server = require('https').Server(options, app);
//var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(3100);

io.on('connection', function (socket) {
	console.log("Client just connected to node.js");
	socket.emit('inboxUpdate', { message: 'You just connected to the 2ndhand inbox updater, watching for update!' });
	socket.on('newMessage', function (data) {
		console.log("Server received new message");
		console.log(data);
		socket.broadcast.emit('inboxUpdate', { message: data});
	});
	socket.on('maintenance_mode', function (data) {
		console.log("Server received maintenance mode");
		console.log(data);
		socket.broadcast.emit('maintenance', { message: data});
	});
});
